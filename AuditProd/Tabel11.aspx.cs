﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using AuditProd.DAL;
using System.Globalization;

namespace AuditProd
{
    public partial class Tabel11 : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        string id3_lhp = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!string.IsNullOrEmpty((string)Session["Id3_LHP"]))
            {
                TxbId3_lhp.Text = Session["Id3_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id3_lhp = TxbId3_lhp.Text;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            BindGrid();
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            Session["Id3_LHP"] = null;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        public void BindGrid()
        {

            if (!(string.IsNullOrEmpty(id3_lhp)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel11 where ID_LHP_Administration={0}", id3_lhp);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                TxtDateDisbursementEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text).ToString();
                TxtAmountDisbursementEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                TxtDateBankEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                TxtAmountBankEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                TxtVarianceAmountEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                TxtVarianceDayEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                TxtRemarksEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                //ddlsmplingEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[11].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            //DateTime dc = DateTime.Parse(TxtDateDisbursementEdit.Text);
            DateTime dc = DateTime.ParseExact(TxtDateDisbursementEdit.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            decimal ac = Convert.ToDecimal(string.IsNullOrEmpty(TxtAmountDisbursementEdit.Text) ? "0" : TxtAmountDisbursementEdit.Text);
            //DateTime dcb = DateTime.Parse(TxtDateBankEdit.Text);
            DateTime dcb = DateTime.ParseExact(TxtDateBankEdit.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            decimal acb = Convert.ToDecimal(string.IsNullOrEmpty(TxtAmountBankEdit.Text) ? "0" : TxtAmountBankEdit.Text);
            decimal va = Convert.ToDecimal(string.IsNullOrEmpty(TxtVarianceAmountEdit.Text) ? "0" : TxtVarianceAmountEdit.Text);
            decimal vd = Convert.ToDecimal(string.IsNullOrEmpty(TxtVarianceDayEdit.Text) ? "0" : TxtVarianceDayEdit.Text);  
            string rfm = TxtRemarksEdit.Text;
            string ddledit = ddlsmplingEdit.SelectedItem.Text;
            executeUpdate(No, dc, ac, dcb, acb, va, vd, rfm, ddledit);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, DateTime dc, decimal ac, DateTime dcb, decimal acb, decimal va, decimal vd, string rfm, string ddledit)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveTabel11Edit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@dc", dc);
                updateCmd.Parameters.AddWithValue("@ac", ac);
                updateCmd.Parameters.AddWithValue("@dcb", dcb);
                updateCmd.Parameters.AddWithValue("@acb", acb);
                updateCmd.Parameters.AddWithValue("@va", va);
                updateCmd.Parameters.AddWithValue("@vd", vd);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.Parameters.AddWithValue("@ddledit", ddledit);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_A = id3_lhp;

            if (string.IsNullOrEmpty(IDProc_A))
            {
                IDProc_A = null;
            }

            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string Headdb = txbUsername.Text;

            if (!(string.IsNullOrEmpty(IDProc_A)))
            {                
                try
                {
                    string idlhk3 = IDProc_A;
                    //DateTime dc = DateTime.Parse(string.IsNullOrEmpty(TxtDateDisbursementAdd.Text) ? "01/01/9999 00:00:00" : TxtDateDisbursementAdd.Text);
                    DateTime dc = DateTime.ParseExact(TxtDateDisbursementAdd.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    string mod = "LHP_Administration";
                    decimal ac = Convert.ToDecimal(string.IsNullOrEmpty(TxtAmountDisbursementAdd.Text) ? "0" : TxtAmountDisbursementAdd.Text);
                    //DateTime dcb = DateTime.Parse(string.IsNullOrEmpty(TxtDateBankAdd.Text) ? "01/01/9999 00:00:00" : TxtDateBankAdd.Text);
                    DateTime dcb = DateTime.ParseExact(TxtDateBankAdd.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    decimal acb = Convert.ToDecimal(string.IsNullOrEmpty(TxtAmountBankAdd.Text) ? "0" : TxtAmountBankAdd.Text);
                    decimal va = Convert.ToDecimal(string.IsNullOrEmpty(TxtVarianceAmountAdd.Text) ? "0" : TxtVarianceAmountAdd.Text);
                    decimal vd = Convert.ToDecimal(string.IsNullOrEmpty(TxtVarianceDayAdd.Text) ? "0" : TxtVarianceDayAdd.Text);
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk3, dc, mod, ac, dcb, acb, va, vd, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }
        }

        private void executeAdd(string idlhk3, DateTime dc, string mod, decimal ac, DateTime dcb, decimal acb, decimal va, decimal vd, string rem, string samp, string dt, string Headdbs)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveTabel11", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk3", idlhk3 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@dc", dc);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@ac", ac);
                addCmd.Parameters.AddWithValue("@dcb", dcb);
                addCmd.Parameters.AddWithValue("@acb", acb);
                addCmd.Parameters.AddWithValue("@va", va);
                addCmd.Parameters.AddWithValue("@vd", vd);
                addCmd.Parameters.AddWithValue("@doe", dt);
                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@rem", rem);
                addCmd.Parameters.AddWithValue("@samp", samp);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_LHP_Tabel11 where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}