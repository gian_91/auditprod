﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using ClosedXML.Excel;
using AuditProd.DAL;

namespace AuditProd
{
    public partial class Procurement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TxbModul.Text = "Procurement";
            if (!IsPostBack)
            {
                this.TxbTanggalHeadDibuat.Text = DateTime.Today.ToString("dd/MM/yyyy");
                TxbDibuatHead.Text = this.Page.User.Identity.Name;
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                TxbDilksnakan.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {
                    BindGridHeader();
                    FillGridViewTemp();
                    //BindGridB();
                    //BindData();

                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {
                    BindGridHeaderTemp();
                    FillGridView();
                    //BindGridBTemp();
                    //BindDataTemp();

                }
            }
            else
            {
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

            } 

        }

        private void BindGridHeader()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_GetData_ProcHeader", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDProc", txtIDSave.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(dr["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(dr["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(dr["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(dr["Periode_Audit_End"]);
                TxbA1Data.Text = Convert.ToString(dr["A_1_Data"]);
                TxbA1Actual.Text = Convert.ToString(dr["A_1_Actual"]);
                TxbA1Diff.Text = Convert.ToString(dr["A_1_Difference"]);
                TxbA1Remarks.InnerText = Convert.ToString(dr["A_1_Remarks"]);
                TxbA2Data.Text = Convert.ToString(dr["A_2_Data"]);
                TxbA2Actual.Text = Convert.ToString(dr["A_2_Actual"]);
                TxbA2Diff.Text = Convert.ToString(dr["A_2_Difference"]);
                TxbA2Remarks.InnerText = Convert.ToString(dr["A_2_Remarks"]);
                TxbA3Data.Text = Convert.ToString(dr["A_3_Data"]);
                TxbA3Actual.Text = Convert.ToString(dr["A_3_Actual"]);
                TxbA3Diff.Text = Convert.ToString(dr["A_3_Difference"]);
                TxbA3Remarks.InnerText = Convert.ToString(dr["A_3_Remarks"]);
                TxbA4Data.Text = Convert.ToString(dr["A_4_Data"]);
                TxbA4Actual.Text = Convert.ToString(dr["A_4_Actual"]);
                TxbA4Diff.Text = Convert.ToString(dr["A_4_Difference"]);
                TxbA4Remarks.InnerText = Convert.ToString(dr["A_4_Remarks"]);
                TxbA5Data.Text = Convert.ToString(dr["A_5_Data"]);
                TxbA5Actual.Text = Convert.ToString(dr["A_5_Actual"]);
                TxbA5Diff.Text = Convert.ToString(dr["A_5_Difference"]);
                TxbA5Remarks.InnerText = Convert.ToString(dr["A_5_Remarks"]);
                TxbA6Data.Text = Convert.ToString(dr["A_6_Data"]);
                TxbA6Actual.Text = Convert.ToString(dr["A_6_Actual"]);
                TxbA6Diff.Text = Convert.ToString(dr["A_6_Difference"]);
                TxbA6Remarks.InnerText = Convert.ToString(dr["A_6_Remarks"]);
                TxbA7Data.Text = Convert.ToString(dr["A_7_Data"]);
                TxbA7Actual.Text = Convert.ToString(dr["A_7_Actual"]);
                TxbA7Diff.Text = Convert.ToString(dr["A_7_Difference"]);
                TxbA7Remarks.InnerText = Convert.ToString(dr["A_7_Remarks"]);
                TxbA8Data.Text = Convert.ToString(dr["A_8_Data"]);
                TxbA8Actual.Text = Convert.ToString(dr["A_8_Actual"]);
                TxbA8Diff.Text = Convert.ToString(dr["A_8_Difference"]);
                TxbA8Remarks.InnerText = Convert.ToString(dr["A_8_Remarks"]);
                TxbA9Data.Text = Convert.ToString(dr["A_9_Data"]);
                TxbA9Actual.Text = Convert.ToString(dr["A_9_Actual"]);
                TxbA9Diff.Text = Convert.ToString(dr["A_9_Difference"]);
                TxbA9Remarks.InnerText = Convert.ToString(dr["A_9_Remarks"]);
                TxbA10Data.Text = Convert.ToString(dr["A_10_Data"]);
                TxbA10Actual.Text = Convert.ToString(dr["A_10_Actual"]);
                TxbA10Diff.Text = Convert.ToString(dr["A_10_Difference"]);
                TxbA10Remarks.InnerText = Convert.ToString(dr["A_10_Remarks"]);
                TxbA11Data.Text = Convert.ToString(dr["A_11_Data"]);
                TxbA11Actual.Text = Convert.ToString(dr["A_11_Actual"]);
                TxbA11Diff.Text = Convert.ToString(dr["A_11_Difference"]);
                TxbA11Remarks.InnerText = Convert.ToString(dr["A_11_Remarks"]);
                TxbA12Data.Text = Convert.ToString(dr["A_12_Data"]);
                TxbA12Actual.Text = Convert.ToString(dr["A_12_Actual"]);
                TxbA12Diff.Text = Convert.ToString(dr["A_12_Difference"]);
                TxbA12Remarks.InnerText = Convert.ToString(dr["A_12_Remarks"]);
                TxbA13Data.Text = Convert.ToString(dr["A_13_Data"]);
                TxbA13Actual.Text = Convert.ToString(dr["A_13_Actual"]);
                TxbA13Diff.Text = Convert.ToString(dr["A_13_Difference"]);
                TxbA13Remarks.InnerText = Convert.ToString(dr["A_13_Remarks"]);
                TxbA14Data.Text = Convert.ToString(dr["A_14_Data"]);
                TxbA14Actual.Text = Convert.ToString(dr["A_14_Actual"]);
                TxbA14Diff.Text = Convert.ToString(dr["A_14_Difference"]);
                TxbA14Remarks.InnerText = Convert.ToString(dr["A_14_Remarks"]);
                TxbA15Data.Text = Convert.ToString(dr["A_15_Data"]);
                TxbA15Actual.Text = Convert.ToString(dr["A_15_Actual"]);
                TxbA15Diff.Text = Convert.ToString(dr["A_15_Difference"]);
                TxbA15Remarks.InnerText = Convert.ToString(dr["A_15_Remarks"]);
                TxbA16Data.Text = Convert.ToString(dr["A_16_Data"]);
                TxbA16Actual.Text = Convert.ToString(dr["A_16_Actual"]);
                TxbA16Diff.Text = Convert.ToString(dr["A_16_Difference"]);
                TxbA16Remarks.InnerText = Convert.ToString(dr["A_16_Remarks"]);
                TxbA17Data.Text = Convert.ToString(dr["A_17_Data"]);
                TxbA17Actual.Text = Convert.ToString(dr["A_17_Actual"]);
                TxbA17Diff.Text = Convert.ToString(dr["A_17_Difference"]);
                TxbA17Remarks.InnerText = Convert.ToString(dr["A_17_Remarks"]);
                TxbA18Data.Text = Convert.ToString(dr["A_18_Data"]);
                TxbA18Actual.Text = Convert.ToString(dr["A_18_Actual"]);
                TxbA18Diff.Text = Convert.ToString(dr["A_18_Difference"]);
                TxbA18Remarks.InnerText = Convert.ToString(dr["A_18_Remarks"]);
                TxbA19Data.Text = Convert.ToString(dr["A_19_Data"]);
                TxbA19Actual.Text = Convert.ToString(dr["A_19_Actual"]);
                TxbA19Diff.Text = Convert.ToString(dr["A_19_Difference"]);
                TxbA19Remarks.InnerText = Convert.ToString(dr["A_19_Remarks"]);
                TxbA20Data.Text = Convert.ToString(dr["A_20_Data"]);
                TxbA20Actual.Text = Convert.ToString(dr["A_20_Actual"]);
                TxbA20Diff.Text = Convert.ToString(dr["A_20_Difference"]);
                TxbA20Remarks.InnerText = Convert.ToString(dr["A_20_Remarks"]);
                TxbDilksnakan.Text = Convert.ToString(dr["Footer_Dilaksanakan"]);
                TxbTnglBwh.Text = Convert.ToString(dr["Footer_Tngl"]);
                TxbJamBawah.Text = Convert.ToString(dr["Footer_Jam"]);

            }
            con.Close();
        }

        private void BindGridHeaderTemp()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_GetData_ProcHeader", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDProc", txtIDSaveTemp.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(dr["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(dr["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(dr["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(dr["Periode_Audit_End"]);
                TxbA1Data.Text = Convert.ToString(dr["A_1_Data"]);
                TxbA1Actual.Text = Convert.ToString(dr["A_1_Actual"]);
                TxbA1Diff.Text = Convert.ToString(dr["A_1_Difference"]);
                TxbA1Remarks.InnerText = Convert.ToString(dr["A_1_Remarks"]);
                TxbA2Data.Text = Convert.ToString(dr["A_2_Data"]);
                TxbA2Actual.Text = Convert.ToString(dr["A_2_Actual"]);
                TxbA2Diff.Text = Convert.ToString(dr["A_2_Difference"]);
                TxbA2Remarks.InnerText = Convert.ToString(dr["A_2_Remarks"]);
                TxbA3Data.Text = Convert.ToString(dr["A_3_Data"]);
                TxbA3Actual.Text = Convert.ToString(dr["A_3_Actual"]);
                TxbA3Diff.Text = Convert.ToString(dr["A_3_Difference"]);
                TxbA3Remarks.InnerText = Convert.ToString(dr["A_3_Remarks"]);
                TxbA4Data.Text = Convert.ToString(dr["A_4_Data"]);
                TxbA4Actual.Text = Convert.ToString(dr["A_4_Actual"]);
                TxbA4Diff.Text = Convert.ToString(dr["A_4_Difference"]);
                TxbA4Remarks.InnerText = Convert.ToString(dr["A_4_Remarks"]);
                TxbA5Data.Text = Convert.ToString(dr["A_5_Data"]);
                TxbA5Actual.Text = Convert.ToString(dr["A_5_Actual"]);
                TxbA5Diff.Text = Convert.ToString(dr["A_5_Difference"]);
                TxbA5Remarks.InnerText = Convert.ToString(dr["A_5_Remarks"]);
                TxbA6Data.Text = Convert.ToString(dr["A_6_Data"]);
                TxbA6Actual.Text = Convert.ToString(dr["A_6_Actual"]);
                TxbA6Diff.Text = Convert.ToString(dr["A_6_Difference"]);
                TxbA6Remarks.InnerText = Convert.ToString(dr["A_6_Remarks"]);
                TxbA7Data.Text = Convert.ToString(dr["A_7_Data"]);
                TxbA7Actual.Text = Convert.ToString(dr["A_7_Actual"]);
                TxbA7Diff.Text = Convert.ToString(dr["A_7_Difference"]);
                TxbA7Remarks.InnerText = Convert.ToString(dr["A_7_Remarks"]);
                TxbA8Data.Text = Convert.ToString(dr["A_8_Data"]);
                TxbA8Actual.Text = Convert.ToString(dr["A_8_Actual"]);
                TxbA8Diff.Text = Convert.ToString(dr["A_8_Difference"]);
                TxbA8Remarks.InnerText = Convert.ToString(dr["A_8_Remarks"]);
                TxbA9Data.Text = Convert.ToString(dr["A_9_Data"]);
                TxbA9Actual.Text = Convert.ToString(dr["A_9_Actual"]);
                TxbA9Diff.Text = Convert.ToString(dr["A_9_Difference"]);
                TxbA9Remarks.InnerText = Convert.ToString(dr["A_9_Remarks"]);
                TxbA10Data.Text = Convert.ToString(dr["A_10_Data"]);
                TxbA10Actual.Text = Convert.ToString(dr["A_10_Actual"]);
                TxbA10Diff.Text = Convert.ToString(dr["A_10_Difference"]);
                TxbA10Remarks.InnerText = Convert.ToString(dr["A_10_Remarks"]);
                TxbA11Data.Text = Convert.ToString(dr["A_11_Data"]);
                TxbA11Actual.Text = Convert.ToString(dr["A_11_Actual"]);
                TxbA11Diff.Text = Convert.ToString(dr["A_11_Difference"]);
                TxbA11Remarks.InnerText = Convert.ToString(dr["A_11_Remarks"]);
                TxbA12Data.Text = Convert.ToString(dr["A_12_Data"]);
                TxbA12Actual.Text = Convert.ToString(dr["A_12_Actual"]);
                TxbA12Diff.Text = Convert.ToString(dr["A_12_Difference"]);
                TxbA12Remarks.InnerText = Convert.ToString(dr["A_12_Remarks"]);
                TxbA13Data.Text = Convert.ToString(dr["A_13_Data"]);
                TxbA13Actual.Text = Convert.ToString(dr["A_13_Actual"]);
                TxbA13Diff.Text = Convert.ToString(dr["A_13_Difference"]);
                TxbA13Remarks.InnerText = Convert.ToString(dr["A_13_Remarks"]);
                TxbA14Data.Text = Convert.ToString(dr["A_14_Data"]);
                TxbA14Actual.Text = Convert.ToString(dr["A_14_Actual"]);
                TxbA14Diff.Text = Convert.ToString(dr["A_14_Difference"]);
                TxbA14Remarks.InnerText = Convert.ToString(dr["A_14_Remarks"]);
                TxbA15Data.Text = Convert.ToString(dr["A_15_Data"]);
                TxbA15Actual.Text = Convert.ToString(dr["A_15_Actual"]);
                TxbA15Diff.Text = Convert.ToString(dr["A_15_Difference"]);
                TxbA15Remarks.InnerText = Convert.ToString(dr["A_15_Remarks"]);
                TxbA16Data.Text = Convert.ToString(dr["A_16_Data"]);
                TxbA16Actual.Text = Convert.ToString(dr["A_16_Actual"]);
                TxbA16Diff.Text = Convert.ToString(dr["A_16_Difference"]);
                TxbA16Remarks.InnerText = Convert.ToString(dr["A_16_Remarks"]);
                TxbA17Data.Text = Convert.ToString(dr["A_17_Data"]);
                TxbA17Actual.Text = Convert.ToString(dr["A_17_Actual"]);
                TxbA17Diff.Text = Convert.ToString(dr["A_17_Difference"]);
                TxbA17Remarks.InnerText = Convert.ToString(dr["A_17_Remarks"]);
                TxbA18Data.Text = Convert.ToString(dr["A_18_Data"]);
                TxbA18Actual.Text = Convert.ToString(dr["A_18_Actual"]);
                TxbA18Diff.Text = Convert.ToString(dr["A_18_Difference"]);
                TxbA18Remarks.InnerText = Convert.ToString(dr["A_18_Remarks"]);
                TxbA19Data.Text = Convert.ToString(dr["A_19_Data"]);
                TxbA19Actual.Text = Convert.ToString(dr["A_19_Actual"]);
                TxbA19Diff.Text = Convert.ToString(dr["A_19_Difference"]);
                TxbA19Remarks.InnerText = Convert.ToString(dr["A_19_Remarks"]);
                TxbA20Data.Text = Convert.ToString(dr["A_20_Data"]);
                TxbA20Actual.Text = Convert.ToString(dr["A_20_Actual"]);
                TxbA20Diff.Text = Convert.ToString(dr["A_20_Difference"]);
                TxbA20Remarks.InnerText = Convert.ToString(dr["A_20_Remarks"]);
                TxbDilksnakan.Text = Convert.ToString(dr["Footer_Dilaksanakan"]);
                TxbTnglBwh.Text = Convert.ToString(dr["Footer_Tngl"]);
                TxbJamBawah.Text = Convert.ToString(dr["Footer_Jam"]);
                TxtCreateDate.Text = Convert.ToString(dr["Doe"]);
                TxtReviewDate.Text = Convert.ToString(dr["ReviewDate"]);

            }
            con.Close();
        }

        object ToDBValue(string str)
        {
            return string.IsNullOrEmpty(str) ? DBNull.Value : (object)str;
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("AuditBranch.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("AuditBranch.aspx?ID=" + txtIDSaveTemp.Text);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                SaveHeaderProc();
                //SaveGridViewB();
                //BindGridB();
                //BindData();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)) && (string.IsNullOrEmpty(TxtCreateDate.Text)) && (string.IsNullOrEmpty(TxtReviewDate.Text)))
            {
                SaveHeaderProcReview();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                SaveHeaderProcTemp();
                //SaveGridViewBTemp();
                //BindGridBTemp();
                //BindDataTemp();
            }

        }

        private void SaveHeaderProc()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_SaveProcHead", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDProc", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;
                    cmd.Parameters.Add("@Bina_Artha_Cabang", SqlDbType.NVarChar).Value = txbCabang.Text;
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@A_1_Data", string.IsNullOrEmpty(TxbA1Data.Text) ? (object)DBNull.Value : TxbA1Data.Text);
                    cmd.Parameters.AddWithValue("@A_1_Actual", string.IsNullOrEmpty(TxbA1Actual.Text) ? (object)DBNull.Value : TxbA1Actual.Text);
                    cmd.Parameters.AddWithValue("@A_1_Difference", string.IsNullOrEmpty(TxbA1Diff.Text) ? (object)DBNull.Value : TxbA1Diff.Text);
                    cmd.Parameters.AddWithValue("@A_1_Remarks", ToDBValue(TxbA1Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_2_Data", string.IsNullOrEmpty(TxbA2Data.Text) ? (object)DBNull.Value : TxbA2Data.Text);
                    cmd.Parameters.AddWithValue("@A_2_Actual", string.IsNullOrEmpty(TxbA2Actual.Text) ? (object)DBNull.Value : TxbA2Actual.Text);
                    cmd.Parameters.AddWithValue("@A_2_Difference", string.IsNullOrEmpty(TxbA2Diff.Text) ? (object)DBNull.Value : TxbA2Diff.Text);
                    cmd.Parameters.AddWithValue("@A_2_Remarks", ToDBValue(TxbA2Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_3_Data", string.IsNullOrEmpty(TxbA3Data.Text) ? (object)DBNull.Value : TxbA3Data.Text);
                    cmd.Parameters.AddWithValue("@A_3_Actual", string.IsNullOrEmpty(TxbA3Actual.Text) ? (object)DBNull.Value : TxbA3Actual.Text);
                    cmd.Parameters.AddWithValue("@A_3_Difference", string.IsNullOrEmpty(TxbA3Diff.Text) ? (object)DBNull.Value : TxbA3Diff.Text);
                    cmd.Parameters.AddWithValue("@A_3_Remarks", ToDBValue(TxbA3Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_4_Data", string.IsNullOrEmpty(TxbA4Data.Text) ? (object)DBNull.Value : TxbA4Data.Text);
                    cmd.Parameters.AddWithValue("@A_4_Actual", string.IsNullOrEmpty(TxbA4Actual.Text) ? (object)DBNull.Value : TxbA4Actual.Text);
                    cmd.Parameters.AddWithValue("@A_4_Difference", string.IsNullOrEmpty(TxbA4Diff.Text) ? (object)DBNull.Value : TxbA4Diff.Text);
                    cmd.Parameters.AddWithValue("@A_4_Remarks", ToDBValue(TxbA4Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_5_Data", string.IsNullOrEmpty(TxbA5Data.Text) ? (object)DBNull.Value : TxbA5Data.Text);
                    cmd.Parameters.AddWithValue("@A_5_Actual", string.IsNullOrEmpty(TxbA5Actual.Text) ? (object)DBNull.Value : TxbA5Actual.Text);
                    cmd.Parameters.AddWithValue("@A_5_Difference", string.IsNullOrEmpty(TxbA5Diff.Text) ? (object)DBNull.Value : TxbA5Diff.Text);
                    cmd.Parameters.AddWithValue("@A_5_Remarks", ToDBValue(TxbA5Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_6_Data", string.IsNullOrEmpty(TxbA6Data.Text) ? (object)DBNull.Value : TxbA6Data.Text);
                    cmd.Parameters.AddWithValue("@A_6_Actual", string.IsNullOrEmpty(TxbA6Actual.Text) ? (object)DBNull.Value : TxbA6Actual.Text);
                    cmd.Parameters.AddWithValue("@A_6_Difference", string.IsNullOrEmpty(TxbA6Diff.Text) ? (object)DBNull.Value : TxbA6Diff.Text);
                    cmd.Parameters.AddWithValue("@A_6_Remarks", ToDBValue(TxbA6Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_7_Data", string.IsNullOrEmpty(TxbA7Data.Text) ? (object)DBNull.Value : TxbA7Data.Text);
                    cmd.Parameters.AddWithValue("@A_7_Actual", string.IsNullOrEmpty(TxbA7Actual.Text) ? (object)DBNull.Value : TxbA7Actual.Text);
                    cmd.Parameters.AddWithValue("@A_7_Difference", string.IsNullOrEmpty(TxbA7Diff.Text) ? (object)DBNull.Value : TxbA7Diff.Text);
                    cmd.Parameters.AddWithValue("@A_7_Remarks", ToDBValue(TxbA7Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_8_Data", string.IsNullOrEmpty(TxbA8Data.Text) ? (object)DBNull.Value : TxbA8Data.Text);
                    cmd.Parameters.AddWithValue("@A_8_Actual", string.IsNullOrEmpty(TxbA8Actual.Text) ? (object)DBNull.Value : TxbA8Actual.Text);
                    cmd.Parameters.AddWithValue("@A_8_Difference", string.IsNullOrEmpty(TxbA8Diff.Text) ? (object)DBNull.Value : TxbA8Diff.Text);
                    cmd.Parameters.AddWithValue("@A_8_Remarks", ToDBValue(TxbA8Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_9_Data", string.IsNullOrEmpty(TxbA9Data.Text) ? (object)DBNull.Value : TxbA9Data.Text);
                    cmd.Parameters.AddWithValue("@A_9_Actual", string.IsNullOrEmpty(TxbA9Actual.Text) ? (object)DBNull.Value : TxbA9Actual.Text);
                    cmd.Parameters.AddWithValue("@A_9_Difference", string.IsNullOrEmpty(TxbA9Diff.Text) ? (object)DBNull.Value : TxbA9Diff.Text);
                    cmd.Parameters.AddWithValue("@A_9_Remarks", ToDBValue(TxbA9Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_10_Data", string.IsNullOrEmpty(TxbA10Data.Text) ? (object)DBNull.Value : TxbA10Data.Text);
                    cmd.Parameters.AddWithValue("@A_10_Actual", string.IsNullOrEmpty(TxbA10Actual.Text) ? (object)DBNull.Value : TxbA10Actual.Text);
                    cmd.Parameters.AddWithValue("@A_10_Difference", string.IsNullOrEmpty(TxbA10Diff.Text) ? (object)DBNull.Value : TxbA10Diff.Text);
                    cmd.Parameters.AddWithValue("@A_10_Remarks", ToDBValue(TxbA10Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_11_Data", string.IsNullOrEmpty(TxbA11Data.Text) ? (object)DBNull.Value : TxbA11Data.Text);
                    cmd.Parameters.AddWithValue("@A_11_Actual", string.IsNullOrEmpty(TxbA11Actual.Text) ? (object)DBNull.Value : TxbA11Actual.Text);
                    cmd.Parameters.AddWithValue("@A_11_Difference", string.IsNullOrEmpty(TxbA11Diff.Text) ? (object)DBNull.Value : TxbA11Diff.Text);
                    cmd.Parameters.AddWithValue("@A_11_Remarks", ToDBValue(TxbA11Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_12_Data", string.IsNullOrEmpty(TxbA12Data.Text) ? (object)DBNull.Value : TxbA12Data.Text);
                    cmd.Parameters.AddWithValue("@A_12_Actual", string.IsNullOrEmpty(TxbA12Actual.Text) ? (object)DBNull.Value : TxbA12Actual.Text);
                    cmd.Parameters.AddWithValue("@A_12_Difference", string.IsNullOrEmpty(TxbA12Diff.Text) ? (object)DBNull.Value : TxbA12Diff.Text);
                    cmd.Parameters.AddWithValue("@A_12_Remarks", ToDBValue(TxbA12Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_13_Data", string.IsNullOrEmpty(TxbA13Data.Text) ? (object)DBNull.Value : TxbA13Data.Text);
                    cmd.Parameters.AddWithValue("@A_13_Actual", string.IsNullOrEmpty(TxbA13Actual.Text) ? (object)DBNull.Value : TxbA13Actual.Text);
                    cmd.Parameters.AddWithValue("@A_13_Difference", string.IsNullOrEmpty(TxbA13Diff.Text) ? (object)DBNull.Value : TxbA13Diff.Text);
                    cmd.Parameters.AddWithValue("@A_13_Remarks", ToDBValue(TxbA13Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_14_Data", string.IsNullOrEmpty(TxbA14Data.Text) ? (object)DBNull.Value : TxbA14Data.Text);
                    cmd.Parameters.AddWithValue("@A_14_Actual", string.IsNullOrEmpty(TxbA14Actual.Text) ? (object)DBNull.Value : TxbA14Actual.Text);
                    cmd.Parameters.AddWithValue("@A_14_Difference", string.IsNullOrEmpty(TxbA14Diff.Text) ? (object)DBNull.Value : TxbA14Diff.Text);
                    cmd.Parameters.AddWithValue("@A_14_Remarks", ToDBValue(TxbA14Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_15_Data", string.IsNullOrEmpty(TxbA15Data.Text) ? (object)DBNull.Value : TxbA15Data.Text);
                    cmd.Parameters.AddWithValue("@A_15_Actual", string.IsNullOrEmpty(TxbA15Actual.Text) ? (object)DBNull.Value : TxbA15Actual.Text);
                    cmd.Parameters.AddWithValue("@A_15_Difference", string.IsNullOrEmpty(TxbA15Diff.Text) ? (object)DBNull.Value : TxbA15Diff.Text);
                    cmd.Parameters.AddWithValue("@A_15_Remarks", ToDBValue(TxbA15Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_16_Data", string.IsNullOrEmpty(TxbA16Data.Text) ? (object)DBNull.Value : TxbA16Data.Text);
                    cmd.Parameters.AddWithValue("@A_16_Actual", string.IsNullOrEmpty(TxbA16Actual.Text) ? (object)DBNull.Value : TxbA16Actual.Text);
                    cmd.Parameters.AddWithValue("@A_16_Difference", string.IsNullOrEmpty(TxbA16Diff.Text) ? (object)DBNull.Value : TxbA16Diff.Text);
                    cmd.Parameters.AddWithValue("@A_16_Remarks", ToDBValue(TxbA16Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_17_Data", string.IsNullOrEmpty(TxbA17Data.Text) ? (object)DBNull.Value : TxbA17Data.Text);
                    cmd.Parameters.AddWithValue("@A_17_Actual", string.IsNullOrEmpty(TxbA17Actual.Text) ? (object)DBNull.Value : TxbA17Actual.Text);
                    cmd.Parameters.AddWithValue("@A_17_Difference", string.IsNullOrEmpty(TxbA17Diff.Text) ? (object)DBNull.Value : TxbA17Diff.Text);
                    cmd.Parameters.AddWithValue("@A_17_Remarks", ToDBValue(TxbA17Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_18_Data", string.IsNullOrEmpty(TxbA18Data.Text) ? (object)DBNull.Value : TxbA18Data.Text);
                    cmd.Parameters.AddWithValue("@A_18_Actual", string.IsNullOrEmpty(TxbA18Actual.Text) ? (object)DBNull.Value : TxbA18Actual.Text);
                    cmd.Parameters.AddWithValue("@A_18_Difference", string.IsNullOrEmpty(TxbA18Diff.Text) ? (object)DBNull.Value : TxbA18Diff.Text);
                    cmd.Parameters.AddWithValue("@A_18_Remarks", ToDBValue(TxbA18Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_19_Data", string.IsNullOrEmpty(TxbA19Data.Text) ? (object)DBNull.Value : TxbA19Data.Text);
                    cmd.Parameters.AddWithValue("@A_19_Actual", string.IsNullOrEmpty(TxbA19Actual.Text) ? (object)DBNull.Value : TxbA19Actual.Text);
                    cmd.Parameters.AddWithValue("@A_19_Difference", string.IsNullOrEmpty(TxbA19Diff.Text) ? (object)DBNull.Value : TxbA19Diff.Text);
                    cmd.Parameters.AddWithValue("@A_19_Remarks", ToDBValue(TxbA19Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_20_Data", string.IsNullOrEmpty(TxbA20Data.Text) ? (object)DBNull.Value : TxbA20Data.Text);
                    cmd.Parameters.AddWithValue("@A_20_Actual", string.IsNullOrEmpty(TxbA20Actual.Text) ? (object)DBNull.Value : TxbA20Actual.Text);
                    cmd.Parameters.AddWithValue("@A_20_Difference", string.IsNullOrEmpty(TxbA20Diff.Text) ? (object)DBNull.Value : TxbA20Diff.Text);
                    cmd.Parameters.AddWithValue("@A_20_Remarks", ToDBValue(TxbA20Remarks.InnerText));

                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilksnakan.Text) ? (object)DBNull.Value : TxbDilksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbJamBawah.Text) ? (object)DBNull.Value : TxbJamBawah.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;

                    con.Open();
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        //lblMessage.Visible = true;
                        //ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        //lblMessage.Text = "Not Succes Saved";
                    }
                    con.Close();
                }

            }
        }

        private void SaveHeaderProcReview()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_SaveProcHead", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDProc", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;
                    cmd.Parameters.Add("@Bina_Artha_Cabang", SqlDbType.NVarChar).Value = txbCabang.Text;
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@A_1_Data", string.IsNullOrEmpty(TxbA1Data.Text) ? (object)DBNull.Value : TxbA1Data.Text);
                    cmd.Parameters.AddWithValue("@A_1_Actual", string.IsNullOrEmpty(TxbA1Actual.Text) ? (object)DBNull.Value : TxbA1Actual.Text);
                    cmd.Parameters.AddWithValue("@A_1_Difference", string.IsNullOrEmpty(TxbA1Diff.Text) ? (object)DBNull.Value : TxbA1Diff.Text);
                    cmd.Parameters.AddWithValue("@A_1_Remarks", ToDBValue(TxbA1Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_2_Data", string.IsNullOrEmpty(TxbA2Data.Text) ? (object)DBNull.Value : TxbA2Data.Text);
                    cmd.Parameters.AddWithValue("@A_2_Actual", string.IsNullOrEmpty(TxbA2Actual.Text) ? (object)DBNull.Value : TxbA2Actual.Text);
                    cmd.Parameters.AddWithValue("@A_2_Difference", string.IsNullOrEmpty(TxbA2Diff.Text) ? (object)DBNull.Value : TxbA2Diff.Text);
                    cmd.Parameters.AddWithValue("@A_2_Remarks", ToDBValue(TxbA2Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_3_Data", string.IsNullOrEmpty(TxbA3Data.Text) ? (object)DBNull.Value : TxbA3Data.Text);
                    cmd.Parameters.AddWithValue("@A_3_Actual", string.IsNullOrEmpty(TxbA3Actual.Text) ? (object)DBNull.Value : TxbA3Actual.Text);
                    cmd.Parameters.AddWithValue("@A_3_Difference", string.IsNullOrEmpty(TxbA3Diff.Text) ? (object)DBNull.Value : TxbA3Diff.Text);
                    cmd.Parameters.AddWithValue("@A_3_Remarks", ToDBValue(TxbA3Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_4_Data", string.IsNullOrEmpty(TxbA4Data.Text) ? (object)DBNull.Value : TxbA4Data.Text);
                    cmd.Parameters.AddWithValue("@A_4_Actual", string.IsNullOrEmpty(TxbA4Actual.Text) ? (object)DBNull.Value : TxbA4Actual.Text);
                    cmd.Parameters.AddWithValue("@A_4_Difference", string.IsNullOrEmpty(TxbA4Diff.Text) ? (object)DBNull.Value : TxbA4Diff.Text);
                    cmd.Parameters.AddWithValue("@A_4_Remarks", ToDBValue(TxbA4Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_5_Data", string.IsNullOrEmpty(TxbA5Data.Text) ? (object)DBNull.Value : TxbA5Data.Text);
                    cmd.Parameters.AddWithValue("@A_5_Actual", string.IsNullOrEmpty(TxbA5Actual.Text) ? (object)DBNull.Value : TxbA5Actual.Text);
                    cmd.Parameters.AddWithValue("@A_5_Difference", string.IsNullOrEmpty(TxbA5Diff.Text) ? (object)DBNull.Value : TxbA5Diff.Text);
                    cmd.Parameters.AddWithValue("@A_5_Remarks", ToDBValue(TxbA5Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_6_Data", string.IsNullOrEmpty(TxbA6Data.Text) ? (object)DBNull.Value : TxbA6Data.Text);
                    cmd.Parameters.AddWithValue("@A_6_Actual", string.IsNullOrEmpty(TxbA6Actual.Text) ? (object)DBNull.Value : TxbA6Actual.Text);
                    cmd.Parameters.AddWithValue("@A_6_Difference", string.IsNullOrEmpty(TxbA6Diff.Text) ? (object)DBNull.Value : TxbA6Diff.Text);
                    cmd.Parameters.AddWithValue("@A_6_Remarks", ToDBValue(TxbA6Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_7_Data", string.IsNullOrEmpty(TxbA7Data.Text) ? (object)DBNull.Value : TxbA7Data.Text);
                    cmd.Parameters.AddWithValue("@A_7_Actual", string.IsNullOrEmpty(TxbA7Actual.Text) ? (object)DBNull.Value : TxbA7Actual.Text);
                    cmd.Parameters.AddWithValue("@A_7_Difference", string.IsNullOrEmpty(TxbA7Diff.Text) ? (object)DBNull.Value : TxbA7Diff.Text);
                    cmd.Parameters.AddWithValue("@A_7_Remarks", ToDBValue(TxbA7Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_8_Data", string.IsNullOrEmpty(TxbA8Data.Text) ? (object)DBNull.Value : TxbA8Data.Text);
                    cmd.Parameters.AddWithValue("@A_8_Actual", string.IsNullOrEmpty(TxbA8Actual.Text) ? (object)DBNull.Value : TxbA8Actual.Text);
                    cmd.Parameters.AddWithValue("@A_8_Difference", string.IsNullOrEmpty(TxbA8Diff.Text) ? (object)DBNull.Value : TxbA8Diff.Text);
                    cmd.Parameters.AddWithValue("@A_8_Remarks", ToDBValue(TxbA8Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_9_Data", string.IsNullOrEmpty(TxbA9Data.Text) ? (object)DBNull.Value : TxbA9Data.Text);
                    cmd.Parameters.AddWithValue("@A_9_Actual", string.IsNullOrEmpty(TxbA9Actual.Text) ? (object)DBNull.Value : TxbA9Actual.Text);
                    cmd.Parameters.AddWithValue("@A_9_Difference", string.IsNullOrEmpty(TxbA9Diff.Text) ? (object)DBNull.Value : TxbA9Diff.Text);
                    cmd.Parameters.AddWithValue("@A_9_Remarks", ToDBValue(TxbA9Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_10_Data", string.IsNullOrEmpty(TxbA10Data.Text) ? (object)DBNull.Value : TxbA10Data.Text);
                    cmd.Parameters.AddWithValue("@A_10_Actual", string.IsNullOrEmpty(TxbA10Actual.Text) ? (object)DBNull.Value : TxbA10Actual.Text);
                    cmd.Parameters.AddWithValue("@A_10_Difference", string.IsNullOrEmpty(TxbA10Diff.Text) ? (object)DBNull.Value : TxbA10Diff.Text);
                    cmd.Parameters.AddWithValue("@A_10_Remarks", ToDBValue(TxbA10Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_11_Data", string.IsNullOrEmpty(TxbA11Data.Text) ? (object)DBNull.Value : TxbA11Data.Text);
                    cmd.Parameters.AddWithValue("@A_11_Actual", string.IsNullOrEmpty(TxbA11Actual.Text) ? (object)DBNull.Value : TxbA11Actual.Text);
                    cmd.Parameters.AddWithValue("@A_11_Difference", string.IsNullOrEmpty(TxbA11Diff.Text) ? (object)DBNull.Value : TxbA11Diff.Text);
                    cmd.Parameters.AddWithValue("@A_11_Remarks", ToDBValue(TxbA11Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_12_Data", string.IsNullOrEmpty(TxbA12Data.Text) ? (object)DBNull.Value : TxbA12Data.Text);
                    cmd.Parameters.AddWithValue("@A_12_Actual", string.IsNullOrEmpty(TxbA12Actual.Text) ? (object)DBNull.Value : TxbA12Actual.Text);
                    cmd.Parameters.AddWithValue("@A_12_Difference", string.IsNullOrEmpty(TxbA12Diff.Text) ? (object)DBNull.Value : TxbA12Diff.Text);
                    cmd.Parameters.AddWithValue("@A_12_Remarks", ToDBValue(TxbA12Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_13_Data", string.IsNullOrEmpty(TxbA13Data.Text) ? (object)DBNull.Value : TxbA13Data.Text);
                    cmd.Parameters.AddWithValue("@A_13_Actual", string.IsNullOrEmpty(TxbA13Actual.Text) ? (object)DBNull.Value : TxbA13Actual.Text);
                    cmd.Parameters.AddWithValue("@A_13_Difference", string.IsNullOrEmpty(TxbA13Diff.Text) ? (object)DBNull.Value : TxbA13Diff.Text);
                    cmd.Parameters.AddWithValue("@A_13_Remarks", ToDBValue(TxbA13Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_14_Data", string.IsNullOrEmpty(TxbA14Data.Text) ? (object)DBNull.Value : TxbA14Data.Text);
                    cmd.Parameters.AddWithValue("@A_14_Actual", string.IsNullOrEmpty(TxbA14Actual.Text) ? (object)DBNull.Value : TxbA14Actual.Text);
                    cmd.Parameters.AddWithValue("@A_14_Difference", string.IsNullOrEmpty(TxbA14Diff.Text) ? (object)DBNull.Value : TxbA14Diff.Text);
                    cmd.Parameters.AddWithValue("@A_14_Remarks", ToDBValue(TxbA14Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_15_Data", string.IsNullOrEmpty(TxbA15Data.Text) ? (object)DBNull.Value : TxbA15Data.Text);
                    cmd.Parameters.AddWithValue("@A_15_Actual", string.IsNullOrEmpty(TxbA15Actual.Text) ? (object)DBNull.Value : TxbA15Actual.Text);
                    cmd.Parameters.AddWithValue("@A_15_Difference", string.IsNullOrEmpty(TxbA15Diff.Text) ? (object)DBNull.Value : TxbA15Diff.Text);
                    cmd.Parameters.AddWithValue("@A_15_Remarks", ToDBValue(TxbA15Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_16_Data", string.IsNullOrEmpty(TxbA16Data.Text) ? (object)DBNull.Value : TxbA16Data.Text);
                    cmd.Parameters.AddWithValue("@A_16_Actual", string.IsNullOrEmpty(TxbA16Actual.Text) ? (object)DBNull.Value : TxbA16Actual.Text);
                    cmd.Parameters.AddWithValue("@A_16_Difference", string.IsNullOrEmpty(TxbA16Diff.Text) ? (object)DBNull.Value : TxbA16Diff.Text);
                    cmd.Parameters.AddWithValue("@A_16_Remarks", ToDBValue(TxbA16Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_17_Data", string.IsNullOrEmpty(TxbA17Data.Text) ? (object)DBNull.Value : TxbA17Data.Text);
                    cmd.Parameters.AddWithValue("@A_17_Actual", string.IsNullOrEmpty(TxbA17Actual.Text) ? (object)DBNull.Value : TxbA17Actual.Text);
                    cmd.Parameters.AddWithValue("@A_17_Difference", string.IsNullOrEmpty(TxbA17Diff.Text) ? (object)DBNull.Value : TxbA17Diff.Text);
                    cmd.Parameters.AddWithValue("@A_17_Remarks", ToDBValue(TxbA17Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_18_Data", string.IsNullOrEmpty(TxbA18Data.Text) ? (object)DBNull.Value : TxbA18Data.Text);
                    cmd.Parameters.AddWithValue("@A_18_Actual", string.IsNullOrEmpty(TxbA18Actual.Text) ? (object)DBNull.Value : TxbA18Actual.Text);
                    cmd.Parameters.AddWithValue("@A_18_Difference", string.IsNullOrEmpty(TxbA18Diff.Text) ? (object)DBNull.Value : TxbA18Diff.Text);
                    cmd.Parameters.AddWithValue("@A_18_Remarks", ToDBValue(TxbA18Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_19_Data", string.IsNullOrEmpty(TxbA19Data.Text) ? (object)DBNull.Value : TxbA19Data.Text);
                    cmd.Parameters.AddWithValue("@A_19_Actual", string.IsNullOrEmpty(TxbA19Actual.Text) ? (object)DBNull.Value : TxbA19Actual.Text);
                    cmd.Parameters.AddWithValue("@A_19_Difference", string.IsNullOrEmpty(TxbA19Diff.Text) ? (object)DBNull.Value : TxbA19Diff.Text);
                    cmd.Parameters.AddWithValue("@A_19_Remarks", ToDBValue(TxbA19Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_20_Data", string.IsNullOrEmpty(TxbA20Data.Text) ? (object)DBNull.Value : TxbA20Data.Text);
                    cmd.Parameters.AddWithValue("@A_20_Actual", string.IsNullOrEmpty(TxbA20Actual.Text) ? (object)DBNull.Value : TxbA20Actual.Text);
                    cmd.Parameters.AddWithValue("@A_20_Difference", string.IsNullOrEmpty(TxbA20Diff.Text) ? (object)DBNull.Value : TxbA20Diff.Text);
                    cmd.Parameters.AddWithValue("@A_20_Remarks", ToDBValue(TxbA20Remarks.InnerText));

                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilksnakan.Text) ? (object)DBNull.Value : TxbDilksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbJamBawah.Text) ? (object)DBNull.Value : TxbJamBawah.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;

                    con.Open();
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        //lblMessage.Visible = true;
                        //ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        //lblMessage.Text = "Not Succes Saved";
                    }
                    con.Close();
                }

            }
        }

        private void SaveHeaderProcTemp()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_SaveProcHead", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDProc", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;
                    cmd.Parameters.Add("@Bina_Artha_Cabang", SqlDbType.NVarChar).Value = txbCabang.Text;
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@A_1_Data", string.IsNullOrEmpty(TxbA1Data.Text) ? (object)DBNull.Value : TxbA1Data.Text);
                    cmd.Parameters.AddWithValue("@A_1_Actual", string.IsNullOrEmpty(TxbA1Actual.Text) ? (object)DBNull.Value : TxbA1Actual.Text);
                    cmd.Parameters.AddWithValue("@A_1_Difference", string.IsNullOrEmpty(TxbA1Diff.Text) ? (object)DBNull.Value : TxbA1Diff.Text);
                    cmd.Parameters.AddWithValue("@A_1_Remarks", ToDBValue(TxbA1Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_2_Data", string.IsNullOrEmpty(TxbA2Data.Text) ? (object)DBNull.Value : TxbA2Data.Text);
                    cmd.Parameters.AddWithValue("@A_2_Actual", string.IsNullOrEmpty(TxbA2Actual.Text) ? (object)DBNull.Value : TxbA2Actual.Text);
                    cmd.Parameters.AddWithValue("@A_2_Difference", string.IsNullOrEmpty(TxbA2Diff.Text) ? (object)DBNull.Value : TxbA2Diff.Text);
                    cmd.Parameters.AddWithValue("@A_2_Remarks", ToDBValue(TxbA2Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_3_Data", string.IsNullOrEmpty(TxbA3Data.Text) ? (object)DBNull.Value : TxbA3Data.Text);
                    cmd.Parameters.AddWithValue("@A_3_Actual", string.IsNullOrEmpty(TxbA3Actual.Text) ? (object)DBNull.Value : TxbA3Actual.Text);
                    cmd.Parameters.AddWithValue("@A_3_Difference", string.IsNullOrEmpty(TxbA3Diff.Text) ? (object)DBNull.Value : TxbA3Diff.Text);
                    cmd.Parameters.AddWithValue("@A_3_Remarks", ToDBValue(TxbA3Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_4_Data", string.IsNullOrEmpty(TxbA4Data.Text) ? (object)DBNull.Value : TxbA4Data.Text);
                    cmd.Parameters.AddWithValue("@A_4_Actual", string.IsNullOrEmpty(TxbA4Actual.Text) ? (object)DBNull.Value : TxbA4Actual.Text);
                    cmd.Parameters.AddWithValue("@A_4_Difference", string.IsNullOrEmpty(TxbA4Diff.Text) ? (object)DBNull.Value : TxbA4Diff.Text);
                    cmd.Parameters.AddWithValue("@A_4_Remarks", ToDBValue(TxbA4Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_5_Data", string.IsNullOrEmpty(TxbA5Data.Text) ? (object)DBNull.Value : TxbA5Data.Text);
                    cmd.Parameters.AddWithValue("@A_5_Actual", string.IsNullOrEmpty(TxbA5Actual.Text) ? (object)DBNull.Value : TxbA5Actual.Text);
                    cmd.Parameters.AddWithValue("@A_5_Difference", string.IsNullOrEmpty(TxbA5Diff.Text) ? (object)DBNull.Value : TxbA5Diff.Text);
                    cmd.Parameters.AddWithValue("@A_5_Remarks", ToDBValue(TxbA5Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_6_Data", string.IsNullOrEmpty(TxbA6Data.Text) ? (object)DBNull.Value : TxbA6Data.Text);
                    cmd.Parameters.AddWithValue("@A_6_Actual", string.IsNullOrEmpty(TxbA6Actual.Text) ? (object)DBNull.Value : TxbA6Actual.Text);
                    cmd.Parameters.AddWithValue("@A_6_Difference", string.IsNullOrEmpty(TxbA6Diff.Text) ? (object)DBNull.Value : TxbA6Diff.Text);
                    cmd.Parameters.AddWithValue("@A_6_Remarks", ToDBValue(TxbA6Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_7_Data", string.IsNullOrEmpty(TxbA7Data.Text) ? (object)DBNull.Value : TxbA7Data.Text);
                    cmd.Parameters.AddWithValue("@A_7_Actual", string.IsNullOrEmpty(TxbA7Actual.Text) ? (object)DBNull.Value : TxbA7Actual.Text);
                    cmd.Parameters.AddWithValue("@A_7_Difference", string.IsNullOrEmpty(TxbA7Diff.Text) ? (object)DBNull.Value : TxbA7Diff.Text);
                    cmd.Parameters.AddWithValue("@A_7_Remarks", ToDBValue(TxbA7Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_8_Data", string.IsNullOrEmpty(TxbA8Data.Text) ? (object)DBNull.Value : TxbA8Data.Text);
                    cmd.Parameters.AddWithValue("@A_8_Actual", string.IsNullOrEmpty(TxbA8Actual.Text) ? (object)DBNull.Value : TxbA8Actual.Text);
                    cmd.Parameters.AddWithValue("@A_8_Difference", string.IsNullOrEmpty(TxbA8Diff.Text) ? (object)DBNull.Value : TxbA8Diff.Text);
                    cmd.Parameters.AddWithValue("@A_8_Remarks", ToDBValue(TxbA8Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_9_Data", string.IsNullOrEmpty(TxbA9Data.Text) ? (object)DBNull.Value : TxbA9Data.Text);
                    cmd.Parameters.AddWithValue("@A_9_Actual", string.IsNullOrEmpty(TxbA9Actual.Text) ? (object)DBNull.Value : TxbA9Actual.Text);
                    cmd.Parameters.AddWithValue("@A_9_Difference", string.IsNullOrEmpty(TxbA9Diff.Text) ? (object)DBNull.Value : TxbA9Diff.Text);
                    cmd.Parameters.AddWithValue("@A_9_Remarks", ToDBValue(TxbA9Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_10_Data", string.IsNullOrEmpty(TxbA10Data.Text) ? (object)DBNull.Value : TxbA10Data.Text);
                    cmd.Parameters.AddWithValue("@A_10_Actual", string.IsNullOrEmpty(TxbA10Actual.Text) ? (object)DBNull.Value : TxbA10Actual.Text);
                    cmd.Parameters.AddWithValue("@A_10_Difference", string.IsNullOrEmpty(TxbA10Diff.Text) ? (object)DBNull.Value : TxbA10Diff.Text);
                    cmd.Parameters.AddWithValue("@A_10_Remarks", ToDBValue(TxbA10Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_11_Data", string.IsNullOrEmpty(TxbA11Data.Text) ? (object)DBNull.Value : TxbA11Data.Text);
                    cmd.Parameters.AddWithValue("@A_11_Actual", string.IsNullOrEmpty(TxbA11Actual.Text) ? (object)DBNull.Value : TxbA11Actual.Text);
                    cmd.Parameters.AddWithValue("@A_11_Difference", string.IsNullOrEmpty(TxbA11Diff.Text) ? (object)DBNull.Value : TxbA11Diff.Text);
                    cmd.Parameters.AddWithValue("@A_11_Remarks", ToDBValue(TxbA11Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_12_Data", string.IsNullOrEmpty(TxbA12Data.Text) ? (object)DBNull.Value : TxbA12Data.Text);
                    cmd.Parameters.AddWithValue("@A_12_Actual", string.IsNullOrEmpty(TxbA12Actual.Text) ? (object)DBNull.Value : TxbA12Actual.Text);
                    cmd.Parameters.AddWithValue("@A_12_Difference", string.IsNullOrEmpty(TxbA12Diff.Text) ? (object)DBNull.Value : TxbA12Diff.Text);
                    cmd.Parameters.AddWithValue("@A_12_Remarks", ToDBValue(TxbA12Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_13_Data", string.IsNullOrEmpty(TxbA13Data.Text) ? (object)DBNull.Value : TxbA13Data.Text);
                    cmd.Parameters.AddWithValue("@A_13_Actual", string.IsNullOrEmpty(TxbA13Actual.Text) ? (object)DBNull.Value : TxbA13Actual.Text);
                    cmd.Parameters.AddWithValue("@A_13_Difference", string.IsNullOrEmpty(TxbA13Diff.Text) ? (object)DBNull.Value : TxbA13Diff.Text);
                    cmd.Parameters.AddWithValue("@A_13_Remarks", ToDBValue(TxbA13Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_14_Data", string.IsNullOrEmpty(TxbA14Data.Text) ? (object)DBNull.Value : TxbA14Data.Text);
                    cmd.Parameters.AddWithValue("@A_14_Actual", string.IsNullOrEmpty(TxbA14Actual.Text) ? (object)DBNull.Value : TxbA14Actual.Text);
                    cmd.Parameters.AddWithValue("@A_14_Difference", string.IsNullOrEmpty(TxbA14Diff.Text) ? (object)DBNull.Value : TxbA14Diff.Text);
                    cmd.Parameters.AddWithValue("@A_14_Remarks", ToDBValue(TxbA14Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_15_Data", string.IsNullOrEmpty(TxbA15Data.Text) ? (object)DBNull.Value : TxbA15Data.Text);
                    cmd.Parameters.AddWithValue("@A_15_Actual", string.IsNullOrEmpty(TxbA15Actual.Text) ? (object)DBNull.Value : TxbA15Actual.Text);
                    cmd.Parameters.AddWithValue("@A_15_Difference", string.IsNullOrEmpty(TxbA15Diff.Text) ? (object)DBNull.Value : TxbA15Diff.Text);
                    cmd.Parameters.AddWithValue("@A_15_Remarks", ToDBValue(TxbA15Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_16_Data", string.IsNullOrEmpty(TxbA16Data.Text) ? (object)DBNull.Value : TxbA16Data.Text);
                    cmd.Parameters.AddWithValue("@A_16_Actual", string.IsNullOrEmpty(TxbA16Actual.Text) ? (object)DBNull.Value : TxbA16Actual.Text);
                    cmd.Parameters.AddWithValue("@A_16_Difference", string.IsNullOrEmpty(TxbA16Diff.Text) ? (object)DBNull.Value : TxbA16Diff.Text);
                    cmd.Parameters.AddWithValue("@A_16_Remarks", ToDBValue(TxbA16Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_17_Data", string.IsNullOrEmpty(TxbA17Data.Text) ? (object)DBNull.Value : TxbA17Data.Text);
                    cmd.Parameters.AddWithValue("@A_17_Actual", string.IsNullOrEmpty(TxbA17Actual.Text) ? (object)DBNull.Value : TxbA17Actual.Text);
                    cmd.Parameters.AddWithValue("@A_17_Difference", string.IsNullOrEmpty(TxbA17Diff.Text) ? (object)DBNull.Value : TxbA17Diff.Text);
                    cmd.Parameters.AddWithValue("@A_17_Remarks", ToDBValue(TxbA17Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_18_Data", string.IsNullOrEmpty(TxbA18Data.Text) ? (object)DBNull.Value : TxbA18Data.Text);
                    cmd.Parameters.AddWithValue("@A_18_Actual", string.IsNullOrEmpty(TxbA18Actual.Text) ? (object)DBNull.Value : TxbA18Actual.Text);
                    cmd.Parameters.AddWithValue("@A_18_Difference", string.IsNullOrEmpty(TxbA18Diff.Text) ? (object)DBNull.Value : TxbA18Diff.Text);
                    cmd.Parameters.AddWithValue("@A_18_Remarks", ToDBValue(TxbA18Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_19_Data", string.IsNullOrEmpty(TxbA19Data.Text) ? (object)DBNull.Value : TxbA19Data.Text);
                    cmd.Parameters.AddWithValue("@A_19_Actual", string.IsNullOrEmpty(TxbA19Actual.Text) ? (object)DBNull.Value : TxbA19Actual.Text);
                    cmd.Parameters.AddWithValue("@A_19_Difference", string.IsNullOrEmpty(TxbA19Diff.Text) ? (object)DBNull.Value : TxbA19Diff.Text);
                    cmd.Parameters.AddWithValue("@A_19_Remarks", ToDBValue(TxbA19Remarks.InnerText));
                    cmd.Parameters.AddWithValue("@A_20_Data", string.IsNullOrEmpty(TxbA20Data.Text) ? (object)DBNull.Value : TxbA20Data.Text);
                    cmd.Parameters.AddWithValue("@A_20_Actual", string.IsNullOrEmpty(TxbA20Actual.Text) ? (object)DBNull.Value : TxbA20Actual.Text);
                    cmd.Parameters.AddWithValue("@A_20_Difference", string.IsNullOrEmpty(TxbA20Diff.Text) ? (object)DBNull.Value : TxbA20Diff.Text);
                    cmd.Parameters.AddWithValue("@A_20_Remarks", ToDBValue(TxbA20Remarks.InnerText));

                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilksnakan.Text) ? (object)DBNull.Value : TxbDilksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbJamBawah.Text) ? (object)DBNull.Value : TxbJamBawah.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DBNull.Value;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;

                    con.Open();
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        //lblMessage.Visible = true;
                        //ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
                    }
                    else
                    {
                        message = "Data Tidak Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        //lblMessage.Text = "Not Succes Saved";
                    }
                    con.Close();
                }

            }
        }

        public void FillGridView()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            try
            {
                string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                string query = string.Format(@"select * from Procurement_B where IDProc_B={0}", IDProc_B);
                SqlConnection con = new SqlConnection(cnString);
                GlobalClass.adap = new SqlDataAdapter(query, con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView1.DataSource = GlobalClass.dt;
                GridView1.DataBind();
            }
            catch
            {
                Response.Write("<script> alert('Connection StringError...') </script>");
            }
        }

        protected void editRecord(object sender, GridViewEditEventArgs e)
        {
            //Image imgEditPhoto = GridView1.Rows[e.NewEditIndex].FindControl("imgPhoto") as Image;
            //GlobalClass.imgEditPath = imgEditPhoto.ImageUrl;
            // Get the current row index for edit record
            GridView1.EditIndex = e.NewEditIndex;
            FillGridView();
        }

        protected void cancelRecord(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillGridView();
        }

        protected void AddNewCancel(object sender, EventArgs e)
        {
            GridView1.ShowFooter = false;
            FillGridView();
        }

        protected void InsertNewRecord(object sender, EventArgs e)
        {
            string id = null;
            string doe = DateTime.Now.ToString();
            //string jam = null;
            //GlobalClass.adap.SelectCommand.Parameters.AddWithValue("@IDProc_B", txtIDSaveTemp.Text);
            try
            {
                string strName = GlobalClass.dt.Rows[0]["B_AssetLainnya_Inv"].ToString();
                if (strName == "0")
                {
                    GlobalClass.dt.Rows[0].Delete();
                    GlobalClass.adap.Update(GlobalClass.dt);
                }

                TextBox txtName = GridView1.FooterRow.FindControl("txtNewName") as TextBox;
                TextBox txtAge = GridView1.FooterRow.FindControl("txtNewAge") as TextBox;
                TextBox txtSalary = GridView1.FooterRow.FindControl("txtNewSalary") as TextBox;
                TextBox txtCountry = GridView1.FooterRow.FindControl("txtNewCountry") as TextBox;
                TextBox txtCity = GridView1.FooterRow.FindControl("txtNewCity") as TextBox;
                id = txtIDSaveTemp.Text;
                //jam = TxbJamBawah.Text;
                //FileUpload fuPhoto = GridView1.FooterRow.FindControl("fuNewPhoto") as FileUpload;
                //Guid FileName = Guid.NewGuid();
                //fuPhoto.SaveAs(Server.MapPath("~/Images/" + FileName + ".png"));
                DataRow dr = GlobalClass.dt.NewRow();
                dr["B_AssetLainnya_Inv"] = txtName.Text.Trim();
                dr["B_AssetLainnya_Data"] = txtAge.Text.Trim();
                dr["B_AssetLainnya_Actual"] = txtSalary.Text.Trim();
                dr["B_AssetLainnya_Diff"] = txtCountry.Text.Trim();
                dr["B_AssetLainnya_Remarks"] = txtCity.Text.Trim();
                dr["IDProc_B"] = txtIDSaveTemp.Text.Trim();
                dr["Doe"] = doe.ToString();
                //dr["photopath"] = "~/Images/" + FileName + ".png";
                GlobalClass.dt.Rows.Add(dr);
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.ShowFooter = false;
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not added...') </script>");
            }
        }

        protected void updateRecord(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                TextBox txtName = GridView1.Rows[e.RowIndex].FindControl("txtName") as TextBox;
                TextBox txtAge = GridView1.Rows[e.RowIndex].FindControl("txtAge") as TextBox;
                TextBox txtSalary = GridView1.Rows[e.RowIndex].FindControl("txtSalary") as TextBox;
                TextBox txtCountry = GridView1.Rows[e.RowIndex].FindControl("txtCountry") as TextBox;
                TextBox txtCity = GridView1.Rows[e.RowIndex].FindControl("txtCity") as TextBox;
                //FileUpload fuPhoto = GridView1.Rows[e.RowIndex].FindControl("fuPhoto") as FileUpload;
                //Guid FileName = Guid.NewGuid();
                //if (fuPhoto.FileName != "")
                //{
                //    fuPhoto.SaveAs(Server.MapPath("~/Images/" + FileName + ".png"));
                //    GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["photopath"] = "~/Images/" + FileName + ".png";
                //    File.Delete(Server.MapPath(GlobalClass.imgEditPath));
                //}               

                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Inv"] = txtName.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Data"] = Convert.ToInt32(txtAge.Text.Trim());
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Actual"] = Convert.ToInt32(txtSalary.Text.Trim());
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Diff"] = txtCountry.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Remarks"] = txtCity.Text.Trim();
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.EditIndex = -1;
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record updation fail...') </script>");
            }
        }

        protected void RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex].Delete();
                GlobalClass.adap.Update(GlobalClass.dt);
                //Image imgPhoto = GridView1.Rows[e.RowIndex].FindControl("imgPhoto") as Image;
                //File.Delete(Server.MapPath(imgPhoto.ImageUrl));
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not deleted...') </script>");
            }
        }

        public void FillGridViewTemp()
        {
            string IDProc_B = txtIDSave.Text;
            try
            {
                string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                string query = string.Format(@"select * from Procurement_B where IDProc_B={0}", IDProc_B);
                SqlConnection con = new SqlConnection(cnString);
                GlobalClass.adap = new SqlDataAdapter(query, con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView2.DataSource = GlobalClass.dt;
                GridView2.DataBind();
            }
            catch
            {
                Response.Write("<script> alert('Connection StringError...') </script>");
            }
        }

        protected void editRecordTemp(object sender, GridViewEditEventArgs e)
        {
            //Image imgEditPhoto = GridView1.Rows[e.NewEditIndex].FindControl("imgPhoto") as Image;
            //GlobalClass.imgEditPath = imgEditPhoto.ImageUrl;
            // Get the current row index for edit record
            GridView2.EditIndex = e.NewEditIndex;
            FillGridViewTemp();
        }

        protected void cancelRecordTemp(object sender, GridViewCancelEditEventArgs e)
        {
            GridView2.EditIndex = -1;
            FillGridViewTemp();
        }

        protected void AddNewRecord(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    if (GlobalClass.dt.Rows.Count > 0)
                    {
                        GridView1.EditIndex = -1;
                        GridView1.ShowFooter = true;
                        FillGridView();
                    }
                    else
                    {
                        GridView1.ShowFooter = true;
                        DataRow dr = GlobalClass.dt.NewRow();
                        dr["B_AssetLainnya_Inv"] = "0";
                        dr["B_AssetLainnya_Data"] = "0";
                        dr["B_AssetLainnya_Actual"] = "0";
                        dr["B_AssetLainnya_Diff"] = "0";
                        dr["B_AssetLainnya_Remarks"] = "0";
                        //dr["photopath"] = "0";
                        GlobalClass.dt.Rows.Add(dr);
                        GridView1.DataSource = GlobalClass.dt;
                        GridView1.DataBind();
                        GridView1.Rows[0].Visible = false;
                    }
                }
                catch
                {
                    Response.Write("<script> alert('Row not added in DataTable...') </script>");
                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    if (GlobalClass.dt.Rows.Count > 0)
                    {
                        GridView2.EditIndex = -1;
                        GridView2.ShowFooter = true;
                        FillGridViewTemp();
                    }
                    else
                    {
                        GridView2.ShowFooter = true;
                        DataRow dr = GlobalClass.dt.NewRow();
                        dr["B_AssetLainnya_Inv"] = "0";
                        dr["B_AssetLainnya_Data"] = "0";
                        dr["B_AssetLainnya_Actual"] = "0";
                        dr["B_AssetLainnya_Diff"] = "0";
                        dr["B_AssetLainnya_Remarks"] = "0";
                        //dr["photopath"] = "0";
                        GlobalClass.dt.Rows.Add(dr);
                        GridView2.DataSource = GlobalClass.dt;
                        GridView2.DataBind();
                        GridView2.Rows[0].Visible = false;
                    }
                }
                catch
                {
                    Response.Write("<script> alert('Row not added in DataTable...') </script>");
                }

            }
            else
            {

            }




        }

        protected void AddNewCancelTemp(object sender, EventArgs e)
        {
            GridView2.ShowFooter = false;
            FillGridViewTemp();
        }

        protected void InsertNewRecordTemp(object sender, EventArgs e)
        {
            string id = null;
            string doe = DateTime.Now.ToString();
            //GlobalClass.adap.SelectCommand.Parameters.AddWithValue("@IDProc_B", txtIDSaveTemp.Text);
            try
            {
                string strName = GlobalClass.dt.Rows[0]["B_AssetLainnya_Inv"].ToString();
                if (strName == "0")
                {
                    GlobalClass.dt.Rows[0].Delete();
                    GlobalClass.adap.Update(GlobalClass.dt);
                }

                TextBox txtName2 = GridView2.FooterRow.FindControl("txtNewName2") as TextBox;
                TextBox txtAge2 = GridView2.FooterRow.FindControl("txtNewAge2") as TextBox;
                TextBox txtSalary2 = GridView2.FooterRow.FindControl("txtNewSalary2") as TextBox;
                TextBox txtCountry2 = GridView2.FooterRow.FindControl("txtNewCountry2") as TextBox;
                TextBox txtCity2 = GridView2.FooterRow.FindControl("txtNewCity2") as TextBox;
                id = txtIDSave.Text;
                //FileUpload fuPhoto = GridView1.FooterRow.FindControl("fuNewPhoto") as FileUpload;
                //Guid FileName = Guid.NewGuid();
                //fuPhoto.SaveAs(Server.MapPath("~/Images/" + FileName + ".png"));
                DataRow dr = GlobalClass.dt.NewRow();
                dr["B_AssetLainnya_Inv"] = txtName2.Text.Trim();
                dr["B_AssetLainnya_Data"] = txtAge2.Text.Trim();
                dr["B_AssetLainnya_Actual"] = txtSalary2.Text.Trim();
                dr["B_AssetLainnya_Diff"] = txtCountry2.Text.Trim();
                dr["B_AssetLainnya_Remarks"] = txtCity2.Text.Trim();
                dr["IDProc_B"] = txtIDSave.Text.Trim();
                dr["Doe"] = doe.ToString();
                //dr["photopath"] = "~/Images/" + FileName + ".png";
                GlobalClass.dt.Rows.Add(dr);
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView2.ShowFooter = false;
                FillGridViewTemp();
            }
            catch
            {
                Response.Write("<script> alert('Record not added...') </script>");
            }
        }

        protected void updateRecordTemp(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                TextBox txtName2 = GridView2.Rows[e.RowIndex].FindControl("txtName2") as TextBox;
                TextBox txtAge2 = GridView2.Rows[e.RowIndex].FindControl("txtAge2") as TextBox;
                TextBox txtSalary2 = GridView2.Rows[e.RowIndex].FindControl("txtSalary2") as TextBox;
                TextBox txtCountry2 = GridView2.Rows[e.RowIndex].FindControl("txtCountry2") as TextBox;
                TextBox txtCity2 = GridView2.Rows[e.RowIndex].FindControl("txtCity2") as TextBox;
                //FileUpload fuPhoto = GridView1.Rows[e.RowIndex].FindControl("fuPhoto") as FileUpload;
                //Guid FileName = Guid.NewGuid();
                //if (fuPhoto.FileName != "")
                //{
                //    fuPhoto.SaveAs(Server.MapPath("~/Images/" + FileName + ".png"));
                //    GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["photopath"] = "~/Images/" + FileName + ".png";
                //    File.Delete(Server.MapPath(GlobalClass.imgEditPath));
                //}
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Inv"] = txtName2.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Data"] = Convert.ToInt32(txtAge2.Text.Trim());
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Actual"] = Convert.ToInt32(txtSalary2.Text.Trim());
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Diff"] = txtCountry2.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["B_AssetLainnya_Remarks"] = txtCity2.Text.Trim();
                GlobalClass.adap.Update(GlobalClass.dt);
                GridView2.EditIndex = -1;
                FillGridViewTemp();
            }
            catch
            {
                Response.Write("<script> alert('Record updation fail...') </script>");
            }
        }

        protected void RowDeletingTemp(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex].Delete();
                GlobalClass.adap.Update(GlobalClass.dt);
                //Image imgPhoto = GridView1.Rows[e.RowIndex].FindControl("imgPhoto") as Image;
                //File.Delete(Server.MapPath(imgPhoto.ImageUrl));
                FillGridViewTemp();
            }
            catch
            {
                Response.Write("<script> alert('Record not deleted...') </script>");
            }
        }
    }
}