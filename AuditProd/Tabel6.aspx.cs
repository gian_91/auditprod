﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using AuditProd.DAL;
using System.Globalization;

namespace AuditProd
{
    public partial class Tabel6 : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        string id = string.Empty;
        string id2 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!string.IsNullOrEmpty((string)Session["Id_LHP"]))
            {
                TxbId.Text = Session["Id_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id = TxbId.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id2_LHP"]))
            {
                TxbId2.Text = Session["Id2_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id2 = TxbId2.Text;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            BindGrid();

        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            Session["Id_LHP"] = null;
            Session["Id2_LHP"] = null;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        public void BindGrid()
        {
            if (!(string.IsNullOrEmpty(id)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel6 where ID_LHK_Collection={0}", id);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
                }
            }
            else if (!(string.IsNullOrEmpty(id2)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel6 where ID_LHP_Collection={0}", id2);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //Getddl1edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                TxtAccountIdEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text).ToString();
                TxtClientNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                TxtCenterNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                TxtDueDaysEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                TxtDisburseEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                TxtInstallmentEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                TxtTotalStickerEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                //ddlsmplingEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[11].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            //decimal amont = Convert.ToDecimal(string.IsNullOrEmpty(TxtAmountAdd.Text) ? "0" : TxtAmountAdd.Text);
            int No = Convert.ToInt32(lblCountryCode.Text);
            decimal rfb = Convert.ToDecimal(string.IsNullOrEmpty(TxtInstallmentEdit.Text) ? "0" : TxtInstallmentEdit.Text); //TxtInstallmentEdit.Text;
            decimal rfm = Convert.ToDecimal(string.IsNullOrEmpty(TxtTotalStickerEdit.Text) ? "0" : TxtTotalStickerEdit.Text); //TxtTotalStickerEdit.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string ddl = ddlsmplingEdit.SelectedItem.Text;
            executeUpdate(No, rfb, rfm, dt, ddl);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, decimal rfb, decimal rfm, string dt, string ddl)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveTabel6Edit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@indyear", rfb);
                updateCmd.Parameters.AddWithValue("@rem", rfm);
                updateCmd.Parameters.AddWithValue("@doe", dt);
                updateCmd.Parameters.AddWithValue("@ddl", ddl);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_B = id2;
            string dd = TxtDueDaysAdd.Text;

            if (string.IsNullOrEmpty(IDProc_B))
            {
                IDProc_B = null;
            }
            if (string.IsNullOrEmpty(dd))
            {
                dd = null;
            }

            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string Headdb = txbUsername.Text;

            if (!(string.IsNullOrEmpty(IDProc_B)))
            {
                try
                {
                    //decimal amont = Convert.ToDecimal(string.IsNullOrEmpty(TxtAmountAdd.Text) ? "0" : TxtAmountAdd.Text);
                    string idlhk2 = IDProc_B;
                    string code = TxtAccountIdAdd.Text;
                    string mod = "LHP_Collection";
                    string name = TxtClientNameAdd.Text;
                    string region = TxtCenterNameAdd.Text;
                    string continent = dd;
                    string population = TxtDisburseAdd.Text;
                    decimal indyear = Convert.ToDecimal(string.IsNullOrEmpty(TxtInstallmentAdd.Text) ? "0" : TxtInstallmentAdd.Text);
                    decimal rem = Convert.ToDecimal(string.IsNullOrEmpty(TxtTotalStickerAdd.Text) ? "0" : TxtTotalStickerAdd.Text);  //TxtTotalStickerAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, code, mod, name, region, continent, population, indyear, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }
        }

        private void executeAdd(string idlhk2, string code, string mod, string name, string region, string continent, string population, decimal indyear, decimal rem, string samp,
            string dt, string Headdbs)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveTabel6", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk2", idlhk2 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", dt);
                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@rem", rem);
                addCmd.Parameters.AddWithValue("@samp", samp);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_LHP_Tabel6 where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}