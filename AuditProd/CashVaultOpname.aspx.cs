﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using AuditProd.DAL;
using ClosedXML.Excel;

namespace AuditProd
{
    public partial class CashVaultOpname : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TxbModul.Text = "CashVaultOpname";
            if (!IsPostBack)
            {
                this.TxbTanggalHeadDibuat.Text = DateTime.Today.ToString("dd/MM/yyyy");
                TxbDibuatHead.Text = this.Page.User.Identity.Name;
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                if (Session["BranchID"] != null)
                    TxbDilksnakan.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {
                    LoadSaveID();
                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {
                    LoadSaveIDTemp();
                }
            }
            else
            {
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

            }

        }

        object ToDBValue(string str)
        {
            return string.IsNullOrEmpty(str) ? DBNull.Value : (object)str;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                SaveId();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)) && (string.IsNullOrEmpty(TxtCreateDate.Text)) && (string.IsNullOrEmpty(TxtReviewDate.Text)))
            {
                SavedReview();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                SaveIdTemp();
            }
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("CashVault.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("CashVault.aspx?ID=" + txtIDSaveTemp.Text);
            }

        }



        protected void txbJumlah100rb_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah100rb.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("100000", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal100rb.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal100rb.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah50rb_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah50rb.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("50000", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal50rb.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal50rb.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));    
            txbTotalUangFisik.Text = total2.ToString();


        }

        protected void txbJumlah20rb_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah20rb.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("20000", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal20rb.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal20rb.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah10rb_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah10rb.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("10000", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal10rb.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal10rb.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah5rb_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah5rb.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("5000", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal5rb.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal5rb.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString())); 
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah2rb_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah2rb.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("2000", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal2rb.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal2rb.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah1rb_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah1rb.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("1000", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal1rb.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal1rb.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString())); 
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah5rts_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah5rts.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("500", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal5rts.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal5rts.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah2rts_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah2rts.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("200", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal2rts.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal2rts.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah1rts_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah1rts.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("100", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal1rts.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal1rts.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah50_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah50.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("50", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal25.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal50.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal50.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void txbJumlah25_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, txt9Value, txt10Value, txt11Value, txt12Value, txt13Value;
            total = 0;
            decimal.TryParse(txbJumlah25.Text, NumberStyles.Currency, null, out txt1Value);
            decimal.TryParse("25", NumberStyles.Currency, null, out txt2Value);
            decimal.TryParse(txbTotal100rb.Text, NumberStyles.Currency, null, out txt3Value);
            decimal.TryParse(txbTotal50rb.Text, NumberStyles.Currency, null, out txt4Value);
            decimal.TryParse(txbTotal20rb.Text, NumberStyles.Currency, null, out txt5Value);
            decimal.TryParse(txbTotal10rb.Text, NumberStyles.Currency, null, out txt6Value);
            decimal.TryParse(txbTotal5rb.Text, NumberStyles.Currency, null, out txt7Value);
            decimal.TryParse(txbTotal2rb.Text, NumberStyles.Currency, null, out txt8Value);
            decimal.TryParse(txbTotal1rb.Text, NumberStyles.Currency, null, out txt9Value);
            decimal.TryParse(txbTotal5rts.Text, NumberStyles.Currency, null, out txt10Value);
            decimal.TryParse(txbTotal2rts.Text, NumberStyles.Currency, null, out txt11Value);
            decimal.TryParse(txbTotal1rts.Text, NumberStyles.Currency, null, out txt12Value);
            decimal.TryParse(txbTotal50.Text, NumberStyles.Currency, null, out txt13Value);
            total = txt1Value * txt2Value;
            //txbTotal25.Text = string.Format("{0:#,##0.00}", decimal.Parse(total.ToString()));
            txbTotal25.Text = total.ToString();

            decimal total2 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value + txt9Value + txt10Value + txt11Value + txt12Value + txt13Value;
            //txbTotalUangFisik.Text = string.Format("{0:#,##0.00}", decimal.Parse(total2.ToString()));
            txbTotalUangFisik.Text = total2.ToString();

        }

        protected void TxbCollProjc_TextChanged1(object sender, EventArgs e)
        {
            var txt2Value = TxbCollProjc.Text;
            decimal totResult2;
            decimal txt1Value, total;
            total = 0;
            decimal.TryParse(txbTotalUangFisik.Text, out txt1Value);
            decimal.TryParse(TxbVariance.Text, out total);
            decimal.TryParse(txt2Value, NumberStyles.Currency, CultureInfo.InvariantCulture, out totResult2);
            total = txt1Value - totResult2;
            TxbVariance.Text = total.ToString();

            //decimal txt1Value, txt2Value, total;
            //total = 0;
            //decimal.TryParse(TxbTotalSlip.Text, out txt1Value);
            //decimal.TryParse(TxbNTTotal.Text, out txt2Value);
            //decimal.TryParse(TxbNTVariance.Text, out total);

            //total = txt1Value - txt2Value;
        }



        protected void TxbJmlhPreciosure_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value;
            total = 0;
            decimal.TryParse(TxbJmlhPreciosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbJmlhPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbJmlhWritOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbJmlhUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbSelisih.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);


            total = txt1Value + txt2Value + txt3Value + txt4Value;
            TxbSelisih.Text = total.ToString();


        }

        protected void TxbJmlhPartial_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value;
            total = 0;
            decimal.TryParse(TxbJmlhPreciosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbJmlhPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbJmlhWritOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbJmlhUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbSelisih.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            //decimal.TryParse(TxbJmlhVarc.Text, out txt5Value);

            //total = txt2Value;
            //TxbJmlhPartial.Text = total.ToString();

            total = txt2Value + txt1Value + txt3Value + txt4Value;
            TxbSelisih.Text = total.ToString();
            //TxbSelisih.Text = string.Format(CultureInfo.CreateSpecificCulture("en-ID"), "{0:C2}", total1);

            //var total3 = total1 * txt5Value;
            //TxbSelisih.Text = total3.ToString();
        }

        protected void TxbJmlhWritOff_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value;
            total = 0;
            decimal.TryParse(TxbJmlhPreciosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbJmlhPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbJmlhWritOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbJmlhUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbSelisih.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            //decimal.TryParse(TxbJmlhVarc.Text, out txt5Value);

            //total = txt3Value;
            //TxbJmlhWritOff.Text = total.ToString();

            total = txt3Value + txt2Value + txt1Value + txt4Value;
            TxbSelisih.Text = total.ToString();
            //TxbSelisih.Text = string.Format(CultureInfo.CreateSpecificCulture("en-ID"), "{0:C2}", total1);

            //var total3 = total1 * txt5Value;
            //TxbSelisih.Text = total3.ToString();
        }

        protected void TxbJmlhUjam_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value;
            total = 0;
            decimal.TryParse(TxbJmlhPreciosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbJmlhPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbJmlhWritOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbJmlhUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbSelisih.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            //decimal.TryParse(TxbJmlhVarc.Text, out txt5Value);

            //total = txt4Value;
            //TxbJmlhUjam.Text = total.ToString();

            total = txt1Value + txt2Value + txt3Value + txt1Value;
            TxbSelisih.Text = total.ToString();
            //TxbSelisih.Text = string.Format(CultureInfo.CreateSpecificCulture("en-ID"), "{0:C2}", total1);

            //var total3 = total1 * txt5Value;
            //TxbSelisih.Text = total3.ToString();
        }



        private void LoadSaveID()
        {
            //SqlDataReader reader;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [IDCVO],[Header_Dibuat],[Header_Dibuat_Tngl],[Bina_Artha_Cabang],[Periode_Audit_Start],[Periode_Audit_End],[N_Jmlh_100rb],[N_Jmlh_50rb],[N_Jmlh_20rb],[N_Jmlh_10rb]," +
            "[N_Jmlh_5rb],[N_Jmlh_2rb],[N_Jmlh_1rb],[N_Jmlh_5rts],[N_Jmlh_2rts],[N_Jmlh_1rts],[N_Jmlh_50],[N_Jmlh_25],[N_Total_100rb],[N_Total_50rb],[N_Total_20rb],[N_Total_10rb],[N_Total_5rb]," +
            "[N_Total_2rb],[N_Total_1rb],[N_Total_5rts],[N_Total_2rts],[N_Total_1rts],[N_Total_50],[N_Total_25],[N_Total_Fisik_Uang],[N_Total_Col_Proj],[N_Total_Col_Tngl],[N_Total_Variance]," +
            "[PV_Preciosure_Jmlh],[PV_Partial_Jmlh],[PV_WriteOff_Jmlh],[PV_Ujam_Jmlh],[PV_Preciosure_NT],[PV_Partial_NT],[PV_WriteOff_NT],[PV_Ujam_NT],[PV_Total_Jmlh],[PV_Total_NT],[PV_Total_Selisih]," +
            "[Remarks_Selisih],[Footer_Dilaksanakan],[Footer_Tngl],[Footer_Jam],[Doe] FROM CashVaultOpname where IDCVO = @IdDoctest ", con);
            cmd.Parameters.AddWithValue("@IdDoctest", txtIDSave.Text);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(reader["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(reader["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(reader["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(reader["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(reader["Periode_Audit_End"]);

                txbJumlah100rb.Text = Convert.ToString(reader["N_Jmlh_100rb"]);
                txbJumlah50rb.Text = Convert.ToString(reader["N_Jmlh_50rb"]);
                txbJumlah20rb.Text = Convert.ToString(reader["N_Jmlh_20rb"]);
                txbJumlah10rb.Text = Convert.ToString(reader["N_Jmlh_10rb"]);
                txbJumlah5rb.Text = Convert.ToString(reader["N_Jmlh_5rb"]);
                txbJumlah2rb.Text = Convert.ToString(reader["N_Jmlh_2rb"]);
                txbJumlah1rb.Text = Convert.ToString(reader["N_Jmlh_1rb"]);
                txbJumlah5rts.Text = Convert.ToString(reader["N_Jmlh_5rts"]);
                txbJumlah2rts.Text = Convert.ToString(reader["N_Jmlh_2rts"]);
                txbJumlah1rts.Text = Convert.ToString(reader["N_Jmlh_1rts"]);
                txbJumlah50.Text = Convert.ToString(reader["N_Jmlh_50"]);
                txbJumlah25.Text = Convert.ToString(reader["N_Jmlh_25"]);

                txbTotal100rb.Text = Convert.ToString(reader["N_Total_100rb"]);
                txbTotal50rb.Text = Convert.ToString(reader["N_Total_50rb"]);
                txbTotal20rb.Text = Convert.ToString(reader["N_Total_20rb"]);
                txbTotal10rb.Text = Convert.ToString(reader["N_Total_10rb"]);
                txbTotal5rb.Text = Convert.ToString(reader["N_Total_5rb"]);
                txbTotal2rb.Text = Convert.ToString(reader["N_Total_2rb"]);
                txbTotal1rb.Text = Convert.ToString(reader["N_Total_1rb"]);
                txbTotal5rts.Text = Convert.ToString(reader["N_Total_5rts"]);
                txbTotal2rts.Text = Convert.ToString(reader["N_Total_2rts"]);
                txbTotal1rts.Text = Convert.ToString(reader["N_Total_1rts"]);
                txbTotal50.Text = Convert.ToString(reader["N_Total_50"]);
                txbTotal25.Text = Convert.ToString(reader["N_Total_25"]);

                txbTotalUangFisik.Text = Convert.ToString(reader["N_Total_Fisik_Uang"]);
                txbDateCol.Text = Convert.ToString(reader["N_Total_Col_Tngl"]);
                TxbCollProjc.Text = Convert.ToString(reader["N_Total_Col_Proj"]);
                TxbVariance.Text = Convert.ToString(reader["N_Total_Variance"]);

                TxbPreciosure.Text = Convert.ToString(reader["PV_Preciosure_Jmlh"]);
                TxbPartial.Text = Convert.ToString(reader["PV_Partial_Jmlh"]);
                TxbWritOff.Text = Convert.ToString(reader["PV_WriteOff_Jmlh"]);
                TxbUjam.Text = Convert.ToString(reader["PV_Ujam_Jmlh"]);

                TxbJmlhPreciosure.Text = Convert.ToString(reader["PV_Preciosure_NT"]);
                TxbJmlhPartial.Text = Convert.ToString(reader["PV_Partial_NT"]);
                TxbJmlhWritOff.Text = Convert.ToString(reader["PV_WriteOff_NT"]);
                TxbJmlhUjam.Text = Convert.ToString(reader["PV_Ujam_NT"]);
                TxbSelisih.Text = Convert.ToString(reader["PV_Total_Selisih"]);

                RemarksSelisih.InnerText = Convert.ToString(reader["Remarks_Selisih"]);

                TxbDilksnakan.Text = Convert.ToString(reader["Footer_Dilaksanakan"]);
                TxbTnglBwh.Text = Convert.ToString(reader["Footer_Tngl"]);
                TxbJamBawah.Text = Convert.ToString(reader["Footer_Jam"]);

            }
            con.Close();


        }

        private void LoadSaveIDTemp()
        {
            //SqlDataReader reader;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [IDCVO],[Header_Dibuat],[Header_Dibuat_Tngl],[Bina_Artha_Cabang],[Periode_Audit_Start],[Periode_Audit_End],[N_Jmlh_100rb],[N_Jmlh_50rb],[N_Jmlh_20rb],[N_Jmlh_10rb]," +
            "[N_Jmlh_5rb],[N_Jmlh_2rb],[N_Jmlh_1rb],[N_Jmlh_5rts],[N_Jmlh_2rts],[N_Jmlh_1rts],[N_Jmlh_50],[N_Jmlh_25],[N_Total_100rb],[N_Total_50rb],[N_Total_20rb],[N_Total_10rb],[N_Total_5rb]," +
            "[N_Total_2rb],[N_Total_1rb],[N_Total_5rts],[N_Total_2rts],[N_Total_1rts],[N_Total_50],[N_Total_25],[N_Total_Fisik_Uang],[N_Total_Col_Proj],[N_Total_Col_Tngl],[N_Total_Variance]," +
            "[PV_Preciosure_Jmlh],[PV_Partial_Jmlh],[PV_WriteOff_Jmlh],[PV_Ujam_Jmlh],[PV_Preciosure_NT],[PV_Partial_NT],[PV_WriteOff_NT],[PV_Ujam_NT],[PV_Total_Jmlh],[PV_Total_NT],[PV_Total_Selisih]," +
            "[Remarks_Selisih],[Footer_Dilaksanakan],[Footer_Tngl],[Footer_Jam],[Doe],[ReviewDate] FROM CashVaultOpname where IDCVO = @IdDoctest order by IDCVO ASC ", con);
            cmd.Parameters.AddWithValue("@IdDoctest", txtIDSaveTemp.Text);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(reader["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(reader["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(reader["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(reader["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(reader["Periode_Audit_End"]);
                txbJumlah100rb.Text = Convert.ToString(reader["N_Jmlh_100rb"]);
                txbJumlah50rb.Text = Convert.ToString(reader["N_Jmlh_50rb"]);
                txbJumlah20rb.Text = Convert.ToString(reader["N_Jmlh_20rb"]);
                txbJumlah10rb.Text = Convert.ToString(reader["N_Jmlh_10rb"]);
                txbJumlah5rb.Text = Convert.ToString(reader["N_Jmlh_5rb"]);
                txbJumlah2rb.Text = Convert.ToString(reader["N_Jmlh_2rb"]);
                txbJumlah1rb.Text = Convert.ToString(reader["N_Jmlh_1rb"]);
                txbJumlah5rts.Text = Convert.ToString(reader["N_Jmlh_5rts"]);
                txbJumlah2rts.Text = Convert.ToString(reader["N_Jmlh_2rts"]);
                txbJumlah1rts.Text = Convert.ToString(reader["N_Jmlh_1rts"]);
                txbJumlah50.Text = Convert.ToString(reader["N_Jmlh_50"]);
                txbJumlah25.Text = Convert.ToString(reader["N_Jmlh_25"]);
                txbTotal100rb.Text = Convert.ToString(reader["N_Total_100rb"]);
                txbTotal50rb.Text = Convert.ToString(reader["N_Total_50rb"]);
                txbTotal20rb.Text = Convert.ToString(reader["N_Total_20rb"]);
                txbTotal10rb.Text = Convert.ToString(reader["N_Total_10rb"]);
                txbTotal5rb.Text = Convert.ToString(reader["N_Total_5rb"]);
                txbTotal2rb.Text = Convert.ToString(reader["N_Total_2rb"]);
                txbTotal1rb.Text = Convert.ToString(reader["N_Total_1rb"]);
                txbTotal5rts.Text = Convert.ToString(reader["N_Total_5rts"]);
                txbTotal2rts.Text = Convert.ToString(reader["N_Total_2rts"]);
                txbTotal1rts.Text = Convert.ToString(reader["N_Total_1rts"]);
                txbTotal50.Text = Convert.ToString(reader["N_Total_50"]);
                txbTotal25.Text = Convert.ToString(reader["N_Total_25"]);
                txbTotalUangFisik.Text = Convert.ToString(reader["N_Total_Fisik_Uang"]);
                txbDateCol.Text = Convert.ToString(reader["N_Total_Col_Tngl"]);
                TxbCollProjc.Text = Convert.ToString(reader["N_Total_Col_Proj"]);
                TxbVariance.Text = Convert.ToString(reader["N_Total_Variance"]);
                TxbPreciosure.Text = Convert.ToString(reader["PV_Preciosure_Jmlh"]);
                TxbPartial.Text = Convert.ToString(reader["PV_Partial_Jmlh"]);
                TxbWritOff.Text = Convert.ToString(reader["PV_WriteOff_Jmlh"]);
                TxbUjam.Text = Convert.ToString(reader["PV_Ujam_Jmlh"]);
                TxbJmlhPreciosure.Text = Convert.ToString(reader["PV_Preciosure_NT"]);
                TxbJmlhPartial.Text = Convert.ToString(reader["PV_Partial_NT"]);
                TxbJmlhWritOff.Text = Convert.ToString(reader["PV_WriteOff_NT"]);
                TxbJmlhUjam.Text = Convert.ToString(reader["PV_Ujam_NT"]);
                TxbSelisih.Text = Convert.ToString(reader["PV_Total_Selisih"]);
                RemarksSelisih.InnerText = Convert.ToString(reader["Remarks_Selisih"]);
                TxbDilksnakan.Text = Convert.ToString(reader["Footer_Dilaksanakan"]);
                TxbTnglBwh.Text = Convert.ToString(reader["Footer_Tngl"]);
                TxbJamBawah.Text = Convert.ToString(reader["Footer_Jam"]);
                TxtCreateDate.Text = Convert.ToString(reader["Doe"]);
                TxtReviewDate.Text = Convert.ToString(reader["ReviewDate"]);
            }
            con.Close();
        }

        private void SaveId()
        {
            string message = string.Empty;

            var tot = TxbVariance.Text;
            decimal totResult;
            decimal.TryParse(tot, out totResult);

            var vat = TxbSelisih.Text;
            decimal vatResult;
            decimal.TryParse(vat, out vatResult);

            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SaveCVO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDCVO", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;
                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_100rb", string.IsNullOrEmpty(txbJumlah100rb.Text) ? (object)DBNull.Value : txbJumlah100rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_50rb", string.IsNullOrEmpty(txbJumlah50rb.Text) ? (object)DBNull.Value : txbJumlah50rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_20rb", string.IsNullOrEmpty(txbJumlah20rb.Text) ? (object)DBNull.Value : txbJumlah20rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_10rb", string.IsNullOrEmpty(txbJumlah10rb.Text) ? (object)DBNull.Value : txbJumlah10rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_5rb", string.IsNullOrEmpty(txbJumlah5rb.Text) ? (object)DBNull.Value : txbJumlah5rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_2rb", string.IsNullOrEmpty(txbJumlah2rb.Text) ? (object)DBNull.Value : txbJumlah2rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_1rb", string.IsNullOrEmpty(txbJumlah1rb.Text) ? (object)DBNull.Value : txbJumlah1rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_5rts", string.IsNullOrEmpty(txbJumlah5rts.Text) ? (object)DBNull.Value : txbJumlah5rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_2rts", string.IsNullOrEmpty(txbJumlah2rts.Text) ? (object)DBNull.Value : txbJumlah2rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_1rts", string.IsNullOrEmpty(txbJumlah1rts.Text) ? (object)DBNull.Value : txbJumlah1rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_50", string.IsNullOrEmpty(txbJumlah50.Text) ? (object)DBNull.Value : txbJumlah50.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_25", string.IsNullOrEmpty(txbJumlah25.Text) ? (object)DBNull.Value : txbJumlah25.Text);
                    cmd.Parameters.AddWithValue("@N_Total_100rb", string.IsNullOrEmpty(txbTotal100rb.Text) ? (object)DBNull.Value : txbTotal100rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_50rb", string.IsNullOrEmpty(txbTotal50rb.Text) ? (object)DBNull.Value : txbTotal50rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_20rb", string.IsNullOrEmpty(txbTotal20rb.Text) ? (object)DBNull.Value : txbTotal20rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_10rb", string.IsNullOrEmpty(txbTotal10rb.Text) ? (object)DBNull.Value : txbTotal10rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_5rb", string.IsNullOrEmpty(txbTotal5rb.Text) ? (object)DBNull.Value : txbTotal5rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_2rb", string.IsNullOrEmpty(txbTotal2rb.Text) ? (object)DBNull.Value : txbTotal2rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_1rb", string.IsNullOrEmpty(txbTotal1rb.Text) ? (object)DBNull.Value : txbTotal1rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_5rts", string.IsNullOrEmpty(txbTotal5rts.Text) ? (object)DBNull.Value : txbTotal5rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_2rts", string.IsNullOrEmpty(txbTotal2rts.Text) ? (object)DBNull.Value : txbTotal2rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_1rts", string.IsNullOrEmpty(txbTotal1rts.Text) ? (object)DBNull.Value : txbTotal1rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_50", string.IsNullOrEmpty(txbTotal50.Text) ? (object)DBNull.Value : txbTotal50.Text);
                    cmd.Parameters.AddWithValue("@N_Total_25", string.IsNullOrEmpty(txbTotal25.Text) ? (object)DBNull.Value : txbTotal25.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Fisik_Uang", string.IsNullOrEmpty(txbTotalUangFisik.Text) ? (object)DBNull.Value : txbTotalUangFisik.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Col_Proj", string.IsNullOrEmpty(TxbCollProjc.Text) ? (object)DBNull.Value : TxbCollProjc.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Col_Tngl", string.IsNullOrEmpty(txbDateCol.Text) ? (object)DBNull.Value : txbDateCol.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Variance", totResult);
                    cmd.Parameters.AddWithValue("@PV_Preciosure_Jmlh", string.IsNullOrEmpty(TxbPreciosure.Text) ? (object)DBNull.Value : TxbPreciosure.Text);
                    cmd.Parameters.AddWithValue("@PV_Partial_Jmlh", string.IsNullOrEmpty(TxbPartial.Text) ? (object)DBNull.Value : TxbPartial.Text);
                    cmd.Parameters.AddWithValue("@PV_WriteOff_Jmlh", string.IsNullOrEmpty(TxbWritOff.Text) ? (object)DBNull.Value : TxbWritOff.Text);
                    cmd.Parameters.AddWithValue("@PV_Ujam_Jmlh", string.IsNullOrEmpty(TxbUjam.Text) ? (object)DBNull.Value : TxbUjam.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_Jmlh", string.IsNullOrEmpty(TxbJmlhVarc.Text) ? (object)DBNull.Value : TxbJmlhVarc.Text);
                    cmd.Parameters.AddWithValue("@PV_Preciosure_NT", string.IsNullOrEmpty(TxbJmlhPreciosure.Text) ? (object)DBNull.Value : TxbJmlhPreciosure.Text);
                    cmd.Parameters.AddWithValue("@PV_Partial_NT", string.IsNullOrEmpty(TxbJmlhPartial.Text) ? (object)DBNull.Value : TxbJmlhPartial.Text);
                    cmd.Parameters.AddWithValue("@PV_WriteOff_NT", string.IsNullOrEmpty(TxbJmlhWritOff.Text) ? (object)DBNull.Value : TxbJmlhWritOff.Text);
                    cmd.Parameters.AddWithValue("@PV_Ujam_NT", string.IsNullOrEmpty(TxbJmlhUjam.Text) ? (object)DBNull.Value : TxbJmlhUjam.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_NT", string.IsNullOrEmpty(TxbNilaiTotalPnjVarc.Text) ? (object)DBNull.Value : TxbNilaiTotalPnjVarc.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_Selisih", vatResult);
                    cmd.Parameters.AddWithValue("@Remarks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilksnakan.Text) ? (object)DBNull.Value : TxbDilksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbJamBawah.Text) ? (object)DBNull.Value : TxbJamBawah.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;

                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }

        private void SavedReview()
        {
            string message = string.Empty;

            var tot = TxbVariance.Text;
            decimal totResult;
            decimal.TryParse(tot, out totResult);

            var vat = TxbSelisih.Text;
            decimal vatResult;
            decimal.TryParse(vat, out vatResult);

            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SaveCVO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDCVO", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;
                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_100rb", string.IsNullOrEmpty(txbJumlah100rb.Text) ? (object)DBNull.Value : txbJumlah100rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_50rb", string.IsNullOrEmpty(txbJumlah50rb.Text) ? (object)DBNull.Value : txbJumlah50rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_20rb", string.IsNullOrEmpty(txbJumlah20rb.Text) ? (object)DBNull.Value : txbJumlah20rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_10rb", string.IsNullOrEmpty(txbJumlah10rb.Text) ? (object)DBNull.Value : txbJumlah10rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_5rb", string.IsNullOrEmpty(txbJumlah5rb.Text) ? (object)DBNull.Value : txbJumlah5rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_2rb", string.IsNullOrEmpty(txbJumlah2rb.Text) ? (object)DBNull.Value : txbJumlah2rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_1rb", string.IsNullOrEmpty(txbJumlah1rb.Text) ? (object)DBNull.Value : txbJumlah1rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_5rts", string.IsNullOrEmpty(txbJumlah5rts.Text) ? (object)DBNull.Value : txbJumlah5rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_2rts", string.IsNullOrEmpty(txbJumlah2rts.Text) ? (object)DBNull.Value : txbJumlah2rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_1rts", string.IsNullOrEmpty(txbJumlah1rts.Text) ? (object)DBNull.Value : txbJumlah1rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_50", string.IsNullOrEmpty(txbJumlah50.Text) ? (object)DBNull.Value : txbJumlah50.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_25", string.IsNullOrEmpty(txbJumlah25.Text) ? (object)DBNull.Value : txbJumlah25.Text);
                    cmd.Parameters.AddWithValue("@N_Total_100rb", string.IsNullOrEmpty(txbTotal100rb.Text) ? (object)DBNull.Value : txbTotal100rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_50rb", string.IsNullOrEmpty(txbTotal50rb.Text) ? (object)DBNull.Value : txbTotal50rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_20rb", string.IsNullOrEmpty(txbTotal20rb.Text) ? (object)DBNull.Value : txbTotal20rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_10rb", string.IsNullOrEmpty(txbTotal10rb.Text) ? (object)DBNull.Value : txbTotal10rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_5rb", string.IsNullOrEmpty(txbTotal5rb.Text) ? (object)DBNull.Value : txbTotal5rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_2rb", string.IsNullOrEmpty(txbTotal2rb.Text) ? (object)DBNull.Value : txbTotal2rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_1rb", string.IsNullOrEmpty(txbTotal1rb.Text) ? (object)DBNull.Value : txbTotal1rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_5rts", string.IsNullOrEmpty(txbTotal5rts.Text) ? (object)DBNull.Value : txbTotal5rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_2rts", string.IsNullOrEmpty(txbTotal2rts.Text) ? (object)DBNull.Value : txbTotal2rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_1rts", string.IsNullOrEmpty(txbTotal1rts.Text) ? (object)DBNull.Value : txbTotal1rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_50", string.IsNullOrEmpty(txbTotal50.Text) ? (object)DBNull.Value : txbTotal50.Text);
                    cmd.Parameters.AddWithValue("@N_Total_25", string.IsNullOrEmpty(txbTotal25.Text) ? (object)DBNull.Value : txbTotal25.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Fisik_Uang", string.IsNullOrEmpty(txbTotalUangFisik.Text) ? (object)DBNull.Value : txbTotalUangFisik.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Col_Proj", string.IsNullOrEmpty(TxbCollProjc.Text) ? (object)DBNull.Value : TxbCollProjc.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Col_Tngl", string.IsNullOrEmpty(txbDateCol.Text) ? (object)DBNull.Value : txbDateCol.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Variance", totResult);
                    cmd.Parameters.AddWithValue("@PV_Preciosure_Jmlh", string.IsNullOrEmpty(TxbPreciosure.Text) ? (object)DBNull.Value : TxbPreciosure.Text);
                    cmd.Parameters.AddWithValue("@PV_Partial_Jmlh", string.IsNullOrEmpty(TxbPartial.Text) ? (object)DBNull.Value : TxbPartial.Text);
                    cmd.Parameters.AddWithValue("@PV_WriteOff_Jmlh", string.IsNullOrEmpty(TxbWritOff.Text) ? (object)DBNull.Value : TxbWritOff.Text);
                    cmd.Parameters.AddWithValue("@PV_Ujam_Jmlh", string.IsNullOrEmpty(TxbUjam.Text) ? (object)DBNull.Value : TxbUjam.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_Jmlh", string.IsNullOrEmpty(TxbJmlhVarc.Text) ? (object)DBNull.Value : TxbJmlhVarc.Text);
                    cmd.Parameters.AddWithValue("@PV_Preciosure_NT", string.IsNullOrEmpty(TxbJmlhPreciosure.Text) ? (object)DBNull.Value : TxbJmlhPreciosure.Text);
                    cmd.Parameters.AddWithValue("@PV_Partial_NT", string.IsNullOrEmpty(TxbJmlhPartial.Text) ? (object)DBNull.Value : TxbJmlhPartial.Text);
                    cmd.Parameters.AddWithValue("@PV_WriteOff_NT", string.IsNullOrEmpty(TxbJmlhWritOff.Text) ? (object)DBNull.Value : TxbJmlhWritOff.Text);
                    cmd.Parameters.AddWithValue("@PV_Ujam_NT", string.IsNullOrEmpty(TxbJmlhUjam.Text) ? (object)DBNull.Value : TxbJmlhUjam.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_NT", string.IsNullOrEmpty(TxbNilaiTotalPnjVarc.Text) ? (object)DBNull.Value : TxbNilaiTotalPnjVarc.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_Selisih", vatResult);
                    cmd.Parameters.AddWithValue("@Remarks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilksnakan.Text) ? (object)DBNull.Value : TxbDilksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbJamBawah.Text) ? (object)DBNull.Value : TxbJamBawah.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;

                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }

        private void SaveIdTemp()
        {
            string message = string.Empty;

            var tot = TxbVariance.Text;
            decimal totResult;
            decimal.TryParse(tot, out totResult);

            var vat = TxbSelisih.Text;
            decimal vatResult;
            decimal.TryParse(vat, out vatResult);

            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SaveCVO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDCVO", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;
                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_100rb", string.IsNullOrEmpty(txbJumlah100rb.Text) ? (object)DBNull.Value : txbJumlah100rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_50rb", string.IsNullOrEmpty(txbJumlah50rb.Text) ? (object)DBNull.Value : txbJumlah50rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_20rb", string.IsNullOrEmpty(txbJumlah20rb.Text) ? (object)DBNull.Value : txbJumlah20rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_10rb", string.IsNullOrEmpty(txbJumlah10rb.Text) ? (object)DBNull.Value : txbJumlah10rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_5rb", string.IsNullOrEmpty(txbJumlah5rb.Text) ? (object)DBNull.Value : txbJumlah5rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_2rb", string.IsNullOrEmpty(txbJumlah2rb.Text) ? (object)DBNull.Value : txbJumlah2rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_1rb", string.IsNullOrEmpty(txbJumlah1rb.Text) ? (object)DBNull.Value : txbJumlah1rb.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_5rts", string.IsNullOrEmpty(txbJumlah5rts.Text) ? (object)DBNull.Value : txbJumlah5rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_2rts", string.IsNullOrEmpty(txbJumlah2rts.Text) ? (object)DBNull.Value : txbJumlah2rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_1rts", string.IsNullOrEmpty(txbJumlah1rts.Text) ? (object)DBNull.Value : txbJumlah1rts.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_50", string.IsNullOrEmpty(txbJumlah50.Text) ? (object)DBNull.Value : txbJumlah50.Text);
                    cmd.Parameters.AddWithValue("@N_Jmlh_25", string.IsNullOrEmpty(txbJumlah25.Text) ? (object)DBNull.Value : txbJumlah25.Text);
                    cmd.Parameters.AddWithValue("@N_Total_100rb", string.IsNullOrEmpty(txbTotal100rb.Text) ? (object)DBNull.Value : txbTotal100rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_50rb", string.IsNullOrEmpty(txbTotal50rb.Text) ? (object)DBNull.Value : txbTotal50rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_20rb", string.IsNullOrEmpty(txbTotal20rb.Text) ? (object)DBNull.Value : txbTotal20rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_10rb", string.IsNullOrEmpty(txbTotal10rb.Text) ? (object)DBNull.Value : txbTotal10rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_5rb", string.IsNullOrEmpty(txbTotal5rb.Text) ? (object)DBNull.Value : txbTotal5rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_2rb", string.IsNullOrEmpty(txbTotal2rb.Text) ? (object)DBNull.Value : txbTotal2rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_1rb", string.IsNullOrEmpty(txbTotal1rb.Text) ? (object)DBNull.Value : txbTotal1rb.Text);
                    cmd.Parameters.AddWithValue("@N_Total_5rts", string.IsNullOrEmpty(txbTotal5rts.Text) ? (object)DBNull.Value : txbTotal5rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_2rts", string.IsNullOrEmpty(txbTotal2rts.Text) ? (object)DBNull.Value : txbTotal2rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_1rts", string.IsNullOrEmpty(txbTotal1rts.Text) ? (object)DBNull.Value : txbTotal1rts.Text);
                    cmd.Parameters.AddWithValue("@N_Total_50", string.IsNullOrEmpty(txbTotal50.Text) ? (object)DBNull.Value : txbTotal50.Text);
                    cmd.Parameters.AddWithValue("@N_Total_25", string.IsNullOrEmpty(txbTotal25.Text) ? (object)DBNull.Value : txbTotal25.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Fisik_Uang", string.IsNullOrEmpty(txbTotalUangFisik.Text) ? (object)DBNull.Value : txbTotalUangFisik.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Col_Proj", string.IsNullOrEmpty(TxbCollProjc.Text) ? (object)DBNull.Value : TxbCollProjc.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Col_Tngl", string.IsNullOrEmpty(txbDateCol.Text) ? (object)DBNull.Value : txbDateCol.Text);
                    cmd.Parameters.AddWithValue("@N_Total_Variance", totResult);
                    cmd.Parameters.AddWithValue("@PV_Preciosure_Jmlh", string.IsNullOrEmpty(TxbPreciosure.Text) ? (object)DBNull.Value : TxbPreciosure.Text);
                    cmd.Parameters.AddWithValue("@PV_Partial_Jmlh", string.IsNullOrEmpty(TxbPartial.Text) ? (object)DBNull.Value : TxbPartial.Text);
                    cmd.Parameters.AddWithValue("@PV_WriteOff_Jmlh", string.IsNullOrEmpty(TxbWritOff.Text) ? (object)DBNull.Value : TxbWritOff.Text);
                    cmd.Parameters.AddWithValue("@PV_Ujam_Jmlh", string.IsNullOrEmpty(TxbUjam.Text) ? (object)DBNull.Value : TxbUjam.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_Jmlh", string.IsNullOrEmpty(TxbJmlhVarc.Text) ? (object)DBNull.Value : TxbJmlhVarc.Text);
                    cmd.Parameters.AddWithValue("@PV_Preciosure_NT", string.IsNullOrEmpty(TxbJmlhPreciosure.Text) ? (object)DBNull.Value : TxbJmlhPreciosure.Text);
                    cmd.Parameters.AddWithValue("@PV_Partial_NT", string.IsNullOrEmpty(TxbJmlhPartial.Text) ? (object)DBNull.Value : TxbJmlhPartial.Text);
                    cmd.Parameters.AddWithValue("@PV_WriteOff_NT", string.IsNullOrEmpty(TxbJmlhWritOff.Text) ? (object)DBNull.Value : TxbJmlhWritOff.Text);
                    cmd.Parameters.AddWithValue("@PV_Ujam_NT", string.IsNullOrEmpty(TxbJmlhUjam.Text) ? (object)DBNull.Value : TxbJmlhUjam.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_NT", string.IsNullOrEmpty(TxbNilaiTotalPnjVarc.Text) ? (object)DBNull.Value : TxbNilaiTotalPnjVarc.Text);
                    cmd.Parameters.AddWithValue("@PV_Total_Selisih", vatResult);
                    cmd.Parameters.AddWithValue("@Remarks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilksnakan.Text) ? (object)DBNull.Value : TxbDilksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbJamBawah.Text) ? (object)DBNull.Value : TxbJamBawah.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DBNull.Value;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;

                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }
    }
}