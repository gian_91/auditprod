﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using AuditProd.DAL;

namespace AuditProd
{
    public partial class Tabel1 : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        string id = string.Empty;
        string id2 = string.Empty;
        string id_lhp = string.Empty;
        string id2_lhp = string.Empty;
        string id4_lhp = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!string.IsNullOrEmpty((string)Session["Id"]))
            {
                TxbId.Text = Session["Id"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                id = TxbId.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id2"]))
            {
                TxbId2.Text = Session["Id2"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                id2 = TxbId2.Text;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            GetHeader();
            BindGrid();

        }

        //[WebMethod]
        //public static List<string> GetEmpNames(string empName)
        //{
        //    List<string> Emp = new List<string>();
        //    String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        //    SqlConnection con = new SqlConnection(strConnString);
        //    string query = string.Format("select AccountID from RawDataCurrent where AccountID LIKE '{0}%'", empName);
        //    {
        //        using (SqlCommand cmd = new SqlCommand(query, con))
        //        {
        //            con.Open();
        //            SqlDataReader reader = cmd.ExecuteReader();
        //            while (reader.Read())
        //            {
        //                Emp.Add(reader.GetString(0));
        //            }
        //        }
        //    }
        //    return Emp;
        //}

        //[WebMethod]
        //public static string[] GetCustomers(string prefix)
        //{
        //    List<string> customers = new List<string>();
        //    using (SqlConnection conn = new SqlConnection())
        //    {
        //        conn.ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        //        using (SqlCommand cmd = new SqlCommand())
        //        {
        //            cmd.CommandText = "select AccountID from RawDataCurrent where AccountID LIKE @SearchText + '%'";
        //            cmd.Parameters.AddWithValue("@SearchText", prefix);
        //            cmd.Connection = conn;
        //            conn.Open();
        //            using (SqlDataReader sdr = cmd.ExecuteReader())
        //            {
        //                while (sdr.Read())
        //                {
        //                    customers.Add(string.Format("{0}", sdr["AccountID"]));
        //                }
        //            }
        //            conn.Close();
        //        }
        //    }
        //    return customers.ToArray();
        //}

        public void GetHeader()
        {
            if (!(string.IsNullOrEmpty(id)))
            {
                try
                {
                    string strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    SqlConnection con = new SqlConnection(strConnString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select * from LHK_Acquisition where [No] = @IDProc", con);
                    cmd.Parameters.AddWithValue("@IDProc", id);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        TxbIdLHKAquc.Text = Convert.ToString(dr["No"]);
                        TxtAccountIdAdd.Text = Convert.ToString(dr["AccountID"]);
                        TxtClientNameAdd.Text = Convert.ToString(dr["NamaMitra"]);
                        TxtCenterNameAdd.Text = Convert.ToString(dr["CenterName"]);
                        TxtDueDaysAdd.Text = Convert.ToString(dr["Par"]);
                        TxtDisburseAdd.Text = Convert.ToString(dr["Disb_Date"]);
                        ddlsmplingAdd.Text = Convert.ToString(dr["StatusSampling"]);
                    }
                    con.Close();
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
                }
            }
            else if (!(string.IsNullOrEmpty(id2)))
            {
                try
                {
                    string strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    SqlConnection con = new SqlConnection(strConnString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select * from LHK_Collection where [No] = @IDProc", con);
                    cmd.Parameters.AddWithValue("@IDProc", id2);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        TxbIdLHKAquc.Text = Convert.ToString(dr["No"]);
                        TxtAccountIdAdd.Text = Convert.ToString(dr["AccountID"]);
                        TxtClientNameAdd.Text = Convert.ToString(dr["NamaMitra"]);
                        TxtCenterNameAdd.Text = Convert.ToString(dr["CenterName"]);
                        TxtDueDaysAdd.Text = Convert.ToString(dr["Par"]);
                        TxtDisburseAdd.Text = Convert.ToString(dr["Disb_Date"]);
                        ddlsmplingAdd.Text = Convert.ToString(dr["StatusSampling"]);
                    }
                    con.Close();

                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
                }
            }
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            Session["Id"] = null;
            Session["Id2"] = null;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("LHK.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("LHK.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        public void BindGrid()
        {

            if (!(string.IsNullOrEmpty(id)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel1 where ID_LHK_Acquisition={0}", id);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);

                }

            }
            else if (!(string.IsNullOrEmpty(id2)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel1 where ID_LHK_Collection={0}", id2);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                IEnumerable<DataRow> query = from i in dt.AsEnumerable()
                                             where i.Field<Int32>("No").Equals(code)
                                             select i;
                DataTable detailTable = query.CopyToDataTable<DataRow>();
                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //Getddl1edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                TxtAccountIdEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text).ToString();
                TxtClientNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                TxtCenterNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                TxtDueDaysEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                TxtDisburseEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                TxtPicNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                TxtRemarksEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                ddlsmplingEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[11].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            string rfb = TxtPicNameEdit.Text;
            string rfm = TxtRemarksEdit.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdate(No, rfb, rfm, dt);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveTabel1EditLHK", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@indyear", rfb);
                updateCmd.Parameters.AddWithValue("@rem", rfm);
                updateCmd.Parameters.AddWithValue("@doe", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_A = id;
            string IDProc_B = id2;
            string IDProc_C = id_lhp;
            string IDProc_D = id2_lhp;
            string IDProc_E = id4_lhp;
            string dd = TxtDueDaysAdd.Text;

            if (string.IsNullOrEmpty(IDProc_A))
            {
                IDProc_A = null;
            }
            if (string.IsNullOrEmpty(IDProc_B))
            {
                IDProc_B = null;
            }
            if (string.IsNullOrEmpty(IDProc_C))
            {
                IDProc_C = null;
            }
            if (string.IsNullOrEmpty(IDProc_D))
            {
                IDProc_D = null;
            }
            if (string.IsNullOrEmpty(IDProc_E))
            {
                IDProc_E = null;
            }
            if (string.IsNullOrEmpty(dd))
            {
                dd = null;
            }

            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string Headdb = txbUsername.Text;

            if (!(string.IsNullOrEmpty(IDProc_A)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string idlhk5 = IDProc_D;
                    string idlhk6 = IDProc_E;
                    string code = TxtAccountIdAdd.Text;
                    string mod = "LHK_Acquisition";
                    string name = TxtClientNameAdd.Text;
                    string region = TxtCenterNameAdd.Text;
                    string continent = dd;
                    string population = TxtDisburseAdd.Text;
                    string indyear = TxtPicNameAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, idlhk5, idlhk6, code, mod, name, region, continent, population, indyear, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }

            else if (!(string.IsNullOrEmpty(IDProc_B)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string idlhk5 = IDProc_D;
                    string idlhk6 = IDProc_E;
                    string code = TxtAccountIdAdd.Text;
                    string mod = "LHK_Collection";
                    string name = TxtClientNameAdd.Text;
                    string region = TxtCenterNameAdd.Text;
                    string continent = dd;
                    string population = TxtDisburseAdd.Text;
                    string indyear = TxtPicNameAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, idlhk5, idlhk6, code, mod, name, region, continent, population, indyear, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }

            }

        }

        private void executeAdd(string idlhk2, string idlhk3, string idlhk4, string idlhk5, string idlhk6, string code, string mod, string name, string region, string continent, string population,
            string indyear, string rem, string samp, string dt, string Headdbs)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveTabel1", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk2", idlhk2 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk3", idlhk3 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk4", idlhk4 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk5", idlhk5 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk6", idlhk6 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", dt);
                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@rem", rem);
                addCmd.Parameters.AddWithValue("@samp", samp);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_LHP_Tabel1 where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

    }
}