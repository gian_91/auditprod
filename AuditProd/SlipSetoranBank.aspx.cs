﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace AuditProd
{
    public partial class SlipSetoranBank : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TxbModul.Text = "SlipSetoranBank";
            if (!IsPostBack)
            {
                this.TxbTanggalHeadDibuat.Text = DateTime.Today.ToString("dd/MM/yyyy");
                TxbDibuatHead.Text = this.Page.User.Identity.Name;
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                TxbDilaksanakan.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {
                    LoadSaveID();
                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {
                    LoadSaveIDTemp();
                }
            }
            else
            {
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

            }

        }

        object ToDBValue(string str)
        {
            return string.IsNullOrEmpty(str) ? DBNull.Value : (object)str;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //TxbNTTotal.Attributes.Add("readonly", "readonly");
            //TxbNTVariance.Attributes.Add("readonly", "readonly");
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                SaveId();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)) && (string.IsNullOrEmpty(TxtCreateDate.Text)) && (string.IsNullOrEmpty(TxtReviewDate.Text)))
            {
                SavedReview();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                SaveIdTemp();
            }
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("CashVault.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("CashVault.aspx?ID=" + txtIDSaveTemp.Text);
            }

        }



        protected void TxbNTColProj_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value;
            total = 0;
            decimal.TryParse(TxbNTColProj.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbNTPreclosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbNTPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbNTWriteOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbNTUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt5Value);
            decimal.TryParse(TxbNTTotal.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            decimal.TryParse(TxbTotalSlip.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt6Value);
            decimal.TryParse(TxbNTVariance.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt7Value);

            total = txt1Value;
            TxbNTTotal.Text = total.ToString();

            decimal total1 = total + txt2Value + txt3Value + txt4Value + txt5Value;
            TxbNTTotal.Text = total1.ToString();

            decimal total2 = txt6Value - total1;
            TxbNTVariance.Text = total2.ToString();


        }

        protected void TxbNTPreclosure_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value;
            total = 0;
            decimal.TryParse(TxbNTColProj.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbNTPreclosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbNTPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbNTWriteOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbNTUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt5Value);
            decimal.TryParse(TxbNTTotal.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            decimal.TryParse(TxbTotalSlip.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt6Value);
            decimal.TryParse(TxbNTVariance.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt7Value);


            total = txt2Value;
            TxbNTTotal.Text = total.ToString();

            var total1 = txt5Value + txt1Value + txt3Value + txt4Value + total;
            TxbNTTotal.Text = total1.ToString();

            var total2 = txt6Value - total1;
            TxbNTVariance.Text = total2.ToString();

        }

        protected void TxbNTPartial_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value;
            total = 0;
            decimal.TryParse(TxbNTColProj.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbNTPreclosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbNTPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbNTWriteOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbNTUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt5Value);
            decimal.TryParse(TxbNTTotal.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            decimal.TryParse(TxbTotalSlip.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt6Value);
            decimal.TryParse(TxbNTVariance.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt7Value);

            total = txt3Value;
            TxbNTTotal.Text = total.ToString();

            var total1 = txt5Value + txt1Value + txt2Value + txt4Value + total;
            TxbNTTotal.Text = total1.ToString();

            var total2 = txt6Value - total1;
            TxbNTVariance.Text = total2.ToString();
        }

        protected void TxbNTWriteOff_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value;
            total = 0;
            decimal.TryParse(TxbNTColProj.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbNTPreclosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbNTPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbNTWriteOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbNTUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt5Value);
            decimal.TryParse(TxbNTTotal.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            decimal.TryParse(TxbTotalSlip.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt6Value);
            decimal.TryParse(TxbNTVariance.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt7Value);

            total = txt4Value;
            TxbNTTotal.Text = total.ToString();

            var total1 = txt5Value + txt1Value + txt2Value + txt3Value + total;
            TxbNTTotal.Text = total1.ToString();

            var total2 = txt6Value - total1;
            TxbNTVariance.Text = total2.ToString();
            //TxbNTVariance.Text = string.Format(CultureInfo.CreateSpecificCulture("id-ID"), "{0:C2}", total2);
        }

        protected void TxbNTUjam_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value;
            total = 0;
            decimal.TryParse(TxbNTColProj.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbNTPreclosure.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(TxbNTPartial.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt3Value);
            decimal.TryParse(TxbNTWriteOff.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt4Value);
            decimal.TryParse(TxbNTUjam.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt5Value);
            decimal.TryParse(TxbNTTotal.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            decimal.TryParse(TxbTotalSlip.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt6Value);
            decimal.TryParse(TxbNTVariance.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt7Value);

            total = txt5Value;
            TxbNTTotal.Text = total.ToString();

            var total1 = txt4Value + txt1Value + txt2Value + txt3Value + total;
            TxbNTTotal.Text = total1.ToString();

            var total2 = txt6Value - total1;
            TxbNTVariance.Text = total2.ToString();

        }

        protected void TxbTotalSlip_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, txt2Value, total;
            total = 0;
            decimal.TryParse(TxbTotalSlip.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(TxbNTTotal.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out txt2Value);
            decimal.TryParse(TxbNTVariance.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            total = txt1Value - txt2Value;
            TxbNTVariance.Text = total.ToString();
        }

        private void LoadSaveID()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [IDSSB],[Header_Dibuat],[Header_Dibuat_Tngl],[Bina_Artha_Cabang],[Periode_Audit_Start],[Periode_Audit_End],[NT_Total_Slip_Setor_Bank],[NT_Col_Proj],[NT_Preclosure],[NT_Partial]," +
            "[NT_WriteOff],[NT_Ujam],[NT_Total],[NT_Variance],[BBS_Tngl_Col_Proj],[BBS_Preclosure],[BBS_Partial],[BBS_WriteOff],[BBS_Ujam],[Remarks_Penj_Selisih]," +
            "[Footer_Dilaksanakan],[Footer_Tngl],[Footer_Jam],[Doe] FROM [Slip_Setoran_Bank] where IDSSB = @IdDoctest ", con);
            cmd.Parameters.AddWithValue("@IdDoctest", txtIDSave.Text);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(reader["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(reader["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(reader["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(reader["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(reader["Periode_Audit_End"]);

                TxbTotalSlip.Text = Convert.ToString(reader["NT_Total_Slip_Setor_Bank"]);
                TxbNTColProj.Text = Convert.ToString(reader["NT_Col_Proj"]);
                TxbNTPreclosure.Text = Convert.ToString(reader["NT_Preclosure"]);
                TxbNTPartial.Text = Convert.ToString(reader["NT_Partial"]);
                TxbNTWriteOff.Text = Convert.ToString(reader["NT_WriteOff"]);
                TxbNTUjam.Text = Convert.ToString(reader["NT_Ujam"]);

                TxbTnglColProj.Text = Convert.ToString(reader["BBS_Tngl_Col_Proj"]);
                TxbPreclosure.Text = Convert.ToString(reader["BBS_Preclosure"]);
                TxbPartial.Text = Convert.ToString(reader["BBS_Partial"]);
                TxbWriteOff.Text = Convert.ToString(reader["BBS_WriteOff"]);
                TxbUjam.Text = Convert.ToString(reader["BBS_Ujam"]);

                TxbNTTotal.Text = Convert.ToString(reader["NT_Total"]);
                TxbNTVariance.Text = Convert.ToString(reader["NT_Variance"]);

                RemarksPenjSelisih.InnerText = Convert.ToString(reader["Remarks_Penj_Selisih"]);

                TxbDilaksanakan.Text = Convert.ToString(reader["Footer_Dilaksanakan"]);
                TxbTnglBwh.Text = Convert.ToString(reader["Footer_Tngl"]);
                TxbPdJam.Text = Convert.ToString(reader["Footer_Jam"]);

            }
            con.Close();


        }

        private void LoadSaveIDTemp()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [IDSSB],[Header_Dibuat],[Header_Dibuat_Tngl],[Bina_Artha_Cabang],[Periode_Audit_Start],[Periode_Audit_End],[NT_Total_Slip_Setor_Bank],[NT_Col_Proj],[NT_Preclosure],[NT_Partial]," +
            "[NT_WriteOff],[NT_Ujam],[NT_Total],[NT_Variance],[BBS_Tngl_Col_Proj],[BBS_Preclosure],[BBS_Partial],[BBS_WriteOff],[BBS_Ujam],[Remarks_Penj_Selisih]," +
            "[Footer_Dilaksanakan],[Footer_Tngl],[Footer_Jam],[Doe],[ReviewDate] FROM [Slip_Setoran_Bank] where IDSSB = @IdDoctest ", con);
            cmd.Parameters.AddWithValue("@IdDoctest", txtIDSaveTemp.Text);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(reader["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(reader["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(reader["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(reader["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(reader["Periode_Audit_End"]);

                TxbTotalSlip.Text = Convert.ToString(reader["NT_Total_Slip_Setor_Bank"]);
                TxbNTColProj.Text = Convert.ToString(reader["NT_Col_Proj"]);
                TxbNTPreclosure.Text = Convert.ToString(reader["NT_Preclosure"]);
                TxbNTPartial.Text = Convert.ToString(reader["NT_Partial"]);
                TxbNTWriteOff.Text = Convert.ToString(reader["NT_WriteOff"]);
                TxbNTUjam.Text = Convert.ToString(reader["NT_Ujam"]);

                TxbTnglColProj.Text = Convert.ToString(reader["BBS_Tngl_Col_Proj"]);
                TxbPreclosure.Text = Convert.ToString(reader["BBS_Preclosure"]);
                TxbPartial.Text = Convert.ToString(reader["BBS_Partial"]);
                TxbWriteOff.Text = Convert.ToString(reader["BBS_WriteOff"]);
                TxbUjam.Text = Convert.ToString(reader["BBS_Ujam"]);

                TxbNTTotal.Text = Convert.ToString(reader["NT_Total"]);
                TxbNTVariance.Text = Convert.ToString(reader["NT_Variance"]);

                RemarksPenjSelisih.InnerText = Convert.ToString(reader["Remarks_Penj_Selisih"]);

                TxbDilaksanakan.Text = Convert.ToString(reader["Footer_Dilaksanakan"]);
                TxbTnglBwh.Text = Convert.ToString(reader["Footer_Tngl"]);
                TxbPdJam.Text = Convert.ToString(reader["Footer_Jam"]);
                TxtCreateDate.Text = Convert.ToString(reader["Doe"]);
                TxtReviewDate.Text = Convert.ToString(reader["ReviewDate"]);

            }
            con.Close();

        }

        private void SaveId()
        {
            string message = string.Empty;
            var tot = TxbNTTotal.Text;
            decimal totResult;
            decimal.TryParse(tot, out totResult);
            var vat = TxbNTVariance.Text;
            decimal vatResult;
            decimal.TryParse(vat, out vatResult);


            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
                try
                {
                    using (SqlCommand cmd = new SqlCommand("SP_SaveSSB", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IDSSB", SqlDbType.Int).Value = txtIDSave.Text;
                        cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                        cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                        cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                        cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                        cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                        cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                        cmd.Parameters.AddWithValue("@NT_Total_Slip_Setor_Bank", string.IsNullOrEmpty(TxbTotalSlip.Text) ? (object)DBNull.Value : TxbTotalSlip.Text);
                        cmd.Parameters.AddWithValue("@NT_Col_Proj", string.IsNullOrEmpty(TxbNTColProj.Text) ? (object)DBNull.Value : TxbNTColProj.Text);
                        cmd.Parameters.AddWithValue("@NT_Preclosure", string.IsNullOrEmpty(TxbNTPreclosure.Text) ? (object)DBNull.Value : TxbNTPreclosure.Text);
                        cmd.Parameters.AddWithValue("@NT_Partial", string.IsNullOrEmpty(TxbNTPartial.Text) ? (object)DBNull.Value : TxbNTPartial.Text);
                        cmd.Parameters.AddWithValue("@NT_WriteOff", string.IsNullOrEmpty(TxbNTWriteOff.Text) ? (object)DBNull.Value : TxbNTWriteOff.Text);
                        cmd.Parameters.AddWithValue("@NT_Ujam", string.IsNullOrEmpty(TxbNTUjam.Text) ? (object)DBNull.Value : TxbNTUjam.Text);
                        cmd.Parameters.AddWithValue("@NT_Total", totResult);
                        //cmd.Parameters.AddWithValue("@NT_Total", Double.Parse(TxbNTTotal.Text, NumberStyles.decimal));
                        cmd.Parameters.AddWithValue("@NT_Variance", vatResult);
                        //cmd.Parameters.AddWithValue("@NT_Variance", Double.Parse(TxbNTVariance.Text, NumberStyles.decimal));

                        cmd.Parameters.AddWithValue("@BBS_Tngl_Col_Proj", string.IsNullOrEmpty(TxbTnglColProj.Text) ? (object)DBNull.Value : TxbTnglColProj.Text);
                        cmd.Parameters.AddWithValue("@BBS_Preclosure", string.IsNullOrEmpty(TxbPreclosure.Text) ? (object)DBNull.Value : TxbPreclosure.Text);
                        cmd.Parameters.AddWithValue("@BBS_Partial", string.IsNullOrEmpty(TxbPartial.Text) ? (object)DBNull.Value : TxbPartial.Text);
                        cmd.Parameters.AddWithValue("@BBS_WriteOff", string.IsNullOrEmpty(TxbWriteOff.Text) ? (object)DBNull.Value : TxbWriteOff.Text);
                        cmd.Parameters.AddWithValue("@BBS_Ujam", string.IsNullOrEmpty(TxbUjam.Text) ? (object)DBNull.Value : TxbUjam.Text);

                        cmd.Parameters.AddWithValue("@Remarks_Penj_Selisih", ToDBValue(RemarksPenjSelisih.InnerText));
                        cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilaksanakan.Text) ? (object)DBNull.Value : TxbDilaksanakan.Text);
                        cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                        cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbPdJam.Text) ? (object)DBNull.Value : TxbPdJam.Text);
                        cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;


                        con.Open();
                        int temp = cmd.ExecuteNonQuery();
                        if (temp < 0)
                        {
                            message = "Data Berhasil disimpan";
                            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        }
                        else
                        {
                            message = "Data tidak berhasil disimpan";
                            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        }
                        con.Close();
                    }

                }
                catch (SqlException me)
                {
                    System.Console.Error.Write(me.InnerException.Data);
                }

        }

        private void SavedReview()
        {
            string message = string.Empty;

            var tot = TxbNTTotal.Text;
            decimal totResult;
            decimal.TryParse(tot, out totResult);
            var vat = TxbNTVariance.Text;
            decimal vatResult;
            decimal.TryParse(vat, out vatResult);


            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
                try
                {
                    using (SqlCommand cmd = new SqlCommand("SP_SaveSSB", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IDSSB", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                        cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                        cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                        cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                        cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                        cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                        cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                        cmd.Parameters.AddWithValue("@NT_Total_Slip_Setor_Bank", string.IsNullOrEmpty(TxbTotalSlip.Text) ? (object)DBNull.Value : TxbTotalSlip.Text);
                        cmd.Parameters.AddWithValue("@NT_Col_Proj", string.IsNullOrEmpty(TxbNTColProj.Text) ? (object)DBNull.Value : TxbNTColProj.Text);
                        cmd.Parameters.AddWithValue("@NT_Preclosure", string.IsNullOrEmpty(TxbNTPreclosure.Text) ? (object)DBNull.Value : TxbNTPreclosure.Text);
                        cmd.Parameters.AddWithValue("@NT_Partial", string.IsNullOrEmpty(TxbNTPartial.Text) ? (object)DBNull.Value : TxbNTPartial.Text);
                        cmd.Parameters.AddWithValue("@NT_WriteOff", string.IsNullOrEmpty(TxbNTWriteOff.Text) ? (object)DBNull.Value : TxbNTWriteOff.Text);
                        cmd.Parameters.AddWithValue("@NT_Ujam", string.IsNullOrEmpty(TxbNTUjam.Text) ? (object)DBNull.Value : TxbNTUjam.Text);
                        cmd.Parameters.AddWithValue("@NT_Total", totResult);
                        //cmd.Parameters.AddWithValue("@NT_Total", Double.Parse(TxbNTTotal.Text, NumberStyles.decimal));
                        cmd.Parameters.AddWithValue("@NT_Variance", vatResult);
                        //cmd.Parameters.AddWithValue("@NT_Variance", Double.Parse(TxbNTVariance.Text, NumberStyles.decimal));

                        cmd.Parameters.AddWithValue("@BBS_Tngl_Col_Proj", string.IsNullOrEmpty(TxbTnglColProj.Text) ? (object)DBNull.Value : TxbTnglColProj.Text);
                        cmd.Parameters.AddWithValue("@BBS_Preclosure", string.IsNullOrEmpty(TxbPreclosure.Text) ? (object)DBNull.Value : TxbPreclosure.Text);
                        cmd.Parameters.AddWithValue("@BBS_Partial", string.IsNullOrEmpty(TxbPartial.Text) ? (object)DBNull.Value : TxbPartial.Text);
                        cmd.Parameters.AddWithValue("@BBS_WriteOff", string.IsNullOrEmpty(TxbWriteOff.Text) ? (object)DBNull.Value : TxbWriteOff.Text);
                        cmd.Parameters.AddWithValue("@BBS_Ujam", string.IsNullOrEmpty(TxbUjam.Text) ? (object)DBNull.Value : TxbUjam.Text);

                        cmd.Parameters.AddWithValue("@Remarks_Penj_Selisih", ToDBValue(RemarksPenjSelisih.InnerText));
                        cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilaksanakan.Text) ? (object)DBNull.Value : TxbDilaksanakan.Text);
                        cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                        cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbPdJam.Text) ? (object)DBNull.Value : TxbPdJam.Text);
                        cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;


                        con.Open();
                        int temp = cmd.ExecuteNonQuery();
                        if (temp < 0)
                        {
                            message = "Data Berhasil disimpan";
                            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        }
                        else
                        {
                            message = "Data tidak berhasil disimpan";
                            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        }
                        con.Close();
                    }

                }
                catch (SqlException me)
                {
                    System.Console.Error.Write(me.InnerException.Data);
                }

        }

        private void SaveIdTemp()
        {
            //float total;
            //if (float.TryParse(TxbNTTotal.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out total))
            //    TxbNTTotal.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0:C2}", total);
            //else
            //    TxbNTTotal.Text = String.Empty;

            //float var;
            //if (float.TryParse(TxbNTVariance.Text, NumberStyles.Currency, CultureInfo.CurrentCulture, out var))
            //    TxbNTVariance.Text = String.Format(System.Globalization.CultureInfo.CurrentCulture, "{0:C2}", var);
            //else
            //    TxbNTVariance.Text = String.Empty;

            string message = string.Empty;
            //var value = decimal.Parse(TxbNTTotal.Text, NumberStyles.Currency, CultureInfo.InvariantCulture);
            //var value1 = decimal.Parse(TxbNTVariance.Text, NumberStyles.Currency, CultureInfo.CurrentCulture);            
            //TxbNTTotal.Text = value.ToString();
            //TxbNTVariance.Text = value1.ToString();
            //TxbNTTotal.Text = total.ToString();
            //TxbNTVariance.Text = var.ToString();

            var tot = TxbNTTotal.Text;
            decimal totResult;
            decimal.TryParse(tot, out totResult);

            var vat = TxbNTVariance.Text;
            decimal vatResult;
            decimal.TryParse(vat, out vatResult);


            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
                try
                {
                    using (SqlCommand cmd = new SqlCommand("SP_SaveSSB", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IDSSB", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                        cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                        cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                        cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                        cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                        cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                        cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                        cmd.Parameters.AddWithValue("@NT_Total_Slip_Setor_Bank", string.IsNullOrEmpty(TxbTotalSlip.Text) ? (object)DBNull.Value : TxbTotalSlip.Text);
                        cmd.Parameters.AddWithValue("@NT_Col_Proj", string.IsNullOrEmpty(TxbNTColProj.Text) ? (object)DBNull.Value : TxbNTColProj.Text);
                        cmd.Parameters.AddWithValue("@NT_Preclosure", string.IsNullOrEmpty(TxbNTPreclosure.Text) ? (object)DBNull.Value : TxbNTPreclosure.Text);
                        cmd.Parameters.AddWithValue("@NT_Partial", string.IsNullOrEmpty(TxbNTPartial.Text) ? (object)DBNull.Value : TxbNTPartial.Text);
                        cmd.Parameters.AddWithValue("@NT_WriteOff", string.IsNullOrEmpty(TxbNTWriteOff.Text) ? (object)DBNull.Value : TxbNTWriteOff.Text);
                        cmd.Parameters.AddWithValue("@NT_Ujam", string.IsNullOrEmpty(TxbNTUjam.Text) ? (object)DBNull.Value : TxbNTUjam.Text);
                        //cmd.Parameters.AddWithValue("@NT_Total", SqlDbType.Float).Value = total.ToString();
                        cmd.Parameters.AddWithValue("@NT_Total", totResult);
                        //cmd.Parameters.AddWithValue("@NT_Total", Double.Parse(TxbNTTotal.Text, NumberStyles.decimal));
                        cmd.Parameters.AddWithValue("@NT_Variance", vatResult);
                        //cmd.Parameters.AddWithValue("@NT_Variance", var.ToString(CultureInfo.InvariantCulture));
                        //cmd.Parameters.AddWithValue("@NT_Variance", Double.Parse(TxbNTVariance.Text, NumberStyles.Currency)); 

                        cmd.Parameters.AddWithValue("@BBS_Tngl_Col_Proj", string.IsNullOrEmpty(TxbTnglColProj.Text) ? (object)DBNull.Value : TxbTnglColProj.Text);
                        cmd.Parameters.AddWithValue("@BBS_Preclosure", string.IsNullOrEmpty(TxbPreclosure.Text) ? (object)DBNull.Value : TxbPreclosure.Text);
                        cmd.Parameters.AddWithValue("@BBS_Partial", string.IsNullOrEmpty(TxbPartial.Text) ? (object)DBNull.Value : TxbPartial.Text);
                        cmd.Parameters.AddWithValue("@BBS_WriteOff", string.IsNullOrEmpty(TxbWriteOff.Text) ? (object)DBNull.Value : TxbWriteOff.Text);
                        cmd.Parameters.AddWithValue("@BBS_Ujam", string.IsNullOrEmpty(TxbUjam.Text) ? (object)DBNull.Value : TxbUjam.Text);

                        cmd.Parameters.AddWithValue("@Remarks_Penj_Selisih", ToDBValue(RemarksPenjSelisih.InnerText));
                        cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilaksanakan.Text) ? (object)DBNull.Value : TxbDilaksanakan.Text);
                        cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglBwh.Text) ? (object)DBNull.Value : TxbTnglBwh.Text);
                        cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbPdJam.Text) ? (object)DBNull.Value : TxbPdJam.Text);
                        cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DBNull.Value;
                        cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;

                        con.Open();
                        int temp = cmd.ExecuteNonQuery();
                        if (temp < 0)
                        {
                            message = "Data Berhasil disimpan";
                            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        }
                        else
                        {
                            message = "Data tidak berhasil disimpan";
                            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                        }
                        con.Close();
                    }

                }
                catch (SqlException me)
                {
                    System.Console.Error.Write(me.InnerException.Data);
                }

        }
    }

}