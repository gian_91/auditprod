﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmployeeAttributes.aspx.cs" Inherits="AuditProd.EmployeeAttributes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style type="text/css">
        ::-webkit-input-placeholder {
            text-align: center;
        }

        .tdz {
            text-align: center;
        }

        th {
            font-size: medium;
            text-align: center  ;
            border: none 0px;
        }

        td {
            padding: 0.8em 3em 0.9em 0.7em;
            text-align: center;
           
        }

        .txt {

            width: 155px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

        .tds {
            text-align-last: right;
        }

        .tdsname {
            text-align-last: left;
        }

        .tdName {
            width: 100px;
        }

        .bordered {
            width: 314px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 310px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 292px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
            padding-left: 10px;
        }

        .TableHR {
            display: table;
            width: 1450px;
            text-align: center;
            position: relative;
            right: 240px;
        }

        .Tabletanggal {
            display: table;
            width: 1450px;
            text-align: center;
            position: relative;
            right: 70px;
        }

        .TableHeader {
            display: table;
            padding-left: 300px;
        }

        .Table-tanggalbawah {
            display: table;
            position: relative;
            left: 5px;
        }

        .Table-ttd {
            display: table;
            position: relative;
            right: 230px;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 1120px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: 140px;
        }

        .div-table-cell-kotak-kanan {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 5px;
        }

        .div-table-cell-kotak-kanan-2 {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 13px;
        }

        .div-table-header-kanan {
            width: 420px;
            height: 120px;
            border: 1px solid Blue;
            box-sizing: border-box;
            right: -500px;
            position: relative;
        }

        .div-grup {
            width: auto;
            display: table-column-group;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 6px; /* cellspacing:poor IE support for  this */
            margin: 0 auto;
            
            
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowMid-ttd {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-colMid {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colNomer {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 15px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colEmployee-Id {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 140px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colNama {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 170px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colTTL {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 125px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colDateIn {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 125px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colDateOut {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 125px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colGender {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 38px;
            background-color: #d36f6f;
        }

        .div-table-colJob {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 140px;
            height: 38px;
            background-color: #d36f6f;
        }


        .div-table-colEmployee-IdTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 140px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colNamaTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 170px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colTTLTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 125px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colDateInTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 127px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colDateOutTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 125px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colGenderTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 102px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colJobTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 138px;
            height: 33px;
            background-color: whitesmoke;
        }


        .div-table-colRainCoat {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 75px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colCU {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 72px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colNC {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 74px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colNT {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 74px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colInsurance {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 68px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colJamsostek {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 73px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colML {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 75px;
            height: 38px;
            background-color: #ebfa25;
        }

        .div-table-colRainCoatTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 73px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colCUTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 73px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colNCTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 74px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colNTTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 74px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colInsuranceTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 73px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colJamsostekTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 72px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-table-colMLTxb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 70px;
            height: 33px;
            background-color: whitesmoke;
        }

        .div-tableButton {
            padding-top: 60px;
        }

        .div-table-colMidTextBoxMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-tableMid-TextArea {
            display: table;
            width: 944px;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-colTanggal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 127px;
            height: 35px;
            background-color: none;
        }

        .div-table-colJam {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 360px;
            height: 35px;
            background-color: none;
        }

        .div-table-colTanggal2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 105px;
            height: 35px;
            background-color: none;
            padding-left: 100px;
        }

        .div-table-colTanggal3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 70px;
            height: 35px;
            background-color: none;
        }

        .div-table-colMidSelisih {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 944px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidSelisihtextArea {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 944px;
            height: 185px;
            background-color: whitesmoke;
        }

        .div-table-colTTD {
            float: right;
            display: table-column;
            width: 460px;
            height: 30px;
            background-color: none;
        }

        .div-table-colAll {
            padding-left : 650px;
            display: table-column;
            float:left;            

        }

        .textarea2 {
            font-family: 'Times New Roman';
            width: 932px;
            height: 175px;
            font-size: 15px;
        }

        .display-validate {
            clear: both;
            display: block;
            position: relative;
            left: 210px;
            bottom: 30px;
        }


        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

        @media print {
            #printbtn, [id$=btnSave], [id$=btnExit], [id$=ExportExcel], [id$=btnAdd] {
                display: none;
            }
        }
    </style>
    <script>
        $(function () {
            $("[id$=txtNewBirth_Date],[id$=txtNewBirth_Date3]").datepicker({
                dateFormat: 'dd-M-yy',
                yearRange: '1950:2000',
                changeMonth: true,
                changeYear: true
            });
        });
        $(function () {
            $("[id$=TxbTnglBwh],[id$=txtNewDate_In],[id$=txtNewDate_Out],[id$=txtNewDate_In3],[id$=txtNewDate_Out3]").datepicker({
                dateFormat: 'dd-M-yy',
                yearRange: '2010:2020',
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script>
        $(function () {
            $('.BirthDate').datepicker({
                dateFormat: 'dd-M-yy',
                yearRange: '1950:2000',
                changeMonth: true,
                changeYear: true
            });
        });
        $(function () {
            $('.DateIn').datepicker({
                dateFormat: 'dd-M-yy',
                yearRange: '2010:2020',
                changeMonth: true,
                changeYear: true
            });
        });
        $(function () {
            $('.DateOut').datepicker({
                dateFormat: 'dd-M-yy',
                yearRange: '2010:2020',
                changeMonth: true,
                changeYear: true
            });
        });
        $(function () {
            $('.BirthDate3').datepicker({
                dateFormat: 'dd-M-yy',
                yearRange: '1950:2000',
                changeMonth: true,
                changeYear: true
            });
        });
        $(function () {
            $('.DateIn3 ').datepicker({
                dateFormat: 'dd-M-yy',
                yearRange: '2010:2020',
                changeMonth: true,
                changeYear: true
            });
        });
        $(function () {
            $('.DateOut3').datepicker({
                dateFormat: 'dd-M-yy',
                yearRange: '2010:2020',
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        function HideLabel() {
            var seconds = 2;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
            <fieldset>
        <legend></legend>
        <div class="Table">
            <div class="div-table-header-kanan">
                <label align="center">IAC GL - 08</label>
                <hr />
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="TxbDibuatHead">Dibuat </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbDibuatHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="TxbDireviewHead">Direview </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbDireviewHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="TxbTanggalHeadDibuat">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbTanggalHeadDibuat" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="TxbTanggalHeadDireview">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbTanggalHeadDireview" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="Table">
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSaveTemp" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                        <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false" />
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>
                    </div>
                </div>                
            </div >
            <br />
            <div class="TableHeader" align="center">
                <h3>BERITA ACARA PEMERIKSAAAN KELENGKAPAN KARYAWAN</h3>
                <h4>(Internal Audit & Control Department)</h4>
            </div>            
        </div>
        <br />
        <br />
        <div class="TableHR">
            <div class="div-tableMid">
                    <asp:GridView ID="GridView1" runat="server" ShowHeaderWhenEmpty="True"
                    AutoGenerateColumns="False" OnRowDeleting="RowDeleting"
                    OnRowCancelingEdit="cancelRecord" OnRowEditing="editRecord"
                    OnRowUpdating="updateRecord" CellPadding="4"
                    EnableModelValidation="True" GridLines="None" Width="1050px"
                    ForeColor="#333333">
                    <RowStyle HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#1C5E55" />
                    <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" /> 
            <Columns>
 
            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="5px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>No</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID ="lblId" runat="server" Text='<%#Bind("ID_Emp_Attr")%>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField ItemStyle-Width="70px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>EmployeeID</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID ="lblEmpId" runat="server" Text='<%#Bind("Employee_ID") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtEmpId" runat="server" Text='<%#Bind("Employee_ID") %>' MaxLength="12"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularCert" runat="server" ControlToValidate="txtEmpId" ErrorMessage="Only Number 12 digits ID"
                    ValidationExpression="^[0-9]{12,12}$" Display="Dynamic" ForeColor="White"></asp:RegularExpressionValidator>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewEmpId" runat="server" MaxLength="12"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularCert2" runat="server" ControlToValidate="txtNewEmpId" ErrorMessage="Only Number 12 digits ID" 
                    ValidationExpression="^[0-9]{12,12}$" Display="Dynamic" ForeColor="White"></asp:RegularExpressionValidator>
                <asp:HiddenField ID="txbtngl_foo" runat="server"   />
                <asp:HiddenField ID="txbjam_foo" runat="server"  />   
            </FooterTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField ItemStyle-Width="150px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>Name</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID="lblName" runat ="server" Text='<%#Bind("Name") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID ="txtName" runat="server" Text='<%#Bind("Name") %>' ></asp:TextBox>        
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewName" runat="server" ></asp:TextBox>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField ItemStyle-Width="50px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>BirthDate</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID = "lblBirth_Date" runat="server" Text='<%#Bind("Birth_Date") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtBirth_Date" runat="server" Text='<%#Bind("Birth_Date") %>' CssClass="textbox BirthDate"></asp:TextBox>       
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewBirth_Date" runat="server"></asp:TextBox>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>DateIn</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID = "lblDate_In" runat="server" Text='<%#Bind("Date_In") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtDate_In" runat="server" Text='<%#Bind("Date_In") %>' CssClass="textbox DateIn"></asp:TextBox>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewDate_In" runat="server"></asp:TextBox>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>DateOut</HeaderTemplate>
            <ItemTemplate> 
            <asp:Label ID = "lblDate_Out" runat="server" Text='<%#Bind("Date_Out") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtDate_Out" runat="server" Text='<%#Bind("Date_Out") %>' CssClass="textbox DateOut"></asp:TextBox>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewDate_Out" runat="server"  ></asp:TextBox>
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Gender</HeaderTemplate>
            <ItemTemplate> 
            <asp:Label ID = "lblGender" runat="server" Text='<%#Bind("Gender") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtGender" runat="server" Text='<%#Bind("Gender") %>' placeholder="M or F" MaxLength="1" AutoCompleteType="Disabled"></asp:TextBox>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewGender" runat="server"  placeholder="M or F" MaxLength="1" AutoCompleteType="Disabled"></asp:TextBox>
            </FooterTemplate>        
            </asp:TemplateField>

             <asp:TemplateField ItemStyle-CssClass="tdz">
            <HeaderTemplate>Job</HeaderTemplate>
            <ItemTemplate> 
            <asp:Label ID = "lblJob" runat="server" Text='<%#Bind("Job") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtJob" runat="server" Text='<%#Bind("Job") %>'  ></asp:TextBox>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewJob" runat="server"  ></asp:TextBox>
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Rain<br />Coat</HeaderTemplate>
            <ItemTemplate>
                <asp:CheckBox ID="Rb1_id" runat="server" Checked='<%# Convert.ToBoolean(Eval("RainCoat")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
                <asp:CheckBox ID="Rb1" runat="server" Checked='<%# Convert.ToBoolean(Eval("RainCoat")) %>'/>
<%--            <asp:TextBox ID="txtRainCoat" runat="server" Text='<%#Bind("RainCoat") %>'  ></asp:TextBox>--%>
            <%--<asp:RequiredFieldValidator ID="rfvtxtCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtCity"></asp:RequiredFieldValidator>--%>
            </EditItemTemplate>
            <FooterTemplate>
                <asp:CheckBox ID="Rb1_New" runat="server"  />
<%--            <asp:TextBox ID="txtNewRainCoat" runat="server"  ></asp:TextBox>--%>
            <%--<asp:RequiredFieldValidator ID="rfvtxtNewCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtNewCity"></asp:RequiredFieldValidator>--%>
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Uni<br />Form</HeaderTemplate>
            <ItemTemplate> 
<%--            <asp:Label ID = "lblUniForm" runat="server" Text='<%#Bind("CompanyUniform") %>'></asp:Label>--%>
                <asp:CheckBox ID="Rb2_id" runat="server" Checked='<%# Convert.ToBoolean(Eval("CompanyUniform")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
<%--            <asp:TextBox ID="txtUniForm" runat="server" Text='<%#Bind("CompanyUniform") %>'  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb2" runat="server" Checked='<%# Convert.ToBoolean(Eval("CompanyUniform")) %>'/>
            <%--<asp:RequiredFieldValidator ID="rfvtxtCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtCity"></asp:RequiredFieldValidator>--%>
            </EditItemTemplate>
            <FooterTemplate>
<%--            <asp:TextBox ID="txtNewUniForm" runat="server"  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb2_New" runat="server"  />
            <%--<asp:RequiredFieldValidator ID="rfvtxtNewCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtNewCity"></asp:RequiredFieldValidator>--%>
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Name<br />Card</HeaderTemplate>
            <ItemTemplate> 
<%--            <asp:Label ID = "lblNameCard" runat="server" Text='<%#Bind("NameCard") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb3_id" runat="server" Checked='<%# Convert.ToBoolean(Eval("NameCard")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
<%--            <asp:TextBox ID="txtNameCard" runat="server" Text='<%#Bind("NameCard") %>'  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb3" runat="server" Checked='<%# Convert.ToBoolean(Eval("NameCard")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewNameCard" runat="server"  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb3_New" runat="server"  />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Name<br />Tag</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblNameTag" runat="server" Text='<%#Bind("NameTag") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb4_id" runat="server" Checked='<%# Convert.ToBoolean(Eval("NameTag")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtNameTag" runat="server" Text='<%#Bind("NameTag") %>'  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb4" runat="server" Checked='<%# Convert.ToBoolean(Eval("NameTag")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewNameTag" runat="server"  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb4_New" runat="server"  />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>BPJS<br />TK</HeaderTemplate>
            <ItemTemplate> 
<%--        <asp:Label ID = "lblInsurance" runat="server" Text='<%#Bind("Insurance") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb5_id" runat="server" Checked='<%# Convert.ToBoolean(Eval("Jamsostek")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtInsurance" runat="server" Text='<%#Bind("Insurance") %>'  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb5" runat="server" Checked='<%# Convert.ToBoolean(Eval("Jamsostek")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewInsurance" runat="server"  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb5_New" runat="server"  />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>BPJS<br />Kesh</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblJamsostek" runat="server" Text='<%#Bind("Insurance") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb6_id" runat="server" Checked='<%# Convert.ToBoolean(Eval("Insurance")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtJamsostek" runat="server" Text='<%#Bind("Insurance") %>'></asp:TextBox>--%>
            <asp:CheckBox ID="Rb6" runat="server" Checked='<%# Convert.ToBoolean(Eval("Insurance")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewJamsostek" runat="server"  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb6_New" runat="server"  />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz" >
            <HeaderTemplate>Mutation<br />Letter</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblML" runat="server" Text='<%#Bind("MutationLetter") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb7_id" runat="server" Checked='<%# Convert.ToBoolean(Eval("MutationLetter")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtML" runat="server" Text='<%#Bind("MutationLetter") %>'></asp:TextBox>--%>
            <asp:CheckBox ID="Rb7" runat="server" Checked='<%# Convert.ToBoolean(Eval("MutationLetter")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewML" runat="server"  ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb7_New" runat="server"/>
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField HeaderStyle-Width="4px"  HeaderStyle-Height="50px">
            <HeaderTemplate>Action</HeaderTemplate>
            <ItemTemplate>
                <asp:LinkButton ID="Lb1" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                <asp:LinkButton ID="LB2" runat="server" CommandName="Delete" Text="Delete" CausesValidation="true" OnClientClick="return confirm('Are you sure delete id?')"></asp:LinkButton>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" OnClientClick="return confirm('Are you sure update id?')" />
            <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" />
            </EditItemTemplate> 
            <FooterTemplate>
<%--        <asp:Button ID="btnNewInsert" runat="server" Text="Insert" OnClick="InsertNewRecord"/>
            <asp:Button ID="btnNewCancel" runat="server" Text="Cancel" OnClick="AddNewCancel" CausesValidation="false" />--%>
            <asp:LinkButton ID="Lb3" runat="server" Text="Insert" OnClick="InsertNewRecord" OnClientClick="return confirm('Are you sure insert id?')"></asp:LinkButton>
            <asp:LinkButton ID="LB4" runat="server" Text="Cancel" OnClick="AddNewCancel" CausesValidation="false"></asp:LinkButton>
            </FooterTemplate>        
            </asp:TemplateField>           
            </Columns>
            <EmptyDataTemplate>
                      No record available                    
            </EmptyDataTemplate>       
        </asp:GridView> 
            </div>

            <div class="div-tableMid">
                <asp:GridView ID="GridView2" runat="server" ShowHeaderWhenEmpty="True"
                    AutoGenerateColumns="False" OnRowDeleting="RowDeletingTemp"
                    OnRowCancelingEdit="cancelRecordTemp" OnRowEditing="editRecordTemp"
                    OnRowUpdating="updateRecordTemp" CellPadding="4"
                    EnableModelValidation="True" GridLines="None" Width="1050px"
                    ForeColor="#333333">
                    <RowStyle HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#1C5E55" />
                    <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                    <Columns>
                         
            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="5px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>No</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID ="lblId3" runat="server" Text='<%#Bind("ID_Emp_Attr")%>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Width="70px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>EmployeeID</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID ="lblEmpId3" runat="server" Text='<%#Bind("Employee_ID") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtEmpId3" runat="server" Text='<%#Bind("Employee_ID") %>' MaxLength="12"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularCert" runat="server" ControlToValidate="txtEmpId3" ErrorMessage="Only Number 12 digits ID"
                    ValidationExpression="^[0-9]{12,12}$" Display="Dynamic" ForeColor="White"></asp:RegularExpressionValidator>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewEmpId3" runat="server" MaxLength="12"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularCert2" runat="server" ControlToValidate="txtNewEmpId3" ErrorMessage="Only Number 12 digits ID" 
                    ValidationExpression="^[0-9]{12,12}$" Display="Dynamic" ForeColor="White"></asp:RegularExpressionValidator>
                <asp:HiddenField ID="txbtngl_foo3" runat="server"   />
                <asp:HiddenField ID="txbjam_foo3" runat="server"  />   
            </FooterTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField ItemStyle-Width="150px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>Name</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID="lblName3" runat ="server" Text='<%#Bind("Name") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID ="txtName3" runat="server" Text='<%#Bind("Name") %>' MaxLength="10"  Width="" AutoCompleteType="Disabled"></asp:TextBox>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewName3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField ItemStyle-Width="50px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>BirthDate</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID = "lblBirth_Date3" runat="server" Text='<%#Bind("Birth_Date") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtBirth_Date3" runat="server" Text='<%#Bind("Birth_Date") %>' CssClass="textbox BirthDate3" AutoCompleteType="Disabled"></asp:TextBox>
<%--            <asp:RequiredFieldValidator ID="rfvtxtSalary" runat="server" Text="*"  ToolTip="Enter salary" ControlToValidate="txtSalary"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revtxtSalary" runat="server" Text="*" ToolTip="Enter numericvalue" ControlToValidate="txtSalary" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator> --%>         
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewBirth_Date3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
<%--            <asp:RequiredFieldValidator ID="rfvtxtNewSalary" runat="server" Text="*"  ToolTip="Enter salary" ControlToValidate="txtNewSalary"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="revtxtNewSalary" runat="server" Text="*" ToolTip="Enter numericvalue" ControlToValidate="txtNewSalary" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>--%>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>DateIn</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID = "lblDate_In3" runat="server" Text='<%#Bind("Date_In") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtDate_In3" runat="server" Text='<%#Bind("Date_In") %>' CssClass="textbox DateIn3" AutoCompleteType="Disabled"></asp:TextBox>
<%--            <asp:RequiredFieldValidator ID="rfvtxtCountry" runat="server" Text="*" ToolTip="Entercountry" ControlToValidate="txtCountry"></asp:RequiredFieldValidator> --%>       
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewDate_In3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
<%--            <asp:RequiredFieldValidator ID="rfvtxtNewCountry" runat="server" Text="*" ToolTip="Entercountry" ControlToValidate="txtNewCountry"></asp:RequiredFieldValidator>--%>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>DateOut</HeaderTemplate>
            <ItemTemplate> 
            <asp:Label ID = "lblDate_Out3" runat="server" Text='<%#Bind("Date_Out") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtDate_Out3" runat="server" Text='<%#Bind("Date_Out") %>' CssClass="textbox DateOut3" AutoCompleteType="Disabled"></asp:TextBox>
<%--            <asp:RequiredFieldValidator ID="rfvtxtCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtCity"></asp:RequiredFieldValidator>--%>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewDate_Out3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>
<%--            <asp:RequiredFieldValidator ID="rfvtxtNewCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtNewCity"></asp:RequiredFieldValidator>--%>
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Gender</HeaderTemplate>
            <ItemTemplate> 
            <asp:Label ID = "lblGender3" runat="server" Text='<%#Bind("Gender") %>' ></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtGender3" runat="server" Text='<%#Bind("Gender") %>' AutoCompleteType="Disabled" placeholder="M or F" MaxLength="1"></asp:TextBox>
            <%--<asp:RequiredFieldValidator ID="rfvtxtCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtCity"></asp:RequiredFieldValidator>--%>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewGender3" runat="server" AutoCompleteType="Disabled" placeholder="M or F" MaxLength="1"></asp:TextBox>
            <%--<asp:RequiredFieldValidator ID="rfvtxtNewCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtNewCity"></asp:RequiredFieldValidator>--%>
            </FooterTemplate>        
            </asp:TemplateField>

             <asp:TemplateField ItemStyle-CssClass="tdz">
            <HeaderTemplate>Job</HeaderTemplate>
            <ItemTemplate> 
            <asp:Label ID = "lblJob3" runat="server" Text='<%#Bind("Job") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtJob3" runat="server" Text='<%#Bind("Job") %>' AutoCompleteType="Disabled" ></asp:TextBox>
            <%--<asp:RequiredFieldValidator ID="rfvtxtCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtCity"></asp:RequiredFieldValidator>--%>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewJob3" runat="server" AutoCompleteType="Disabled" ></asp:TextBox>
            <%--<asp:RequiredFieldValidator ID="rfvtxtNewCity" runat="server" Text="*" ToolTip="Enter city" ControlToValidate="txtNewCity"></asp:RequiredFieldValidator>--%>
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Rain<br />Coat</HeaderTemplate>
            <ItemTemplate> 
<%--        <asp:Label ID = "lblRainCoat3" runat="server" Text='<%#Bind("RainCoat") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb1_id2" runat="server" Checked='<%# Convert.ToBoolean(Eval("RainCoat")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtRainCoat3" runat="server" Text='<%#Bind("RainCoat") %>' AutoCompleteType="Disabled" ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb1_2" runat="server" Checked='<%# Convert.ToBoolean(Eval("RainCoat")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewRainCoat3" runat="server" AutoCompleteType="Disabled" ></asp:TextBox>--%>
            <asp:CheckBox ID="Rb1_New2" runat="server" />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Uni<br />Form</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblUniForm3" runat="server" Text='<%#Bind("CompanyUniform") %>' ></asp:Label>--%>
            <asp:CheckBox ID="Rb2_id2" runat="server" Checked='<%# Convert.ToBoolean(Eval("CompanyUniform")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtUniForm3" runat="server" Text='<%#Bind("CompanyUniform") %>' AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb2_2" runat="server" Checked='<%# Convert.ToBoolean(Eval("CompanyUniform")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewUniForm3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb2_New2" runat="server" />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Name<br />Card</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblNameCard3" runat="server" Text='<%#Bind("NameCard") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb3_id2" runat="server" Checked='<%# Convert.ToBoolean(Eval("NameCard")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtNameCard3" runat="server" Text='<%#Bind("NameCard") %>' AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb3_2" runat="server" Checked='<%# Convert.ToBoolean(Eval("NameCard")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewNameCard3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb3_New2" runat="server" />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>Name<br />Tag</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblNameTag3" runat="server" Text='<%#Bind("NameTag") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb4_id2" runat="server" Checked='<%# Convert.ToBoolean(Eval("NameTag")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtNameTag3" runat="server" Text='<%#Bind("NameTag") %>' AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb4_2" runat="server" Checked='<%# Convert.ToBoolean(Eval("NameTag")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewNameTag3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb4_New2" runat="server" />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>BPJS<br />TK</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblInsurance3" runat="server" Text='<%#Bind("Insurance") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb5_id2" runat="server" Checked='<%# Convert.ToBoolean(Eval("Jamsostek")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtInsurance3" runat="server" Text='<%#Bind("Insurance") %>' AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb5_2" runat="server" Checked='<%# Convert.ToBoolean(Eval("Jamsostek")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewInsurance3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb5_New2" runat="server" />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz">
            <HeaderTemplate>BPJS<br />Kesh</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblJamsostek3" runat="server" Text='<%#Bind("Jamsostek") %>' ></asp:Label>--%>
            <asp:CheckBox ID="Rb6_id2" runat="server" Checked='<%# Convert.ToBoolean(Eval("Insurance")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtJamsostek3" runat="server" Text='<%#Bind("Jamsostek") %>' AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb6_2" runat="server" Checked='<%# Convert.ToBoolean(Eval("Insurance")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewJamsostek3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb6_New2" runat="server" />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField  ItemStyle-CssClass="tdz" >
            <HeaderTemplate>Mutation<br />Letter</HeaderTemplate>
            <ItemTemplate> 
            <%--<asp:Label ID = "lblML3" runat="server" Text='<%#Bind("MutationLetter") %>'></asp:Label>--%>
            <asp:CheckBox ID="Rb7_id2" runat="server" Checked='<%# Convert.ToBoolean(Eval("MutationLetter")) %>' onclick="return false;"/>
            </ItemTemplate>
            <EditItemTemplate>
            <%--<asp:TextBox ID="txtML3" runat="server" Text='<%#Bind("MutationLetter") %>' AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb7_2" runat="server" Checked='<%# Convert.ToBoolean(Eval("MutationLetter")) %>'/>
            </EditItemTemplate>
            <FooterTemplate>
            <%--<asp:TextBox ID="txtNewML3" runat="server" AutoCompleteType="Disabled"></asp:TextBox>--%>
            <asp:CheckBox ID="Rb7_New2" runat="server" />
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField HeaderStyle-Width="4px"  HeaderStyle-Height="50px">
            <HeaderTemplate>Action</HeaderTemplate>
            <ItemTemplate>
                <asp:LinkButton ID="Lb13" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                <asp:LinkButton ID="LB23" runat="server" CommandName="Delete" Text="Delete" CausesValidation="true" OnClientClick="return confirm('Are you sure?')"></asp:LinkButton>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:Button ID="btnUpdate3" runat="server" CommandName="Update" Text="Update" />
            <asp:Button ID="btnCancel3" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" />
            </EditItemTemplate> 
            <FooterTemplate>
<%--        <asp:Button ID="btnNewInsert" runat="server" Text="Insert" OnClick="InsertNewRecord"/>
            <asp:Button ID="btnNewCancel" runat="server" Text="Cancel" OnClick="AddNewCancel" CausesValidation="false" />--%>
            <asp:LinkButton ID="Lb33" runat="server" Text="Insert" OnClick="InsertNewRecord" ></asp:LinkButton>
            <asp:LinkButton ID="LB43" runat="server" Text="Cancel" OnClick="AddNewCancel" CausesValidation="false"></asp:LinkButton>
            </FooterTemplate>        
            </asp:TemplateField>           
            </Columns>
            <EmptyDataTemplate>
                      No record available                    
            </EmptyDataTemplate>       
        </asp:GridView> 
            </div>

            <div class="div-table-colAll" align="center">
                <asp:Button ID="btnAdd" runat="server" Text="Add New Record" OnClick="AddNewRecord" />
            </div>
        </div>
        <br />
        <br />
        <br />
        <div class="Tabletanggal">
            <div class="Table-tanggalbawah">
                <div class="div-table-colTanggal">
                    <label>Dilaksanakan Di</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbDilaksanakan" runat="server" Width="210px" AutoCompleteType="Disabled" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal2">
                    <label>Pada Tanggal</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbTnglBwh" runat="server" Width="110px" AutoCompleteType="Disabled" onchange="txbtngl_foo(this)"  ></asp:TextBox>
                </div>
                <div class="div-table-colTanggal3">
                    <label>Pada Jam</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbPdJam" runat="server" Width="110px" AutoCompleteType="Disabled" onchange="txbjam_foo(this)"></asp:TextBox>
                </div>
            </div>
        </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        <div class="Table">
            <div class="Table-ttd">
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Diperiksa Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Disaksikan Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Mengetahui,</div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                </div>
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Internal Audit&Control</div>
                    <div class="div-table-colTTD" align="Center">Admin/Kasir</div>
                    <div class="div-table-colTTD" align="Center">Branch Manager</div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <div class="Table">
            <div class="TableButton">
                <input type="button" value="Print" onclick="window.print()" id="printbtn">
                <asp:Button ID="btnExit" runat="server" Text="Exit" Width="70px" OnClick="btnExit_Click" />
                <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false"></asp:Label>
            </div>
        </div>
            <div>
                <asp:TextBox ID="TxtCreateDate" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="TxtReviewDate" runat="server" Visible="false"></asp:TextBox>
            </div>
    </fieldset>
</asp:Content>
