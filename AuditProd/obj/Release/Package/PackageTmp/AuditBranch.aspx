﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuditBranch.aspx.cs" Inherits="AuditProd.AuditBranch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

    <style type="text/css">
        .auto-style1 {
            width: 231px;
        }
        .auto-style2 {
            height: 49px;
        }
        .auto-style3 {
            width: 231px;
            height: 49px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id$=BtnExport]").click(function () {
                var DDLExport = $("[id$=DDLExport]");
                if (DDLExport.val() == "") {
                    //If the "Please Select" option is selected display error.
                    alert("Please Select Modul !");
                    return false;
                }
                return true;
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <fieldset>
        <legend></legend>
        <h2>Internal Audit Control </h2>
        <h3>AuditBranch</h3>

        <table>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" AssociatedControlID="UserName">User name</asp:Label></td>
                <td>
                    <asp:TextBox runat="server" ID="UserName" ReadOnly="True" Width="310px"/></td>
                <td>
                    <asp:TextBox runat="server" ID="txtIDSave" ReadOnly="True" Visible="false" />
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtIDSaveTemp" ReadOnly="True" Visible="false" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="textarea">User Data Audit</label></td>
                <td>
                    <asp:TextBox runat="server" ID="UserDataAudit" ReadOnly="True" Width="310px" /></td>
            </tr>
            <tr>
                <td>
                    <label for="textarea">BranchID</label></td>
                <td>
                    <asp:TextBox runat="server" ID="txtBranchId" ReadOnly="True" Width="310px" /></td>
            </tr>
            <tr>
                <td>
                    <label for="textarea">Month Of Audit</label></td>
                <td>
                    <asp:TextBox runat="server" ID="txtMonth" ReadOnly="True" Width="147px" />
                    <asp:TextBox runat="server" ID="txtYear" ReadOnly="True" Width="147px" />
                </td>
            </tr>
            <tr>
                <td>
                    <label for="textarea">AuditSchedule</label></td>
                <td>
                    <asp:TextBox runat="server" ID="txtAuditSchedule" ReadOnly="true" Width="310px" /></td>
            </tr>
            <tr>
                <td>
                    <label for="textarea">Audit Periode</label></td>
                <td>
                    <asp:TextBox runat="server" ID="txtAuditStart" ReadOnly="true" Width="147px" />
                    <asp:TextBox runat="server" ID="txtAuditEnds" ReadOnly="true" Width="147px" />
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnDocTest" runat="server" Text="Doc Test" Width="200px" OnClick="btnDocTest_Click" /></td>
                <td class="auto-style1">
                    <asp:Button ID="btnCashVault" runat="server" Text="Cash Vault" Width="200px" OnClick="btnCashVault_Click" /></td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="btnPettyCash" runat="server" Text="Petty Cash" Width="200px" OnClick="btnPettyCash_Click" /></td>
                <td class="auto-style3">
                    <asp:Button ID="btnHR" runat="server" Text="HR" Width="200px" OnClick="btnHR_Click" /></td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnProcmt" runat="server" Text="Procurement" Width="200px" OnClick="btnProcmt_Click" />
                </td>
                <td class="auto-style1">
                    <asp:Button ID="btnLHK" runat="server" Text="LHK" Width="200px" OnClick="btnLHK_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnLHP" runat="server" Text="LHP" Width="200px" OnClick="btnLHP_Click" />
                </td>
            </tr>      
            <tr>
                <td>
                    <label for="textarea">Export To Excel :</label>
                </td>
                <td>
                    <asp:DropDownList ID="DDLExport" runat="server" Width="235px" Height="30px">
                        <asp:ListItem Text="--Please Select Modul--" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="BtnExport" runat="server" Text="Export" Width="100px" OnClick="BtnExport_Click"  />
                </td>
            </tr>
        </table>
        <br />
    </fieldset>
</asp:Content>
