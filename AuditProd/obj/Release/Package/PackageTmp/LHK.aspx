﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LHK.aspx.cs" Inherits="AuditProd.LHK" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="Stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.10/themes/redmond/jquery-ui.css" />
    <script src="Scripts/Bootstrap/jquery-1.8.2.js"></script>

    <script type="text/javascript">
        function HideLabel() {
            var seconds = 2;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
                }, seconds * 1000);
            };
    </script>
    <script type="text/javascript">
        $(function () {
            $("[id$=txtAccountId]").autocomplete({
                source: function (request, response) {
                    var param = { empName: $("[id$=txtAccountId]").val() };
                    $.ajax({
                        url: "DocTest.aspx/GetEmpNames",
                        data: JSON.stringify(param),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    value: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var err = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(err.Message)
                            // console.log("Ajax Error!");  
                        }
                    });
                },
                minLength: 9 //This is the Char length of inputTextBox  

            });
        });
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 231px;
        }

        .auto-style2 {
            height: 49px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

        .bordered {
            width: 306px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 306px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 304px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 855px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowbutton {
            padding-left: 880px;
        }

        .div-table-cell {
            display: table-cell;
            width: auto;
            clear: both;
        }

        .buttonCek {
            background: lightblue;
            border-radius: 10px;
            vertical-align: middle;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <header>
        <link href="Scripts/Bootstrap/bootstrap.css" rel="stylesheet" />
        <script src="Scripts/Bootstrap/bootstrap.js"></script>
    </header>
    <fieldset>
        <legend></legend>
        <div class="Table">
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang</asp:Label>
                </div>
                <div class="div-table-cell">                    
                    <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                </div>
            </div>
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                </div>
                <div class="div-table-cell">
                    <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>
                    <asp:TextBox ID="txbID_RDT" runat="server" ReadOnly="true" Width="83px" Visible="false" AutoPostBack="true"></asp:TextBox>
                    <asp:TextBox ID="txbAccountId" runat="server" ReadOnly="true" Width="140px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbID_DocTest" runat="server" ReadOnly="true" Width="140px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbUsername" runat="server" ReadOnly="true" Width="123px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="123px" Visible="false"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtIDSaveTemp" ReadOnly="True" Visible="false" />
                    <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false" />
                    <asp:TextBox runat="server" ID="TxbNoTabel" ReadOnly="True" Visible="false" />
                </div>
            </div>
        </div>
        <div align="center">
            <h4 style="margin:0px">Laporan Hasil Kunjungan</h4>
            <h5 style="margin:0px">Internal Audit & Control Department</h5>
        </div>
        <br />
        <div class="div-tableatas">
            <div class="bordered">
                <div class="div-table-row">
                    Account ID
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtAccountId" runat="server" Width="188px" AutoCompleteType="Disabled"></asp:TextBox>
                                <asp:TextBox ID="txtIdRaw" runat="server" Width="188px" Visible="false"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Nama Mitra
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtNamaMitra" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Center Name
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtCentName" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Disb Date
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtDisbDate" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
            </div>
            <div class="bordered3">
                <div class="div-table-row">
                    UKM Date
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtUkmDate" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    OutStanding Principal
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtOutstanding" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Ticket Size
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtTicketSize" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Status Sampling
                    <div class="div-table-cell" >
                        <asp:DropDownList ID="ddlStatSamp" runat="server" Width="202px">
                            <asp:ListItem Text="Random" Value="Random"></asp:ListItem>
                            <asp:ListItem Text="Stratify" Value="Stratify"></asp:ListItem>
                        </asp:DropDownList>
                    </div>                    
                </div>                
            </div>

            <div class="bordered2">
                <div class="div-table-row">
                    PAR (Hari)
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtPAR" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Loan Status
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtLoanStat" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Nama SO
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtNamaSO" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Nama RO
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtNamaRO" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>                                
                            </div>
                </div>
            </div>
        </div>

        <div class="div-table-rowbutton">
            <asp:Button ID="btnCek" runat="server" Text="Show" Width="65px" Height="50px" CssClass="buttonCek" OnClick="btnCcek_Click" />
        </div>

        <h4 style="text-align: left">Acquisition Process</h4>
        <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
                <asp:UpdatePanel ID="upCrudGrid" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView1" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView1_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None"  >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No"  Visible="false"  />
                            <asp:BoundField DataField="ID_LHK" HeaderText="Id LHK" Visible="false" />
                            <asp:BoundField DataField="LHK_Acquisition_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHK_Acquisition_TOD" HeaderText="Type Of Deviation" />
                            <asp:BoundField DataField="LHK_Acquisition_CaseFinding" HeaderText="Case Finding" />
                            <asp:ButtonField DataTextField="LHK_Acquisition_Tabel" HeaderText="Tabel" ButtonType="Link" CommandName="detail_click"  />
                            <asp:BoundField DataField="LHK_Acquisition_Recomd_Branch" HeaderText="Recomendation For Branch" />
                            <asp:BoundField DataField="LHK_Acquisition_Recomd_Management" HeaderText="Recomendation Management" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAdd" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAdd_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Detailed View</h3>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DetailsView ID="DetailsView1" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                                <Fields>
                            <asp:BoundField DataField="No" HeaderText="No" />
                            <asp:BoundField DataField="ID_LHK" HeaderText="Id LHK" Visible="false" />
                            <asp:BoundField DataField="LHK_Acquisition_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHK_Acquisition_TOD" HeaderText="Tod" />
                            <asp:BoundField DataField="LHK_Acquisition_CaseFinding" HeaderText="CaseFinding" />
                            <asp:BoundField DataField="LHK_Acquisition_Tabel" HeaderText="Tabel" />
                            <asp:BoundField DataField="LHK_Acquisition_Recomd_Branch" HeaderText="Recomd Branch" />
                            <asp:BoundField DataField="LHK_Acquisition_Recomd_Management" HeaderText="Recomd Management" />
                                </Fields>
                            </asp:DetailsView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="editModalLabel">Edit Record</h3>
                </div>
                <asp:UpdatePanel ID="upEdit" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                                <tr>
                                    <td>
                            <asp:Label ID="lblCountryCode" runat="server" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                            <%--<asp:TextBox ID="txtCategory" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl1edit" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl1edit_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Category--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label> 
                            <%--<asp:TextBox ID="txtTod" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl2edit" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl2edit_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                            <%--<asp:TextBox ID="txtCF" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl3edit" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddl3edit_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                            <%--<asp:TextBox ID="txtTabel" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl4edit" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch:</label>
                            <asp:TextBox ID="txtRFB" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management:</label>
                            <asp:TextBox ID="txtRFM" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnSave_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="addModalLabel">Add New Record</h3>
                </div>
                <asp:UpdatePanel ID="upAdd" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="txtidLHK" runat="server" Visible="false"></asp:TextBox>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddl1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl1_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Category--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                <%--<asp:TextBox ID="txtCountryName" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl2_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                <%--<asp:TextBox ID="txtContinent" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl3" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddl3_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                <%--<asp:TextBox ID="txtRegion" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl4" runat="server" Width="250px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch :</label>
                                    <asp:TextBox ID="txtTotalPopulation" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management :</label>
                                        <asp:TextBox ID="txtIndYear" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddRecord" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="delModalLabel">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="upDel" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>
        <h4 style="text-align: left">Collection Process</h4>
        <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView2" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView2_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView2_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No" Visible="false" />
                            <asp:BoundField DataField="ID_LHK" HeaderText="Id LHK" Visible="false" />
                            <asp:BoundField DataField="LHK_Collection_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHK_Collection_TOD" HeaderText="Type Of Deviation" />
                            <asp:BoundField DataField="LHK_Collection_CaseFinding" HeaderText="Case Finding" />
                            <%--<asp:BoundField DataField="LHK_Collection_Tabel" HeaderText="Tabel" />--%>
                            <asp:ButtonField DataTextField="LHK_Collection_Tabel" HeaderText="Tabel" ButtonType="Link" CommandName="detail_click" />
                            <asp:BoundField DataField="LHK_Collection_Recomd_Branch" HeaderText="Recomendation For Branch" />
                            <asp:BoundField DataField="LHK_Collection_Recomd_Management" HeaderText="Recomendation Management" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAddCollection" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAddCollection_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <div id="Div1Detail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H1">Detailed View</h3>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:DetailsView ID="DetailsView2" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                                <Fields>
                            <asp:BoundField DataField="No" HeaderText="No" />
                            <asp:BoundField DataField="ID_LHK" HeaderText="Id LHK" Visible="false" />
                            <asp:BoundField DataField="LHK_Collection_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHK_Collection_TOD" HeaderText="Tod" />
                            <asp:BoundField DataField="LHK_Collection_CaseFinding" HeaderText="CaseFinding" />
                            <asp:BoundField DataField="LHK_Collection_Tabel" HeaderText="Tabel" />
                            <asp:BoundField DataField="LHK_Collection_Recomd_Branch" HeaderText="Recomd Branch" />
                            <asp:BoundField DataField="LHK_Collection_Recomd_Management" HeaderText="Recomd Management" />
                                </Fields>
                            </asp:DetailsView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="GridView2" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="btnAddCollection" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="Div2Edit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H2">Edit Record</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                                <tr>
                                    <td>
                            <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddledit1coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit1coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Category--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddledit2coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit2coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
<%--                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label
                                        <asp:DropDownList ID="ddledit3coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit3coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>--%>
                                    <td>
                                        <label style="font-weight:bold; font-size: 15px">Case Finding></label>
                                        <asp:DropDownList ID="ddledit3coll_2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit3coll_2_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Finding--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddledit4coll" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch:</label>
                            <asp:TextBox ID="txbEditCollRFB" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management:</label>
                            <asp:TextBox ID="txbEditCollRFM" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="Label2" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="Button3" runat="server" Text="Update" CssClass="btn btn-info" OnClick="BtnSaveColl_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView2" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAddCollection" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="Div3Add" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H3">Add New Record</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="TextBox3" runat="server" Visible="false"></asp:TextBox>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddladd1coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd1coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Category--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddladd2coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd2coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddladd3coll" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddladd3coll_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddladd4coll" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch :</label>
                                    <asp:TextBox ID="txbAddCollRFB" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management :</label>
                                        <asp:TextBox ID="txbAddCollRFM" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddColl" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddColl_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddColl" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="Div4Del" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H4">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelCol" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelCol_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelCol" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>

        <br />
        <br />

        <div class="TableButton">
            <asp:Button ID="btnExit" runat="server" Text="Exit" Width="100px" OnClick="btnExit_Click" />
            <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </fieldset>

</asp:Content>
