﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DocTest.aspx.cs" Inherits="AuditProd.DocTest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.8.0.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.22/jquery-ui.js"></script>
    <link rel="Stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.10/themes/redmond/jquery-ui.css" />

    <script type="text/javascript">
        function HideLabel() {
            var seconds = 2;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
    <script type="text/javascript">
        $(function () {
            $("[id$=txtAccountId]").autocomplete({
                source: function (request, response) {
                    var param = { empName: $("[id$=txtAccountId]").val() };
                    $.ajax({
                        url: "DocTest.aspx/GetEmpNames",
                        data: JSON.stringify(param),
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    value: item
                                }
                            }))
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            var err = eval("(" + XMLHttpRequest.responseText + ")");
                            alert(err.Message)
                            // console.log("Ajax Error!");  
                        }
                    });
                },
                minLength: 9 //This is the Char length of inputTextBox  

            });
        });
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 231px;
        }

        .auto-style2 {
            height: 49px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

        .bordered {
            width: 306px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 306px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 304px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 744px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowbutton {
            padding-left:220px;
        }

        .div-table-cell {
            display: table-cell;
            width: auto;
            clear: both;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-colMid {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMid-rbt {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 570px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidNomer {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 40px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMiddesc {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 146px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidRemarks {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 70px;
            background-color: whitesmoke;
        }

        .textarea2 {
            font-family: 'Times New Roman';
            width: 105px;
            font-size: 14px;
        }

        .buttonCek {
            background: lightblue;
            border-radius: 10px;
            vertical-align: middle;
        }

        .RBL label {
            display: block;
        }

        .RBL td {
            text-align: center;
            width: 120px;
            padding-left : 40px;
        }

        .div-table-RBL {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 450px;
            height: 35px;
            background-color: whitesmoke;
        }
    </style> 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend></legend>
        <div class="Table">
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                </div>
                <div class="div-table-cell">                    
                    <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                </div>

            </div>
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                </div>
                <div class="div-table-cell">
                    <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>
                    <asp:TextBox ID="txbID_RDT" runat="server" ReadOnly="true" Width="83px" Visible="false" AutoPostBack="true"></asp:TextBox>
                    <asp:TextBox ID="txbAccountId" runat="server" ReadOnly="true" Width="140px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbID_DocTest" runat="server" ReadOnly="true" Width="140px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbUsername" runat="server" ReadOnly="true" Width="123px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="123px" Visible="false"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtIDSaveTemp" ReadOnly="True" Visible="false" />
                    <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false" />
                    <asp:TextBox runat="server" ID="TxbAuditSchedule" ReadOnly="True" Visible="false" />
                </div>
            </div>
        </div>

        <div align="center">
            <h5>Document Test</h5>
            <h5>Internal Audit & Control Department</h5>
        </div>

        <div class="div-tableatas">
            <div class="bordered">
                <div class="div-table-row">
                    Account ID
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtAccountId" runat="server" Width="188px" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Nama Mitra
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtNamaMitra" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Center Name
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtCentName" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Disb Date
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtDisbDate" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
            </div>

            <div class="bordered3">
                <div class="div-table-row">
                    UKM Date
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtUkmDate" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    OutStanding Principal
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtOutstanding" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Ticket Size
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtTicketSize" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-rowbutton">
                        <asp:Button ID="btnCcek" runat="server" Text="Show" Width="75px" OnClick="btnCcek_Click" Height="60px" CssClass="buttonCek" />
                </div>
            </div>

            <div class="bordered2">
                <div class="div-table-row">
                    PAR (Hari)
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtPAR" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Loan Status
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtLoanStat" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Nama SO
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtNamaSO" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                </div>
                <div class="div-table-row">
                    Nama RO
                            <div class="div-table-cell">
                                <asp:TextBox ID="txtNamaRO" runat="server" Width="188px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>                                
                            </div>
                </div>
            </div>
        </div>
        <h4>Form UKM</h4>

        <div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colMidNomer" align="center">No</div>
                    <div class="div-table-colMiddesc">Deskripsi</div>
                    <div class="div-table-colMid" align="center">Ada</div>
                    <div class="div-table-colMid" align="center">Tidak Ada</div>
                    <div class="div-table-colMid" align="center">Tidak Sesuai</div>
                    <div class="div-table-colMid" align="center">Remarks</div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colMidNomer" align="center">1</div>
                    <div class="div-table-colMiddesc"><label id="ktp">KTP Mitra</label></div>
                      <div class="div-table-colMid-rbt" >
                        <asp:RadioButtonList ID="rbtUKMMitra" runat="server" CssClass="RBL" RepeatDirection="Horizontal" >
                            <asp:ListItem Text="" Value="Ada"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakAda"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakSesuai" ></asp:ListItem>
                        </asp:RadioButtonList>                          
                    </div>
                    <div class="div-table-colMidRemarks">
                        <textarea class="textarea2" runat="server" id="RemarksKtpUkmm" rows="2" style="width: 180px; height: 58px;"></textarea>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-colMidNomer" align="center">2</div>
                    <div class="div-table-colMiddesc"><label id="KTPPenjamin">KTP Penjamin</label></div>
                      <div class="div-table-colMid-rbt" >
                        <asp:RadioButtonList ID="rbtUKMPenjamin" runat="server" CssClass="RBL" RepeatDirection="Horizontal" >
                            <asp:ListItem Text="" Value="Ada"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakAda"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakSesuai" ></asp:ListItem>
                        </asp:RadioButtonList>                          
                    </div>
                    <div class="div-table-colMidRemarks">
                        <textarea class="textarea2" runat="server" id="RemarksKtpPenjamin" rows="2" style="width: 180px; height: 58px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colMidNomer" align="center">3</div>
                    <div class="div-table-colMiddesc"><label id="FamilyCard">Family Card</label></div>
                      <div class="div-table-colMid-rbt" >
                        <asp:RadioButtonList ID="rbtUKMKK" runat="server" CssClass="RBL" RepeatDirection="Horizontal" >
                            <asp:ListItem Text="" Value="Ada"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakAda"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakSesuai" ></asp:ListItem>
                        </asp:RadioButtonList>                          
                    </div>
                    <div class="div-table-colMidRemarks">
                        <textarea class="textarea2" runat="server" id="RemarksFamilycard" rows="2" style="width: 180px; height: 58px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colMidNomer" align="center">4</div>
                    <div class="div-table-colMiddesc"><label id="UKM">UKM</label></div>
                      <div class="div-table-colMid-rbt" >
                        <asp:RadioButtonList ID="rbtUKMFromUKM" runat="server" CssClass="RBL" RepeatDirection="Horizontal" >
                            <asp:ListItem Text="" Value="Ada"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakAda"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakSesuai" ></asp:ListItem>
                        </asp:RadioButtonList>                          
                    </div>
                    <div class="div-table-colMidRemarks">
                        <textarea class="textarea2" runat="server" id="RemarksFormUkm" rows="2" style="width: 180px; height: 58px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colMidNomer" align="center">5</div>
                    <div class="div-table-colMiddesc"><label id="FotoMitra">Foto Mitra</label></div>
                      <div class="div-table-colMid-rbt" >
                        <asp:RadioButtonList ID="rbtUKMFtMitra" runat="server" CssClass="RBL" RepeatDirection="Horizontal" >
                            <asp:ListItem Text="" Value="Ada"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakAda"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakSesuai" ></asp:ListItem>
                        </asp:RadioButtonList>                          
                    </div>
                    <div class="div-table-colMidRemarks">
                        <textarea class="textarea2" runat="server" id="RemarksFtMitraUKM" rows="2" style="width: 180px; height: 58px;"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <br />
        <h4>Form Pencairan</h4>
        <div>
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colMidNomer" align="center">No</div>
                    <div class="div-table-colMiddesc">Deskripsi</div>
                    <div class="div-table-colMid" align="center">Ada</div>
                    <div class="div-table-colMid" align="center">Tidak Ada</div>
                    <div class="div-table-colMid" align="center">Tidak Sesuai</div>
                    <div class="div-table-colMid" align="center">Remarks</div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colMidNomer" align="center">1</div>
                    <div class="div-table-colMiddesc"><label id="MitraDisb">KTP Mitra</label></div>
                      <div class="div-table-colMid-rbt" >
                        <asp:RadioButtonList ID="rbtDisbKTP" runat="server" CssClass="RBL" RepeatDirection="Horizontal" >
                            <asp:ListItem Text="" Value="Ada"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakAda"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakSesuai" ></asp:ListItem>
                        </asp:RadioButtonList>                          
                    </div>
                    <div class="div-table-colMidRemarks">
                        <textarea class="textarea2" runat="server" id="RemarksKtpDisb" rows="2" style="width: 180px; height: 58px;"></textarea>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-colMidNomer" align="center">2</div>
                    <div class="div-table-colMiddesc"><label id="FormDisbursement">Form Disbursement</label></div>
                      <div class="div-table-colMid-rbt" >
                        <asp:RadioButtonList ID="rbtDisbFormDisb" runat="server" CssClass="RBL" RepeatDirection="Horizontal" >
                            <asp:ListItem Text="" Value="Ada"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakAda"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakSesuai" ></asp:ListItem>
                        </asp:RadioButtonList>                          
                    </div>
                    <div class="div-table-colMidRemarks">
                        <textarea class="textarea2" runat="server" id="RemarksFormDisb" rows="2" style="width: 180px; height: 58px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colMidNomer" align="center">3</div>
                    <div class="div-table-colMiddesc"><label id="FotoMitraDisb">Foto Mitra</label></div>
                      <div class="div-table-colMid-rbt" >
                        <asp:RadioButtonList ID="rbtDisbFtMitra" runat="server" CssClass="RBL" RepeatDirection="Horizontal" >
                            <asp:ListItem Text="" Value="Ada"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakAda"></asp:ListItem>
                            <asp:ListItem Text="" Value="TidakSesuai" ></asp:ListItem>
                        </asp:RadioButtonList>                          
                    </div>
                    <div class="div-table-colMidRemarks">
                        <textarea class="textarea2" runat="server" id="RemarksFtMitraDisb" rows="2" style="width: 180px; height: 58px;"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="TableButton">
            <asp:Button ID="btnSave" runat="server" Text="Save" Width="100px" OnClick="btnSave_Click" />
            <asp:Button ID="btnExit" runat="server" Text="Exit" Width="100px" OnClick="btnExit_Click" />
            <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false" ></asp:Label>            
        </div>
        <div>
            <asp:TextBox ID="TxtCreateDate" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TxtReviewDate" runat="server" Visible="false"></asp:TextBox>
        </div>
    </fieldset>
</asp:Content>
