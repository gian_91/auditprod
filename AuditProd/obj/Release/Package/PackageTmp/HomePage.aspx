﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HomePage.aspx.cs" Inherits="AuditProd.HomePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

   <script>
       $(function () {
           var dateFormat = "mm/dd/yy",
             from = $("[id$=AuditPeriodeStart]")
               .datepicker({
                   defaultDate: "+1w",
                   changeMonth: true,
                   changeYear: true,
                   numberOfMonths: 2
               })
               .on("change", function () {
                   to.datepicker("option", "minDate", getDate(this));
               }),
             to = $("[id$=AuditPeriodeEnd]").datepicker({
                 defaultDate: "+1w",
                 changeMonth: true,
                 changeYear: true,
                 numberOfMonths: 2
             })
             .on("change", function () {
                 from.datepicker("option", "maxDate", getDate(this));
             });

           function getDate(element) {
               var date;
               try {
                   date = $.datepicker.parseDate(dateFormat, element.value);
               } catch (error) {
                   date = null;
               }
               return date;
           }
       });
    </script>
   <script type="text/javascript">
       $(function () {
           $("[id$=btnPilih]").click(function () {
               var ddlIDsave = $("[id$=ddlIDsave]");
               if (ddlIDsave.val() == "") {
                   //If the "Please Select" option is selected display error.
                   alert("Pilih Audit Schedule !");
                   return false;
               }
               return true;
           });
       });
       $(function () {
           $("[id$=btnSave]").click(function () {
               var ddlBranchID = $("[id$=ddlBranchID]");
               if (ddlBranchID.val() == "") {
                   //If the "Please Select" option is selected display error.
                   alert("Pilih BranchName !");
                   return false;
               }
               return true;
           });
       });
       $(function () {
           $("[id$=btnSave]").click(function () {
               var ddlmonth = $("[id$=ddlMonth]");
               if (ddlmonth.val() == "") {
                   //If the "Please Select" option is selected display error.
                   alert("Pilih Month !");
                   return false;
               }
               return true;
           });
       });
       $(function () {
           $("[id$=btnSave]").click(function () {
               var ddlyear = $("[id$=ddlYear]");
               if (ddlyear.val() == "") {
                   //If the "Please Select" option is selected display error.
                   alert("Pilih Year !");
                   return false;
               }
               return true;
           });
       });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <fieldset>
        <legend>Internal Audit Control</legend>
        <h4>Internal Audit Control </h4>
        <ol>            
            <li>
                <asp:Label ID="Label6" runat="server" AssociatedControlID="UserName">User name :</asp:Label>
                <asp:TextBox runat="server" ID="UserName" ReadOnly="True" Width="225px"/>
            </li>
            <li>
                <label for="textarea">Pilih Audit Schedule Tersimpan:</label>
                <asp:DropDownList runat="server" ID="ddlIDsave" Width="235px" Height="30px" AutoPostBack="false" >
                    <asp:ListItem Text="--Select  Audit Data--" Value=""></asp:ListItem>
                </asp:DropDownList>
            </li>
           <li>
               <asp:Button ID="btnPilih" runat="server" Text="Show" OnClick="btnPilih_Click"/>
           </li> 
            <li>
                <label for="textarea">BranchName :</label>
                <asp:DropDownList runat="server" ID="ddlBranchID" Width="235px" Height="30px" AutoPostBack="true" OnSelectedIndexChanged="ddlBranchID_SelectedIndexChanged">
                    <asp:ListItem Text="--Select BranchID--" Value=""></asp:ListItem>
                </asp:DropDownList>
            </li>
            <li>
                <label for="textarea">Month Of Audit :</label>
                <asp:DropDownList ID="ddlMonth" runat="server" Width="116px" Height="30px" AutoPostBack="true" OnSelectedIndexChanged="ddlMonth_SelectedIndexChanged">
                    <asp:ListItem Text="--Select Month--" Value=""></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList ID="ddlYear" runat="server" Width="116px" Height="30px" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                    <asp:ListItem Text="--Select Year--" Value=""></asp:ListItem>
                </asp:DropDownList>
            </li>
            <li>
                <asp:Label ID="Label10" runat="server" AssociatedControlID="AuditSchedule">Audit Schedule :</asp:Label>
                <asp:TextBox runat="server" ID="AuditSchedule" AutoCompleteType="Disabled" Width="310px" ReadOnly="true" />
            </li>
            <li>
                <asp:Label ID="Label11" runat="server" AssociatedControlID="AuditPeriodeStart">Audit Periode :</asp:Label>
                <asp:TextBox runat="server" ID="AuditPeriodeStart" AutoCompleteType="Disabled" />
                TO                
                <asp:TextBox runat="server" ID="AuditPeriodeEnd" AutoCompleteType="Disabled" />
            </li>
        </ol>
        <asp:Button ID="btnReport" runat="server" Text="Report" />
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
        <asp:Button ID="btnUser" runat="server" Text="ManageUser" OnClick="btnUser_Click" Visible="false" />

    </fieldset>
</asp:Content>
