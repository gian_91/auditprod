﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CashVault.aspx.cs" Inherits="AuditProd.CashVault" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
        <style type="text/css">
        .Table {
            display: table;
            padding-left: 50px;
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: auto;
            clear: both;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <fieldset>
        <legend>fsfsfsf</legend>        
        <div align="Center">
            <h2>Internal Audit Control</h2>
            <h3>Audit Branch</h3>
            <h4>Cash Vault</h4>
        </div>
        <div class="Table">
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                </div>
                <div class="div-table-cell">
                    <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                    <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtIDSaveTemp" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                </div>
            </div>
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>

                </div>
                <div class="div-table-cell">
                    <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>
                </div>
            </div>
        </div>
        <br />
        <div class="Table">
            <div class="div-table-row">
                <asp:Button ID="btnCVO" runat="server" Text="Cash Vault Opname" Width="306px" OnClick="btnCVO_Click"/>
                
            </div>
            <div class="div-table-row">
                <asp:Button ID="btnSSB" runat="server" Text="Slip Setoran Bank" Width="306px" OnClick="btnSSB_Click"/>
            </div>
        </div>
    </fieldset>
</asp:Content>
