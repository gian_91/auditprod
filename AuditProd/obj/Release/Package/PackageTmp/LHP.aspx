﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="LHP.aspx.cs" Inherits="AuditProd.LHP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="Stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.10/themes/redmond/jquery-ui.css" />
    <script src="Scripts/Bootstrap/jquery-1.8.2.js"></script>
    <style type="text/css">
        .txt {
            padding-left: 5px;
            width: 155px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

        .Table {
            display: table;
            padding-left: 10px;
        }

        .TableHeader {
            display: table;
            padding-left: 300px;
        }

        .Table-tanggalbawah {
            display: table;
            position: relative;
            left: 5px;
        }

        .Table-ttd {
            display: table;
            position: relative;
            left: 70px;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 855px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: 140px;
        }

        .div-table-cell-kotak-kanan {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 5px;
        }

        .div-table-cell-kotak-kanan-2 {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 13px;
        }

        .div-table-header-kanan {
            width: 420px;
            height: 120px;
            border: 1px solid Blue;
            box-sizing: border-box;
            right: -500px;
            position: relative;
        }

        .div-grup {
            width: auto;
            display: table-column-group;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 2px; /* cellspacing:poor IE support for  this */
        }

        @media print {
            #printbtn, [id$=btnSave], [id$=btnExit], [id$=ExportExcel] {
                display: none;
            }
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <header>
        <link href="Scripts/Bootstrap/bootstrap.css" rel="stylesheet" />
        <script src="Scripts/Bootstrap/bootstrap.js"></script>
    </header>
    <fieldset>
        <legend></legend>
        <div class="Table">
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                </div>
                <div class="div-table-cell">
                    <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                </div>
                <div class="div-table-cell">
                    <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                </div>
                <div class="div-table-cell">
                    <asp:TextBox ID="txtIDSaveTemp" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                    <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false"/>
                </div>
            </div>
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                </div>
                <div class="div-table-cell">
                    <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>
                    <asp:TextBox ID="txbID_RDT" runat="server" ReadOnly="true" Width="83px" Visible="false" AutoPostBack="true"></asp:TextBox>
                    <asp:TextBox ID="txbAccountId" runat="server" ReadOnly="true" Width="140px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbID_DocTest" runat="server" ReadOnly="true" Width="140px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbUsername" runat="server" ReadOnly="true" Width="123px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbtabel" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbcbng" runat="server" Visible="false"></asp:TextBox>
                </div>
            </div>
        </div>
        <div align="center">
            <h4 style="margin: 0px">Laporan Hasil Pemeriksaan</h4>
            <h5 style="margin: 0px">Internal Audit & Control Department</h5>
        </div>
        <br />
        <h4 style="text-align: left">Acquisition Process</h4>
              <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
                <asp:UpdatePanel ID="upCrudGrid" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView1" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="10" OnPageIndexChanging="GridView1_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No" Visible="false" />
                            <asp:BoundField DataField="ID_LHP" HeaderText="Id LHP" Visible="false" />
                            <asp:BoundField DataField="LHP_Acquisition_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHP_Acquisition_TOD" HeaderText="Type Of Deviation" />
                            <asp:BoundField DataField="LHP_Acquisition_CaseFinding" HeaderText="Case Finding" />
                            <asp:ButtonField DataTextField="LHP_Acquisition_Tabel" HeaderText="Tabel" ButtonType="Link" CommandName="detail_click"  />
                            <asp:BoundField DataField="LHP_Acquisition_Recomd_Branch" HeaderText="Recomendation For Branch" />
                            <asp:BoundField DataField="LHP_Acquisition_Recomd_Management" HeaderText="Recomendation Management" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAdd" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAdd_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Detailed View</h3>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DetailsView ID="DetailsView1" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                                <Fields>
                            <asp:BoundField DataField="No" HeaderText="No" />
                            <asp:BoundField DataField="ID_LHP" HeaderText="Id LHP" Visible="false" />
                            <asp:BoundField DataField="LHP_Acquisition_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHP_Acquisition_TOD" HeaderText="Tod" />
                            <asp:BoundField DataField="LHP_Acquisition_CaseFinding" HeaderText="CaseFinding" />
                            <asp:BoundField DataField="LHP_Acquisition_Tabel" HeaderText="Tabel" />
                            <asp:BoundField DataField="LHP_Acquisition_Recomd_Branch" HeaderText="Recomd Branch" />
                            <asp:BoundField DataField="LHP_Acquisition_Recomd_Management" HeaderText="Recomd Management" />
                                </Fields>
                            </asp:DetailsView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="editModalLabel">Edit Record Acquisition</h3>
                </div>
                <asp:UpdatePanel ID="upEdit" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                                <tr>
                                    <td>
                            <asp:Label ID="lblCountryCode" runat="server" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddl1edit" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl1edit_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Category--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddl2edit" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl2edit_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddl3edit" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddl3edit_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddl4edit" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch:</label>
                            <asp:TextBox ID="txtRFB" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management:</label>
                            <asp:TextBox ID="txtRFM" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnSave_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="addModalLabel">Add New Record Acquisition</h3>
                </div>
                <asp:UpdatePanel ID="upAdd" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="txtidLHP" runat="server" Visible="false"></asp:TextBox>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddl1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl1_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Category--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                <%--<asp:TextBox ID="txtCountryName" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl2_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                <%--<asp:TextBox ID="txtContinent" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddl3" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddl3_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddl4" runat="server" Width="250px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch :</label>
                                    <asp:TextBox ID="txtTotalPopulation" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management :</label>
                                        <asp:TextBox ID="txtIndYear" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddRecord" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="delModalLabel">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="upDel" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>
        <h4 style="text-align: left">Collection Process</h4>
              <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView2" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView2_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView2_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No" Visible="false" />
                            <asp:BoundField DataField="ID_LHP" HeaderText="Id LHP" Visible="false" />
                            <asp:BoundField DataField="LHP_Collection_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHP_Collection_TOD" HeaderText="Type Of Deviation" />
                            <asp:BoundField DataField="LHP_Collection_CaseFinding" HeaderText="Case Finding" />
                            <asp:ButtonField DataTextField="LHP_Collection_Tabel" HeaderText="Tabel" ButtonType="Link" CommandName="detail_click"  />
                            <asp:BoundField DataField="LHP_Collection_Recomd_Branch" HeaderText="Recomendation For Branch" />
                            <asp:BoundField DataField="LHP_Collection_Recomd_Management" HeaderText="Recomendation Management" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAddCollection" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAddCollection_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <div id="Div1Detail" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H1">Detailed View</h3>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:DetailsView ID="DetailsView2" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                                <Fields>
                            <asp:BoundField DataField="No" HeaderText="No" />
                            <asp:BoundField DataField="ID_LHP" HeaderText="Id LHP" Visible="false" />
                            <asp:BoundField DataField="LHP_Collection_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHP_Collection_TOD" HeaderText="Tod" />
                            <asp:BoundField DataField="LHP_Collection_CaseFinding" HeaderText="CaseFinding" />
                            <asp:BoundField DataField="LHP_Collection_Tabel" HeaderText="Tabel" />
                            <asp:BoundField DataField="LHP_Collection_Recomd_Branch" HeaderText="Recomd Branch" />
                            <asp:BoundField DataField="LHP_Collection_Recomd_Management" HeaderText="Recomd Management" />
                                </Fields>
                            </asp:DetailsView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="GridView2" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="btnAddCollection" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="Div2Edit" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H2">Edit Record Collection</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                            <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddledit1coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit1coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Category--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddledit2coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit2coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddledit3coll" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddledit3coll_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddledit4coll" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch:</label>
                            <asp:TextBox ID="txbEditCollRFB" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management:</label>
                            <asp:TextBox ID="txbEditCollRFM" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="Label2" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="Button3" runat="server" Text="Update" CssClass="btn btn-info" OnClick="BtnSaveColl_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView2" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAddCollection" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="Div3Add" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H3">Add New Record Collection</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="TextBox4" runat="server" Visible="false"></asp:TextBox>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddladd1coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd1coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Category--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddladd2coll" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd2coll_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddladd3coll" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddladd3coll_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddladd4coll" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch :</label>
                                    <asp:TextBox ID="txbAddCollRFB" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management :</label>
                                        <asp:TextBox ID="txbAddCollRFM" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddColl" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddColl_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddColl" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="Div4Del" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H4">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelCol" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelCol_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelCol" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>
        <h4 style="text-align: left">Administration Process</h4>
              <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView3" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView3_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView3_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No" Visible="false" />
                            <asp:BoundField DataField="ID_LHP" HeaderText="Id LHP" Visible="false" />
                            <asp:BoundField DataField="LHP_Administration_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHP_Administration_TOD" HeaderText="Type Of Deviation" />
                            <asp:BoundField DataField="LHP_Administration_CaseFinding" HeaderText="Case Finding" />
                            <asp:ButtonField DataTextField="LHP_Administration_Tabel" HeaderText="Tabel" ButtonType="Link" CommandName="detail_click"  />
                            <asp:BoundField DataField="LHP_Administration_Recomd_Branch" HeaderText="Recomendation For Branch" />
                            <asp:BoundField DataField="LHP_Administration_Recomd_Management" HeaderText="Recomendation Management" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAddAdministration" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAddAdministration_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="Div2EditAdm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H6">Edit Record Administration</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                            <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddledit1adm" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit1adm_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Category--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddledit2adm" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit2adm_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddledit3adm2" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddledit3adm2_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select Finding--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddledit4adm" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch:</label>
                            <asp:TextBox ID="txbeditAdmRFB" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management:</label>
                            <asp:TextBox ID="txbeditAdmRFM" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="Label4" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="btnEditAdmistration" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnEditAdmistration_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView3" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAddAdministration" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="Div3AddAdm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H7">Add New Record Administration</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="TextBox3" runat="server" Visible="false"></asp:TextBox>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddladd1adm" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd1adm_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Category--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddladd2adm" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd2adm_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddladd3adm" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddladd3adm_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddladd4adm" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch :</label>
                                    <asp:TextBox ID="txbAddAdmRFB" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management :</label>
                                        <asp:TextBox ID="txbAddAdmRFM" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddAdm" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddAdm_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddAdm" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="DivAdmDel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H8">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelAdm" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelAdm_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelAdm" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>
        <h4 style="text-align: left">CPU Process</h4>
              <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView4" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView4_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView4_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No" Visible="false" />
                            <asp:BoundField DataField="ID_LHP" HeaderText="Id LHP" Visible="false" />
                            <asp:BoundField DataField="LHP_CPU_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHP_CPU_TOD" HeaderText="Type Of Deviation" />
                            <asp:BoundField DataField="LHP_CPU_CaseFinding" HeaderText="Case Finding" />
                            <asp:ButtonField DataTextField="LHP_CPU_Tabel" HeaderText="Tabel" ButtonType="Link" CommandName="detail_click"  />
                            <asp:BoundField DataField="LHP_CPU_Recomd_Branch" HeaderText="Recomendation For Branch" />
                            <asp:BoundField DataField="LHP_CPU_Recomd_Management" HeaderText="Recomendation Management" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAddCPU" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAddCPU_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="Div2EditCPU" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H5">Edit Record CPU</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                            <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddledit1CPU" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit1CPU_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Category--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddledit2CPU" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit2CPU_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddledit3CPU" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddledit3CPU_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select Finding--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddledit4CPU" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch:</label>
                            <asp:TextBox ID="txbeditCPURFB" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management:</label>
                            <asp:TextBox ID="txbeditCPURFM" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="Label6" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="btnEditCPU" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnEditCPU_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView4" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAddCPU" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="Div3AddCPU" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H9">Add New Record CPU</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="TextBox5" runat="server" Visible="false"></asp:TextBox>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddladd1CPU" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd1CPU_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Category--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddladd2CPU" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd2CPU_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddladd3CPU" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddladd3CPU_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddladd4CPU" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch :</label>
                                    <asp:TextBox ID="txbAddCpuRFB" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management :</label>
                                        <asp:TextBox ID="txbAddCpuRFM" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddCpu2" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddCpu2_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddCpu" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="DivCPUDel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H10">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="HiddenField3" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelCPU" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelCPU_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelCPU" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>
        <h4 style="text-align: left">HR Process</h4>
              <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView5" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView5_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView5_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No" Visible="false" />
                            <asp:BoundField DataField="ID_LHP" HeaderText="Id LHP" Visible="false" />
                            <asp:BoundField DataField="LHP_HR_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHP_HR_TOD" HeaderText="Type Of Deviation" />
                            <asp:BoundField DataField="LHP_HR_CaseFinding" HeaderText="Case Finding" />
                            <asp:ButtonField DataTextField="LHP_HR_Tabel" HeaderText="Tabel" ButtonType="Link" CommandName="detail_click"  />
                            <asp:BoundField DataField="LHP_HR_Recomd_Branch" HeaderText="Recomendation For Branch" />
                            <asp:BoundField DataField="LHP_HR_Recomd_Management" HeaderText="Recomendation Management" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAddHR" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAddHR_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="Div2EditHR" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H11">Edit Record HR</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                            <asp:Label ID="Label7" runat="server" Visible="false"></asp:Label>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddledit1HR" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit1HR_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Category--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddledit2HR" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit2HR_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddledit3HR" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddledit3HR_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select Finding--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddledit4HR" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch:</label>
                            <asp:TextBox ID="txbeditHRRFB" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management:</label>
                            <asp:TextBox ID="txbeditHRRFM" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="Label8" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="btnEditHR" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnEditHR_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView5" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAddHR" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="Div3AddHR" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H12">Add New Record HR</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="TextBox6" runat="server" Visible="false"></asp:TextBox>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddladd1HR" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd1HR_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Category--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddladd2HR" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd2HR_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddladd3HR" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddladd3HR_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddladd4HR" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch :</label>
                                    <asp:TextBox ID="txbAddHRRFB" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management :</label>
                                        <asp:TextBox ID="txbAddHRRFM" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddHR2" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddHR2_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddHR" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="DivHRDel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H13">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="HiddenField4" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelHR" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelHR_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelHR" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>
        <h4 style="text-align: left">Procurement Process</h4>
            <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
                <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView6" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView6_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" OnPageIndexChanging="GridView6_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None" >
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No" Visible="false" />
                            <asp:BoundField DataField="ID_LHP" HeaderText="Id LHP" Visible="false" />
                            <asp:BoundField DataField="LHP_Procurement_Category" HeaderText="Category" />
                            <asp:BoundField DataField="LHP_Procurement_TOD" HeaderText="Type Of Deviation" />
                            <asp:BoundField DataField="LHP_Procurement_CaseFinding" HeaderText="Case Finding" />
                            <asp:ButtonField DataTextField="LHP_Procurement_Tabel" HeaderText="Tabel" ButtonType="Link" CommandName="detail_click"  />
                            <asp:BoundField DataField="LHP_Procurement_Recomd_Branch" HeaderText="Recomendation For Branch" />
                            <asp:BoundField DataField="LHP_Procurement_Recomd_Management" HeaderText="Recomendation Management" />
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAddProc" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAddProc_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="Div2EditProc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H14">Edit Record Procurement</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                            <asp:Label ID="Label9" runat="server" Visible="false"></asp:Label>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddledit1Proc" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit1Proc_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Category--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddledit2Proc" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddledit2Proc_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddledit3Proc" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddledit3Proc_SelectedIndexChanged">
                                            <asp:ListItem Text="--Select Finding--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddledit4Proc" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch:</label>
                                        <asp:TextBox ID="txbeditProcRFB" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management:</label>
                                        <asp:TextBox ID="txbeditProcRFM" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="Label10" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="btnEditProc" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnEditProc_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView6" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAddProc" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="Div3AddProc" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H15">Add New Record Procurement</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="TextBox7" runat="server" Visible="false"></asp:TextBox>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Category :</label>
                                        <asp:DropDownList ID="ddladd1Proc" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd1Proc_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text="--Select Category--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td> 
                                    <label style="font-weight: bold; font-size: 15px">Type Of Deviation :</label>
                                        <asp:DropDownList ID="ddladd2Proc" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddladd2Proc_SelectedIndexChanged" Width="450px">
                                            <asp:ListItem Text = "--Select Type--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Case Finding :</label>
                                        <asp:DropDownList ID="ddladd3Proc" runat="server" Width="450px" AutoPostBack="true" OnSelectedIndexChanged="ddladd3Proc_SelectedIndexChanged">
                                            <asp:ListItem Text = "--Select Finding--" Value = ""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Tabel :</label>
                                        <asp:DropDownList ID="ddladd4Proc" runat="server" Width="450px">
                                            <asp:ListItem Text="--Select Tabel--" Value=""></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Branch :</label>
                                    <asp:TextBox ID="txbAddProcRFB" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label style="font-weight: bold; font-size: 15px">Recomendation For Management :</label>
                                        <asp:TextBox ID="txbAddProcRFM" runat="server" AutoCompleteType="Disabled" TextMode="MultiLine" ></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">                          
                            <asp:Button ID="btnAddProc2" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddProc2_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddProc" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="DivProcDel" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="H16">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="HiddenField5" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelProc" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelProc_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelProc" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>
        <br />
        <br />
        <div class="TableButton">
            <asp:Button ID="btnExit" runat="server" Text="Exit" Width="100px" OnClick="btnExit_Click" />
            <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </fieldset>
</asp:Content>
