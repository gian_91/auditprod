﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Procurement.aspx.cs" Inherits="AuditProd.Procurement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <style type="text/css">
        td {
            text-align: center;
            border: groove;
        }

        .tdz {
            text-align: left;
        }

        th {
            font-size: small;
            text-align: left;
            border: none 0px;
        }

        .txt100rb {
            width: 100px;
        }

        .txt50rb {
            width: 100px;
        }

        .txt {
            padding-left: 5px;
            width: 155px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }


        .bordered {
            width: 314px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 310px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 292px;
            height: 225px;
            /*padding: 3px;*/
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
            padding-left: 10px;
        }

        .TableHeader {
            display: table;
            padding-left: 300px;
        }

        .Table-ttd {
            display: table;
            position: relative;
            left: 20px;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 606px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: 140px;
        }

        .div-table-cell-kotak-kanan {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 5px;
        }

        .div-table-cell-kotak-kanan-2 {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 13px;
        }

        .div-table-header-kanan {
            width: 420px;
            height: 120px;
            border: 1px solid Blue;
            box-sizing: border-box;
            right: -500px;
            position: relative;
        }

        .div-grup {
            width: auto;
            display: table-column-group;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 2px; /* cellspacing:poor IE support for  this */
            font-style:normal;
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
            text-align:center;
        }

       .div-table-rowMid-txb {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowMid-ttd {
            display: table-row;
            text-align:center;
            width: auto;
            clear: both;
        }

        .div-table-colMid {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

       .div-table-colNomer {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 40px;
            height: 35px;
            background-color: #aaa9a9
        }

        .div-table-colInventaris {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 350px;
            height: 35px;
            background-color: #aaa9a9
        }

        .div-table-colData {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 110px;
            height: 35px;
            background-color: #aaa9a9;
        }

        .div-table-colRemarks {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 220px;
            height: 35px;
            background-color: #aaa9a9

        }

        .div-table-colSpaceAsset {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 940px;
            height: 35px;
            background-color: #aaa9a9;
        }

        .div-table-colNomer-txb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 40px;
            height: 25px;
            background-color: whitesmoke;
        }

        .div-table-colInventaris-txb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 345px;
            height: 25px;
            background-color: whitesmoke;
        }

        .div-table-colInventaris-txb-C{
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 350px;
            height: 25px;
            background-color: whitesmoke;
        }

        .div-table-colData-txb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 110px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colData-txb-C{
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 80px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colRemarks-txb {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 220px;
            height: 52px;
            background-color: whitesmoke;
        }

        .div-table-colRemarks-txb-C {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 310px;
            height: 52px;
            background-color: whitesmoke;
        }

         .div-table-colAll {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 940px;
            height: 32px;
            background-color: whitesmoke;
        }

  
        .div-table-colMidTextMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidTextBoxMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidVarianceMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 605px;
            height: 35px;
            background-color: #f85c5c;
        }
        
        .div-table-colMidVarianceTotal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 185px;
            height: 35px;
            background-color: #f85c5c;
        }

        .div-tableMid-TextArea {
            display: table;
            width: 860px;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-colTanggal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 127px;
            height: 35px;
            background-color: none;
        }

        .div-table-colTanggal2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 105px;
            height: 35px;
            background-color: none;
            padding-left: 100px;
        }

        .div-table-colTanggal3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 70px;
            height: 35px;
            background-color: none;
        }

        .div-table-colMidSelisih {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 850px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidSelisihtextArea {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 840px;
            height: 185px;
            background-color: whitesmoke;
        }

        .div-table-colTTD {
            float: right;
            display: table-column;
            width: 315px;
            height: 20px;
            background-color: none;
        }

        .textarea2 {
            font-family: 'Times New Roman';
            width: 840px;
            height: 175px;
            font-size: 15px;
        }

        .textarea3 {
            font-family: 'Times New Roman';
            width: 105px;
            font-size: 14px;
        }

        .display-validate {
            clear: both;
            display: block;
            position: relative;
            left: 210px;
            bottom: 30px;
        }

        @media print {
            #printbtn, [id$=btnSave], [id$=btnExit], [id$=ExportExcel] {
                display: none;
            }
        }
       </style>
    <script type="text/javascript">
        function HideLabel() {
            var seconds = 2;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
    <script>
        $(function () {
            $("[id$=TxbTnglBwh]").datepicker({
                dateFormat: 'dd/mm/yy',
                yearRange: '1950:2020',
                changeMonth: true,
                changeYear: true
            });
        });
        $(function () {
            $("[id*=TxbA1Actual],[id*=TxbA1Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA1Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA1Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA1Diff]").val(total);
                $("[id*=TxbA1Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA2Actual],[id*=TxbA2Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA2Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA2Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA2Diff]").val(total);
                $("[id*=TxbA2Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA3Actual],[id*=TxbA3Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA3Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA3Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA3Diff]").val(total);
                $("[id*=TxbA3Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA4Actual],[id*=TxbA4Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA4Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA4Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA4Diff]").val(total);
                $("[id*=TxbA4Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA5Actual],[id*=TxbA5Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA5Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA5Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA5Diff]").val(total);
                $("[id*=TxbA5Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA6Actual],[id*=TxbA6Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA6Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA6Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA6Diff]").val(total);
                $("[id*=TxbA6Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA7Actual],[id*=TxbA7Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA7Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA7Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA7Diff]").val(total);
                $("[id*=TxbA7Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA8Actual],[id*=TxbA8Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA8Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA8Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA8Diff]").val(total);
                $("[id*=TxbA8Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA9Actual],[id*=TxbA9Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA9Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA9Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA9Diff]").val(total);
                $("[id*=TxbA9Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA10Actual],[id*=TxbA10Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA10Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA10Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA10Diff]").val(total);
                $("[id*=TxbA10Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA11Actual],[id*=TxbA11Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA11Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA11Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA11Diff]").val(total);
                $("[id*=TxbA11Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA12Actual],[id*=TxbA12Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA12Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA12Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA12Diff]").val(total);
                $("[id*=TxbA12Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA13Actual],[id*=TxbA13Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA13Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA13Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA13Diff]").val(total);
                $("[id*=TxbA13Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA14Actual],[id*=TxbA14Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA14Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA14Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA14Diff]").val(total);
                $("[id*=TxbA14Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA15Actual],[id*=TxbA15Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA15Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA15Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA15Diff]").val(total);
                $("[id*=TxbA15Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA16Actual],[id*=TxbA16Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA16Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA16Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA16Diff]").val(total);
                $("[id*=TxbA16Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA17Actual],[id*=TxbA17Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA17Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA17Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA17Diff]").val(total);
                $("[id*=TxbA17Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA18Actual],[id*=TxbA18Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA18Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA18Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA18Diff]").val(total);
                $("[id*=TxbA18Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA19Actual],[id*=TxbA19Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA19Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA19Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA19Diff]").val(total);
                $("[id*=TxbA19Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=TxbA20Actual],[id*=TxbA20Data]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=TxbA20Data]").val() || 0);
                var Qnt = parseFloat($("[id*=TxbA20Actual]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=TxbA20Diff]").val(total);
                $("[id*=TxbA20Diff]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=txtAge],[id*=txtSalary]").keyup(function () {
                debugger;
                var priceB = parseFloat($("[id*=txtAge]").val() || 0);
                var QntB = parseFloat($("[id*=txtSalary]").val() || 0);
                var total = parseFloat(priceB - QntB);
                $("[id*=txtCountry]").val(total);
                $("[id*=txtCountry]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=txtNewAge],[id*=txtNewSalary]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=txtNewAge]").val() || 0);
                var Qnt = parseFloat($("[id*=txtNewSalary]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=txtNewCountry]").val(total);
                $("[id*=txtNewCountry]").attr("readonly", true);
            });
        });

        $(function () {
            $("[id*=txtAge2],[id*=txtSalary2]").keyup(function () {
                debugger;
                var priceB = parseFloat($("[id*=txtAge2]").val() || 0);
                var QntB = parseFloat($("[id*=txtSalary2]").val() || 0);
                var total = parseFloat(priceB - QntB);
                $("[id*=txtCountry2]").val(total);
                $("[id*=txtCountry2]").attr("readonly", true);
            });
        });
        $(function () {
            $("[id*=txtNewAge2],[id*=txtNewSalary2]").keyup(function () {
                debugger;
                var price = parseFloat($("[id*=txtNewAge2]").val() || 0);
                var Qnt = parseFloat($("[id*=txtNewSalary2]").val() || 0);
                var total = parseFloat(price - Qnt);
                $("[id*=txtNewCountry2]").val(total);
                $("[id*=txtNewCountry2]").attr("readonly", true);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <fieldset>
        <legend></legend>
        <div class="Table">
                <div class="div-table-header-kanan">
                <label align="center">IAC GL - 08</label>
                <hr />
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="TxbDibuatHead">Dibuat </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbDibuatHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="TxbDireviewHead">Direview </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbDireviewHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="TxbTanggalHeadDibuat">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbTanggalHeadDibuat" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="TxbTanggalHeadDireview">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbTanggalHeadDireview" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                </div>
            </div>
                        <div class="Table">
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSaveTemp" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                        <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false" />
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>

        <div align="center" class="TableHeader">
            <h2>Berita Acara Fixed Asset</h2>
            <h4>(Internal Audit & Control Department)</h4>
        </div>
        <br />
        <br />
        <div class="Table">
            <div class="div-tableMid">
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" style="font-size: 18px; font-weight: bold">No</div>
                    <div class="div-table-colInventaris" style="font-size: 18px; font-weight: bold">A. Inventaris - Jenis Daftar Barang</div>
                    <div class="div-table-colData" style="font-size: 18px; font-weight: bold">Data</div>
                    <div class="div-table-colData" style="font-size: 18px; font-weight: bold">Actual</div>
                    <div class="div-table-colData" style="font-size: 18px; font-weight: bold">Difference</div>
                    <div class="div-table-colRemarks" style="font-size: 18px; font-weight: bold">Remarks</div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">1</div>
                    <div class="div-table-colInventaris-txb">Meja Kerja Staff + Meja Admin</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA1Data" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px" ></asp:TextBox>
                        <asp:RegularExpressionValidator ID="Reg1" runat="server" ControlToValidate="TxbA1Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA1Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px" ></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TxbA1Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA1Diff" runat="server" Width="95px" ReadOnly="false" Height="18px" ></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA1Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">2</div>
                    <div class="div-table-colInventaris-txb">Meja Kerja SO (Meja Kerja Panjang Berkaca)</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA2Data" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TxbA2Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA2Actual" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="TxbA2Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA2Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA2Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">3</div>
                    <div class="div-table-colInventaris-txb">Meja Makan Kayu Panjang</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA3Data" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="TxbA3Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA3Actual" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="TxbA3Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA3Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA3Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">4</div>
                    <div class="div-table-colInventaris-txb">Kursi Panjang Mitra</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA4Data" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="TxbA4Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA4Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="TxbA4Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA4Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA4Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">5</div>
                    <div class="div-table-colInventaris-txb">Rak Buku</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA5Data" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="TxbA5Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA5Actual" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="TxbA5Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA5Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA5Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">6</div>
                    <div class="div-table-colInventaris-txb">Lemari Cucian Piring</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA6Data" runat="server" Width="95px" AutoCompleteType="Disabled"  CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="TxbA6Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA6Actual" runat="server" Width="95px"  AutoCompleteType="Disabled"  CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ControlToValidate="TxbA6Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA6Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA6Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">7</div>
                    <div class="div-table-colInventaris-txb">White Board</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA7Data" runat="server" Width="95px" AutoCompleteType="Disabled"  CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="TxbA7Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA7Actual" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="TxbA7Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA7Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA7Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">8</div>
                    <div class="div-table-colInventaris-txb">Dispenser Panas Dingin</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA8Data" runat="server" Width="95px" AutoCompleteType="Disabled"  CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server" ControlToValidate="TxbA8Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA8Actual" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" ControlToValidate="TxbA8Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA8Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA8Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">9</div>
                    <div class="div-table-colInventaris-txb">Dispenser Guci + Kaki</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA9Data" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server" ControlToValidate="TxbA9Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA9Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server" ControlToValidate="TxbA9Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA9Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA9Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">10</div>
                    <div class="div-table-colInventaris-txb">Kipas Angin</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA10Data" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server" ControlToValidate="TxbA10Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA10Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server" ControlToValidate="TxbA10Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA10Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA10Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">11</div>
                    <div class="div-table-colInventaris-txb">Detektor Uang Palsu (RO Dan Cashier)</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA11Data" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server" ControlToValidate="TxbA11Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA11Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server" ControlToValidate="TxbA11Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA11Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA11Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">12</div>
                    <div class="div-table-colInventaris-txb">Telepon Meja</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA12Data" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server" ControlToValidate="TxbA12Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA12Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server" ControlToValidate="TxbA12Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA12Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA12Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">13</div>
                    <div class="div-table-colInventaris-txb">Tabung Pemadam Kebakaran</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA13Data" runat="server" Width="95px" AutoCompleteType="Disabled"  CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server" ControlToValidate="TxbA13Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA13Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator25" runat="server" ControlToValidate="TxbA13Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA13Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA13Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">14</div>
                    <div class="div-table-colInventaris-txb">Kamera Pocket</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA14Data" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server" ControlToValidate="TxbA14Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA14Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator27" runat="server" ControlToValidate="TxbA14Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA14Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA14Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">15</div>
                    <div class="div-table-colInventaris-txb">Cash Box</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA15Data" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator28" runat="server" ControlToValidate="TxbA15Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA15Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator29" runat="server" ControlToValidate="TxbA15Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA15Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA15Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">16</div>
                    <div class="div-table-colInventaris-txb">Kursi Kerja Biru</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA16Data" runat="server" Width="95px" AutoCompleteType="Disabled"  CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator30" runat="server" ControlToValidate="TxbA16Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA16Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator31" runat="server" ControlToValidate="TxbA16Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA16Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA16Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">17</div>
                    <div class="div-table-colInventaris-txb">Meja Tamu Kaca</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA17Data" runat="server" Width="95px"  AutoCompleteType="Disabled"  CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator32" runat="server" ControlToValidate="TxbA17Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA17Actual" runat="server" Width="95px" AutoCompleteType="Disabled"  CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator33" runat="server" ControlToValidate="TxbA17Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA17Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA17Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">18</div>
                    <div class="div-table-colInventaris-txb">Meja Olimpic RM</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA18Data" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator34" runat="server" ControlToValidate="TxbA18Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA18Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator35" runat="server" ControlToValidate="TxbA18Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA18Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA18Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">19</div>
                    <div class="div-table-colInventaris-txb">Kursi Lipat Bermeja</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA19Data" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator36" runat="server" ControlToValidate="TxbA19Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA19Actual" runat="server" Width="95px"  AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator37" runat="server" ControlToValidate="TxbA19Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>

                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA19Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA19Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
                    <div class="div-table-rowMid-txb">
                    <div class="div-table-colNomer-txb" style="font-size: 15px; font-weight: normal" align="center">20</div>
                    <div class="div-table-colInventaris-txb">Lemari Kayu</div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA20Data" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator38" runat="server" ControlToValidate="TxbA20Data" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>
                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA20Actual" runat="server" Width="95px" AutoCompleteType="Disabled" CausesValidation="true" Height="18px"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator39" runat="server" ControlToValidate="TxbA20Actual" ErrorMessage="Only Number" SetFocusOnError="true" ValidationExpression="^[0-9]*$" ForeColor="Red"></asp:RegularExpressionValidator>
                    </div>

                    <div class="div-table-colData-txb">
                        <asp:TextBox ID="TxbA20Diff" runat="server" Width="95px" ReadOnly="false" Height="18px"></asp:TextBox>
                    </div>
                    <div class="div-table-colRemarks-txb">
                        <textarea class="textarea2" runat="server" id="TxbA20Remarks" rows="2" style="width: 208px; height: 38px;"></textarea>
                    </div>
                </div>
            <div class="div-table-rowMid">
                <div class="div-table-colSpaceAsset" style="font-size: 18px; font-weight: bold">B. Fix Asset Dan Asset Lainnya</div>
                <asp:GridView ID="GridView1" runat="server" ShowHeaderWhenEmpty="True"
                    AutoGenerateColumns="False" OnRowDeleting="RowDeleting"
                    OnRowCancelingEdit="cancelRecord" OnRowEditing="editRecord"
                    OnRowUpdating="updateRecord" CellPadding="4"
                    EnableModelValidation="True" GridLines="None" Width="950px"
                    ForeColor="#333333">
                    <RowStyle HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#7C6F57" />
                    <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
 
            <Columns>
 
            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="5px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>No</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID ="lblId" runat="server" Text='<%#Bind("ID_Proc_B")%>'></asp:Label>
            </ItemTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField ItemStyle-Width="150px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>Inventaris</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID ="lblName" runat="server" Text='<%#Bind("B_AssetLainnya_Inv") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtName" runat="server" Text='<%#Bind("B_AssetLainnya_Inv") %>' MaxLength="50" AutoCompleteType="Disabled"></asp:TextBox>        
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewName" runat="server" MaxLength="50" AutoCompleteType="Disabled"></asp:TextBox>       
            </FooterTemplate>
            </asp:TemplateField>
           
            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>Data</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID="lblAge" runat ="server" Text='<%#Bind("B_AssetLainnya_Data") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID ="txtAge" runat="server" Text='<%#Bind("B_AssetLainnya_Data") %>' MaxLength="5" AutoCompleteType="Disabled"></asp:TextBox>         
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewAge" runat="server" MaxLength="5" AutoCompleteType="Disabled"></asp:TextBox>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>Actual</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID = "lblSalary" runat="server" Text='<%#Bind("B_AssetLainnya_Actual") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtSalary" runat="server" Text='<%#Bind("B_AssetLainnya_Actual") %>'  MaxLength="5" AutoCompleteType="Disabled"></asp:TextBox>         
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewSalary" runat="server" MaxLength="5" AutoCompleteType="Disabled"></asp:TextBox>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz"> 
            <HeaderTemplate>Diff</HeaderTemplate>
            <ItemTemplate>
            <asp:Label ID = "lblCountry" runat="server" Text='<%#Bind("B_AssetLainnya_Diff") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtCountry" runat="server" Text='<%#Bind("B_AssetLainnya_Diff") %>' ReadOnly="false"></asp:TextBox>      
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewCountry" runat="server"  AutoCompleteType="Disabled" ReadOnly="false"></asp:TextBox>
            </FooterTemplate>
            </asp:TemplateField>
 
            <asp:TemplateField ItemStyle-Width="150px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>Remarks</HeaderTemplate>
            <ItemTemplate> 
            <asp:Label ID = "lblCity" runat="server" Text='<%#Bind("B_AssetLainnya_Remarks") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:TextBox ID="txtCity" runat="server" Text='<%#Bind("B_AssetLainnya_Remarks") %>' TextMode="MultiLine" Columns="50" Rows="3" ></asp:TextBox>
            </EditItemTemplate>
            <FooterTemplate>
            <asp:TextBox ID="txtNewCity" runat="server" TextMode="MultiLine" Columns="50" Rows="3" ></asp:TextBox>
            </FooterTemplate>        
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Width="99px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
            <HeaderTemplate>Operation</HeaderTemplate>
            <ItemTemplate>
<%--        <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
            <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" CausesValidation="true" OnClientClick="return confirm('Are you sure?')" />--%>
            <asp:LinkButton ID="btnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
            <asp:LinkButton ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" CausesValidation="true" OnClientClick="return confirm('Are you sure?')"></asp:LinkButton>
            </ItemTemplate>
            <EditItemTemplate>
            <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" />
            <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" />
            </EditItemTemplate> 
            <FooterTemplate>
<%--        <asp:Button ID="btnNewInsert" runat="server" Text="Insert" OnClick="InsertNewRecord"/>
            <asp:Button ID="btnNewCancel" runat="server" Text="Cancel" OnClick="AddNewCancel" CausesValidation="false" />--%>
            <asp:LinkButton ID="btnNewInsert" runat="server" Text="Insert" OnClick="InsertNewRecord" ></asp:LinkButton>
            <asp:LinkButton ID="btnNewCancel" runat="server" Text="Cancel" OnClick="AddNewCancel" CausesValidation="false"></asp:LinkButton>
            </FooterTemplate>        
            </asp:TemplateField>           
            </Columns>
            <EmptyDataTemplate>
                      No record available                    
            </EmptyDataTemplate>       
        </asp:GridView>

                <asp:GridView ID="GridView2" runat="server" ShowHeaderWhenEmpty="True"
                    AutoGenerateColumns="False" OnRowDeleting="RowDeletingTemp"
                    OnRowCancelingEdit="cancelRecordTemp" OnRowEditing="editRecordTemp"
                    OnRowUpdating="updateRecordTemp" CellPadding="4"
                    EnableModelValidation="True" GridLines="None" Width="950px"
                    ForeColor="#333333">
                    <RowStyle HorizontalAlign="Center" />
                    <AlternatingRowStyle BackColor="White" />
                    <EditRowStyle BackColor="#7C6F57" />
                    <FooterStyle BackColor="#1C5E55" ForeColor="White" Font-Bold="True" />
                    <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#E3EAEB" />
                    <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />

                    <Columns>

                        <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="5px" ItemStyle-CssClass="tdz">
                            <HeaderTemplate>No</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblId2" runat="server" Text='<%#Bind("ID_Proc_B")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="150px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
                            <HeaderTemplate>Inventaris</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblName2" runat="server" Text='<%#Bind("B_AssetLainnya_Inv") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtName2" runat="server" Text='<%#Bind("B_AssetLainnya_Inv") %>' MaxLength="50" AutoCompleteType="Disabled"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewName2" runat="server" MaxLength="50" AutoCompleteType="Disabled"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
                            <HeaderTemplate>Data</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblAge2" runat="server" Text='<%#Bind("B_AssetLainnya_Data") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtAge2" runat="server" Text='<%#Bind("B_AssetLainnya_Data") %>' MaxLength="5" AutoCompleteType="Disabled"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtAge2" runat="server" Text="*" ToolTip="Enter age" ControlToValidate="txtAge2"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revtxtAge2" runat="server" Text="*" ToolTip="Enter numericvalue" ControlToValidate="txtAge2" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewAge2" runat="server" MaxLength="5" AutoCompleteType="Disabled"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtNewAge2" runat="server" Text="*" ToolTip="Enter age" ControlToValidate="txtNewAge2"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revNewtxtAge2" runat="server" Text="*" ToolTip="Enter numericvalue" ControlToValidate="txtNewAge2" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
                            <HeaderTemplate>Actual</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSalary2" runat="server" Text='<%#Bind("B_AssetLainnya_Actual") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSalary2" runat="server" Text='<%#Bind("B_AssetLainnya_Actual") %>' MaxLength="5" AutoCompleteType="Disabled"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtSalary2" runat="server" Text="*" ToolTip="Enter salary" ControlToValidate="txtSalary2"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revtxtSalary2" runat="server" Text="*" ToolTip="Enter numericvalue" ControlToValidate="txtSalary2" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewSalary2" runat="server" MaxLength="5" AutoCompleteType="Disabled"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtNewSalary2" runat="server" Text="*" ToolTip="Enter salary" ControlToValidate="txtNewSalary2"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revtxtNewSalary2" runat="server" Text="*" ToolTip="Enter numericvalue" ControlToValidate="txtNewSalary2" ValidationExpression="^[0-9]+$"></asp:RegularExpressionValidator>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="10px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
                            <HeaderTemplate>Diff</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCountry2" runat="server" Text='<%#Bind("B_AssetLainnya_Diff") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCountry2" runat="server" Text='<%#Bind("B_AssetLainnya_Diff") %>' ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtCountry2" runat="server" Text="*" ToolTip="Entercountry" ControlToValidate="txtCountry2"></asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewCountry2" runat="server" ></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvtxtNewCountry2" runat="server" Text="*" ToolTip="Entercountry" ControlToValidate="txtNewCountry2"></asp:RequiredFieldValidator>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="150px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
                            <HeaderTemplate>Remarks</HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblCity2" runat="server" Text='<%#Bind("B_AssetLainnya_Remarks") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtCity2" runat="server" Text='<%#Bind("B_AssetLainnya_Remarks") %>' TextMode="MultiLine" Columns="50" Rows="3"></asp:TextBox>
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:TextBox ID="txtNewCity2" runat="server" TextMode="MultiLine" Columns="50" Rows="3"></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="99px" ItemStyle-Height="20px" ItemStyle-CssClass="tdz">
                            <HeaderTemplate>Operation</HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEdit2" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                <asp:LinkButton ID="btnDelete2" runat="server" CommandName="Delete" Text="Delete" CausesValidation="true" OnClientClick="return confirm('Are you sure?')"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:Button ID="btnUpdate2" runat="server" CommandName="Update" Text="Update" />
                                <asp:Button ID="btnCancel2" runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" />
                            </EditItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton ID="btnNewInsert2" runat="server" Text="Insert" OnClick="InsertNewRecordTemp" ></asp:LinkButton>
                                <asp:LinkButton ID="btnNewCancel2" runat="server" Text="Cancel" OnClick="AddNewCancelTemp" CausesValidation="false"></asp:LinkButton>        
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No record available                    
                    </EmptyDataTemplate>
                </asp:GridView>

                </div>
                <br />
                <br />
                <div class="div-table-colAll" align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="Add New Record" OnClick="AddNewRecord"  />  
                    <%--<asp:Button ID="btnAddBassetB" runat="server" Text="Add" />--%>
                </div>
            </div>
        </div>
        <br />
        <div class="Table">
            <div class="Table" align="center">
                <div class="div-table-colTanggal">
                    <label>Dilaksanakan Di</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbDilksnakan" runat="server" Width="210px" AutoCompleteType="Disabled" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal2">
                    <label>Pada Tanggal</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbTnglBwh" runat="server" Width="110px" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal3">
                    <label>Pada Jam</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbJamBawah" runat="server" Width="110px" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="Table">
            <div class="Table-ttd">
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Diperiksa Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Disaksikan Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Mengetahui,</div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                </div>
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Internal Audit&Control</div>
                    <div class="div-table-colTTD" align="Center">Admin/Kasir</div>
                    <div class="div-table-colTTD" align="Center">Branch Manager</div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <br />
        <div class="Table">
            <div class="TableButton">                
                <input type="button" value="Print" onclick="window.print()" id="printbtn">
                <asp:Button ID="btnExit" runat="server" Text="Exit" Width="80px" OnClick="btnExit_Click" />
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="80px" OnClick="btnSave_Click" />
                <%--<asp:Button ID="ExportExcel" runat="server" Text="Export" Width="80px" OnClick="ExportExcel_Click" />--%>
                <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false"></asp:Label>
            </div>
        </div>
        <div>
            <asp:TextBox ID="TxtCreateDate" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TxtReviewDate" runat="server" Visible="false"></asp:TextBox>
        </div>
    </fieldset>
</asp:Content>
