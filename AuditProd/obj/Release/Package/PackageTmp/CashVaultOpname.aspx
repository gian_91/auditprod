﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CashVaultOpname.aspx.cs" Inherits="AuditProd.CashVaultOpname" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="Scripts/jquery-1.8.2.js"></script>
    <script src="Scripts/jquery-ui-1.8.24.js"></script>

    <script src="Scripts/jquery.number.js"></script>
    <script src="Scripts/jquery.number.min.js"></script>
    <script src="Scripts/autoNumeric.js"></script>

    <script type="text/javascript">
        function HideLabel() {
            var seconds = 2;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
    <script>
        jQuery(function ($) {
            $('[id$=TxbCollProjc],[id$=TxbJmlhPreciosure],[id$=TxbJmlhPartial],[id$=TxbJmlhWritOff],[id$=TxbJmlhUjam],[id$=TxbSelisih],[id$=txbTotal100rb],[id$=txbTotal50rb],[id$=txbTotal20rb],[id$=txbTotal10rb],[id$=TxbVariance],' +
                '[id$=txbTotal5rb],[id$=txbTotal2rb],[id$=txbTotal1rb],[id$=txbTotal5rts],[id$=txbTotal2rts],[id$=txbTotal1rts],[id$=txbTotal50],[id$=txbTotal25],[id$=txbTotalUangFisik],[id$=TxbCollProjc]').autoNumeric(
                {
                    aSep: '',
                    aDec: '.',
                    vMin: '-9999999999'
                });
        });
    </script>
    <style type="text/css">
        .txt100rb {
            width: 100px;
        }

        .txt50rb {
            width: 100px;
        }

        .txt {
            padding-left: 5px;
            width:155px;
        }

        
        .bordered {
            width: 314px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 310px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 292px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
            padding-left: 10px;
        }

        .Table-ttd {
            display: table;
            position: relative;
            right: 50px;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 728px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: 140px;
        }

        .div-table-cell-kotak-kanan {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 5px;
        }

        .div-table-cell-kotak-kanan-2 {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 13px;
        }

        .div-table-header-kanan {
            width: 420px;
            height: 120px;
            border: 1px solid Blue;
            box-sizing: border-box;
            right: -500px;
            position: relative;
        }

        .div-grup {
            width: auto;
            display: table-column-group;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowMid-ttd {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-colMid {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidPecahan {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 250px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidPecahan2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 250px;
            height: 35px;
            background-color: orange;
        }

        .div-table-colMidPecahan3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 300px;
            height: 35px;
            background-color: lightgrey;
        }

        .div-table-colMidJumlah {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 200px;
            height: 35px;
            background-color: whitesmoke;
            padding-left: 10px;
        }

        .div-table-colMidNilaiTotal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 200px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidVariance {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 440px;
            height: 35px;
            background-color: whitesmoke;
            padding-left: 10px;
            background-color: red;
        }

        .div-table-colMidJumlahVariance {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 200px;
            height: 35px;
            background-color: whitesmoke;
            padding-left: 10px;
            background-color: red;
        }

        .div-table-colSelisih {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 440px;
            height: 35px;
            background-color: whitesmoke;
            padding-left: 10px;
            background-color: lightgrey;
        }

        .div-table-colSelisih2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 200px;
            height: 35px;
            background-color: whitesmoke;
            padding-left: 10px;
            background-color: lightgrey;
        }

        .div-table-colTanggal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 127px;
            height: 35px;
            background-color: none;
        }

        .div-table-colTanggal2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 105px;
            height: 35px;
            background-color: none;
            padding-left: 100px;
        }

        .div-table-colTanggal3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 70px;
            height: 35px;
            background-color: none;
        }

        .div-tableMid-TextArea {
            display: table;
            width: 860px;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-colMidSelisih {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 850px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidSelisihtextArea {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 840px;
            height: 185px;
            background-color: whitesmoke;
        }

        .div-table-colTTD {
            float: right;
            display: table-column;
            width: 280px;
            height: 20px;
            background-color: none;
        }

        .textarea2 {
            font-family: 'Times New Roman';
            width: 840px;
            height: 175px;
            font-size: 15px;
        }

        .display-validate {
            clear: both;
            display: block;
            position: relative;
            left: 210px;
            bottom: 30px;
        }

        ::-webkit-input-placeholder {
            font-style: italic;
        }

        @media print {
            #printbtn, [id$=btnSave], [id$=btnExit], [id$=ExportExcel]{
                display: none;
            }
        }

    </style>        
    <script>
        $(function () {
            $("[id$=txbDateCol]").datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true
            });
        });
        $(function () {
            $("[id$=TxbTnglBwh]").datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">

        <fieldset>
        <legend></legend>
            <div class="Table">
                <div class="div-table-header-kanan">
                    <label align="center">IAC GL - 04A</label>
                    <hr />
                    <div class="div-table-row">
                        <div class="div-table-cell-kotak-kanan">
                            <asp:Label ID="Label3" runat="server" AssociatedControlID="TxbDibuatHead">Dibuat </asp:Label>
                        </div>
                        <div class="div-table-cell-kotak-kanan">
                            <asp:TextBox ID="TxbDibuatHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                        </div>
                        <div class="div-table-cell-kotak-kanan-2">
                            <asp:Label ID="Label1" runat="server" AssociatedControlID="TxbDireviewHead">Direview </asp:Label>
                        </div>
                        <div class="div-table-cell-kotak-kanan-2">
                            <asp:TextBox ID="TxbDireviewHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                        </div>

                    </div>
                    <div class="div-table-row">
                        <div class="div-table-cell-kotak-kanan">
                            <asp:Label ID="Label4" runat="server" AssociatedControlID="TxbTanggalHeadDibuat">Tanggal </asp:Label>
                        </div>
                        <div class="div-table-cell-kotak-kanan">
                            <asp:TextBox ID="TxbTanggalHeadDibuat" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                        </div>
                        <div class="div-table-cell-kotak-kanan-2">
                            <asp:Label ID="Label2" runat="server" AssociatedControlID="TxbTanggalHeadDireview">Tanggal </asp:Label>
                        </div>
                        <div class="div-table-cell-kotak-kanan-2">
                            <asp:TextBox ID="TxbTanggalHeadDireview" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div class="Table">
                    <div class="div-table-row">
                        <div class="div-table-cell">
                            <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                        </div>
                        <div class="div-table-cell">
                            <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                        </div>
                        <div class="div-table-cell">                            
                            <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                        </div>
                        <div class="div-table-cell">
                            <asp:TextBox ID="txtIDSaveTemp" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                            <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false" />
                        </div>
                    </div>
                    <div class="div-table-row">
                        <div class="div-table-cell">
                            <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                        </div>
                        <div class="div-table-cell">
                            <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div align="center">
                    <h2>Berita Acara Cash Vault</h2>
                    <h4>Internal Audit & Control Department</h4>
                </div>
                <br />
                <br />
                <div class="Table">
                    <div class="div-tableMid">
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan" align="center">Pecahan Uang Tunai</div>
                            <div class="div-table-colMidJumlah" align="Center">Jumlah</div>
                            <div class="div-table-colMidNilaiTotal" align="Center">Nilai Total</div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp100.000,- (Seratus ribu Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah100rb" runat="server" Width="100px" OnTextChanged="txbJumlah100rb_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"  ></asp:TextBox>Lembar
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal100rb" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-row">
                            <div class="div-table-colMidPecahan">Rp50.000,- (Lima Puluh Ribu Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah50rb" runat="server" Width="100px" OnTextChanged="txbJumlah50rb_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Lembar
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal50rb" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp20.000,- (Dua Puluh Ribu Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah20rb" runat="server" Width="100px" OnTextChanged="txbJumlah20rb_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Lembar
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal20rb" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp10.000,- (SePuluh Ribu Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah10rb" runat="server" Width="100px" OnTextChanged="txbJumlah10rb_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Lembar
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal10rb" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp5000,- (Lima Ribu Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah5rb" runat="server" Width="100px" OnTextChanged="txbJumlah5rb_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Lembar
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal5rb" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp2000,- (Dua Ribu Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah2rb" runat="server" Width="100px" AutoPostBack="true" AutoCompleteType="Disabled" OnTextChanged="txbJumlah2rb_TextChanged"></asp:TextBox>Lembar
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal2rb" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp1000,- (SeRibu Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah1rb" runat="server" Width="100px" OnTextChanged="txbJumlah1rb_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Lembar/Kpg
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal1rb" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp500,- (Lima Ratus Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah5rts" runat="server" Width="100px" OnTextChanged="txbJumlah5rts_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Keping
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal5rts" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp200,- (Dua Ratus Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah2rts" runat="server" Width="100px" OnTextChanged="txbJumlah2rts_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Keping
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal2rts" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp100,- (Seratus Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah1rts" runat="server" Width="100px" OnTextChanged="txbJumlah1rts_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Keping
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal1rts" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp50,- (Lima Puluh Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah50" runat="server" Width="100px" OnTextChanged="txbJumlah50_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Keping
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal50" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Rp25,- (Dua Puluh Lima Rupiah)</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbJumlah25" runat="server" Width="100px" OnTextChanged="txbJumlah25_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>Keping
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotal25" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox>
                                ,-
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">
                                <label>Total Fisik Uang</label>
                            </div>
                            <div class="div-table-colMidJumlah"></div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="txbTotalUangFisik" runat="server" Width="155px" ReadOnly="true" AutoCompleteType="Disabled" onblur="toUSD(this)"></asp:TextBox>
                                ,-                                
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">
                                <label>Colection Projection Tanggal</label>
                            </div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="txbDateCol" runat="server" Width="170px" AutoCompleteType="Disabled" ></asp:TextBox>
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                Rp
                            <asp:TextBox ID="TxbCollProjc" runat="server" Width="155px" AutoCompleteType="Disabled"  AutoPostBack="true" OnTextChanged="TxbCollProjc_TextChanged1" ></asp:TextBox>
                                ,-
                                <asp:RegularExpressionValidator ID="Validasi1" runat="server" ControlToValidate="TxbCollProjc" ErrorMessage="Input Hanya Angka" ValidationExpression="^[0-9,.]*$" ForeColor="Red" CssClass="display-validate">
                                </asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidVariance" align="center">
                                <label>Variance</label>
                            </div>
                            <div class="div-table-colMidJumlahVariance">                                
                            <asp:TextBox ID="TxbVariance" runat="server" Width="174px" AutoCompleteType="Disabled" ReadOnly="true" AutoPostBack="true" ></asp:TextBox>
                                ,-
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div class="Table">
                    <div class="div-tableMid">
                        <div class="div-table-rowMid"></div>
                        <div class="div-table-colMidPecahan2">
                            <label>Penjelasan Variance</label>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan" align="center">Type</div>
                            <div class="div-table-colMidJumlah" align="Center">Jumlah</div>
                            <div class="div-table-colMidNilaiTotal" align="Center">Nilai Total</div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Preclosure</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="TxbPreciosure" runat="server" Width="170px" placeholder="Masukan Jumlah Mitra"  AutoCompleteType="Disabled"></asp:TextBox>
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                <asp:TextBox ID="TxbJmlhPreciosure" runat="server" Width="187px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbJmlhPreciosure_TextChanged" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Partial/Tunggakan</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="TxbPartial" runat="server" Width="170px" placeholder="Masukan Jumlah Mitra"  AutoCompleteType="Disabled" ></asp:TextBox>
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                <asp:TextBox ID="TxbJmlhPartial" runat="server" Width="187px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbJmlhPartial_TextChanged" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">Write Off</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="TxbWritOff" runat="server" Width="170px" placeholder="Masukan Jumlah Mitra"  AutoCompleteType="Disabled" ></asp:TextBox>
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                <asp:TextBox ID="TxbJmlhWritOff" runat="server" Width="187px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbJmlhWritOff_TextChanged" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan">UTJ</div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="TxbUjam" runat="server" Width="170px" placeholder="Masukan Jumlah Mitra"  AutoCompleteType="Disabled" ></asp:TextBox>
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                <asp:TextBox ID="TxbJmlhUjam" runat="server" Width="187px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbJmlhUjam_TextChanged"></asp:TextBox>
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colMidPecahan"></div>
                            <div class="div-table-colMidJumlah">
                                <asp:TextBox ID="TxbJmlhVarc" runat="server" Width="170px" placeholder="Masukan Jumlah Mitra" ReadOnly="true" AutoCompleteType="Disabled" AutoPostBack="true" Visible="false" ></asp:TextBox>
                            </div>
                            <div class="div-table-colMidNilaiTotal">
                                <asp:TextBox ID="TxbNilaiTotalPnjVarc" runat="server" Width="187px" ReadOnly="true" AutoCompleteType="Disabled" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="div-table-rowMid">
                            <div class="div-table-colSelisih" align="center">
                                <label>Selisih (Kurang/Lebih)</label>
                            </div>
                            <div class="div-table-colSelisih2">
                                <asp:TextBox ID="TxbSelisih" runat="server" Width="185px" AutoPostBack="true" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <br />
                <div class="div-tableMid-TextArea">
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidSelisih">
                            <label>Penjelasan Selisih :</label>
                        </div>
                        <div class="div-table-colMidSelisihtextArea">
                            <textarea class="textarea2" id="RemarksSelisih" runat="server"></textarea>
                        </div>
                    </div>
                </div>
                <br />
                <div class="Table" align="center">
                    <div class="div-table-colTanggal">
                        <label>Dilaksanakan Di</label>
                    </div>
                    <div class="div-table-colTanggal">
                        <asp:TextBox ID="TxbDilksnakan" runat="server" Width="210px" AutoCompleteType="Disabled" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="div-table-colTanggal2">
                        <label>Pada Tanggal</label>
                    </div>
                    <div class="div-table-colTanggal">
                        <asp:TextBox ID="TxbTnglBwh" runat="server" Width="110px" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                    <div class="div-table-colTanggal3">
                        <label>Pada Jam</label>
                    </div>
                    <div class="div-table-colTanggal">
                        <asp:TextBox ID="TxbJamBawah" runat="server" Width="110px" AutoCompleteType="Disabled"></asp:TextBox>
                    </div>
                    <%--<div class="container">
                        <div class="form-group col-lg-2">
                            <div class="input-group clockpicker" data-placement="bottom" data-alignment="left" data-donetext="Done">
                                <input type="text" class="form-control" value="08:00" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>--%>

                </div>
                <br />
                <div class="Table-ttd">
                    <div class="div-table-rowMid-ttd">
                        <div class="div-table-colTTD" align="center">Diperiksa Oleh,</div>
                        <div class="div-table-colTTD" align="Center">Disaksikan Oleh,</div>
                        <div class="div-table-colTTD" align="Center">Mengetahui,</div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <div class="div-table-rowMid-ttd">
                        <div class="div-table-colTTD" align="center">(...............................)</div>
                        <div class="div-table-colTTD" align="Center">(...............................)</div>
                        <div class="div-table-colTTD" align="Center">(...............................)</div>
                    </div>
                    <div class="div-table-rowMid-ttd">
                        <div class="div-table-colTTD" align="center">Internal Audit&Control</div>
                        <div class="div-table-colTTD" align="Center">Admin/Kasir</div>
                        <div class="div-table-colTTD" align="Center">Branch Manager</div>
                    </div>
                </div>
                <br />
                <br />
                <div class="TableButton">
                    <input id ="printbtn" type="button" value="Print" onclick="window.print();" >
                    <asp:Button ID="btnExit" runat="server" Text="Exit" Width="70px" OnClick="btnExit_Click" /> 
                    <asp:Button ID="btnSave" runat="server" Text="Save" Width="70px" OnClick="btnSave_Click" />
                    <%--<asp:Button ID="ExportExcel" runat="server" Text="Export" Width="70px" OnClick="ExportExcel_Click"/>--%>
                    <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false" ></asp:Label>
                </div>
            </div>
        <div>
            <asp:TextBox ID="TxtCreateDate" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TxtReviewDate" runat="server" Visible="false"></asp:TextBox>
        </div>
    </fieldset>  
</asp:Content>
