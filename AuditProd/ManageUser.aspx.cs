﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using AuditProd.DAL;

namespace AuditProd
{
    public partial class ManageUser : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        public void BindGrid()
        {
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  UserId,Username,NIK,Email,case when RoleId = 1 then 'Administrator' when RoleId is NULL then NULL else 'User' end [Roles] FROM tlbuser order by [UserId]");
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                rolesmessage.Visible = false;
                Getddl1Edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                UsernameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text);
                NIKEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                EmailEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                ddlRolesEdit.SelectedItem.Value = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            if (ddlRolesEdit.SelectedItem.Text == "--Select Roles--")
            {
                rolesmessage.Visible = true;
                rolesmessage.Text = "Pilih salah satu Roles";
            }
            else
            {
                rolesmessage.Visible = false;
                int No = Convert.ToInt32(lblCountryCode.Text);
                string name = UsernameEdit.Text;
                string nik = NIKEdit.Text;
                string email = EmailEdit.Text;
                int roles = Convert.ToInt32(ddlRolesEdit.SelectedItem.Value);
                string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
                executeUpdate(No, name, nik, email, roles, dt);///masuk ke private void update                  
                BindGrid();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Records Updated Successfully');");
                sb.Append("$('#editModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
            }
        }

        private void executeUpdate(int No, string name, string nik, string email, int roles, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_ManageUserUpdate", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.Parameters.AddWithValue("@name", name);
                updateCmd.Parameters.AddWithValue("@nik", nik);
                updateCmd.Parameters.AddWithValue("@email", email);
                updateCmd.Parameters.AddWithValue("@roles", roles);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            Getddl1();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            {
                try
                {
                    string code = txtusername.Text;
                    string name = txtNIK.Text;
                    string region = txtEmail.Text;
                    string continent = txtPassword.Text;
                    string role = ddlRoles.SelectedValue;

                    executeAdd(code, name, continent, region, role);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

        }

        private void executeAdd(string code, string name, string continent, string region, string role)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_ManageUser", conn);
                addCmd.CommandType = CommandType.StoredProcedure;

                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@continent", dl.Encript(continent.Trim()));
                addCmd.Parameters.AddWithValue("@role", role);
                //addCmd.Parameters.AddWithValue("@dt", dt);

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete tlbuser where [UserId]=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        private void Getddl1()
        {
            ddlRoles.AppendDataBoundItems = true;
            ddlRoles.Items.Clear();
            ddlRoles.Items.Add(new ListItem("--Select Roles--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [RoleId],[RoleName] from Roles";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddlRoles.DataSource = cmd.ExecuteReader();
                ddlRoles.DataTextField = "RoleName";
                ddlRoles.DataValueField = "RoleId";
                ddlRoles.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private void Getddl1Edit()
        {
            ddlRolesEdit.AppendDataBoundItems = true;
            ddlRolesEdit.Items.Clear();
            ddlRolesEdit.Items.Add(new ListItem("--Select Roles--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [RoleId],[RoleName] from Roles";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddlRolesEdit.DataSource = cmd.ExecuteReader();
                ddlRolesEdit.DataTextField = "RoleName";
                ddlRolesEdit.DataValueField = "RoleId";
                ddlRolesEdit.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void BtnExit_Click(object sender, EventArgs e)
        {
            Response.Redirect("HomePage.aspx");
        }

    }
}