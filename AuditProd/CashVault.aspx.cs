﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AuditProd
{
    public partial class CashVault : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {

                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {

                }
            }
            else
            {
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");
            }

        }

        protected void btnCVO_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txbCabang.Text;
            Session["AuditStart"] = txbPeriodeStart.Text;
            Session["AuditEnd"] = txbPeriodeEnd.Text;
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("CashVaultOpname.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("CashVaultOpname.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        protected void btnSSB_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txbCabang.Text;
            Session["AuditStart"] = txbPeriodeStart.Text;
            Session["AuditEnd"] = txbPeriodeEnd.Text;
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("SlipSetoranBank.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("SlipSetoranBank.aspx?ID=" + txtIDSaveTemp.Text);
            }

        }
    }
}