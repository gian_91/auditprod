﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using AuditProd.DAL;
using System.Globalization;

namespace AuditProd
{
    public partial class Tabel14 : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        string id5_lhp = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!string.IsNullOrEmpty((string)Session["Id5_LHP"]))
            {
                TxbId5_lhp.Text = Session["Id5_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id5_lhp = TxbId5_lhp.Text;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            BindGrid();
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            Session["Id5_LHP"] = null;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        public void BindGrid()
        {

            if (!(string.IsNullOrEmpty(id5_lhp)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel14 where ID_LHP_HR={0}", id5_lhp);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                TxtEmployee_IDEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text).ToString();
                TxtVarianceEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                TxtNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                TxtJobEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                TxtRainCoatEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                TxtCompanyUniformEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[11].Text);
                TxtNameCardEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[12].Text);
                TxtIdCardEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[13].Text);
                TxtBLJSkesEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[14].Text);
                TxtBPJStkEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[15].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            string empid = TxtEmployee_IDEdit.Text;
            //DateTime joindt = DateTime.Parse(string.IsNullOrEmpty(TxtJointDateEdit.Text) ? "01/01/9999 00:00:00" : TxtJointDateEdit.Text);
            //DateTime coa = DateTime.Parse(string.IsNullOrEmpty(TxtCutOffAuditEdit.Text) ? "01/01/9999 00:00:00" : TxtCutOffAuditEdit.Text);
            DateTime joindt = DateTime.ParseExact(TxtJointDateEdit.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime coa = DateTime.ParseExact(TxtCutOffAuditEdit.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            decimal var = Convert.ToDecimal(string.IsNullOrEmpty(TxtVarianceEdit.Text) ? "0" : TxtVarianceEdit.Text);
            string name = TxtNameEdit.Text;
            string job = TxtJobEdit.Text;
            string rc = TxtRainCoatEdit.Text;
            string cu = TxtCompanyUniformEdit.Text;
            string nc = TxtNameCardEdit.Text;
            string idCard = TxtIdCardEdit.Text;
            string bpjsKes = TxtBLJSkesEdit.Text;
            string bpjsTK = TxtBPJStkEdit.Text;
            executeUpdate(No, empid, joindt, coa, var,name,job,rc,cu,nc,idCard,bpjsKes,bpjsTK);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate( int No,string empid, DateTime joindt, DateTime coa, decimal var, string name, string job, string rc, string cu, string nc, string idCard, string bpjsKes,
            string bpjsTK)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveTabel14Edit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@empid", empid);
                updateCmd.Parameters.AddWithValue("@joindt", joindt);
                updateCmd.Parameters.AddWithValue("@coa", coa);
                updateCmd.Parameters.AddWithValue("@var", var);
                updateCmd.Parameters.AddWithValue("@name", name);
                updateCmd.Parameters.AddWithValue("@job", job);
                updateCmd.Parameters.AddWithValue("@rc", rc);
                updateCmd.Parameters.AddWithValue("@cu", cu);
                updateCmd.Parameters.AddWithValue("@nc", nc);
                updateCmd.Parameters.AddWithValue("@idCard", idCard);
                updateCmd.Parameters.AddWithValue("@bpjsKes", bpjsKes);
                updateCmd.Parameters.AddWithValue("@bpjsTK", bpjsTK);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_A = id5_lhp;

            if (string.IsNullOrEmpty(IDProc_A))
            {
                IDProc_A = null;
            }

            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string Headdb = txbUsername.Text;

            if (!(string.IsNullOrEmpty(IDProc_A)))
            {
                try
                {
                    //DateTime dcb = DateTime.Parse(string.IsNullOrEmpty(TxtDateBankAdd.Text) ? "01/01/9999 00:00:00" : TxtDateBankAdd.Text);
                    string idlhk5 = IDProc_A;
                    string empid = TxtEmployeeIDAdd.Text;
                    //DateTime joindt = DateTime.Parse(string.IsNullOrEmpty(TxtJoinDateAdd.Text) ? "01/01/9999 00:00:00" : TxtJoinDateAdd.Text);
                    //DateTime coa = DateTime.Parse(string.IsNullOrEmpty(TxtCutOffAuditAdd.Text) ? "01/01/9999 00:00:00" : TxtCutOffAuditAdd.Text);
                    DateTime joindt = DateTime.ParseExact(TxtJoinDateAdd.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime coa = DateTime.ParseExact(TxtCutOffAuditAdd.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    decimal var = Convert.ToDecimal(string.IsNullOrEmpty(TxtVarianceAdd.Text) ? "0" : TxtVarianceAdd.Text);
                    string name = TxtNameAdd.Text;
                    string job = TxtJobAdd.Text;
                    string rc = TxtRainCoatAdd.Text;
                    string cu = TxtCompanyUniFormAdd.Text;
                    string nc = TxtNameCardAdd.Text;
                    string idCard = TxtIdCardAdd.Text;
                    string bpjsKes = TxtBPJSkesAdd.Text;
                    string bpjsTK = TxtBPJStkAdd.Text;
                    string mod = "LHP_HR";
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk5, empid, joindt, coa, var, name, job, rc, cu, nc, idCard, bpjsKes, bpjsTK, mod, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }
        }

        private void executeAdd(string idlhk5, string empid, DateTime joindt, DateTime coa, decimal var, string name, string job, string rc ,string cu, string nc, string idCard, string bpjsKes,
            string bpjsTK, string mod, string dt, string Headdbs)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveTabel14", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk5", idlhk5 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@empid", empid);
                addCmd.Parameters.AddWithValue("@joindt", joindt);
                addCmd.Parameters.AddWithValue("@coa", coa);
                addCmd.Parameters.AddWithValue("@var", var);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@job", job);
                addCmd.Parameters.AddWithValue("@rc", rc);
                addCmd.Parameters.AddWithValue("@cu", cu);
                addCmd.Parameters.AddWithValue("@nc", nc);
                addCmd.Parameters.AddWithValue("@idCard", idCard);
                addCmd.Parameters.AddWithValue("@bpjsKes", bpjsKes);
                addCmd.Parameters.AddWithValue("@bpjsTK", bpjsTK);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@dt", dt);
                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)///Delete klik
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_LHP_Tabel14 where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}