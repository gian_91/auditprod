﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Globalization;
using AuditProd.DAL;
using ClosedXML.Excel;

namespace AuditProd
{
    public partial class AuditBranch : System.Web.UI.Page
    {
        string message = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            UserName.Text = this.Page.User.Identity.Name;
            if (!IsPostBack)
            {
                DDLExcel();
                if (!string.IsNullOrEmpty(Session["ID_SH"] as string))
                {
                    txtIDSave.Text = Session["ID_SH"].ToString();
                    SqlDataReader reader = null;
                    String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    SqlConnection con = new SqlConnection(strConnString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select UserName,Branch_ID,Month,Year,Audit_Schedule,convert(varchar,Audit_Periode_Start,105)Audit_Periode_Start, " +
                    "convert(varchar,Audit_Periode_End,105)Audit_Periode_End,Doe from SaveHeader where ID_SH=@ID_SH", con);
                    cmd.Parameters.AddWithValue("@ID_SH", txtIDSave.Text);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        txtBranchId.Text = Convert.ToString(reader["Branch_ID"]);
                        txtMonth.Text = Convert.ToString(reader["Month"]);
                        txtYear.Text = Convert.ToString(reader["Year"]);
                        txtAuditSchedule.Text = Convert.ToString(reader["Audit_Schedule"]);
                        txtAuditStart.Text = Convert.ToString(reader["Audit_Periode_Start"]);
                        txtAuditEnds.Text = Convert.ToString(reader["Audit_Periode_End"]);
                        UserDataAudit.Text = Convert.ToString(reader["UserName"]);
                    }
                    con.Close();
                }

                else if (!string.IsNullOrEmpty(Session["ID_SH_Temp"] as string))
                {
                    txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                    SqlDataReader reader2 = null;
                    String strConnString2 = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    SqlConnection con2 = new SqlConnection(strConnString2);
                    con2.Open();
                    SqlCommand cmd2 = new SqlCommand("select UserName,Branch_ID,Month,Year,Audit_Schedule,convert(varchar,Audit_Periode_Start,105)Audit_Periode_Start, " +
                    "convert(varchar,Audit_Periode_End,105)Audit_Periode_End,Doe from SaveHeader where ID_SH=@ID_SH_Temp", con2);
                    cmd2.Parameters.AddWithValue("@ID_SH_Temp", txtIDSaveTemp.Text);
                    reader2 = cmd2.ExecuteReader();
                    while (reader2.Read())
                    {
                        txtBranchId.Text = Convert.ToString(reader2["Branch_ID"]);
                        txtMonth.Text = Convert.ToString(reader2["Month"]);
                        txtYear.Text = Convert.ToString(reader2["Year"]);
                        txtAuditSchedule.Text = Convert.ToString(reader2["Audit_Schedule"]);
                        txtAuditStart.Text = Convert.ToString(reader2["Audit_Periode_Start"]);
                        txtAuditEnds.Text = Convert.ToString(reader2["Audit_Periode_End"]);
                        UserDataAudit.Text = Convert.ToString(reader2["UserName"]);
                    }
                    con2.Close();
                }
            }
            else
            {

            }

        }

        protected void btnDocTest_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txtBranchId.Text;
            Session["AuditStart"] = txtAuditStart.Text;
            Session["AuditEnd"] = txtAuditEnds.Text;
            Session["ID_SH"] = txtIDSave.Text;
            Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
            Session["Audit_Schedule"] = txtAuditSchedule.Text;

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                SaveTempDoctest();
                Response.Redirect("DocTest.aspx?ID=" + txtIDSaveTemp.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                SaveDoctest();
                Response.Redirect("DocTest.aspx?ID=" + txtIDSave.Text);
            }
        }

        protected void btnCashVault_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txtBranchId.Text;
            Session["AuditStart"] = txtAuditStart.Text;
            Session["AuditEnd"] = txtAuditEnds.Text;
            Session["ID_SH"] = txtIDSave.Text;
            Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
            Session["Audit_Schedule"] = txtAuditSchedule.Text;

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                SaveTempCV();
                Response.Redirect("CashVault.aspx?ID=" + txtIDSaveTemp.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                SaveCV();
                Response.Redirect("CashVault.aspx?ID=" + txtIDSave.Text);
            }
        }

        protected void btnPettyCash_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txtBranchId.Text;
            Session["AuditStart"] = txtAuditStart.Text;
            Session["AuditEnd"] = txtAuditEnds.Text;
            Session["ID_SH"] = txtIDSave.Text;
            Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
            Session["Audit_Schedule"] = txtAuditSchedule.Text;

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                SaveTempPC();
                Response.Redirect("PettyCash.aspx?ID=" + txtIDSaveTemp.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                SavePC();
                Response.Redirect("PettyCash.aspx?ID=" + txtIDSave.Text);
            }

        }

        protected void btnHR_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txtBranchId.Text;
            Session["AuditStart"] = txtAuditStart.Text;
            Session["AuditEnd"] = txtAuditEnds.Text;
            Session["ID_SH"] = txtIDSave.Text;
            Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
            Session["Audit_Schedule"] = txtAuditSchedule.Text;

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                SaveTempHR();
                Response.Redirect("HR.aspx?ID=" + txtIDSaveTemp.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                SaveHR();
                Response.Redirect("HR.aspx?ID=" + txtIDSave.Text);
            }
        }

        protected void btnProcmt_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txtBranchId.Text;
            Session["AuditStart"] = txtAuditStart.Text;
            Session["AuditEnd"] = txtAuditEnds.Text;
            Session["ID_SH"] = txtIDSave.Text;
            Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
            Session["Audit_Schedule"] = txtAuditSchedule.Text;

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                SaveTempProc();
                Response.Redirect("Procurement.aspx?ID=" + txtIDSaveTemp.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                SaveProc();
                Response.Redirect("Procurement.aspx?ID=" + txtIDSave.Text);
            }
        }

        protected void btnLHK_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txtBranchId.Text;
            Session["AuditStart"] = txtAuditStart.Text;
            Session["AuditEnd"] = txtAuditEnds.Text;
            Session["ID_SH"] = txtIDSave.Text;
            Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
            Session["Audit_Schedule"] = txtAuditSchedule.Text;

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                SaveTempHR();
                Response.Redirect("LHK.aspx?ID=" + txtIDSaveTemp.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                SaveHR();
                Response.Redirect("LHK.aspx?ID=" + txtIDSave.Text);
            }
            //message = "Coming Soon !";
            //ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

        }

        protected void btnLHP_Click(object sender, EventArgs e)
        {
            Session["BranchID"] = txtBranchId.Text;
            Session["AuditStart"] = txtAuditStart.Text;
            Session["AuditEnd"] = txtAuditEnds.Text;
            Session["ID_SH"] = txtIDSave.Text;
            Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
            Session["Audit_Schedule"] = txtAuditSchedule.Text;

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                SaveTempHR();
                Response.Redirect("LHP.aspx?ID=" + txtIDSaveTemp.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                SaveHR();
                Response.Redirect("LHP.aspx?ID=" + txtIDSave.Text);
            }
            //message = "Coming Soon !";
            //ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

        }

        private void SaveDoctest()
        {
            string z = btnDocTest.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSave.Text;
                        cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                        cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                        cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                        cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                        cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                        cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                        cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                        cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                        cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                        cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                        cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                        con.Open();
                        int b = cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);

                }
            }

        }

        private void SaveTempDoctest()
        {
            string z = btnDocTest.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                        cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                        cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                        cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                        cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                        cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                        cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                        cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                        cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                        cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                        cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DBNull.Value;
                        cmd.Parameters.Add("ReviewDate", SqlDbType.Int).Value = DateTime.Now;
                        con.Open();
                        int a = cmd.ExecuteNonQuery();
                        con.Close();

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    Response.Write("<script> alert('Not Succcees ..') </script>");
                }
            }

        }

        private void SaveCV()
        {
            string z = btnCashVault.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                    cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                    cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                    cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                    cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                    cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                    cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                    cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                    con.Open();
                    int b = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }

        private void SaveTempCV()
        {
            string z = btnCashVault.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                    cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                    cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                    cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                    cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                    cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                    cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                    cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;
                    con.Open();
                    int a = cmd.ExecuteNonQuery();
                    con.Close();

                }
            }

        }

        private void SavePC()
        {
            string z = btnPettyCash.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                    cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                    cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                    cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                    cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                    cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                    cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                    cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                    con.Open();
                    int b = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }

        private void SaveTempPC()
        {
            string z = btnPettyCash.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                    cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                    cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                    cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                    cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                    cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                    cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                    cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;
                    con.Open();
                    int a = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        private void SaveHR()
        {
            string z = btnHR.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                    cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                    cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                    cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                    cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                    cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                    cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                    cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                    con.Open();
                    int b = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }

        private void SaveTempHR()
        {
            string z = btnHR.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                    cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                    cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                    cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                    cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                    cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                    cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                    cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;
                    con.Open();
                    int a = cmd.ExecuteNonQuery();
                    con.Close();

                }
            }

        }

        private void SaveProc()
        {
            string z = btnProcmt.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                    cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                    cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                    cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                    cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                    cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                    cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                    cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                    con.Open();
                    int b = cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }

        private void SaveTempProc()
        {
            string z = btnProcmt.Text;
            string Constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(Constr))
            {
                using (SqlCommand cmd = new SqlCommand("SP_AB_Log", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("Id_AU", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.Add("Modul", SqlDbType.NVarChar).Value = z.ToString();
                    cmd.Parameters.Add("NIK_User_Login", SqlDbType.NVarChar).Value = UserName.Text;
                    cmd.Parameters.Add("NIK_User_DataAudit", SqlDbType.NVarChar).Value = UserDataAudit.Text;
                    cmd.Parameters.Add("BranchID", SqlDbType.NVarChar).Value = txtBranchId.Text;
                    cmd.Parameters.Add("MOA_Bulan", SqlDbType.NVarChar).Value = txtMonth.Text;
                    cmd.Parameters.Add("MOA_Tahun", SqlDbType.NVarChar).Value = txtYear.Text;
                    cmd.Parameters.Add("AuditSchedule", SqlDbType.NVarChar).Value = txtAuditSchedule.Text;
                    cmd.Parameters.Add("Audit_Periode_Start", SqlDbType.NVarChar).Value = txtAuditStart.Text;
                    cmd.Parameters.Add("Audit_Periode_Finish", SqlDbType.NVarChar).Value = txtAuditEnds.Text;
                    cmd.Parameters.Add("CreatedDate", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;
                    con.Open();
                    int a = cmd.ExecuteNonQuery();
                    con.Close();

                }
            }

        }

        private void DDLExcel()
        {
            DDLExport.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select * from Master_Modul";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            con.Open();
            DDLExport.DataSource = cmd.ExecuteReader();
            DDLExport.DataTextField = "ModulName";
            DDLExport.DataValueField = "Id_Master";
            DDLExport.DataBind();
            con.Close();
        }

        protected void BtnExport_Click(object sender, EventArgs e)
        {
            string value = DDLExport.SelectedItem.Value;
            string name = DDLExport.SelectedItem.Text;

            //try
            //{
            if (value == "1")/////Doctest
            {
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Export_DocTest", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", txtAuditSchedule.Text);
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "DocTest");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=Export_DocTest.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        con.Close();
                    }

                }
            }
            else if (value == "2")///Cash_Vault_Opname
            {
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Export_CVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", txtAuditSchedule.Text);
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "CashVaultOpname");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=Export_CVO.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        con.Close();
                    }
                }

            }
            else if (value == "3")///SlipSetoranBank
            {
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();

                SqlCommand cmd = new SqlCommand("SP_Export_SSB", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", txtAuditSchedule.Text);

                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "Slip_Setoran_Bank");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=Export_SSB.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        con.Close();
                    }
                }

            }
            else if (value == "4")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Export_PCO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", txtAuditSchedule.Text);
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "PettyCashOpname");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=Export_PCO.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        con.Close();
                    }
                }

            }
            else if (value == "5")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Export_PCMO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", txtAuditSchedule.Text);
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "PettyCashMateraiOpname");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=Export_PCMO.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        con.Close();
                    }
                }

            }
            else if (value == "6")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Export_HR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", txtAuditSchedule.Text);
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "Employee_Attributes");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=Export_HR.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        con.Close();
                    }
                }
            }
            else if (value == "7")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("SP_Export_Procurement", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserName", txtAuditSchedule.Text);
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;
                    sda.SelectCommand = cmd;
                    using (DataTable dt = new DataTable())
                    {
                        sda.Fill(dt);
                        using (XLWorkbook wb = new XLWorkbook())
                        {
                            wb.Worksheets.Add(dt, "Procurement");

                            Response.Clear();
                            Response.Buffer = true;
                            Response.Charset = "";
                            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            Response.AddHeader("content-disposition", "attachment;filename=Export_Procurement.xlsx");
                            using (MemoryStream MyMemoryStream = new MemoryStream())
                            {
                                wb.SaveAs(MyMemoryStream);
                                MyMemoryStream.WriteTo(Response.OutputStream);
                                Response.Flush();
                                Response.End();
                            }
                        }
                        con.Close();
                    }
                }
            }

        }
    }
}