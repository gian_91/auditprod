﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using AuditProd.DAL;
using System.Globalization;


namespace AuditProd
{
    public partial class Tabel4 : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        string id = string.Empty;
        string id2 = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!string.IsNullOrEmpty((string)Session["Id"]))
            {
                TxbId.Text = Session["Id"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                id = TxbId.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id2"]))
            {
                TxbId2.Text = Session["Id2"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                id2 = TxbId2.Text;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            GetHeader();
            BindGrid();

        }

        public void GetHeader()
        {
            if (!(string.IsNullOrEmpty(id)))
            {
                try
                {
                    string strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    SqlConnection con = new SqlConnection(strConnString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select * from LHK_Acquisition where [No] = @IDProc", con);
                    cmd.Parameters.AddWithValue("@IDProc", id);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        TxbIdLHKAquc.Text = Convert.ToString(dr["No"]);
                        TxtAccountIdAdd.Text = Convert.ToString(dr["AccountID"]);
                        TxtClientNameAdd.Text = Convert.ToString(dr["NamaMitra"]);
                        TxtCenterNameAdd.Text = Convert.ToString(dr["CenterName"]);
                        TxtDueDaysAdd.Text = Convert.ToString(dr["Par"]);
                        //TxtPicNameAdd.Text = Convert.ToString(dr["Disb_Date"]);
                        ddlsmplingAdd.Text = Convert.ToString(dr["StatusSampling"]);

                    }
                    con.Close();

                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
                }
            }
            else if (!(string.IsNullOrEmpty(id2)))
            {
                try
                {
                    string strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    SqlConnection con = new SqlConnection(strConnString);
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select * from LHK_Collection where [No] = @IDProc", con);
                    cmd.Parameters.AddWithValue("@IDProc", id2);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        TxbIdLHKAquc.Text = Convert.ToString(dr["No"]);
                        TxtAccountIdAdd.Text = Convert.ToString(dr["AccountID"]);
                        TxtClientNameAdd.Text = Convert.ToString(dr["NamaMitra"]);
                        TxtCenterNameAdd.Text = Convert.ToString(dr["CenterName"]);
                        TxtDueDaysAdd.Text = Convert.ToString(dr["Par"]);
                        //TxtPicNameAdd.Text = Convert.ToString(dr["Disb_Date"]);
                        ddlsmplingAdd.Text = Convert.ToString(dr["StatusSampling"]);
                    }
                    con.Close();

                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
                }
            }
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            Session["Id"] = null;
            Session["Id2"] = null;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("LHK.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("LHK.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        public void BindGrid()
        {

            if (!(string.IsNullOrEmpty(id)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel4 where ID_LHK_Acquisition={0}", id);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();

                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);

                }

            }
            else if (!(string.IsNullOrEmpty(id2)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel4 where ID_LHK_Collection={0}", id2);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();

                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                IEnumerable<DataRow> query = from i in dt.AsEnumerable()
                                             where i.Field<Int32>("No").Equals(code)
                                             select i;
                DataTable detailTable = query.CopyToDataTable<DataRow>();
                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //Getddl1edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                TxtAccountIdEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text).ToString();
                TxtClientNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                TxtCenterNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                TxtDueDaysEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);

                TxtPicNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                DateClientpaiDEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text).ToString();
                AmountClientPaidEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text).ToString();
                DateDepositToBranchEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[11].Text).ToString();
                AmountDepositToBranchEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[12].Text).ToString();
                VarianceDayEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[13].Text).ToString();
                VarianceAmountEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[14].Text).ToString();
                TxtRemarksEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[15].Text);
                ddlsmplingEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[16].Text);

                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            string pn = TxtPicNameEdit.Text;
            string dcp = DateClientpaiDEdit.Text;
            decimal acp = Convert.ToDecimal(string.IsNullOrEmpty(AmountClientPaidEdit.Text) ? "0" : AmountClientPaidEdit.Text);
            string ddtb = DateDepositToBranchEdit.Text;
            decimal adtb = Convert.ToDecimal(string.IsNullOrEmpty(AmountDepositToBranchEdit.Text) ? "0" : AmountDepositToBranchEdit.Text);
            string vd = VarianceDayEdit.Text;
            decimal va = Convert.ToDecimal(string.IsNullOrEmpty(VarianceAmountEdit.Text) ? "0" : VarianceAmountEdit.Text);
            string remarks = TxtRemarksEdit.Text;
            string ss = ddlsmplingEdit.Text;
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdate(No, pn, dcp, acp, ddtb, adtb, vd, va, remarks, ss, doe);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, string pn, string dcp, decimal acp, string ddtb, decimal adtb, string vd, decimal va, string remarks, string ss, string doe)///buat edit(klik button update di form edit)
        {
            //cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(txbUsername.Text) ? (object)DBNull.Value : txbUsername.Text);TxtRemarksEdit
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveTabel4Edit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@pn", string.IsNullOrEmpty(pn) ? (object)DBNull.Value : pn);
                updateCmd.Parameters.AddWithValue("@dcp", string.IsNullOrEmpty(dcp) ? (object)DBNull.Value : dcp);
                updateCmd.Parameters.AddWithValue("@acp", acp);
                updateCmd.Parameters.AddWithValue("@ddtb", string.IsNullOrEmpty(ddtb) ? (object)DBNull.Value : ddtb);
                updateCmd.Parameters.AddWithValue("@adtb", adtb);
                updateCmd.Parameters.AddWithValue("@vd", vd);
                updateCmd.Parameters.AddWithValue("@va", va);
                updateCmd.Parameters.AddWithValue("@remarks", string.IsNullOrEmpty(remarks) ? (object)DBNull.Value : remarks);
                updateCmd.Parameters.AddWithValue("@ss", ss);
                updateCmd.Parameters.AddWithValue("@doe", doe);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_A = id;
            string IDProc_B = id2;
            string dd = TxtDueDaysAdd.Text;

            if (string.IsNullOrEmpty(IDProc_A))
            {
                IDProc_A = null;
            }
            if (string.IsNullOrEmpty(IDProc_B))
            {
                IDProc_B = null;
            }
            if (string.IsNullOrEmpty(dd))
            {
                dd = null;
            }

            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string nik = txbUsername.Text;


            if (!(string.IsNullOrEmpty(IDProc_A)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string niks = nik.ToString();
                    string acc = TxtAccountIdAdd.Text;
                    string jt = "LHK_Acquisition";
                    string cLn = TxtClientNameAdd.Text;
                    string cTn = TxtCenterNameAdd.Text;
                    string ddays = dd;
                    string pn = TxtPicNameAdd.Text;
                    string dcp = DateClientpaIDAdd.Text;
                    decimal acp = Convert.ToDecimal(string.IsNullOrEmpty(AmountClientPaidAdd.Text) ? "0" : AmountClientPaidAdd.Text);
                    string ddtb = DateDeposittoBranchAdd.Text;
                    decimal adtb = Convert.ToDecimal(string.IsNullOrEmpty(AmountDeposittoBranchAdd.Text) ? "0" : AmountDeposittoBranchAdd.Text);
                    double vd = Convert.ToDouble(string.IsNullOrEmpty(VarianceDayAdd.Text) ? "0" : VarianceDayAdd.Text);//VarianceDayAdd.Text;
                    decimal va = Convert.ToDecimal(string.IsNullOrEmpty(VarianceAmountAdd.Text) ? "0" : VarianceAmountAdd.Text);
                    string remarks = TxtRemarksAdd.Text;
                    string ss = ddlsmplingAdd.Text;
                    string dt = doe.ToString();


                    executeAdd(idlhk2, idlhk3, niks, acc, jt, cLn, cTn, ddays, pn, dcp, acp, ddtb, adtb, vd, va, remarks, ss, dt);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }

            }

            else if (!(string.IsNullOrEmpty(IDProc_B)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string niks = nik.ToString();
                    string acc = TxtAccountIdAdd.Text;
                    string jt = "LHK_Collection";
                    string cLn = TxtClientNameAdd.Text;
                    string cTn = TxtCenterNameAdd.Text;
                    string ddays = dd;
                    string pn = TxtPicNameAdd.Text;
                    string dcp = DateClientpaIDAdd.Text;
                    decimal acp = Convert.ToDecimal(string.IsNullOrEmpty(AmountClientPaidAdd.Text) ? "0" : AmountClientPaidAdd.Text);
                    string ddtb = DateDeposittoBranchAdd.Text;
                    decimal adtb = Convert.ToDecimal(string.IsNullOrEmpty(AmountDeposittoBranchAdd.Text) ? "0" : AmountDeposittoBranchAdd.Text);
                    double vd = Convert.ToDouble(string.IsNullOrEmpty(VarianceDayAdd.Text) ? "0" : VarianceDayAdd.Text);//VarianceDayAdd.Text;
                    decimal va = Convert.ToDecimal(string.IsNullOrEmpty(VarianceAmountAdd.Text) ? "0" : VarianceAmountAdd.Text);
                    string remarks = TxtRemarksAdd.Text;
                    string ss = ddlsmplingAdd.Text;
                    string dt = doe.ToString();

                    executeAdd(idlhk2, idlhk3, niks, acc, jt, cLn, cTn, ddays, pn, dcp, acp, ddtb, adtb, vd, va, remarks, ss, dt);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }

            }

        }

        private void executeAdd(string idlhk2, string idlhk3, string niks, string acc, string jt, string cLn, string cTn, string ddays, string pn, string dcp, decimal acp,
            string ddtb, decimal adtb, double vd, decimal va, string remarks, string ss, string dt)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveTabel4", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk2", idlhk2 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk3", idlhk3 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@nik", niks);
                addCmd.Parameters.AddWithValue("@acc", acc);
                addCmd.Parameters.AddWithValue("@jt", jt);
                addCmd.Parameters.AddWithValue("@cLn", cLn);
                addCmd.Parameters.AddWithValue("@cTn", cTn);
                addCmd.Parameters.AddWithValue("@ddays", ddays ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@pn", string.IsNullOrEmpty(pn) ? (object)DBNull.Value : pn);
                addCmd.Parameters.AddWithValue("@dcp", string.IsNullOrEmpty(dcp) ? (object)DBNull.Value : dcp);
                addCmd.Parameters.AddWithValue("@acp", acp);
                addCmd.Parameters.AddWithValue("@ddtb", string.IsNullOrEmpty(ddtb) ? (object)DBNull.Value : ddtb);
                addCmd.Parameters.AddWithValue("@adtb", adtb);
                addCmd.Parameters.AddWithValue("@vd", vd);
                addCmd.Parameters.AddWithValue("@va", va);
                addCmd.Parameters.AddWithValue("@remarks", string.IsNullOrEmpty(remarks) ? (object)DBNull.Value : remarks);
                addCmd.Parameters.AddWithValue("@ss", ss);
                addCmd.Parameters.AddWithValue("@doe ", dt);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_LHP_Tabel4 where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        //protected void DateClientpaIDAdd_TextChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(DateDeposittoBranchAdd.Text) && !string.IsNullOrEmpty(DateClientpaIDAdd.Text))
        //    {
        //        DateTime start = DateTime.ParseExact(DateDeposittoBranchAdd.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);
        //        DateTime end = DateTime.ParseExact(DateClientpaIDAdd.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);

        //        TimeSpan ts = start - end;
        //        VarianceDayAdd.Text = ts.TotalDays.ToString();
        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('input start date')</script>");
        //    }   

        //}

        //protected void DateDeposittoBranchAdd_TextChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(DateDeposittoBranchAdd.Text) && !string.IsNullOrEmpty(DateClientpaIDAdd.Text))
        //    {
        //        DateTime start = DateTime.ParseExact(DateDeposittoBranchAdd.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);
        //        DateTime end = DateTime.ParseExact(DateClientpaIDAdd.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);

        //        TimeSpan ts = start - end;
        //        VarianceDayAdd.Text = ts.TotalDays.ToString(); ;
        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('input start date')</script>");
        //    }            

        //}

        protected void AmountDeposittoBranchAdd_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value;
            total = 0;
            decimal.TryParse(AmountClientPaidAdd.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(AmountDeposittoBranchAdd.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(VarianceAmountAdd.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            total = txt2Value - txt1Value;
            VarianceAmountAdd.Text = total.ToString();
        }

        protected void AmountClientPaidAdd_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value;
            total = 0;
            decimal.TryParse(AmountClientPaidAdd.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(AmountDeposittoBranchAdd.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(VarianceAmountAdd.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            total = txt2Value - txt1Value;
            VarianceAmountAdd.Text = total.ToString();
        }

        //protected void DateClientpaiDEdit_TextChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(DateDepositToBranchEdit.Text) && !string.IsNullOrEmpty(DateClientpaiDEdit.Text))
        //    {
        //        DateTime start = DateTime.ParseExact(DateDepositToBranchEdit.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);
        //        DateTime end = DateTime.ParseExact(DateClientpaiDEdit.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);

        //        TimeSpan ts = start - end;
        //        VarianceDayEdit.Text = ts.TotalDays.ToString();

        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('input start date')</script>");
        //    }   

        //}

        //protected void DateDepositToBranchEdit_TextChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(DateDepositToBranchEdit.Text) && !string.IsNullOrEmpty(DateClientpaiDEdit.Text))
        //    {
        //        DateTime start = DateTime.ParseExact(DateDepositToBranchEdit.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);
        //        DateTime end = DateTime.ParseExact(DateClientpaiDEdit.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture);

        //        TimeSpan ts = start - end;
        //        VarianceDayEdit.Text = ts.TotalDays.ToString();
        //    }
        //    else
        //    {
        //        Response.Write("<script>alert('input start date')</script>");
        //    }   

        //}

        protected void AmountClientPaidEdit_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value;
            total = 0;
            decimal.TryParse(AmountClientPaidEdit.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(AmountDepositToBranchEdit.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(VarianceAmountEdit.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            total = txt2Value - txt1Value;
            VarianceAmountEdit.Text = total.ToString();

        }

        protected void AmountDepositToBranchEdit_TextChanged(object sender, EventArgs e)
        {
            decimal txt1Value, total, txt2Value;
            total = 0;
            decimal.TryParse(AmountClientPaidEdit.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt1Value);
            decimal.TryParse(AmountDepositToBranchEdit.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out txt2Value);
            decimal.TryParse(VarianceAmountEdit.Text, NumberStyles.Currency, CultureInfo.InvariantCulture, out total);

            total = txt2Value - txt1Value;
            VarianceAmountEdit.Text = total.ToString();

        }
    }
}