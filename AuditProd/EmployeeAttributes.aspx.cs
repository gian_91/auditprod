﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using ClosedXML.Excel;
using AuditProd.DAL;

namespace AuditProd
{
    public partial class EmployeeAttributes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TxbModul.Text = "Employee Attributes";
            if (!IsPostBack)
            {
                this.TxbTanggalHeadDibuat.Text = DateTime.Today.ToString("dd/MM/yyyy");
                TxbDibuatHead.Text = this.Page.User.Identity.Name;
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                TxbDilaksanakan.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {
                    BindGrid();
                    FillGridViewTemp();
                    //LoadSaveID();
                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {
                    BindGridTemp();
                    FillGridView();
                    //LoadSaveIDTemp();
                }
            }
            else
            {
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

            }

        }

        private void BindGrid()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Footer_Tngl],[Footer_Jam] FROM [Employee_Attributes] where IDEmpAttr=@IDEmpAttr order by  IDEmpAttr asc", con);
            cmd.Parameters.AddWithValue("@IDEmpAttr", txtIDSave.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TxbTnglBwh.Text = Convert.ToString(dr["Footer_Tngl"]);
                TxbPdJam.Text = Convert.ToString(dr["Footer_Jam"]);
            }
            con.Close();
        }

        private void BindGridTemp()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [Footer_Tngl],[Footer_Jam],[Doe],[ReviewDate] FROM [Employee_Attributes] where IDEmpAttr=@IDEmpAttr order by  IDEmpAttr asc", con);
            cmd.Parameters.AddWithValue("@IDEmpAttr", txtIDSaveTemp.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TxbTnglBwh.Text = Convert.ToString(dr["Footer_Tngl"]);
                TxbPdJam.Text = Convert.ToString(dr["Footer_Jam"]);
                TxtCreateDate.Text = Convert.ToString(dr["Doe"]);
                TxtReviewDate.Text = Convert.ToString(dr["ReviewDate"]);
            }
            con.Close();


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("HR.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("HR.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        /// <summary>
        /// buat Save_Id_temp
        /// </summary>
        public void FillGridView()
        {
            string IDProc_B_Temp = txtIDSaveTemp.Text;

            try
            {
                string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                string query = string.Format(@"select * from Employee_Attributes where IDEmpAttr={0}", IDProc_B_Temp);
                SqlConnection con = new SqlConnection(cnString);
                GlobalClass.adap = new SqlDataAdapter(query, con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView1.DataSource = GlobalClass.dt;
                GridView1.DataBind();
            }
            catch
            {
                Response.Write("<script> alert('Connection StringError...') </script>");
            }
        }

        /// <summary>
        /// buat Save_id
        /// </summary>
        public void FillGridViewTemp()
        {
            string IDProc_B = txtIDSave.Text;

            try
            {
                string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                string query = string.Format(@"select * from Employee_Attributes where IDEmpAttr={0}", IDProc_B);
                SqlConnection con = new SqlConnection(cnString);
                GlobalClass.adap = new SqlDataAdapter(query, con);
                SqlCommandBuilder bui = new SqlCommandBuilder(GlobalClass.adap);
                GlobalClass.dt = new DataTable();
                GlobalClass.adap.Fill(GlobalClass.dt);
                GridView2.DataSource = GlobalClass.dt;
                GridView2.DataBind();
            }
            catch
            {
                Response.Write("<script> alert('Connection StringError...') </script>");
            }
        }

        protected void editRecord(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            FillGridView();
        }

        protected void editRecordTemp(object sender, GridViewEditEventArgs e)
        {
            GridView2.EditIndex = e.NewEditIndex;
            FillGridViewTemp();
        }

        protected void cancelRecord(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            FillGridView();
        }

        protected void cancelRecordTemp(object sender, GridViewCancelEditEventArgs e)
        {
            GridView2.EditIndex = -1;
            FillGridViewTemp();
        }

        protected void updateRecord(object sender, GridViewUpdateEventArgs e)
        {
            //string strEmail = string.Empty;
            try
            {
                TextBox txtEmpIds = GridView1.Rows[e.RowIndex].FindControl("txtEmpId") as TextBox;
                TextBox txtNames = GridView1.Rows[e.RowIndex].FindControl("txtName") as TextBox;
                TextBox txtBirth_Dates = GridView1.Rows[e.RowIndex].FindControl("txtBirth_Date") as TextBox;
                TextBox txtDate_Ins = GridView1.Rows[e.RowIndex].FindControl("txtDate_In") as TextBox;
                //string datestring = txtDate_Ins.Text;
                TextBox txtDate_Outs = GridView1.Rows[e.RowIndex].FindControl("txtDate_Out") as TextBox;
                TextBox txtGenders = GridView1.Rows[e.RowIndex].FindControl("txtGender") as TextBox;
                TextBox txtJobs = GridView1.Rows[e.RowIndex].FindControl("txtJob") as TextBox;

                CheckBox txtRainCoats = GridView1.Rows[e.RowIndex].FindControl("Rb1") as CheckBox;
                CheckBox txtUniForms = GridView1.Rows[e.RowIndex].FindControl("Rb2") as CheckBox;
                CheckBox txtNameCards = GridView1.Rows[e.RowIndex].FindControl("Rb3") as CheckBox;
                CheckBox txtNameTags = GridView1.Rows[e.RowIndex].FindControl("Rb4") as CheckBox;
                CheckBox txtInsurances = GridView1.Rows[e.RowIndex].FindControl("Rb6") as CheckBox;
                CheckBox txtJamsosteks = GridView1.Rows[e.RowIndex].FindControl("Rb5") as CheckBox;
                CheckBox txtMLs = GridView1.Rows[e.RowIndex].FindControl("Rb7") as CheckBox;

                HiddenField txtTgls = GridView1.FooterRow.FindControl("txbtngl_foo") as HiddenField;
                HiddenField txtJams = GridView1.FooterRow.FindControl("txbjam_foo") as HiddenField;
                txtTgls.Value = TxbTnglBwh.Text.Trim();
                txtJams.Value = TxbPdJam.Text.Trim();


                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Employee_ID"] = txtEmpIds.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Name"] = txtNames.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Birth_Date"] = txtBirth_Dates.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Date_In"] = txtDate_Ins.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Date_Out"] = txtDate_Outs.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Gender"] = txtGenders.Text.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Job"] = txtJobs.Text.Trim();

                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["RainCoat"] = txtRainCoats.Checked;
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["CompanyUniform"] = txtUniForms.Checked;
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["NameCard"] = txtNameCards.Checked;
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["NameTag"] = txtNameTags.Checked;
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Insurance"] = txtInsurances.Checked;
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Jamsostek"] = txtJamsosteks.Checked;
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["MutationLetter"] = txtMLs.Checked;

                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Footer_Tngl"] = txtTgls.Value.Trim();
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex]["Footer_Jam"] = txtJams.Value.Trim();


                GlobalClass.adap.Update(GlobalClass.dt);
                GridView1.EditIndex = -1;
                FillGridView();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Response.Write("<script> alert('Record updation fail...') </script> {0}");
            }
        }

        protected void updateRecordTemp(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                TextBox txtEmpIds = GridView2.Rows[e.RowIndex].FindControl("txtEmpId3") as TextBox;
                TextBox txtNames = GridView2.Rows[e.RowIndex].FindControl("txtName3") as TextBox;
                TextBox txtBirth_Dates = GridView2.Rows[e.RowIndex].FindControl("txtBirth_Date3") as TextBox;
                TextBox txtDate_Ins = GridView2.Rows[e.RowIndex].FindControl("txtDate_In3") as TextBox;
                TextBox txtDate_Outs = GridView2.Rows[e.RowIndex].FindControl("txtDate_Out3") as TextBox;
                TextBox txtGenders = GridView2.Rows[e.RowIndex].FindControl("txtGender3") as TextBox;
                TextBox txtJobs = GridView2.Rows[e.RowIndex].FindControl("txtJob3") as TextBox;

                CheckBox txtRainCoats = GridView2.Rows[e.RowIndex].FindControl("Rb1_2") as CheckBox;
                CheckBox txtUniForms = GridView2.Rows[e.RowIndex].FindControl("Rb2_2") as CheckBox;
                CheckBox txtNameCards = GridView2.Rows[e.RowIndex].FindControl("Rb3_2") as CheckBox;
                CheckBox txtNameTags = GridView2.Rows[e.RowIndex].FindControl("Rb4_2") as CheckBox;
                CheckBox txtJamsosteks = GridView2.Rows[e.RowIndex].FindControl("Rb5_2") as CheckBox;//
                CheckBox txtInsurances = GridView2.Rows[e.RowIndex].FindControl("Rb6_2") as CheckBox;
                CheckBox txtMLs = GridView2.Rows[e.RowIndex].FindControl("Rb7_2") as CheckBox;

                HiddenField txtTgls = GridView2.FooterRow.FindControl("txbtngl_foo3") as HiddenField;
                HiddenField txtJams = GridView2.FooterRow.FindControl("txbjam_foo3") as HiddenField;
                txtTgls.Value = TxbTnglBwh.Text.Trim();
                txtJams.Value = TxbPdJam.Text.Trim();

                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Employee_ID"] = txtEmpIds.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Name"] = txtNames.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Birth_Date"] = txtBirth_Dates.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Date_In"] = txtDate_Ins.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Date_Out"] = txtDate_Outs.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Gender"] = txtGenders.Text.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Job"] = txtJobs.Text.Trim();

                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["RainCoat"] = txtRainCoats.Checked;
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["CompanyUniform"] = txtUniForms.Checked;
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["NameCard"] = txtNameCards.Checked;
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["NameTag"] = txtNameTags.Checked;
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Insurance"] = txtInsurances.Checked;
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Jamsostek"] = txtJamsosteks.Checked;
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["MutationLetter"] = txtMLs.Checked;

                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Footer_Tngl"] = txtTgls.Value.Trim();
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex]["Footer_Jam"] = txtJams.Value.Trim();

                GlobalClass.adap.Update(GlobalClass.dt);
                GridView2.EditIndex = -1;
                FillGridViewTemp();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Response.Write("<script> alert('Record updation fail...') </script>");
            }
        }

        protected void RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GlobalClass.dt.Rows[GridView1.Rows[e.RowIndex].RowIndex].Delete();
                GlobalClass.adap.Update(GlobalClass.dt);
                //Image imgPhoto = GridView1.Rows[e.RowIndex].FindControl("imgPhoto") as Image;
                //File.Delete(Server.MapPath(imgPhoto.ImageUrl));
                FillGridView();
            }
            catch
            {
                Response.Write("<script> alert('Record not deleted...') </script>");
            }
        }

        protected void RowDeletingTemp(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                GlobalClass.dt.Rows[GridView2.Rows[e.RowIndex].RowIndex].Delete();
                GlobalClass.adap.Update(GlobalClass.dt);
                FillGridViewTemp();
            }
            catch
            {
                Response.Write("<script> alert('Record not deleted...') </script>");
            }
        }

        protected void AddNewRecord(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    if (GlobalClass.dt.Rows.Count > 0)
                    {
                        GridView1.EditIndex = -1;
                        GridView1.ShowFooter = true;
                        FillGridView();
                    }
                    else
                    {
                        GridView1.ShowFooter = true;
                        DataRow dr = GlobalClass.dt.NewRow();
                        dr["Employee_ID"] = "0";
                        dr["Name"] = "0";
                        dr["Birth_Date"] = "0";
                        dr["Date_In"] = "0";
                        dr["Date_Out"] = "0";
                        dr["Gender"] = "0";
                        dr["Job"] = "0";
                        dr["RainCoat"] = 0;
                        dr["CompanyUniform"] = 0;
                        dr["NameCard"] = 0;
                        dr["NameTag"] = 0;
                        dr["Insurance"] = 0;
                        dr["Jamsostek"] = 0;
                        dr["MutationLetter"] = 0;
                        //dr["IDEmpAttr"] = "0";

                        //dr["photopath"] = "0";
                        GlobalClass.dt.Rows.Add(dr);
                        GridView1.DataSource = GlobalClass.dt;
                        GridView1.DataBind();
                        GridView1.Rows[0].Visible = false;
                    }
                }
                catch
                {
                    Response.Write("<script> alert('Row not added in DataTable...') </script>");
                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    if (GlobalClass.dt.Rows.Count > 0)
                    {
                        GridView2.EditIndex = -1;
                        GridView2.ShowFooter = true;
                        FillGridViewTemp();
                    }
                    else
                    {
                        GridView2.ShowFooter = true;
                        DataRow dr = GlobalClass.dt.NewRow();
                        dr["Employee_ID"] = "0";
                        dr["Name"] = "0";
                        dr["Birth_Date"] = "0";
                        dr["Date_In"] = "0";
                        dr["Date_Out"] = "0";
                        dr["Gender"] = "0";
                        dr["Job"] = "0";
                        dr["RainCoat"] = 0;
                        dr["CompanyUniform"] = 0;
                        dr["NameCard"] = 0;
                        dr["NameTag"] = 0;
                        dr["Insurance"] = 0;
                        dr["Jamsostek"] = 0;
                        dr["MutationLetter"] = 0;
                        //dr["IDProc_B"] = "0";

                        //dr["photopath"] = "0";
                        GlobalClass.dt.Rows.Add(dr);
                        GridView2.DataSource = GlobalClass.dt;
                        GridView2.DataBind();
                        GridView2.Rows[0].Visible = false;
                    }
                }
                catch
                {
                    Response.Write("<script> alert('Row not added in DataTable...') </script>");
                }

            }
            else
            {

            }

        }

        protected void AddNewCancel(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                GridView1.ShowFooter = false;
                FillGridView();

            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                GridView2.ShowFooter = false;
                FillGridViewTemp();
            }
            else
            {

            }

        }

        protected void InsertNewRecord(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            string valtgl = TxbTnglBwh.Text;
            string valjam = TxbPdJam.Text;
            string message = null;

            if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)) && (string.IsNullOrEmpty(TxtCreateDate.Text)) && (string.IsNullOrEmpty(TxtReviewDate.Text)))
            {
                string id = null;
                string modul = null;
                string HeadDibuat = null;
                string HeadDibuattngl = null;
                string Cabang = null;
                string AuditMulai = null;
                string AuditAkhir = null;
                string FooterLaks = null;
                string Footertngl = null;
                string Footerjam = null;
                string doe = DateTime.Now.ToString();
                string RevDate = DateTime.Now.ToString();
                try
                {
                    if (string.IsNullOrEmpty(valtgl) || string.IsNullOrEmpty(valjam))
                    {
                        message = "Pilih Tanggal Audit dan Jam audit !";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        string strName = GlobalClass.dt.Rows[0]["Employee_ID"].ToString();
                        if (strName == "0")
                        {
                            GlobalClass.dt.Rows[0].Delete();
                            GlobalClass.adap.Update(GlobalClass.dt);
                        }
                        TextBox txtEmpId = GridView1.FooterRow.FindControl("txtNewEmpId") as TextBox;
                        TextBox txtName = GridView1.FooterRow.FindControl("txtNewName") as TextBox;
                        TextBox txtBirth_Date = GridView1.FooterRow.FindControl("txtNewBirth_Date") as TextBox;
                        TextBox txtDate_In = GridView1.FooterRow.FindControl("txtNewDate_In") as TextBox;
                        TextBox txtDate_Out = GridView1.FooterRow.FindControl("txtNewDate_Out") as TextBox;
                        TextBox txtGender = GridView1.FooterRow.FindControl("txtNewGender") as TextBox;
                        TextBox txtJob = GridView1.FooterRow.FindControl("txtNewJob") as TextBox;

                        //TextBox txtRainCoat = GridView1.FooterRow.FindControl("txtNewRainCoat") as TextBox;
                        CheckBox txtRainCoat = GridView1.FooterRow.FindControl("Rb1_New") as CheckBox;
                        CheckBox txtUniForm = GridView1.FooterRow.FindControl("Rb2_New") as CheckBox;
                        CheckBox txtNameCard = GridView1.FooterRow.FindControl("Rb3_New") as CheckBox;
                        CheckBox txtNameTag = GridView1.FooterRow.FindControl("Rb4_New") as CheckBox;
                        CheckBox txtJamsostek = GridView1.FooterRow.FindControl("Rb5_New") as CheckBox;
                        CheckBox txtInsurance = GridView1.FooterRow.FindControl("Rb6_New") as CheckBox;
                        CheckBox txtML = GridView1.FooterRow.FindControl("Rb7_New") as CheckBox;
                        id = txtIDSaveTemp.Text;
                        modul = TxbModul.Text;
                        HeadDibuat = TxbDibuatHead.Text;
                        HeadDibuattngl = TxbTanggalHeadDibuat.Text;
                        Cabang = txbCabang.Text;
                        AuditMulai = txbPeriodeStart.Text;
                        AuditAkhir = txbPeriodeEnd.Text;
                        FooterLaks = TxbDilaksanakan.Text;
                        Footertngl = TxbTnglBwh.Text;
                        Footerjam = TxbPdJam.Text;
                        //doe = DateTime;

                        DataRow dr = GlobalClass.dt.NewRow();

                        dr["Employee_ID"] = txtEmpId.Text.Trim();
                        dr["Name"] = txtName.Text.Trim();
                        dr["Birth_Date"] = txtBirth_Date.Text.Trim();
                        dr["Date_In"] = txtDate_In.Text.Trim();
                        dr["Date_Out"] = txtDate_Out.Text.Trim();
                        dr["Gender"] = txtGender.Text.Trim();
                        dr["Job"] = txtJob.Text.Trim();

                        //dr["RainCoat"] = txtRainCoat.Text.Trim();
                        dr["RainCoat"] = txtRainCoat.Checked;
                        dr["CompanyUniform"] = txtUniForm.Checked;
                        dr["NameCard"] = txtNameCard.Checked;
                        dr["NameTag"] = txtNameTag.Checked;
                        dr["Jamsostek"] = txtJamsostek.Checked;
                        dr["Insurance"] = txtInsurance.Checked;
                        dr["MutationLetter"] = txtML.Checked;

                        dr["IDEmpAttr"] = txtIDSaveTemp.Text.Trim();
                        dr["Modul"] = TxbModul.Text.Trim();
                        dr["Header_Dibuat"] = TxbDibuatHead.Text.Trim();
                        dr["Header_Dibuat_Tngl"] = TxbTanggalHeadDibuat.Text.Trim();
                        dr["Bina_Artha_Cabang"] = txbCabang.Text.Trim();
                        dr["Periode_Audit_Start"] = txbPeriodeStart.Text.Trim();
                        dr["Periode_Audit_End"] = txbPeriodeEnd.Text.Trim();
                        dr["Footer_Dilaksanakan"] = TxbDilaksanakan.Text.Trim();
                        dr["Footer_Tngl"] = TxbTnglBwh.Text.Trim();
                        dr["Footer_Jam"] = TxbPdJam.Text.Trim();
                        dr["Doe"] = doe.ToString();
                        dr["ReviewDate"] = DBNull.Value;
                        //dr["photopath"] = "~/Images/" + FileName + ".png";
                        GlobalClass.dt.Rows.Add(dr);
                        GlobalClass.adap.Update(GlobalClass.dt);
                        GridView1.ShowFooter = false;
                        //SaveHeadHR();
                        FillGridView();
                        BindGridTemp();
                    }

                }
                catch
                {
                    Response.Write("<script> alert('Record not added...') </script>");
                }

            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                string id = null;
                string modul = null;
                string HeadDibuat = null;
                string HeadDibuattngl = null;
                string Cabang = null;
                string AuditMulai = null;
                string AuditAkhir = null;
                string FooterLaks = null;
                string Footertngl = null;
                string Footerjam = null;
                string doe = DateTime.Now.ToString();
                string RevDate = DateTime.Now.ToString();
                try
                {
                    if (string.IsNullOrEmpty(valtgl) || string.IsNullOrEmpty(valjam))
                    {
                        message = "Pilih Tanggal Audit dan Jam audit !";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        string strName = GlobalClass.dt.Rows[0]["Employee_ID"].ToString();
                        if (strName == "0")
                        {
                            GlobalClass.dt.Rows[0].Delete();
                            GlobalClass.adap.Update(GlobalClass.dt);
                        }
                        TextBox txtEmpId = GridView1.FooterRow.FindControl("txtNewEmpId") as TextBox;
                        TextBox txtName = GridView1.FooterRow.FindControl("txtNewName") as TextBox;
                        TextBox txtBirth_Date = GridView1.FooterRow.FindControl("txtNewBirth_Date") as TextBox;
                        TextBox txtDate_In = GridView1.FooterRow.FindControl("txtNewDate_In") as TextBox;
                        TextBox txtDate_Out = GridView1.FooterRow.FindControl("txtNewDate_Out") as TextBox;
                        TextBox txtGender = GridView1.FooterRow.FindControl("txtNewGender") as TextBox;
                        TextBox txtJob = GridView1.FooterRow.FindControl("txtNewJob") as TextBox;

                        //TextBox txtRainCoat = GridView1.FooterRow.FindControl("txtNewRainCoat") as TextBox;
                        CheckBox txtRainCoat = GridView1.FooterRow.FindControl("Rb1_New") as CheckBox;
                        CheckBox txtUniForm = GridView1.FooterRow.FindControl("Rb2_New") as CheckBox;
                        CheckBox txtNameCard = GridView1.FooterRow.FindControl("Rb3_New") as CheckBox;
                        CheckBox txtNameTag = GridView1.FooterRow.FindControl("Rb4_New") as CheckBox;
                        CheckBox txtJamsostek = GridView1.FooterRow.FindControl("Rb5_New") as CheckBox;
                        CheckBox txtInsurance = GridView1.FooterRow.FindControl("Rb6_New") as CheckBox;
                        CheckBox txtML = GridView1.FooterRow.FindControl("Rb7_New") as CheckBox;
                        id = txtIDSaveTemp.Text;
                        modul = TxbModul.Text;
                        HeadDibuat = TxbDibuatHead.Text;
                        HeadDibuattngl = TxbTanggalHeadDibuat.Text;
                        Cabang = txbCabang.Text;
                        AuditMulai = txbPeriodeStart.Text;
                        AuditAkhir = txbPeriodeEnd.Text;
                        FooterLaks = TxbDilaksanakan.Text;
                        Footertngl = TxbTnglBwh.Text;
                        Footerjam = TxbPdJam.Text;
                        //doe = DateTime;

                        DataRow dr = GlobalClass.dt.NewRow();

                        dr["Employee_ID"] = txtEmpId.Text.Trim();
                        dr["Name"] = txtName.Text.Trim();
                        dr["Birth_Date"] = txtBirth_Date.Text.Trim();
                        dr["Date_In"] = txtDate_In.Text.Trim();
                        dr["Date_Out"] = txtDate_Out.Text.Trim();
                        dr["Gender"] = txtGender.Text.Trim();
                        dr["Job"] = txtJob.Text.Trim();

                        //dr["RainCoat"] = txtRainCoat.Text.Trim();
                        dr["RainCoat"] = txtRainCoat.Checked;
                        dr["CompanyUniform"] = txtUniForm.Checked;
                        dr["NameCard"] = txtNameCard.Checked;
                        dr["NameTag"] = txtNameTag.Checked;
                        dr["Jamsostek"] = txtJamsostek.Checked;
                        dr["Insurance"] = txtInsurance.Checked;
                        dr["MutationLetter"] = txtML.Checked;

                        dr["IDEmpAttr"] = txtIDSaveTemp.Text.Trim();
                        dr["Modul"] = TxbModul.Text.Trim();
                        dr["Header_Dibuat"] = TxbDibuatHead.Text.Trim();
                        dr["Header_Dibuat_Tngl"] = TxbTanggalHeadDibuat.Text.Trim();
                        dr["Bina_Artha_Cabang"] = txbCabang.Text.Trim();
                        dr["Periode_Audit_Start"] = txbPeriodeStart.Text.Trim();
                        dr["Periode_Audit_End"] = txbPeriodeEnd.Text.Trim();
                        dr["Footer_Dilaksanakan"] = TxbDilaksanakan.Text.Trim();
                        dr["Footer_Tngl"] = TxbTnglBwh.Text.Trim();
                        dr["Footer_Jam"] = TxbPdJam.Text.Trim();
                        dr["Doe"] = DBNull.Value;
                        dr["ReviewDate"] = RevDate.ToString();
                        //dr["photopath"] = "~/Images/" + FileName + ".png";
                        GlobalClass.dt.Rows.Add(dr);
                        GlobalClass.adap.Update(GlobalClass.dt);
                        GridView1.ShowFooter = false;
                        //SaveHeadHR();
                        FillGridView();
                        BindGridTemp();
                    }

                }
                catch
                {
                    Response.Write("<script> alert('Record not added...') </script>");
                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                string id = null;
                string modul = null;
                string HeadDibuat = null;
                string HeadDibuattngl = null;
                string Cabang = null;
                string AuditMulai = null;
                string AuditAkhir = null;
                string FooterLaks = null;
                string Footertngl = null;
                string Footerjam = null;
                string doe = DateTime.Now.ToString();
                string RevDate = DateTime.Now.ToString();
                try
                {
                    if (string.IsNullOrEmpty(valtgl) || string.IsNullOrEmpty(valjam))
                    {
                        message = "Pilih Tanggal Audit dan Jam audit !";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        string strName = GlobalClass.dt.Rows[0]["Employee_ID"].ToString();
                        if (strName == "0")
                        {
                            GlobalClass.dt.Rows[0].Delete();
                            GlobalClass.adap.Update(GlobalClass.dt);
                        }
                        TextBox txtEmpId = GridView2.FooterRow.FindControl("txtNewEmpId3") as TextBox;
                        TextBox txtName = GridView2.FooterRow.FindControl("txtNewName3") as TextBox;
                        TextBox txtBirth_Date = GridView2.FooterRow.FindControl("txtNewBirth_Date3") as TextBox;
                        TextBox txtDate_In = GridView2.FooterRow.FindControl("txtNewDate_In3") as TextBox;
                        TextBox txtDate_Out = GridView2.FooterRow.FindControl("txtNewDate_Out3") as TextBox;
                        TextBox txtGender = GridView2.FooterRow.FindControl("txtNewGender3") as TextBox;
                        TextBox txtJob = GridView2.FooterRow.FindControl("txtNewJob3") as TextBox;

                        CheckBox txtRainCoat = GridView2.FooterRow.FindControl("Rb1_New2") as CheckBox;
                        CheckBox txtUniForm = GridView2.FooterRow.FindControl("Rb2_New2") as CheckBox;
                        CheckBox txtNameCard = GridView2.FooterRow.FindControl("Rb3_New2") as CheckBox;
                        CheckBox txtNameTag = GridView2.FooterRow.FindControl("Rb4_New2") as CheckBox;
                        CheckBox txtJamsostek = GridView2.FooterRow.FindControl("Rb5_New2") as CheckBox;
                        CheckBox txtInsurance = GridView2.FooterRow.FindControl("Rb6_New2") as CheckBox;//txtInsurance
                        CheckBox txtML = GridView2.FooterRow.FindControl("Rb7_New2") as CheckBox;

                        id = txtIDSave.Text;
                        modul = TxbModul.Text;
                        HeadDibuat = TxbDibuatHead.Text;
                        HeadDibuattngl = TxbTanggalHeadDibuat.Text;
                        Cabang = txbCabang.Text;
                        AuditMulai = txbPeriodeStart.Text;
                        AuditAkhir = txbPeriodeEnd.Text;
                        FooterLaks = TxbDilaksanakan.Text;
                        Footertngl = TxbTnglBwh.Text;
                        Footerjam = TxbPdJam.Text;
                        //doe = DateTime;

                        DataRow dr = GlobalClass.dt.NewRow();

                        dr["Employee_ID"] = txtEmpId.Text.Trim();
                        dr["Name"] = txtName.Text.Trim();
                        dr["Birth_Date"] = txtBirth_Date.Text.Trim();
                        dr["Date_In"] = txtDate_In.Text.Trim();
                        dr["Date_Out"] = txtDate_Out.Text.Trim();
                        dr["Gender"] = txtGender.Text.Trim();
                        dr["Job"] = txtJob.Text.Trim();

                        dr["RainCoat"] = txtRainCoat.Checked;
                        dr["CompanyUniform"] = txtUniForm.Checked;
                        dr["NameCard"] = txtNameCard.Checked;
                        dr["NameTag"] = txtNameTag.Checked;
                        dr["Insurance"] = txtInsurance.Checked;
                        dr["Jamsostek"] = txtJamsostek.Checked;
                        dr["MutationLetter"] = txtML.Checked;

                        dr["IDEmpAttr"] = txtIDSave.Text.Trim();
                        dr["Modul"] = TxbModul.Text.Trim();
                        dr["Header_Dibuat"] = TxbDibuatHead.Text.Trim();
                        dr["Header_Dibuat_Tngl"] = TxbTanggalHeadDibuat.Text.Trim();
                        dr["Bina_Artha_Cabang"] = txbCabang.Text.Trim();
                        dr["Periode_Audit_Start"] = txbPeriodeStart.Text.Trim();
                        dr["Periode_Audit_End"] = txbPeriodeEnd.Text.Trim();
                        dr["Footer_Dilaksanakan"] = TxbDilaksanakan.Text.Trim();
                        dr["Footer_Tngl"] = TxbTnglBwh.Text.Trim();
                        dr["Footer_Jam"] = TxbPdJam.Text.Trim();
                        dr["Doe"] = doe.ToString();
                        dr["ReviewDate"] = DBNull.Value;
                        //dr["photopath"] = "~/Images/" + FileName + ".png";
                        GlobalClass.dt.Rows.Add(dr);
                        GlobalClass.adap.Update(GlobalClass.dt);
                        GridView2.ShowFooter = false;
                        //SaveHeadHR();
                        FillGridViewTemp();
                        BindGrid();
                    }

                }
                catch
                {
                    Response.Write("<script> alert('Record not added...') </script>");
                }
            }
            else
            {

            }
        }
    }
}