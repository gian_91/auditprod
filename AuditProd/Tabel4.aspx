﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tabel4.aspx.cs" Inherits="AuditProd.Tabel4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link rel="Stylesheet" href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.10/themes/redmond/jquery-ui.css" />
    <script src="Scripts/Bootstrap/jquery-1.8.2.js"></script>
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="Scripts/Js/jquery-ui.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                $('.mydatepickerclass').datepicker({ dateFormat: 'dd/mm/yy' });
            }

        });
    </script>
    <style type="text/css">
        .datepicker2 {z-index: 1151 !important;}
        .clsDatePicker {z-index: 100000;}

        .auto-style1 {
            width: 231px;
        }

        .auto-style2 {
            height: 49px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

        .bordered {
            width: 306px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 306px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 304px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 855px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowbutton {
            padding-left: 220px;
        }

        .div-table-cell {
            display: table-cell;
            width: auto;
            clear: both;
        }

        .buttonCek {
            background: lightblue;
            border-radius: 10px;
            vertical-align: middle;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
        <header>
        <link href="Scripts/Bootstrap/bootstrap.css" rel="stylesheet" />
        <script src="Scripts/Bootstrap/bootstrap.js"></script>
    </header>
    <fieldset>
        <legend></legend>
        <div class="Table">
            <div class="div-table-row">
                <div class="div-table-cell">
                    <asp:TextBox ID="TxbIdLHKAquc" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="TxbIdLHKColl" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="TxbId" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="TxbId2" runat="server" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbID_RDT" runat="server" ReadOnly="true" Width="83px" Visible="false" AutoPostBack="true"></asp:TextBox>
                    <asp:TextBox ID="txbAccountId" runat="server" ReadOnly="true" Width="140px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbID_DocTest" runat="server" ReadOnly="true" Width="140px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txbUsername" runat="server" ReadOnly="true" Width="123px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="123px" Visible="false"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtIDSaveTemp" ReadOnly="True" Visible="false" />
                    <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false" />
                    <asp:TextBox runat="server" ID="TxbNoTabel" ReadOnly="True" Visible="false" />
                    <asp:TextBox runat="server" ID="TxbJenTabel" ReadOnly="true" Visible="false"></asp:TextBox>
                </div>
            </div>
        </div>
        <div align="center">
            <h2 style="margin: 0px; color: #112aa1;">Detail Tabel4</h2>
        </div>
        <br />
        <div style="width: 100%; margin-right: 5%; margin-left: 0%; text-align: center">
            <asp:UpdatePanel ID="upCrudGrid" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="GridView1" runat="server" Width="940px" HorizontalAlign="Center"
                        OnRowCommand="GridView1_RowCommand" AutoGenerateColumns="False" AllowPaging="True" PageSize="10" OnPageIndexChanging="GridView1_PageIndexChanging"
                        DataKeyNames="No" CssClass="table table-hover table-striped" CellPadding="4" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:ButtonField CommandName="detail" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Detail" HeaderText="Detailed View" Visible="false">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="editRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Edit" HeaderText="Edit Record">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="deleteRecord" ControlStyle-CssClass="btn btn-info"
                                ButtonType="Button" Text="Delete" HeaderText="Delete Record" Visible="true">
                                <ControlStyle CssClass="btn btn-info"></ControlStyle>
                            </asp:ButtonField>
                            <asp:BoundField DataField="No" HeaderText="No" Visible="false"/>
                            <asp:BoundField DataField="AccountID" HeaderText="AccountId" Visible="true" />
                            <asp:BoundField DataField="ClientName" HeaderText="Client Name" />
                            <asp:BoundField DataField="CenterName" HeaderText="Center Name" />
                            <asp:BoundField DataField="DueDays" HeaderText="Due Days" />
                            <asp:BoundField DataField="PICName" HeaderText="PIC Name" />
                            <asp:BoundField DataField="DateClientPaid" HeaderText="Date Client Paid" />

                            <asp:BoundField DataField="AmountClientPaid" HeaderText="Amount Client Paid" />
                            <asp:BoundField DataField="DateDeposittoBranch" HeaderText="Date Deposit to Branch" />
                            <asp:BoundField DataField="AmountDeposittoBranch" HeaderText="Amount Deposit to Branch" />
                            <asp:BoundField DataField="VarianceDay" HeaderText="Variance Day" />
                            <asp:BoundField DataField="VarianceAmount" HeaderText="Variance Amount" />
                            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />

                            <asp:BoundField DataField="SamplingStatus" HeaderText="Sampling Status" />
                        </Columns>                        
                        <EditRowStyle BackColor="#2461BF" />
                        <EmptyDataTemplate>
                            <div align="center">
                                No record available
                            </div>
                        </EmptyDataTemplate>                        
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                    <asp:Button ID="btnAdd" runat="server" Text="Add New Record" CssClass="btn btn-info" OnClick="btnAdd_Click" />
                </ContentTemplate>
                <Triggers>
                </Triggers>
            </asp:UpdatePanel>
            <!-- Detail Modal Starts here-->
            <div id="detailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Detailed View</h3>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DetailsView ID="DetailsView1" runat="server" CssClass="table table-bordered table-hover" BackColor="White" ForeColor="Black" FieldHeaderStyle-Wrap="false" FieldHeaderStyle-Font-Bold="true" FieldHeaderStyle-BackColor="LavenderBlush" FieldHeaderStyle-ForeColor="Black" BorderStyle="Groove" AutoGenerateRows="False">
                                <Fields>
                                    <asp:BoundField DataField="No" HeaderText="No" />
                                    <asp:BoundField DataField="ID_LHK" HeaderText="Id LHK" Visible="false" />
                                    <asp:BoundField DataField="LHK_Acquisition_Category" HeaderText="Category" />
                                    <asp:BoundField DataField="LHK_Acquisition_TOD" HeaderText="Tod" />
                                    <asp:BoundField DataField="LHK_Acquisition_CaseFinding" HeaderText="CaseFinding" />
                                    <asp:BoundField DataField="LHK_Acquisition_Tabel" HeaderText="Tabel" />
                                    <asp:BoundField DataField="LHK_Acquisition_Recomd_Branch" HeaderText="Recomd Branch" />
                                    <asp:BoundField DataField="LHK_Acquisition_Recomd_Management" HeaderText="Recomd Management" />
                                </Fields>
                            </asp:DetailsView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                            <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
            </div>
            <!-- Detail Modal Ends here -->
            <!-- Edit Modal Starts here -->
            <div id="editModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="editModalLabel">Edit Record</h3>
                </div>
                <asp:UpdatePanel ID="upEdit" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblCountryCode" runat="server" Visible="false"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">AccountId :</label>
                                        <asp:TextBox ID="TxtAccountIdEdit" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">ClientName :</label>
                                        <asp:TextBox ID="TxtClientNameEdit" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">CenterName :</label>
                                        <asp:TextBox ID="TxtCenterNameEdit" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">DueDays :</label>
                                        <asp:TextBox ID="TxtDueDaysEdit" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">PICName :</label>
                                        <asp:TextBox ID="TxtPicNameEdit" runat="server" AutoCompleteType="Disabled" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">DateClientPaiD :</label>
                                        <asp:TextBox ID="DateClientpaiDEdit" runat="server" CssClass="mydatepickerclass" AutoPostBack="false" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">AmountClientPaid :</label>
                                        <asp:TextBox ID="AmountClientPaidEdit" runat="server" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="AmountClientPaidEdit_TextChanged"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">DateDepositToBranch :</label>
                                        <asp:TextBox ID="DateDepositToBranchEdit" runat="server" CssClass="mydatepickerclass" AutoPostBack="false" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">AmountDepositToBranch :</label>
                                        <asp:TextBox ID="AmountDepositToBranchEdit" runat="server" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="AmountDepositToBranchEdit_TextChanged"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">VarianceDay :</label>
                                        <asp:TextBox ID="VarianceDayEdit" runat="server" ReadOnly="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">VarianceAmount :</label>
                                        <asp:TextBox ID="VarianceAmountEdit" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Remarks :</label>
                                        <asp:TextBox ID="TxtRemarksEdit" runat="server" TextMode="MultiLine" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">SamplingStatus :</label>
                                        <asp:TextBox ID="ddlsmplingEdit" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Label ID="lblResult" Visible="false" runat="server"></asp:Label>
                            <asp:Button ID="Button1" runat="server" Text="Update" CssClass="btn btn-info" OnClick="btnSave_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="GridView1" EventName="RowCommand" />
                        <asp:AsyncPostBackTrigger ControlID="btnAdd" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!-- Edit Modal Ends here -->
            <!-- Add Record Modal Starts here-->
            <div id="addModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="addModalLabel">Add New Record Tabel 4</h3>
                </div>
                <asp:UpdatePanel ID="upAdd" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            <table class="table table-bordered table-hover">
                                <asp:TextBox ID="TxtidlhkAdd" runat="server" Visible="false"></asp:TextBox>
<%--                                <tr>
                                    <td>
                                        <asp:Label ID="lblIdAdd" runat="server" Visible="false"></asp:Label>                                        
                                    </td>
                                </tr>--%>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">AccountId :</label>
                                        <asp:TextBox ID="TxtAccountIdAdd" runat="server" CssClass="form-control" ReadOnly="true" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">ClientName :</label>
                                        <asp:TextBox ID="TxtClientNameAdd" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">CenterName :</label>
                                        <asp:TextBox ID="TxtCenterNameAdd" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">DueDays :</label>
                                        <asp:TextBox ID="TxtDueDaysAdd" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">PICName :</label>
                                        <asp:TextBox ID="TxtPicNameAdd" runat="server" AutoCompleteType="Disabled" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">DateClientpaID :</label>
                                        <asp:TextBox ID="DateClientpaIDAdd" runat="server" CssClass="mydatepickerclass" AutoPostBack="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">AmountClientPaid :</label>
                                        <asp:TextBox ID="AmountClientPaidAdd" runat="server" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="AmountClientPaidAdd_TextChanged"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">DateDeposittoBranch :</label>
                                        <asp:TextBox ID="DateDeposittoBranchAdd" runat="server" CssClass="mydatepickerclass" AutoPostBack="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">AmountDeposittoBranch :</label>
                                        <asp:TextBox ID="AmountDeposittoBranchAdd" runat="server" OnTextChanged="AmountDeposittoBranchAdd_TextChanged" AutoPostBack="true" AutoCompleteType="Disabled"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">VarianceDay :</label>
                                        <asp:TextBox ID="VarianceDayAdd" runat="server" ReadOnly="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">VarianceAmount :</label>
                                        <asp:TextBox ID="VarianceAmountAdd" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">Remarks :</label>
                                        <asp:TextBox ID="TxtRemarksAdd" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="font-weight: bold; font-size: 15px">SamplingStatus :</label>
                                        <asp:TextBox ID="ddlsmplingAdd" runat="server" ReadOnly="true"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnAddRecord" runat="server" Text="Add" CssClass="btn btn-info" OnClick="btnAddRecord_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Close</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnAddRecord" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Add Record Modal Ends here-->
            <!-- Delete Record Modal Starts here-->
            <div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delModalLabel" aria-hidden="true">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="delModalLabel">Delete Record</h3>
                </div>
                <asp:UpdatePanel ID="upDel" runat="server">
                    <ContentTemplate>
                        <div class="modal-body">
                            Are you sure you want to delete the record?
                            <asp:HiddenField ID="hfCode" runat="server" />
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-info" OnClick="btnDelete_Click" />
                            <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Cancel</button>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnDelete" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <!--Delete Record Modal Ends here -->
        </div>
        <div class="TableButton">
            <asp:Button ID="btnExit" runat="server" Text="Exit" Width="100px" OnClick="btnExit_Click" />
            <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false"></asp:Label>
        </div>
    </fieldset>
</asp:Content>
