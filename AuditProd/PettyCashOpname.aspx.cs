﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using ClosedXML.Excel;

namespace AuditProd
{
    public partial class PettyCashOpname : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TxbModul.Text = "PettyCashOpname";
            if (!IsPostBack)
            {
                this.TxbTanggalHeadDibuat.Text = DateTime.Today.ToString("dd/MM/yyyy");
                TxbDibuatHead.Text = this.Page.User.Identity.Name;
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                TxbBwahDilaksnakan.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {
                    LoadSaveID();
                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {
                    LoadSaveIDTemp();
                }
            }
            else
            {
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

            }

        }

        object ToDBValue(string str)
        {
            return string.IsNullOrEmpty(str) ? DBNull.Value : (object)str;
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("PettyCash.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("PettyCash.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                SaveId();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)) && (string.IsNullOrEmpty(TxtCreateDate.Text)) && (string.IsNullOrEmpty(TxtReviewDate.Text)))
            {
                SavedReview();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                SaveIdTemp();
            }
        }

        protected void TxbUK100rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUK100rb.Text, out txt1Value);
            double.TryParse("100000", out txt2Value);
            double.TryParse(TxbHsl100rb.Text, out total);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl50rb.Text, out txt3Value);
            double.TryParse(TxbHsl20rb.Text, out txt4Value);
            double.TryParse(TxbHsl10rb.Text, out txt5Value);
            double.TryParse(TxbHsl5rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl100rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            TxbUKTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUK50rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUK50rb.Text, out txt1Value);
            double.TryParse("50000", out txt2Value);
            double.TryParse(TxbHsl50rb.Text, out total);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl20rb.Text, out txt4Value);
            double.TryParse(TxbHsl10rb.Text, out txt5Value);
            double.TryParse(TxbHsl5rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl50rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            TxbUKTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUK20rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUK20rb.Text, out txt1Value);
            double.TryParse("20000", out txt2Value);
            double.TryParse(TxbHsl20rb.Text, out total);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl10rb.Text, out txt5Value);
            double.TryParse(TxbHsl5rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl20rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            TxbUKTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));
        }

        protected void TxbUK10rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUK10rb.Text, out txt1Value);
            double.TryParse("10000", out txt2Value);
            double.TryParse(TxbHsl10rb.Text, out total);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl20rb.Text, out txt5Value);
            double.TryParse(TxbHsl5rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl10rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            TxbUKTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUK5rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUK5rb.Text, out txt1Value);
            double.TryParse("5000", out txt2Value);
            double.TryParse(TxbHsl5rb.Text, out total);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl20rb.Text, out txt5Value);
            double.TryParse(TxbHsl10rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl5rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            TxbUKTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUK2rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUK2rb.Text, out txt1Value);
            double.TryParse("2000", out txt2Value);
            double.TryParse(TxbHsl2rb.Text, out total);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl20rb.Text, out txt5Value);
            double.TryParse(TxbHsl10rb.Text, out txt6Value);
            double.TryParse(TxbHsl5rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl2rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            TxbUKTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUK1rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUK1rb.Text, out txt1Value);
            double.TryParse("1000", out txt2Value);
            double.TryParse(TxbHsl1rb.Text, out total);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl20rb.Text, out txt5Value);
            double.TryParse(TxbHsl10rb.Text, out txt6Value);
            double.TryParse(TxbHsl5rb.Text, out txt7Value);
            double.TryParse(TxbHsl2rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl1rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            TxbUKTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUL1rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, ULtotal, txt3Value, txt4Value, txt5Value, txt6Value, UKtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUL1rb.Text, out txt1Value);
            double.TryParse("1000", out txt2Value);
            double.TryParse(TxbHsl1serb.Text, out total);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUKTotal.Text, out UKtotal);

            double.TryParse(TxbHsl5rts.Text, out txt3Value);
            double.TryParse(TxbHsl2rts.Text, out txt4Value);
            double.TryParse(TxbHsl1rts.Text, out txt5Value);
            double.TryParse(TxbHsl50.Text, out txt6Value);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            total = txt1Value * txt2Value;
            TxbHsl1serb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbULTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUL5rts_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, ULtotal, txt3Value, txt4Value, txt5Value, txt6Value, UKtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUL5rts.Text, out txt1Value);
            double.TryParse("500", out txt2Value);
            double.TryParse(TxbHsl1serb.Text, out total);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl1serb.Text, out txt3Value);
            double.TryParse(TxbHsl2rts.Text, out txt4Value);
            double.TryParse(TxbHsl1rts.Text, out txt5Value);
            double.TryParse(TxbHsl50.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl5rts.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbULTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUL2rts_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, ULtotal, txt3Value, txt4Value, txt5Value, txt6Value, UKtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUL2rts.Text, out txt1Value);
            double.TryParse("200", out txt2Value);
            double.TryParse(TxbHsl1serb.Text, out total);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl1serb.Text, out txt3Value);
            double.TryParse(TxbHsl5rts.Text, out txt4Value);
            double.TryParse(TxbHsl1rts.Text, out txt5Value);
            double.TryParse(TxbHsl50.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl2rts.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbULTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUL1rts_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, ULtotal, txt3Value, txt4Value, txt5Value, txt6Value, UKtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUL1rts.Text, out txt1Value);
            double.TryParse("100", out txt2Value);
            double.TryParse(TxbHsl1serb.Text, out total);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl1serb.Text, out txt3Value);
            double.TryParse(TxbHsl5rts.Text, out txt4Value);
            double.TryParse(TxbHsl2rts.Text, out txt5Value);
            double.TryParse(TxbHsl50.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl1rts.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbULTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbUL50_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, ULtotal, txt3Value, txt4Value, txt5Value, txt6Value, UKtotal, UkUlTotal, BonTotal;
            total = 0;
            double.TryParse(TxbUL50.Text, out txt1Value);
            double.TryParse("50", out txt2Value);
            double.TryParse(TxbHsl1serb.Text, out total);
            double.TryParse(TxbULTotal.Text, out ULtotal);
            double.TryParse(TxbUKTotal.Text, out UKtotal);
            double.TryParse(TxbUkUlTotal.Text, out UkUlTotal);
            double.TryParse(TxbBonTotal.Text, out BonTotal);

            double.TryParse(TxbHsl1serb.Text, out txt3Value);
            double.TryParse(TxbHsl5rts.Text, out txt4Value);
            double.TryParse(TxbHsl2rts.Text, out txt5Value);
            double.TryParse(TxbHsl1rts.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl50.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbULTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal;
            TxbUkUlTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

            var total3 = total2 + BonTotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total3.ToString()));

        }

        protected void TxbOpCbng_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, txt3Value, txt4Value, total, totalAtas;
            total = 0;
            double.TryParse(TxbOpCbng.Text, out txt1Value);
            double.TryParse(TxbKRT.Text, out txt2Value);
            double.TryParse(TxbATK.Text, out txt3Value);
            double.TryParse(TxbOthers.Text, out txt4Value);
            double.TryParse(TxbBonTotal.Text, out total);
            double.TryParse(TxbUkUlTotal.Text, out totalAtas);

            total = txt1Value + txt2Value + txt3Value + txt4Value;
            TxbBonTotal.Text = total.ToString();

            var total1 = total + totalAtas;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString())); ;

        }

        protected void TxbKRT_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, txt3Value, txt4Value, total, totalAtas;
            total = 0;
            double.TryParse(TxbOpCbng.Text, out txt1Value);
            double.TryParse(TxbKRT.Text, out txt2Value);
            double.TryParse(TxbATK.Text, out txt3Value);
            double.TryParse(TxbOthers.Text, out txt4Value);
            double.TryParse(TxbBonTotal.Text, out total);
            double.TryParse(TxbUkUlTotal.Text, out totalAtas);

            total = txt1Value + txt2Value + txt3Value + txt4Value;
            TxbBonTotal.Text = total.ToString();

            var total1 = total + totalAtas;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

        }

        protected void TxbATK_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, txt3Value, txt4Value, total, totalAtas;
            total = 0;
            double.TryParse(TxbOpCbng.Text, out txt1Value);
            double.TryParse(TxbKRT.Text, out txt2Value);
            double.TryParse(TxbATK.Text, out txt3Value);
            double.TryParse(TxbOthers.Text, out txt4Value);
            double.TryParse(TxbBonTotal.Text, out total);
            double.TryParse(TxbUkUlTotal.Text, out totalAtas);

            total = txt1Value + txt2Value + txt3Value + txt4Value;
            TxbBonTotal.Text = total.ToString();

            var total1 = total + totalAtas;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

        }

        protected void TxbOthers_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, txt3Value, txt4Value, total, totalAtas;
            total = 0;
            double.TryParse(TxbOpCbng.Text, out txt1Value);
            double.TryParse(TxbKRT.Text, out txt2Value);
            double.TryParse(TxbATK.Text, out txt3Value);
            double.TryParse(TxbOthers.Text, out txt4Value);
            double.TryParse(TxbBonTotal.Text, out total);
            double.TryParse(TxbUkUlTotal.Text, out totalAtas);

            total = txt1Value + txt2Value + txt3Value + txt4Value;
            TxbBonTotal.Text = total.ToString();

            var total1 = total + totalAtas;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

        }

        protected void TxbDanaTetap_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total;
            total = 0;
            double.TryParse(TxbGrandTotal.Text, out txt1Value);
            double.TryParse(TxbDanaTetap.Text, out txt2Value);
            double.TryParse(TxbSelisih.Text, out total);

            total = txt1Value - txt2Value;
            if (total <= 0)
            {
                TxbSelisih.Text = string.Format(CultureInfo.CreateSpecificCulture("en-ID"), "({0:C2})", total);
            }
            else
            {
                TxbSelisih.Text = string.Format(CultureInfo.CreateSpecificCulture("en-ID"), "{0:C2}", total);
            }

        }

        private void LoadSaveID()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_GetData_PCO", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPCO", txtIDSave.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(dr["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(dr["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(dr["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(dr["Periode_Audit_End"]);

                TxbUK100rb.Text = Convert.ToString(dr["UK_Item_100rb"]);
                TxbUK50rb.Text = Convert.ToString(dr["UK_Item_50rb"]);
                TxbUK20rb.Text = Convert.ToString(dr["UK_Item_20rb"]);
                TxbUK10rb.Text = Convert.ToString(dr["UK_Item_10rb"]);
                TxbUK5rb.Text = Convert.ToString(dr["UK_Item_5rb"]);
                TxbUK2rb.Text = Convert.ToString(dr["UK_Item_2rb"]);
                TxbUK1rb.Text = Convert.ToString(dr["UK_Item_1rb"]);
                TxbUL1rb.Text = Convert.ToString(dr["UL_Item_1rb"]);
                TxbUL5rts.Text = Convert.ToString(dr["UL_Item_5rts"]);
                TxbUL2rts.Text = Convert.ToString(dr["UL_Item_2rts"]);
                TxbUL1rts.Text = Convert.ToString(dr["UL_Item_1rts"]);
                TxbUL50.Text = Convert.ToString(dr["UL_Item_50"]);

                TxbHsl100rb.Text = Convert.ToString(dr["UK_Total_100rb"]);
                TxbHsl50rb.Text = Convert.ToString(dr["UK_Total_50rb"]);
                TxbHsl20rb.Text = Convert.ToString(dr["UK_Total_20rb"]);
                TxbHsl10rb.Text = Convert.ToString(dr["UK_Total_10rb"]);
                TxbHsl5rb.Text = Convert.ToString(dr["UK_Total_5rb"]);
                TxbHsl2rb.Text = Convert.ToString(dr["UK_Total_2rb"]);
                TxbHsl1rb.Text = Convert.ToString(dr["UK_Total_1rb"]);
                TxbHsl1serb.Text = Convert.ToString(dr["UL_Total_1rb"]);
                TxbHsl5rts.Text = Convert.ToString(dr["UL_Total_5rts"]);
                TxbHsl2rts.Text = Convert.ToString(dr["UL_Total_2rts"]);
                TxbHsl1rts.Text = Convert.ToString(dr["UL_Total_1rts"]);
                TxbHsl50.Text = Convert.ToString(dr["UL_Total_50"]);

                TxbUKTotal.Text = Convert.ToString(dr["UK_Total"]);
                TxbULTotal.Text = Convert.ToString(dr["UL_Total"]);
                TxbUkUlTotal.Text = Convert.ToString(dr["UKUL_Total"]);

                TglDariOpCbng.Text = Convert.ToString(dr["Bon_Tgl_Sampai_1"]);
                TglSmpaiOpCbng.Text = Convert.ToString(dr["Bon_Tgl_Dengan_1"]);
                TglDariKPT.Text = Convert.ToString(dr["Bon_Tgl_Sampai_2"]);
                TglSmpaiKPT.Text = Convert.ToString(dr["Bon_Tgl_Dengan_2"]);
                TglDariATK.Text = Convert.ToString(dr["Bon_Tgl_Sampai_3"]);
                TglSmpaiATK.Text = Convert.ToString(dr["Bon_Tgl_Dengan_3"]);
                TglDariOther.Text = Convert.ToString(dr["Bon_Tgl_Sampai_4"]);
                TglSmpaiOther.Text = Convert.ToString(dr["Bon_Tgl_Dengan_4"]);

                TxbOpCbng.Text = Convert.ToString(dr["Bon_Total_1"]);
                TxbKRT.Text = Convert.ToString(dr["Bon_Total_2"]);
                TxbATK.Text = Convert.ToString(dr["Bon_Total_3"]);
                TxbOthers.Text = Convert.ToString(dr["Bon_Total_4"]);
                TxbBonTotal.Text = Convert.ToString(dr["Bon_Total"]);

                TxbGrandTotal.Text = Convert.ToString(dr["PCO_GrandTotal"]);
                TxbDanaTetap.Text = Convert.ToString(dr["PCO_DanaTetap"]);
                TxbSelisih.Text = Convert.ToString(dr["PCO_Selisih"]);

                RemarksSelisih.InnerText = Convert.ToString(dr["Remaks_Selisih"]);

                TxbBwahDilaksnakan.Text = Convert.ToString(dr["Footer_Dilaksanakan"]);
                TxbBwahtngl.Text = Convert.ToString(dr["Footer_Tngl"]);
                txbBwahJam.Text = Convert.ToString(dr["Footer_Jam"]);

            }
            con.Close();
        }

        private void LoadSaveIDTemp()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_GetData_PCO", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPCO", txtIDSaveTemp.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(dr["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(dr["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(dr["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(dr["Periode_Audit_End"]);

                TxbUK100rb.Text = Convert.ToString(dr["UK_Item_100rb"]);
                TxbUK50rb.Text = Convert.ToString(dr["UK_Item_50rb"]);
                TxbUK20rb.Text = Convert.ToString(dr["UK_Item_20rb"]);
                TxbUK10rb.Text = Convert.ToString(dr["UK_Item_10rb"]);
                TxbUK5rb.Text = Convert.ToString(dr["UK_Item_5rb"]);
                TxbUK2rb.Text = Convert.ToString(dr["UK_Item_2rb"]);
                TxbUK1rb.Text = Convert.ToString(dr["UK_Item_1rb"]);
                TxbUL1rb.Text = Convert.ToString(dr["UL_Item_1rb"]);
                TxbUL5rts.Text = Convert.ToString(dr["UL_Item_5rts"]);
                TxbUL2rts.Text = Convert.ToString(dr["UL_Item_2rts"]);
                TxbUL1rts.Text = Convert.ToString(dr["UL_Item_1rts"]);
                TxbUL50.Text = Convert.ToString(dr["UL_Item_50"]);

                TxbHsl100rb.Text = Convert.ToString(dr["UK_Total_100rb"]);
                TxbHsl50rb.Text = Convert.ToString(dr["UK_Total_50rb"]);
                TxbHsl20rb.Text = Convert.ToString(dr["UK_Total_20rb"]);
                TxbHsl10rb.Text = Convert.ToString(dr["UK_Total_10rb"]);
                TxbHsl5rb.Text = Convert.ToString(dr["UK_Total_5rb"]);
                TxbHsl2rb.Text = Convert.ToString(dr["UK_Total_2rb"]);
                TxbHsl1rb.Text = Convert.ToString(dr["UK_Total_1rb"]);
                TxbHsl1serb.Text = Convert.ToString(dr["UL_Total_1rb"]);
                TxbHsl5rts.Text = Convert.ToString(dr["UL_Total_5rts"]);
                TxbHsl2rts.Text = Convert.ToString(dr["UL_Total_2rts"]);
                TxbHsl1rts.Text = Convert.ToString(dr["UL_Total_1rts"]);
                TxbHsl50.Text = Convert.ToString(dr["UL_Total_50"]);

                TxbUKTotal.Text = Convert.ToString(dr["UK_Total"]);
                TxbULTotal.Text = Convert.ToString(dr["UL_Total"]);
                TxbUkUlTotal.Text = Convert.ToString(dr["UKUL_Total"]);

                TglDariOpCbng.Text = Convert.ToString(dr["Bon_Tgl_Sampai_1"]);
                TglSmpaiOpCbng.Text = Convert.ToString(dr["Bon_Tgl_Dengan_1"]);
                TglDariKPT.Text = Convert.ToString(dr["Bon_Tgl_Sampai_2"]);
                TglSmpaiKPT.Text = Convert.ToString(dr["Bon_Tgl_Dengan_2"]);
                TglDariATK.Text = Convert.ToString(dr["Bon_Tgl_Sampai_3"]);
                TglSmpaiATK.Text = Convert.ToString(dr["Bon_Tgl_Dengan_3"]);
                TglDariOther.Text = Convert.ToString(dr["Bon_Tgl_Sampai_4"]);
                TglSmpaiOther.Text = Convert.ToString(dr["Bon_Tgl_Dengan_4"]);

                TxbOpCbng.Text = Convert.ToString(dr["Bon_Total_1"]);
                TxbKRT.Text = Convert.ToString(dr["Bon_Total_2"]);
                TxbATK.Text = Convert.ToString(dr["Bon_Total_3"]);
                TxbOthers.Text = Convert.ToString(dr["Bon_Total_4"]);
                TxbBonTotal.Text = Convert.ToString(dr["Bon_Total"]);

                TxbGrandTotal.Text = Convert.ToString(dr["PCO_GrandTotal"]);
                TxbDanaTetap.Text = Convert.ToString(dr["PCO_DanaTetap"]);
                TxbSelisih.Text = Convert.ToString(dr["PCO_Selisih"]);

                RemarksSelisih.InnerText = Convert.ToString(dr["Remaks_Selisih"]);

                TxbBwahDilaksnakan.Text = Convert.ToString(dr["Footer_Dilaksanakan"]);
                TxbBwahtngl.Text = Convert.ToString(dr["Footer_Tngl"]);
                txbBwahJam.Text = Convert.ToString(dr["Footer_Jam"]);
                TxtCreateDate.Text = Convert.ToString(dr["Doe"]);
                TxtReviewDate.Text = Convert.ToString(dr["ReviewDate"]);

            }
            con.Close();
        }

        private void SaveId()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SavePCO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDPCO", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_100rb", string.IsNullOrEmpty(TxbUK100rb.Text) ? (object)DBNull.Value : TxbUK100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_50rb", string.IsNullOrEmpty(TxbUK50rb.Text) ? (object)DBNull.Value : TxbUK50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_20rb", string.IsNullOrEmpty(TxbUK20rb.Text) ? (object)DBNull.Value : TxbUK20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_10rb", string.IsNullOrEmpty(TxbUK10rb.Text) ? (object)DBNull.Value : TxbUK10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_5rb", string.IsNullOrEmpty(TxbUK5rb.Text) ? (object)DBNull.Value : TxbUK5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_2rb", string.IsNullOrEmpty(TxbUK2rb.Text) ? (object)DBNull.Value : TxbUK2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_1rb", string.IsNullOrEmpty(TxbUK1rb.Text) ? (object)DBNull.Value : TxbUK1rb.Text);

                    cmd.Parameters.AddWithValue("@UL_Item_1rb", string.IsNullOrEmpty(TxbUL1rb.Text) ? (object)DBNull.Value : TxbUL1rb.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_5rts", string.IsNullOrEmpty(TxbUL5rts.Text) ? (object)DBNull.Value : TxbUL5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_2rts", string.IsNullOrEmpty(TxbUL2rts.Text) ? (object)DBNull.Value : TxbUL2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_1rts", string.IsNullOrEmpty(TxbUL1rts.Text) ? (object)DBNull.Value : TxbUL1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_50", string.IsNullOrEmpty(TxbUL50.Text) ? (object)DBNull.Value : TxbUL50.Text);

                    cmd.Parameters.AddWithValue("@UK_Total_100rb", string.IsNullOrEmpty(TxbHsl100rb.Text) ? (object)DBNull.Value : TxbHsl100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_50rb", string.IsNullOrEmpty(TxbHsl50rb.Text) ? (object)DBNull.Value : TxbHsl50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_20rb", string.IsNullOrEmpty(TxbHsl20rb.Text) ? (object)DBNull.Value : TxbHsl20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_10rb", string.IsNullOrEmpty(TxbHsl10rb.Text) ? (object)DBNull.Value : TxbHsl10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_5rb", string.IsNullOrEmpty(TxbHsl5rb.Text) ? (object)DBNull.Value : TxbHsl5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_2rb", string.IsNullOrEmpty(TxbHsl2rb.Text) ? (object)DBNull.Value : TxbHsl2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_1rb", string.IsNullOrEmpty(TxbHsl1rb.Text) ? (object)DBNull.Value : TxbHsl1rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total", string.IsNullOrEmpty(TxbUKTotal.Text) ? (object)DBNull.Value : TxbUKTotal.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rb", string.IsNullOrEmpty(TxbHsl1serb.Text) ? (object)DBNull.Value : TxbHsl1serb.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_5rts", string.IsNullOrEmpty(TxbHsl5rts.Text) ? (object)DBNull.Value : TxbHsl5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_2rts", string.IsNullOrEmpty(TxbHsl2rts.Text) ? (object)DBNull.Value : TxbHsl2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rts", string.IsNullOrEmpty(TxbHsl1rts.Text) ? (object)DBNull.Value : TxbHsl1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_50", string.IsNullOrEmpty(TxbHsl50.Text) ? (object)DBNull.Value : TxbHsl50.Text);
                    cmd.Parameters.AddWithValue("@UL_Total", string.IsNullOrEmpty(TxbULTotal.Text) ? (object)DBNull.Value : TxbULTotal.Text);
                    cmd.Parameters.AddWithValue("@UKUL_Total", string.IsNullOrEmpty(TxbUkUlTotal.Text) ? (object)DBNull.Value : TxbUkUlTotal.Text);

                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_1", string.IsNullOrEmpty(TglDariOpCbng.Text) ? (object)DBNull.Value : TglDariOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_1", string.IsNullOrEmpty(TglSmpaiOpCbng.Text) ? (object)DBNull.Value : TglSmpaiOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_2", string.IsNullOrEmpty(TglDariKPT.Text) ? (object)DBNull.Value : TglDariKPT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_2", string.IsNullOrEmpty(TglSmpaiKPT.Text) ? (object)DBNull.Value : TglSmpaiKPT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_3", string.IsNullOrEmpty(TglDariATK.Text) ? (object)DBNull.Value : TglDariATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_3", string.IsNullOrEmpty(TglSmpaiATK.Text) ? (object)DBNull.Value : TglSmpaiATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_4", string.IsNullOrEmpty(TglDariOther.Text) ? (object)DBNull.Value : TglDariOther.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_4", string.IsNullOrEmpty(TglSmpaiOther.Text) ? (object)DBNull.Value : TglSmpaiOther.Text);

                    cmd.Parameters.AddWithValue("@Bon_Total_1", string.IsNullOrEmpty(TxbOpCbng.Text) ? (object)DBNull.Value : TxbOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_2", string.IsNullOrEmpty(TxbKRT.Text) ? (object)DBNull.Value : TxbKRT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_3", string.IsNullOrEmpty(TxbATK.Text) ? (object)DBNull.Value : TxbATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_4", string.IsNullOrEmpty(TxbOthers.Text) ? (object)DBNull.Value : TxbOthers.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total", string.IsNullOrEmpty(TxbBonTotal.Text) ? (object)DBNull.Value : TxbBonTotal.Text);

                    cmd.Parameters.AddWithValue("@PCO_GrandTotal", string.IsNullOrEmpty(TxbGrandTotal.Text) ? (object)DBNull.Value : TxbGrandTotal.Text);
                    cmd.Parameters.AddWithValue("@PCO_DanaTetap", string.IsNullOrEmpty(TxbDanaTetap.Text) ? (object)DBNull.Value : TxbDanaTetap.Text);
                    cmd.Parameters.AddWithValue("@PCO_Selisih", string.IsNullOrEmpty(TxbSelisih.Text) ? (object)DBNull.Value : TxbSelisih.Text);

                    cmd.Parameters.AddWithValue("@Remaks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbBwahDilaksnakan.Text) ? (object)DBNull.Value : TxbBwahDilaksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbBwahtngl.Text) ? (object)DBNull.Value : TxbBwahtngl.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(txbBwahJam.Text) ? (object)DBNull.Value : txbBwahJam.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }

        private void SavedReview()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SavePCO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDPCO", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_100rb", string.IsNullOrEmpty(TxbUK100rb.Text) ? (object)DBNull.Value : TxbUK100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_50rb", string.IsNullOrEmpty(TxbUK50rb.Text) ? (object)DBNull.Value : TxbUK50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_20rb", string.IsNullOrEmpty(TxbUK20rb.Text) ? (object)DBNull.Value : TxbUK20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_10rb", string.IsNullOrEmpty(TxbUK10rb.Text) ? (object)DBNull.Value : TxbUK10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_5rb", string.IsNullOrEmpty(TxbUK5rb.Text) ? (object)DBNull.Value : TxbUK5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_2rb", string.IsNullOrEmpty(TxbUK2rb.Text) ? (object)DBNull.Value : TxbUK2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_1rb", string.IsNullOrEmpty(TxbUK1rb.Text) ? (object)DBNull.Value : TxbUK1rb.Text);

                    cmd.Parameters.AddWithValue("@UL_Item_1rb", string.IsNullOrEmpty(TxbUL1rb.Text) ? (object)DBNull.Value : TxbUL1rb.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_5rts", string.IsNullOrEmpty(TxbUL5rts.Text) ? (object)DBNull.Value : TxbUL5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_2rts", string.IsNullOrEmpty(TxbUL2rts.Text) ? (object)DBNull.Value : TxbUL2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_1rts", string.IsNullOrEmpty(TxbUL1rts.Text) ? (object)DBNull.Value : TxbUL1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_50", string.IsNullOrEmpty(TxbUL50.Text) ? (object)DBNull.Value : TxbUL50.Text);

                    cmd.Parameters.AddWithValue("@UK_Total_100rb", string.IsNullOrEmpty(TxbHsl100rb.Text) ? (object)DBNull.Value : TxbHsl100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_50rb", string.IsNullOrEmpty(TxbHsl50rb.Text) ? (object)DBNull.Value : TxbHsl50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_20rb", string.IsNullOrEmpty(TxbHsl20rb.Text) ? (object)DBNull.Value : TxbHsl20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_10rb", string.IsNullOrEmpty(TxbHsl10rb.Text) ? (object)DBNull.Value : TxbHsl10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_5rb", string.IsNullOrEmpty(TxbHsl5rb.Text) ? (object)DBNull.Value : TxbHsl5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_2rb", string.IsNullOrEmpty(TxbHsl2rb.Text) ? (object)DBNull.Value : TxbHsl2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_1rb", string.IsNullOrEmpty(TxbHsl1rb.Text) ? (object)DBNull.Value : TxbHsl1rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total", string.IsNullOrEmpty(TxbUKTotal.Text) ? (object)DBNull.Value : TxbUKTotal.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rb", string.IsNullOrEmpty(TxbHsl1serb.Text) ? (object)DBNull.Value : TxbHsl1serb.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_5rts", string.IsNullOrEmpty(TxbHsl5rts.Text) ? (object)DBNull.Value : TxbHsl5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_2rts", string.IsNullOrEmpty(TxbHsl2rts.Text) ? (object)DBNull.Value : TxbHsl2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rts", string.IsNullOrEmpty(TxbHsl1rts.Text) ? (object)DBNull.Value : TxbHsl1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_50", string.IsNullOrEmpty(TxbHsl50.Text) ? (object)DBNull.Value : TxbHsl50.Text);
                    cmd.Parameters.AddWithValue("@UL_Total", string.IsNullOrEmpty(TxbULTotal.Text) ? (object)DBNull.Value : TxbULTotal.Text);
                    cmd.Parameters.AddWithValue("@UKUL_Total", string.IsNullOrEmpty(TxbUkUlTotal.Text) ? (object)DBNull.Value : TxbUkUlTotal.Text);

                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_1", string.IsNullOrEmpty(TglDariOpCbng.Text) ? (object)DBNull.Value : TglDariOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_1", string.IsNullOrEmpty(TglSmpaiOpCbng.Text) ? (object)DBNull.Value : TglSmpaiOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_2", string.IsNullOrEmpty(TglDariKPT.Text) ? (object)DBNull.Value : TglDariKPT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_2", string.IsNullOrEmpty(TglSmpaiKPT.Text) ? (object)DBNull.Value : TglSmpaiKPT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_3", string.IsNullOrEmpty(TglDariATK.Text) ? (object)DBNull.Value : TglDariATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_3", string.IsNullOrEmpty(TglSmpaiATK.Text) ? (object)DBNull.Value : TglSmpaiATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_4", string.IsNullOrEmpty(TglDariOther.Text) ? (object)DBNull.Value : TglDariOther.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_4", string.IsNullOrEmpty(TglSmpaiOther.Text) ? (object)DBNull.Value : TglSmpaiOther.Text);

                    cmd.Parameters.AddWithValue("@Bon_Total_1", string.IsNullOrEmpty(TxbOpCbng.Text) ? (object)DBNull.Value : TxbOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_2", string.IsNullOrEmpty(TxbKRT.Text) ? (object)DBNull.Value : TxbKRT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_3", string.IsNullOrEmpty(TxbATK.Text) ? (object)DBNull.Value : TxbATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_4", string.IsNullOrEmpty(TxbOthers.Text) ? (object)DBNull.Value : TxbOthers.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total", string.IsNullOrEmpty(TxbBonTotal.Text) ? (object)DBNull.Value : TxbBonTotal.Text);

                    cmd.Parameters.AddWithValue("@PCO_GrandTotal", string.IsNullOrEmpty(TxbGrandTotal.Text) ? (object)DBNull.Value : TxbGrandTotal.Text);
                    cmd.Parameters.AddWithValue("@PCO_DanaTetap", string.IsNullOrEmpty(TxbDanaTetap.Text) ? (object)DBNull.Value : TxbDanaTetap.Text);
                    cmd.Parameters.AddWithValue("@PCO_Selisih", string.IsNullOrEmpty(TxbSelisih.Text) ? (object)DBNull.Value : TxbSelisih.Text);

                    cmd.Parameters.AddWithValue("@Remaks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbBwahDilaksnakan.Text) ? (object)DBNull.Value : TxbBwahDilaksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbBwahtngl.Text) ? (object)DBNull.Value : TxbBwahtngl.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(txbBwahJam.Text) ? (object)DBNull.Value : txbBwahJam.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }

        private void SaveIdTemp()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SavePCO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDPCO", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_100rb", string.IsNullOrEmpty(TxbUK100rb.Text) ? (object)DBNull.Value : TxbUK100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_50rb", string.IsNullOrEmpty(TxbUK50rb.Text) ? (object)DBNull.Value : TxbUK50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_20rb", string.IsNullOrEmpty(TxbUK20rb.Text) ? (object)DBNull.Value : TxbUK20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_10rb", string.IsNullOrEmpty(TxbUK10rb.Text) ? (object)DBNull.Value : TxbUK10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_5rb", string.IsNullOrEmpty(TxbUK5rb.Text) ? (object)DBNull.Value : TxbUK5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_2rb", string.IsNullOrEmpty(TxbUK2rb.Text) ? (object)DBNull.Value : TxbUK2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_1rb", string.IsNullOrEmpty(TxbUK1rb.Text) ? (object)DBNull.Value : TxbUK1rb.Text);

                    cmd.Parameters.AddWithValue("@UL_Item_1rb", string.IsNullOrEmpty(TxbUL1rb.Text) ? (object)DBNull.Value : TxbUL1rb.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_5rts", string.IsNullOrEmpty(TxbUL5rts.Text) ? (object)DBNull.Value : TxbUL5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_2rts", string.IsNullOrEmpty(TxbUL2rts.Text) ? (object)DBNull.Value : TxbUL2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_1rts", string.IsNullOrEmpty(TxbUL1rts.Text) ? (object)DBNull.Value : TxbUL1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_50", string.IsNullOrEmpty(TxbUL50.Text) ? (object)DBNull.Value : TxbUL50.Text);

                    cmd.Parameters.AddWithValue("@UK_Total_100rb", string.IsNullOrEmpty(TxbHsl100rb.Text) ? (object)DBNull.Value : TxbHsl100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_50rb", string.IsNullOrEmpty(TxbHsl50rb.Text) ? (object)DBNull.Value : TxbHsl50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_20rb", string.IsNullOrEmpty(TxbHsl20rb.Text) ? (object)DBNull.Value : TxbHsl20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_10rb", string.IsNullOrEmpty(TxbHsl10rb.Text) ? (object)DBNull.Value : TxbHsl10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_5rb", string.IsNullOrEmpty(TxbHsl5rb.Text) ? (object)DBNull.Value : TxbHsl5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_2rb", string.IsNullOrEmpty(TxbHsl2rb.Text) ? (object)DBNull.Value : TxbHsl2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_1rb", string.IsNullOrEmpty(TxbHsl1rb.Text) ? (object)DBNull.Value : TxbHsl1rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total", string.IsNullOrEmpty(TxbUKTotal.Text) ? (object)DBNull.Value : TxbUKTotal.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rb", string.IsNullOrEmpty(TxbHsl1serb.Text) ? (object)DBNull.Value : TxbHsl1serb.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_5rts", string.IsNullOrEmpty(TxbHsl5rts.Text) ? (object)DBNull.Value : TxbHsl5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_2rts", string.IsNullOrEmpty(TxbHsl2rts.Text) ? (object)DBNull.Value : TxbHsl2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rts", string.IsNullOrEmpty(TxbHsl1rts.Text) ? (object)DBNull.Value : TxbHsl1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_50", string.IsNullOrEmpty(TxbHsl50.Text) ? (object)DBNull.Value : TxbHsl50.Text);
                    cmd.Parameters.AddWithValue("@UL_Total", string.IsNullOrEmpty(TxbULTotal.Text) ? (object)DBNull.Value : TxbULTotal.Text);
                    cmd.Parameters.AddWithValue("@UKUL_Total", string.IsNullOrEmpty(TxbUkUlTotal.Text) ? (object)DBNull.Value : TxbUkUlTotal.Text);

                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_1", string.IsNullOrEmpty(TglDariOpCbng.Text) ? (object)DBNull.Value : TglDariOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_1", string.IsNullOrEmpty(TglSmpaiOpCbng.Text) ? (object)DBNull.Value : TglSmpaiOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_2", string.IsNullOrEmpty(TglDariKPT.Text) ? (object)DBNull.Value : TglDariKPT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_2", string.IsNullOrEmpty(TglSmpaiKPT.Text) ? (object)DBNull.Value : TglSmpaiKPT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_3", string.IsNullOrEmpty(TglDariATK.Text) ? (object)DBNull.Value : TglDariATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_3", string.IsNullOrEmpty(TglSmpaiATK.Text) ? (object)DBNull.Value : TglSmpaiATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Sampai_4", string.IsNullOrEmpty(TglDariOther.Text) ? (object)DBNull.Value : TglDariOther.Text);
                    cmd.Parameters.AddWithValue("@Bon_Tgl_Dengan_4", string.IsNullOrEmpty(TglSmpaiOther.Text) ? (object)DBNull.Value : TglSmpaiOther.Text);

                    cmd.Parameters.AddWithValue("@Bon_Total_1", string.IsNullOrEmpty(TxbOpCbng.Text) ? (object)DBNull.Value : TxbOpCbng.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_2", string.IsNullOrEmpty(TxbKRT.Text) ? (object)DBNull.Value : TxbKRT.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_3", string.IsNullOrEmpty(TxbATK.Text) ? (object)DBNull.Value : TxbATK.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total_4", string.IsNullOrEmpty(TxbOthers.Text) ? (object)DBNull.Value : TxbOthers.Text);
                    cmd.Parameters.AddWithValue("@Bon_Total", string.IsNullOrEmpty(TxbBonTotal.Text) ? (object)DBNull.Value : TxbBonTotal.Text);

                    cmd.Parameters.AddWithValue("@PCO_GrandTotal", string.IsNullOrEmpty(TxbGrandTotal.Text) ? (object)DBNull.Value : TxbGrandTotal.Text);
                    cmd.Parameters.AddWithValue("@PCO_DanaTetap", string.IsNullOrEmpty(TxbDanaTetap.Text) ? (object)DBNull.Value : TxbDanaTetap.Text);
                    cmd.Parameters.AddWithValue("@PCO_Selisih", string.IsNullOrEmpty(TxbSelisih.Text) ? (object)DBNull.Value : TxbSelisih.Text);

                    cmd.Parameters.AddWithValue("@Remaks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbBwahDilaksnakan.Text) ? (object)DBNull.Value : TxbBwahDilaksnakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbBwahtngl.Text) ? (object)DBNull.Value : TxbBwahtngl.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(txbBwahJam.Text) ? (object)DBNull.Value : txbBwahJam.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DBNull.Value;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }
    }
}