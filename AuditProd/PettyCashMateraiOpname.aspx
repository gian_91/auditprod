﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PettyCashMateraiOpname.aspx.cs" Inherits="AuditProd.PettyCashMateraiOpname" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="Scripts/jquery.number.js" type="text/javascript"></script>
    <script src="Scripts/jquery.number.min.js" type="text/javascript"></script>
    <script src="Scripts/autoNumeric.js"></script>

    <script>
        jQuery(function ($) {
            $('[id$=TxbTotalMaterai],[id$=TxbHsl100rb],[id$=TxbHsl50rb],[id$=TxbHsl20rb],[id$=TxbHsl10rb],[id$=TxbHsl5rb],[id$=TxbHsl2rb],[id$=TxbHsl1rb],[id$=txbTotalFUUK],[id$=TxbHsl1rbLgm],[id$=TxbHsl5rts],[id$=TxbHsl2rts],[id$=TxbHsl1rts],[id$=TxbHsl50],[id$=TxbTotalFUUL],[id$=TxbGrandTotal],[id$=TxbDanaTetap]').autoNumeric('init');
        });
    </script>
    <script>
        $(function () {
            $("[id$=TxbTnglMaterai],[id$=TxbTnglbwah]").datepicker({
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true
            });
        });
    </script>
    <script type="text/javascript">
        function HideLabel() {
            var seconds = 2;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
    </script>
    <style type="text/css">
        .txt {
            padding-left: 5px;
            width: 155px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

        .bordered {
            width: 314px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 310px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 292px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
            padding-left: 10px;
        }

        .TableHeader {
            display: table;
            padding-left: 300px;
        }

        .Table-tanggalbawah {
            display: table;
            position: relative;
            left: 5px;
        }

        .Table-ttd {
            display: table;
            position: relative;
            left: 70px;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 730px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: 140px;
        }

        .div-table-cell-kotak-kanan {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 5px;
        }

        .div-table-cell-kotak-kanan-2 {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 13px;
        }

        .div-table-header-kanan {
            width: 420px;
            height: 120px;
            border: 1px solid Blue;
            box-sizing: border-box;
            right: -500px;
            position: relative;
        }

        .div-grup {
            width: auto;
            display: table-column-group;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 2px; /* cellspacing:poor IE support for  this */
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowMid-ttd {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-colHeaderJudul {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 928px;
            height: 38px;
            background-color: whitesmoke;
            padding-left: 15px;
        }

        .div-table-colMid {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colNomer {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 40px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colJenisUang {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 100px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colSpaceAwal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 140px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colJumlahUang {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colTotalUang {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 150px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colSisaPage {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 464px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidTotalUang3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 185px;
            height: 35px;
            background-color: #352d2d;
        }

        .div-table-colMidTextMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colTotalUangKertasSpaceawal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 440px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colTotalUangKertas {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 154px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colTotalUangKertas2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 150px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colTotalUangKertasSpaceAkhir {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 200px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colBon {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 904px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colGranTotal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 544px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colGranTotalTextbox {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 200px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colGranTotalTextbox2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 200px;
            height: 35px;
            background-color: #f14444;
        }

        .div-table-colMidTextBoxMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-tableMid-TextArea {
            display: table;
            width: 944px;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-colTanggal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 127px;
            height: 35px;
            background-color: none;
        }

        .div-table-colTanggal2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 105px;
            height: 35px;
            background-color: none;
            padding-left: 100px;
        }

        .div-table-colTanggal3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 70px;
            height: 35px;
            background-color: none;
        }

        .div-table-colMidSelisih {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 940px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidSelisihtextArea {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 940px;
            height: 185px;
            background-color: whitesmoke;
        }

        .div-table-colTTD {
            float: right;
            display: table-column;
            width: 260px;
            height: 30px;
            background-color: none;
        }

        .textarea2 {
            font-family: 'Times New Roman';
            width: 928px;
            height: 175px;
            font-size: 15px;
        }

        .display-validate {
            clear: both;
            display: block;
            position: relative;
            left: 210px;
            bottom: 30px;
        }

        @media print {
            #printbtn, [id$=btnSave], [id$=btnExit], [id$=ExportExcel] {
                display: none;
            }
        }

        .div-table-Judul {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 940px;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <fieldset>
        <legend></legend>
        <div class="Table">
            <div class="div-table-header-kanan">
                <label align="center">IAC GL - 03</label>
                <hr />
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="TxbDibuatHead">Dibuat </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbDibuatHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="TxbDireviewHead">Direview </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbDireviewHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="TxbTanggalHeadDibuat">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbTanggalHeadDibuat" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="TxbTanggalHeadDireview">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbTanggalHeadDireview" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="Table">
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSaveTemp" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                        <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false" />
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>
                    </div>
                </div>                
            </div>
            <br />
            <div align="center" class="div-table-Judul">
                <h4>Pada hari ini kami melakukan pemeriksaan Petty Cash terhadap Admin & Cashier dengan rincian sebagai berikut :</h4>
            </div>            
        </div>
            <br />
            <br />
        <div align="center" class="TableHeader">
            <h2 style="margin:0px;">Petty Cash Materai Opname</h2>
            <h4 style="margin:0px;">Internal Audit & Control Department</h4>
        </div>
            <br />
            <br />
            <br />
            <br />
        <div class="Table">
            <div class="div-tableMid" >
                <p style="font-weight:bold"></p>
                <div class="div-table-rowMid">
                    <div class="div-table-colHeaderJudul" style="font-size: 20px; font-weight: bold; text-decoration: underline;">
                        Stok Materai
                    </div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colTotalUangKertasSpaceawal">
                        Stock Tanggal :<asp:TextBox ID="TxbTnglMaterai" runat="server" Width="90px" AutoCompleteType="Disabled" ></asp:TextBox>Jumlah Materai
                        <asp:TextBox ID="TxbJmlMaterai" runat="server" Width="30px" OnTextChanged="TxbJmlMaterai_TextChanged" AutoCompleteType="Disabled" AutoPostBack="true"></asp:TextBox>X 6000
                    </div>
                    <div class="div-table-colTotalUangKertas2"></div>
                    <div class="div-table-colTotalUangKertas" align="center">Rp<asp:TextBox ID="TxbTotalMaterai" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colTotalUangKertasSpaceAkhir"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colHeaderJudul" style="font-size: 20px; font-weight: bold; text-decoration: underline;">
                        Fisik Uang
                    </div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center" style="font-weight:bold">1.</div>
                    <div class="div-table-colJenisUang" align="center" style="font-weight:bold">Uang Kertas</div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbFU100rb" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbFU100rb_TextChanged"></asp:TextBox>Lbr X Rp100.000
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl100rb" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbFU50rb" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbFU50rb_TextChanged"></asp:TextBox>Lbr X Rp 50.000
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl50rb" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbFU20rb" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbFU20rb_TextChanged"></asp:TextBox>Lbr X Rp 20.000
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl20rb" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbFU10rb" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbFU10rb_TextChanged" ></asp:TextBox>Lbr X Rp 10.000
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl10rb" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbFU5rb" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbFU5rb_TextChanged"></asp:TextBox>Lbar  X Rp 5.000
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl5rb" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbFU2rb" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbFU2rb_TextChanged" ></asp:TextBox>Lbr  X Rp 2.000
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl2rb" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbFU1rb" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbFU1rb_TextChanged" ></asp:TextBox>Lbr  X Rp 1.000
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl1rb" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                    <div class="div-table-rowMid">
                    <div class="div-table-colTotalUangKertasSpaceawal" align="center" style="font-weight:bold">Total Uang Kertas</div>
                    <div class="div-table-colTotalUangKertas2"></div>
                    <div class="div-table-colTotalUangKertas" align="center">Rp<asp:TextBox ID="txbTotalFUUK" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colTotalUangKertasSpaceAkhir"></div>
                </div>
                    <div class="div-table-rowMid">
                    <div class="div-table-colNomer" align="center" style="font-weight:bold">2.</div>
                    <div class="div-table-colJenisUang" align="center" style="font-weight:bold">Uang Logam</div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbUL1rb" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbUL1rb_TextChanged"></asp:TextBox>Kpg X Rp1.000
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl1rbLgm" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbUL5rts" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbUL5rts_TextChanged"></asp:TextBox>Kpg X Rp 500
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl5rts" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbUL2rts" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbUL2rts_TextChanged"></asp:TextBox>Kpg X Rp 200
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl2rts" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbUL1rts" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbUL1rts_TextChanged"></asp:TextBox>Kpg X Rp 100
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl1rts" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colSpaceAwal" align="center"></div>
                    <div class="div-table-colJumlahUang" >
                        <asp:TextBox ID="TxbUL50" runat="server" Width="70px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbUL50_TextChanged"></asp:TextBox>Kpg  X Rp 50
                    </div>
                    <div class="div-table-colTotalUang" align="center">Rp<asp:TextBox ID="TxbHsl50" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colSisaPage"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colTotalUangKertasSpaceawal" align="center" style="font-weight:bold">Total Uang Logam</div>
                    <div class="div-table-colTotalUangKertas2"></div>
                    <div class="div-table-colTotalUangKertas" align="center">Rp<asp:TextBox ID="TxbTotalFUUL" runat="server" Width="120px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                    <div class="div-table-colTotalUangKertasSpaceAkhir"></div>
                </div>
                <div class="div-table-rowMid">
                    <div class="div-table-colGranTotal" align="center"></div>
                    <div class="div-table-colGranTotalTextbox" style="font-weight:bold" >Grand Total</div>
                    <div class="div-table-colGranTotalTextbox" align="center">Rp<asp:TextBox ID="TxbGrandTotal" runat="server" Width="170px" ReadOnly="true" AutoCompleteType="Disabled" ></asp:TextBox></div>
                </div>
                    <div class="div-table-rowMid">
                    <div class="div-table-colGranTotal" align="center"></div>
                    <div class="div-table-colGranTotalTextbox" style="font-weight:bold" >Dana Tetap</div>
                    <div class="div-table-colGranTotalTextbox" align="center">Rp<asp:TextBox ID="TxbDanaTetap" runat="server" Width="170px" AutoCompleteType="Disabled" AutoPostBack="true" OnTextChanged="TxbDanaTetap_TextChanged" ></asp:TextBox></div>
                </div>
                    <div class="div-table-rowMid">
                    <div class="div-table-colGranTotal" align="center"></div>
                    <div class="div-table-colGranTotalTextbox2" style="font-weight:bold">Selisih Lebih/Kurang</div>
                    <div class="div-table-colGranTotalTextbox2" align="center"><asp:TextBox ID="TxbSelisih" runat="server" Width="185px" ReadOnly="true" AutoCompleteType="Disabled"></asp:TextBox></div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="Table">
            <div class="div-tableMid-TextArea">
                <div class="div-table-rowMid">
                    <div class="div-table-colMidSelisih">
                        <label>Penjelasan Selisih :</label>
                    </div>
                    <div class="div-table-colMidSelisihtextArea">
                        <textarea class="textarea2" id="RemarksSelisih" runat="server"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="Table">
            <div class="Table-tanggalbawah" align="center">
                <div class="div-table-colTanggal">
                    <label>Dilaksanakan Di</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbDilaksanakan" runat="server" Width="210px" AutoCompleteType="Disabled" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal2">
                    <label>Pada Tanggal</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbTnglbwah" runat="server" Width="110px" AutoCompleteType="Disabled" ></asp:TextBox>
                </div>
                <div class="div-table-colTanggal3">
                    <label>Pada Jam</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbPadaJam" runat="server" Width="110px" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
            </div>
            </div>
        <br />
        <br />
        <br />
                <div class="Table">
                <div class="Table-ttd">
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Diperiksa Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Disaksikan Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Mengetahui,</div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">(................................)</div>
                    <div class="div-table-colTTD" align="Center">(................................)</div>
                    <div class="div-table-colTTD" align="Center">(................................)</div>
                </div>
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Internal Audit&Control</div>
                    <div class="div-table-colTTD" align="Center">Admin/Kasir</div>
                    <div class="div-table-colTTD" align="Center">Branch Manager</div>
                </div>
            </div>
        </div>
        <br />
        <div class="Table">
            <div class="TableButton">
                <input id="printbtn" type="button" value="Print" onclick="window.print();">
                <asp:Button ID="btnExit" runat="server" Text="Exit" Width="70px" OnClick="btnExit_Click" />
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="70px" OnClick="btnSave_Click" />
                <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false" ></asp:Label>
            </div>
        </div>
            <asp:TextBox ID="TxtCreateDate" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TxtReviewDate" runat="server" Visible="false"></asp:TextBox>
    </fieldset>
</asp:Content>
