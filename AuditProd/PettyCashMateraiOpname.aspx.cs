﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using ClosedXML.Excel;

namespace AuditProd
{
    public partial class PettyCashMateraiOpname : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TxbModul.Text = "PettyCashMateraiOpname";
            if (!IsPostBack)
            {
                this.TxbTanggalHeadDibuat.Text = DateTime.Today.ToString("dd/MM/yyyy");
                TxbDibuatHead.Text = this.Page.User.Identity.Name;
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                TxbDilaksanakan.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {
                    LoadSaveID();
                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {
                    LoadSaveIDTemp();
                }
            }
            else
            {
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

            }

        }

        object ToDBValue(string str)
        {
            return string.IsNullOrEmpty(str) ? DBNull.Value : (object)str;
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("PettyCash.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("PettyCash.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                SaveID();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)) && (string.IsNullOrEmpty(TxtCreateDate.Text)) && (string.IsNullOrEmpty(TxtReviewDate.Text)))
            {
                SavedReview();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                SaveIDtemp();
            }
        }

        protected void TxbJmlMaterai_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbJmlMaterai.Text, out txt1Value);
            double.TryParse("6000", out txt2Value);
            double.TryParse(TxbTotalMaterai.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);
            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            total = txt1Value * txt2Value;
            TxbTotalMaterai.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));
            var total1 = total + UKtotal + ULtotal;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

        }

        protected void TxbFU100rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbFU100rb.Text, out txt1Value);
            double.TryParse("100000", out txt2Value);
            double.TryParse(TxbHsl100rb.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl50rb.Text, out txt3Value);
            double.TryParse(TxbHsl20rb.Text, out txt4Value);
            double.TryParse(TxbHsl10rb.Text, out txt5Value);
            double.TryParse(TxbHsl5rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl100rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            txbTotalFUUK.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

        }

        protected void TxbFU50rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbFU50rb.Text, out txt1Value);
            double.TryParse("50000", out txt2Value);
            double.TryParse(TxbHsl50rb.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl20rb.Text, out txt4Value);
            double.TryParse(TxbHsl10rb.Text, out txt5Value);
            double.TryParse(TxbHsl5rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl50rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            txbTotalFUUK.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

        }

        protected void TxbFU20rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbFU20rb.Text, out txt1Value);
            double.TryParse("20000", out txt2Value);
            double.TryParse(TxbHsl20rb.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl10rb.Text, out txt5Value);
            double.TryParse(TxbHsl5rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl20rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            txbTotalFUUK.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));
        }

        protected void TxbFU10rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbFU10rb.Text, out txt1Value);
            double.TryParse("10000", out txt2Value);
            double.TryParse(TxbHsl10rb.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl20rb.Text, out txt5Value);
            double.TryParse(TxbHsl5rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl10rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            txbTotalFUUK.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));
        }

        protected void TxbFU5rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbFU5rb.Text, out txt1Value);
            double.TryParse("5000", out txt2Value);
            double.TryParse(TxbHsl5rb.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl20rb.Text, out txt5Value);
            double.TryParse(TxbHsl10rb.Text, out txt6Value);
            double.TryParse(TxbHsl2rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl5rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            txbTotalFUUK.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));
        }

        protected void TxbFU2rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbFU2rb.Text, out txt1Value);
            double.TryParse("2000", out txt2Value);
            double.TryParse(TxbHsl2rb.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl20rb.Text, out txt5Value);
            double.TryParse(TxbHsl10rb.Text, out txt6Value);
            double.TryParse(TxbHsl5rb.Text, out txt7Value);
            double.TryParse(TxbHsl1rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl2rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            txbTotalFUUK.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString())); ;
        }

        protected void TxbFU1rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, txt7Value, txt8Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbFU1rb.Text, out txt1Value);
            double.TryParse("1000", out txt2Value);
            double.TryParse(TxbHsl1rb.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl100rb.Text, out txt3Value);
            double.TryParse(TxbHsl50rb.Text, out txt4Value);
            double.TryParse(TxbHsl20rb.Text, out txt5Value);
            double.TryParse(TxbHsl10rb.Text, out txt6Value);
            double.TryParse(TxbHsl5rb.Text, out txt7Value);
            double.TryParse(TxbHsl2rb.Text, out txt8Value);

            total = txt1Value * txt2Value;
            TxbHsl1rb.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value + txt7Value + txt8Value;
            txbTotalFUUK.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + ULtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));
        }

        protected void TxbUL1rb_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbUL1rb.Text, out txt1Value);
            double.TryParse("1000", out txt2Value);
            double.TryParse(TxbHsl1rbLgm.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl5rts.Text, out txt3Value);
            double.TryParse(TxbHsl2rts.Text, out txt4Value);
            double.TryParse(TxbHsl1rts.Text, out txt5Value);
            double.TryParse(TxbHsl50.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl1rbLgm.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbTotalFUUL.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

        }

        protected void TxbUL5rts_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbUL5rts.Text, out txt1Value);
            double.TryParse("500", out txt2Value);
            double.TryParse(TxbHsl5rts.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl1rbLgm.Text, out txt3Value);
            double.TryParse(TxbHsl2rts.Text, out txt4Value);
            double.TryParse(TxbHsl1rts.Text, out txt5Value);
            double.TryParse(TxbHsl50.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl5rts.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbTotalFUUL.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

        }

        protected void TxbUL2rts_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbUL2rts.Text, out txt1Value);
            double.TryParse("200", out txt2Value);
            double.TryParse(TxbHsl2rts.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl1rbLgm.Text, out txt3Value);
            double.TryParse(TxbHsl5rts.Text, out txt4Value);
            double.TryParse(TxbHsl1rts.Text, out txt5Value);
            double.TryParse(TxbHsl50.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl2rts.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbTotalFUUL.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

        }

        protected void TxbUL1rts_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbUL1rts.Text, out txt1Value);
            double.TryParse("100", out txt2Value);
            double.TryParse(TxbHsl1rts.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl1rbLgm.Text, out txt3Value);
            double.TryParse(TxbHsl5rts.Text, out txt4Value);
            double.TryParse(TxbHsl2rts.Text, out txt5Value);
            double.TryParse(TxbHsl50.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl1rts.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbTotalFUUL.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

        }

        protected void TxbUL50_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total, UKtotal, txt3Value, txt4Value, txt5Value, txt6Value, ULMaterai, ULtotal, UkUlTotal;
            total = 0;
            double.TryParse(TxbUL50.Text, out txt1Value);
            double.TryParse("50", out txt2Value);
            double.TryParse(TxbHsl50.Text, out total);
            double.TryParse(TxbTotalMaterai.Text, out ULMaterai);
            double.TryParse(txbTotalFUUK.Text, out UKtotal);
            double.TryParse(TxbTotalFUUL.Text, out ULtotal);

            double.TryParse(TxbGrandTotal.Text, out UkUlTotal);

            double.TryParse(TxbHsl1rbLgm.Text, out txt3Value);
            double.TryParse(TxbHsl5rts.Text, out txt4Value);
            double.TryParse(TxbHsl2rts.Text, out txt5Value);
            double.TryParse(TxbHsl1rts.Text, out txt6Value);

            total = txt1Value * txt2Value;
            TxbHsl50.Text = string.Format("{0:#,##0.00}", double.Parse(total.ToString()));

            var total1 = total + txt3Value + txt4Value + txt5Value + txt6Value;
            TxbTotalFUUL.Text = string.Format("{0:#,##0.00}", double.Parse(total1.ToString()));

            var total2 = total1 + UKtotal + ULMaterai;
            TxbGrandTotal.Text = string.Format("{0:#,##0.00}", double.Parse(total2.ToString()));

        }

        protected void TxbDanaTetap_TextChanged(object sender, EventArgs e)
        {
            double txt1Value, txt2Value, total;
            total = 0;
            double.TryParse(TxbGrandTotal.Text, out txt1Value);
            double.TryParse(TxbDanaTetap.Text, out txt2Value);
            double.TryParse(TxbSelisih.Text, out total);

            total = txt1Value - txt2Value;
            if (total <= 0)
            {
                TxbSelisih.Text = string.Format(CultureInfo.CreateSpecificCulture("en-ID"), "({0:C2})", total);
            }
            else
            {
                TxbSelisih.Text = string.Format(CultureInfo.CreateSpecificCulture("en-ID"), "{0:C2}", total);
            }
        }

        private void LoadSaveID()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_GetData_PCMO", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPCMO", txtIDSave.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(dr["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(dr["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(dr["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(dr["Periode_Audit_End"]);

                TxbTnglMaterai.Text = Convert.ToString(dr["Tgl_Beli_Materai"]);
                TxbJmlMaterai.Text = Convert.ToString(dr["Jmlh_Materai"]);
                TxbTotalMaterai.Text = Convert.ToString(dr["Total_Harga_Materai"]);

                TxbFU100rb.Text = Convert.ToString(dr["UK_Item_100rb"]);
                TxbFU50rb.Text = Convert.ToString(dr["UK_Item_50rb"]);
                TxbFU20rb.Text = Convert.ToString(dr["UK_Item_20rb"]);
                TxbFU10rb.Text = Convert.ToString(dr["UK_Item_10rb"]);
                TxbFU5rb.Text = Convert.ToString(dr["UK_Item_5rb"]);
                TxbFU2rb.Text = Convert.ToString(dr["UK_Item_2rb"]);
                TxbFU1rb.Text = Convert.ToString(dr["UK_Item_1rb"]);
                TxbUL1rb.Text = Convert.ToString(dr["UL_Item_1rb"]);
                TxbUL5rts.Text = Convert.ToString(dr["UL_Item_5rts"]);
                TxbUL2rts.Text = Convert.ToString(dr["UL_Item_2rts"]);
                TxbUL1rts.Text = Convert.ToString(dr["UL_Item_1rts"]);
                TxbUL50.Text = Convert.ToString(dr["UL_Item_50"]);

                TxbHsl100rb.Text = Convert.ToString(dr["UK_Total_100rb"]);
                TxbHsl50rb.Text = Convert.ToString(dr["UK_Total_50rb"]);
                TxbHsl20rb.Text = Convert.ToString(dr["UK_Total_20rb"]);
                TxbHsl10rb.Text = Convert.ToString(dr["UK_Total_10rb"]);
                TxbHsl5rb.Text = Convert.ToString(dr["UK_Total_5rb"]);
                TxbHsl2rb.Text = Convert.ToString(dr["UK_Total_2rb"]);
                TxbHsl1rb.Text = Convert.ToString(dr["UK_Total_1rb"]);
                TxbHsl1rbLgm.Text = Convert.ToString(dr["UL_Total_1rb"]);
                TxbHsl5rts.Text = Convert.ToString(dr["UL_Total_5rts"]);
                TxbHsl2rts.Text = Convert.ToString(dr["UL_Total_2rts"]);
                TxbHsl1rts.Text = Convert.ToString(dr["UL_Total_1rts"]);
                TxbHsl50.Text = Convert.ToString(dr["UL_Total_50"]);

                txbTotalFUUK.Text = Convert.ToString(dr["UK_Total"]);
                TxbTotalFUUL.Text = Convert.ToString(dr["UL_Total"]);

                TxbGrandTotal.Text = Convert.ToString(dr["PCMO_GrandTotal"]);
                TxbDanaTetap.Text = Convert.ToString(dr["PCMO_DanaTetap"]);
                TxbSelisih.Text = Convert.ToString(dr["PCMO_Selisih"]);

                RemarksSelisih.InnerText = Convert.ToString(dr["Remaks_Selisih"]);

                TxbDilaksanakan.Text = Convert.ToString(dr["Footer_Dilaksanakan"]);
                TxbTnglbwah.Text = Convert.ToString(dr["Footer_Tngl"]);
                TxbPadaJam.Text = Convert.ToString(dr["Footer_Jam"]);

            }
            con.Close();
        }

        private void LoadSaveIDTemp()
        {
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_GetData_PCMO", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IDPCMO", txtIDSaveTemp.Text);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                TxbDibuatHead.Text = Convert.ToString(dr["Header_Dibuat"]);
                TxbTanggalHeadDibuat.Text = Convert.ToString(dr["Header_Dibuat_Tngl"]);
                txbCabang.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                txbPeriodeStart.Text = Convert.ToString(dr["Periode_Audit_Start"]);
                txbPeriodeEnd.Text = Convert.ToString(dr["Periode_Audit_End"]);

                TxbTnglMaterai.Text = Convert.ToString(dr["Tgl_Beli_Materai"]);
                TxbJmlMaterai.Text = Convert.ToString(dr["Jmlh_Materai"]);
                TxbTotalMaterai.Text = Convert.ToString(dr["Total_Harga_Materai"]);

                TxbFU100rb.Text = Convert.ToString(dr["UK_Item_100rb"]);
                TxbFU50rb.Text = Convert.ToString(dr["UK_Item_50rb"]);
                TxbFU20rb.Text = Convert.ToString(dr["UK_Item_20rb"]);
                TxbFU10rb.Text = Convert.ToString(dr["UK_Item_10rb"]);
                TxbFU5rb.Text = Convert.ToString(dr["UK_Item_5rb"]);
                TxbFU2rb.Text = Convert.ToString(dr["UK_Item_2rb"]);
                TxbFU1rb.Text = Convert.ToString(dr["UK_Item_1rb"]);
                TxbUL1rb.Text = Convert.ToString(dr["UL_Item_1rb"]);
                TxbUL5rts.Text = Convert.ToString(dr["UL_Item_5rts"]);
                TxbUL2rts.Text = Convert.ToString(dr["UL_Item_2rts"]);
                TxbUL1rts.Text = Convert.ToString(dr["UL_Item_1rts"]);
                TxbUL50.Text = Convert.ToString(dr["UL_Item_50"]);

                TxbHsl100rb.Text = Convert.ToString(dr["UK_Total_100rb"]);
                TxbHsl50rb.Text = Convert.ToString(dr["UK_Total_50rb"]);
                TxbHsl20rb.Text = Convert.ToString(dr["UK_Total_20rb"]);
                TxbHsl10rb.Text = Convert.ToString(dr["UK_Total_10rb"]);
                TxbHsl5rb.Text = Convert.ToString(dr["UK_Total_5rb"]);
                TxbHsl2rb.Text = Convert.ToString(dr["UK_Total_2rb"]);
                TxbHsl1rb.Text = Convert.ToString(dr["UK_Total_1rb"]);
                TxbHsl1rbLgm.Text = Convert.ToString(dr["UL_Total_1rb"]);
                TxbHsl5rts.Text = Convert.ToString(dr["UL_Total_5rts"]);
                TxbHsl2rts.Text = Convert.ToString(dr["UL_Total_2rts"]);
                TxbHsl1rts.Text = Convert.ToString(dr["UL_Total_1rts"]);
                TxbHsl50.Text = Convert.ToString(dr["UL_Total_50"]);

                txbTotalFUUK.Text = Convert.ToString(dr["UK_Total"]);
                TxbTotalFUUL.Text = Convert.ToString(dr["UL_Total"]);

                TxbGrandTotal.Text = Convert.ToString(dr["PCMO_GrandTotal"]);
                TxbDanaTetap.Text = Convert.ToString(dr["PCMO_DanaTetap"]);
                TxbSelisih.Text = Convert.ToString(dr["PCMO_Selisih"]);

                RemarksSelisih.InnerText = Convert.ToString(dr["Remaks_Selisih"]);

                TxbDilaksanakan.Text = Convert.ToString(dr["Footer_Dilaksanakan"]);
                TxbTnglbwah.Text = Convert.ToString(dr["Footer_Tngl"]);
                TxbPadaJam.Text = Convert.ToString(dr["Footer_Jam"]);
                TxtCreateDate.Text = Convert.ToString(dr["Doe"]);
                TxtReviewDate.Text = Convert.ToString(dr["ReviewDate"]);

            }
            con.Close();
        }

        private void SaveID()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SavePCMO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDPCMO", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                    cmd.Parameters.Add("@Tgl_Beli_Materai", SqlDbType.NVarChar).Value = TxbTnglMaterai.Text;
                    cmd.Parameters.AddWithValue("@Jmlh_Materai", string.IsNullOrEmpty(TxbJmlMaterai.Text) ? (object)DBNull.Value : TxbJmlMaterai.Text);
                    cmd.Parameters.AddWithValue("@Total_Harga_Materai", string.IsNullOrEmpty(TxbTotalMaterai.Text) ? (object)DBNull.Value : TxbTotalMaterai.Text);

                    cmd.Parameters.AddWithValue("@UK_Item_100rb", string.IsNullOrEmpty(TxbFU100rb.Text) ? (object)DBNull.Value : TxbFU100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_50rb", string.IsNullOrEmpty(TxbFU50rb.Text) ? (object)DBNull.Value : TxbFU50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_20rb", string.IsNullOrEmpty(TxbFU20rb.Text) ? (object)DBNull.Value : TxbFU20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_10rb", string.IsNullOrEmpty(TxbFU10rb.Text) ? (object)DBNull.Value : TxbFU10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_5rb", string.IsNullOrEmpty(TxbFU5rb.Text) ? (object)DBNull.Value : TxbFU5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_2rb", string.IsNullOrEmpty(TxbFU2rb.Text) ? (object)DBNull.Value : TxbFU2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_1rb", string.IsNullOrEmpty(TxbFU1rb.Text) ? (object)DBNull.Value : TxbFU1rb.Text);

                    cmd.Parameters.AddWithValue("@UL_Item_1rb", string.IsNullOrEmpty(TxbUL1rb.Text) ? (object)DBNull.Value : TxbUL1rb.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_5rts", string.IsNullOrEmpty(TxbUL5rts.Text) ? (object)DBNull.Value : TxbUL5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_2rts", string.IsNullOrEmpty(TxbUL2rts.Text) ? (object)DBNull.Value : TxbUL2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_1rts", string.IsNullOrEmpty(TxbUL1rts.Text) ? (object)DBNull.Value : TxbUL1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_50", string.IsNullOrEmpty(TxbUL50.Text) ? (object)DBNull.Value : TxbUL50.Text);

                    cmd.Parameters.AddWithValue("@UK_Total_100rb", string.IsNullOrEmpty(TxbHsl100rb.Text) ? (object)DBNull.Value : TxbHsl100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_50rb", string.IsNullOrEmpty(TxbHsl50rb.Text) ? (object)DBNull.Value : TxbHsl50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_20rb", string.IsNullOrEmpty(TxbHsl20rb.Text) ? (object)DBNull.Value : TxbHsl20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_10rb", string.IsNullOrEmpty(TxbHsl10rb.Text) ? (object)DBNull.Value : TxbHsl10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_5rb", string.IsNullOrEmpty(TxbHsl5rb.Text) ? (object)DBNull.Value : TxbHsl5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_2rb", string.IsNullOrEmpty(TxbHsl2rb.Text) ? (object)DBNull.Value : TxbHsl2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_1rb", string.IsNullOrEmpty(TxbHsl1rb.Text) ? (object)DBNull.Value : TxbHsl1rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total", string.IsNullOrEmpty(txbTotalFUUK.Text) ? (object)DBNull.Value : txbTotalFUUK.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rb", string.IsNullOrEmpty(TxbHsl1rbLgm.Text) ? (object)DBNull.Value : TxbHsl1rbLgm.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_5rts", string.IsNullOrEmpty(TxbHsl5rts.Text) ? (object)DBNull.Value : TxbHsl5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_2rts", string.IsNullOrEmpty(TxbHsl2rts.Text) ? (object)DBNull.Value : TxbHsl2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rts", string.IsNullOrEmpty(TxbHsl1rts.Text) ? (object)DBNull.Value : TxbHsl1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_50", string.IsNullOrEmpty(TxbHsl50.Text) ? (object)DBNull.Value : TxbHsl50.Text);
                    cmd.Parameters.AddWithValue("@UL_Total", string.IsNullOrEmpty(TxbTotalFUUL.Text) ? (object)DBNull.Value : TxbTotalFUUL.Text);

                    cmd.Parameters.AddWithValue("@PCMO_GrandTotal", string.IsNullOrEmpty(TxbGrandTotal.Text) ? (object)DBNull.Value : TxbGrandTotal.Text);
                    cmd.Parameters.AddWithValue("@PCMO_DanaTetap", string.IsNullOrEmpty(TxbDanaTetap.Text) ? (object)DBNull.Value : TxbDanaTetap.Text);
                    cmd.Parameters.AddWithValue("@PCMO_Selisih", string.IsNullOrEmpty(TxbSelisih.Text) ? (object)DBNull.Value : TxbSelisih.Text);

                    cmd.Parameters.AddWithValue("@Remaks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilaksanakan.Text) ? (object)DBNull.Value : TxbDilaksanakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglbwah.Text) ? (object)DBNull.Value : TxbTnglbwah.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbPadaJam.Text) ? (object)DBNull.Value : TxbPadaJam.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }

        }

        private void SavedReview()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SavePCMO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDPCMO", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                    cmd.Parameters.Add("@Tgl_Beli_Materai", SqlDbType.NVarChar).Value = TxbTnglMaterai.Text;
                    cmd.Parameters.AddWithValue("@Jmlh_Materai", string.IsNullOrEmpty(TxbJmlMaterai.Text) ? (object)DBNull.Value : TxbJmlMaterai.Text);
                    cmd.Parameters.AddWithValue("@Total_Harga_Materai", string.IsNullOrEmpty(TxbTotalMaterai.Text) ? (object)DBNull.Value : TxbTotalMaterai.Text);

                    cmd.Parameters.AddWithValue("@UK_Item_100rb", string.IsNullOrEmpty(TxbFU100rb.Text) ? (object)DBNull.Value : TxbFU100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_50rb", string.IsNullOrEmpty(TxbFU50rb.Text) ? (object)DBNull.Value : TxbFU50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_20rb", string.IsNullOrEmpty(TxbFU20rb.Text) ? (object)DBNull.Value : TxbFU20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_10rb", string.IsNullOrEmpty(TxbFU10rb.Text) ? (object)DBNull.Value : TxbFU10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_5rb", string.IsNullOrEmpty(TxbFU5rb.Text) ? (object)DBNull.Value : TxbFU5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_2rb", string.IsNullOrEmpty(TxbFU2rb.Text) ? (object)DBNull.Value : TxbFU2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_1rb", string.IsNullOrEmpty(TxbFU1rb.Text) ? (object)DBNull.Value : TxbFU1rb.Text);

                    cmd.Parameters.AddWithValue("@UL_Item_1rb", string.IsNullOrEmpty(TxbUL1rb.Text) ? (object)DBNull.Value : TxbUL1rb.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_5rts", string.IsNullOrEmpty(TxbUL5rts.Text) ? (object)DBNull.Value : TxbUL5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_2rts", string.IsNullOrEmpty(TxbUL2rts.Text) ? (object)DBNull.Value : TxbUL2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_1rts", string.IsNullOrEmpty(TxbUL1rts.Text) ? (object)DBNull.Value : TxbUL1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_50", string.IsNullOrEmpty(TxbUL50.Text) ? (object)DBNull.Value : TxbUL50.Text);

                    cmd.Parameters.AddWithValue("@UK_Total_100rb", string.IsNullOrEmpty(TxbHsl100rb.Text) ? (object)DBNull.Value : TxbHsl100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_50rb", string.IsNullOrEmpty(TxbHsl50rb.Text) ? (object)DBNull.Value : TxbHsl50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_20rb", string.IsNullOrEmpty(TxbHsl20rb.Text) ? (object)DBNull.Value : TxbHsl20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_10rb", string.IsNullOrEmpty(TxbHsl10rb.Text) ? (object)DBNull.Value : TxbHsl10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_5rb", string.IsNullOrEmpty(TxbHsl5rb.Text) ? (object)DBNull.Value : TxbHsl5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_2rb", string.IsNullOrEmpty(TxbHsl2rb.Text) ? (object)DBNull.Value : TxbHsl2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_1rb", string.IsNullOrEmpty(TxbHsl1rb.Text) ? (object)DBNull.Value : TxbHsl1rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total", string.IsNullOrEmpty(txbTotalFUUK.Text) ? (object)DBNull.Value : txbTotalFUUK.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rb", string.IsNullOrEmpty(TxbHsl1rbLgm.Text) ? (object)DBNull.Value : TxbHsl1rbLgm.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_5rts", string.IsNullOrEmpty(TxbHsl5rts.Text) ? (object)DBNull.Value : TxbHsl5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_2rts", string.IsNullOrEmpty(TxbHsl2rts.Text) ? (object)DBNull.Value : TxbHsl2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rts", string.IsNullOrEmpty(TxbHsl1rts.Text) ? (object)DBNull.Value : TxbHsl1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_50", string.IsNullOrEmpty(TxbHsl50.Text) ? (object)DBNull.Value : TxbHsl50.Text);
                    cmd.Parameters.AddWithValue("@UL_Total", string.IsNullOrEmpty(TxbTotalFUUL.Text) ? (object)DBNull.Value : TxbTotalFUUL.Text);

                    cmd.Parameters.AddWithValue("@PCMO_GrandTotal", string.IsNullOrEmpty(TxbGrandTotal.Text) ? (object)DBNull.Value : TxbGrandTotal.Text);
                    cmd.Parameters.AddWithValue("@PCMO_DanaTetap", string.IsNullOrEmpty(TxbDanaTetap.Text) ? (object)DBNull.Value : TxbDanaTetap.Text);
                    cmd.Parameters.AddWithValue("@PCMO_Selisih", string.IsNullOrEmpty(TxbSelisih.Text) ? (object)DBNull.Value : TxbSelisih.Text);

                    cmd.Parameters.AddWithValue("@Remaks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilaksanakan.Text) ? (object)DBNull.Value : TxbDilaksanakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglbwah.Text) ? (object)DBNull.Value : TxbTnglbwah.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbPadaJam.Text) ? (object)DBNull.Value : TxbPadaJam.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DBNull.Value;
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }

        }

        private void SaveIDtemp()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SavePCMO", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDPCMO", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(TxbDibuatHead.Text) ? (object)DBNull.Value : TxbDibuatHead.Text);
                    cmd.Parameters.Add("@Header_Dibuat_Tngl", SqlDbType.NVarChar).Value = TxbTanggalHeadDibuat.Text;

                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                    cmd.Parameters.Add("@Tgl_Beli_Materai", SqlDbType.NVarChar).Value = TxbTnglMaterai.Text;
                    cmd.Parameters.AddWithValue("@Jmlh_Materai", string.IsNullOrEmpty(TxbJmlMaterai.Text) ? (object)DBNull.Value : TxbJmlMaterai.Text);
                    cmd.Parameters.AddWithValue("@Total_Harga_Materai", string.IsNullOrEmpty(TxbTotalMaterai.Text) ? (object)DBNull.Value : TxbTotalMaterai.Text);

                    cmd.Parameters.AddWithValue("@UK_Item_100rb", string.IsNullOrEmpty(TxbFU100rb.Text) ? (object)DBNull.Value : TxbFU100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_50rb", string.IsNullOrEmpty(TxbFU50rb.Text) ? (object)DBNull.Value : TxbFU50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_20rb", string.IsNullOrEmpty(TxbFU20rb.Text) ? (object)DBNull.Value : TxbFU20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_10rb", string.IsNullOrEmpty(TxbFU10rb.Text) ? (object)DBNull.Value : TxbFU10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_5rb", string.IsNullOrEmpty(TxbFU5rb.Text) ? (object)DBNull.Value : TxbFU5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_2rb", string.IsNullOrEmpty(TxbFU2rb.Text) ? (object)DBNull.Value : TxbFU2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Item_1rb", string.IsNullOrEmpty(TxbFU1rb.Text) ? (object)DBNull.Value : TxbFU1rb.Text);

                    cmd.Parameters.AddWithValue("@UL_Item_1rb", string.IsNullOrEmpty(TxbUL1rb.Text) ? (object)DBNull.Value : TxbUL1rb.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_5rts", string.IsNullOrEmpty(TxbUL5rts.Text) ? (object)DBNull.Value : TxbUL5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_2rts", string.IsNullOrEmpty(TxbUL2rts.Text) ? (object)DBNull.Value : TxbUL2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_1rts", string.IsNullOrEmpty(TxbUL1rts.Text) ? (object)DBNull.Value : TxbUL1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Item_50", string.IsNullOrEmpty(TxbUL50.Text) ? (object)DBNull.Value : TxbUL50.Text);

                    cmd.Parameters.AddWithValue("@UK_Total_100rb", string.IsNullOrEmpty(TxbHsl100rb.Text) ? (object)DBNull.Value : TxbHsl100rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_50rb", string.IsNullOrEmpty(TxbHsl50rb.Text) ? (object)DBNull.Value : TxbHsl50rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_20rb", string.IsNullOrEmpty(TxbHsl20rb.Text) ? (object)DBNull.Value : TxbHsl20rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_10rb", string.IsNullOrEmpty(TxbHsl10rb.Text) ? (object)DBNull.Value : TxbHsl10rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_5rb", string.IsNullOrEmpty(TxbHsl5rb.Text) ? (object)DBNull.Value : TxbHsl5rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_2rb", string.IsNullOrEmpty(TxbHsl2rb.Text) ? (object)DBNull.Value : TxbHsl2rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total_1rb", string.IsNullOrEmpty(TxbHsl1rb.Text) ? (object)DBNull.Value : TxbHsl1rb.Text);
                    cmd.Parameters.AddWithValue("@UK_Total", string.IsNullOrEmpty(txbTotalFUUK.Text) ? (object)DBNull.Value : txbTotalFUUK.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rb", string.IsNullOrEmpty(TxbHsl1rbLgm.Text) ? (object)DBNull.Value : TxbHsl1rbLgm.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_5rts", string.IsNullOrEmpty(TxbHsl5rts.Text) ? (object)DBNull.Value : TxbHsl5rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_2rts", string.IsNullOrEmpty(TxbHsl2rts.Text) ? (object)DBNull.Value : TxbHsl2rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_1rts", string.IsNullOrEmpty(TxbHsl1rts.Text) ? (object)DBNull.Value : TxbHsl1rts.Text);
                    cmd.Parameters.AddWithValue("@UL_Total_50", string.IsNullOrEmpty(TxbHsl50.Text) ? (object)DBNull.Value : TxbHsl50.Text);
                    cmd.Parameters.AddWithValue("@UL_Total", string.IsNullOrEmpty(TxbTotalFUUL.Text) ? (object)DBNull.Value : TxbTotalFUUL.Text);

                    cmd.Parameters.AddWithValue("@PCMO_GrandTotal", string.IsNullOrEmpty(TxbGrandTotal.Text) ? (object)DBNull.Value : TxbGrandTotal.Text);
                    cmd.Parameters.AddWithValue("@PCMO_DanaTetap", string.IsNullOrEmpty(TxbDanaTetap.Text) ? (object)DBNull.Value : TxbDanaTetap.Text);
                    cmd.Parameters.AddWithValue("@PCMO_Selisih", string.IsNullOrEmpty(TxbSelisih.Text) ? (object)DBNull.Value : TxbSelisih.Text);

                    cmd.Parameters.AddWithValue("@Remaks_Selisih", ToDBValue(RemarksSelisih.InnerText));
                    cmd.Parameters.AddWithValue("@Footer_Dilaksanakan", string.IsNullOrEmpty(TxbDilaksanakan.Text) ? (object)DBNull.Value : TxbDilaksanakan.Text);
                    cmd.Parameters.AddWithValue("@Footer_Tngl", string.IsNullOrEmpty(TxbTnglbwah.Text) ? (object)DBNull.Value : TxbTnglbwah.Text);
                    cmd.Parameters.AddWithValue("@Footer_Jam", string.IsNullOrEmpty(TxbPadaJam.Text) ? (object)DBNull.Value : TxbPadaJam.Text);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DBNull.Value;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;
                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }

        }
    }
}