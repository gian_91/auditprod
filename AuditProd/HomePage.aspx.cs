﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using AuditProd.DAL;

namespace AuditProd
{
    public partial class HomePage : System.Web.UI.Page
    {
        DaLayer ObjDAL2 = new DaLayer();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.RemoveAll();
            UserName.Text = this.Page.User.Identity.Name;
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectFromLoginPage("Login1.UserName", false);
            }

            btnUser.Visible = this.Page.User.IsInRole("Administrator");
            if (!IsPostBack)
            {
                SV();
                DDLBranchName();
                DDLMonth();
                DDLYear();
            }
            else
            {
                //ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection Error ..');", true);

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(AuditPeriodeStart.Text) || string.IsNullOrEmpty(AuditPeriodeEnd.Text))
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Masukan Periode Audit !!!');", true);
            }
            else
            {
                string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand("SP_SaveData", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("UserName", SqlDbType.BigInt).Value = UserName.Text;
                        cmd.Parameters.AddWithValue("Branch_ID", ddlBranchID.SelectedItem.Value.Substring(5));
                        cmd.Parameters.AddWithValue("Month", ddlMonth.SelectedItem.Value);
                        cmd.Parameters.AddWithValue("Year", ddlYear.SelectedItem.Value);
                        cmd.Parameters.AddWithValue("Audit_Schedule", SqlDbType.NVarChar).Value = AuditSchedule.Text;
                        cmd.Parameters.AddWithValue("Audit_Periode_Start", DateTime.ParseExact(AuditPeriodeStart.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("Audit_Periode_End", DateTime.ParseExact(AuditPeriodeEnd.Text, "MM/dd/yyyy", CultureInfo.InvariantCulture));
                        cmd.Parameters.AddWithValue("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                        con.Open();
                        object o = cmd.ExecuteScalar();
                        if (o != null)
                        {
                            string id = o.ToString();
                        }
                        con.Close();
                        Session["ID_SH"] = o.ToString();
                        Response.Redirect("AuditBranch.aspx?ID=" + o.ToString());
                    }
                }
            }
        }

        protected void btnPilih_Click(object sender, EventArgs e)
        {
            string value = ddlIDsave.SelectedItem.Value;
            Session["ID_SH_Temp"] = value.ToString();
            Response.Redirect("AuditBranch.aspx?ID=" + value);
        }

        private void DDLYear()
        {
            ddlYear.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "SELECT ID,year FROM Master_Year";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            con.Open();
            ddlYear.DataSource = cmd.ExecuteReader();
            ddlYear.DataTextField = "year";
            ddlYear.DataValueField = "year";
            ddlYear.DataBind();
            con.Close();
        }

        private void DDLBranchName()
        {
            ddlBranchID.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "SELECT [ID],[BranchId_BranchName] FROM [Master_Branch]";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            con.Open();
            ddlBranchID.DataSource = cmd.ExecuteReader();
            ddlBranchID.DataTextField = "BranchId_BranchName";
            ddlBranchID.DataValueField = "BranchId_BranchName";
            ddlBranchID.DataBind();
            con.Close();
        }

        private void DDLMonth()
        {
            ddlMonth.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "SELECT ID,MonthName FROM Master_Month";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            con.Open();
            ddlMonth.DataSource = cmd.ExecuteReader();
            ddlMonth.DataTextField = "MonthName";
            ddlMonth.DataValueField = "MonthName";
            ddlMonth.DataBind();
            con.Close();
        }

        private void SV()
        {
            ddlIDsave.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "Select ID_SH,Audit_Schedule from SaveHeader";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            //cmd.Parameters.AddWithValue("@Username", UserName.Text);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;
            con.Open();
            ddlIDsave.DataSource = cmd.ExecuteReader();
            ddlIDsave.DataTextField = "Audit_Schedule";
            ddlIDsave.DataValueField = "ID_SH";
            ddlIDsave.DataBind();
            con.Close();

        }

        protected void ddlBranchID_SelectedIndexChanged(object sender, EventArgs e)
        {
            string message = null;
            string month = ddlMonth.SelectedValue;
            string year = ddlYear.SelectedValue;
            string branch = ddlBranchID.SelectedValue;

            if (string.IsNullOrEmpty(branch))
            {
                message = "Pilih branchName terlebih dahulu !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else if (string.IsNullOrEmpty(month))
            {
                message = "Pilih Month !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else if (string.IsNullOrEmpty(year))
            {
                message = "Pilih Year !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else
            {
                AuditSchedule.Text = branch.Substring(5) + "_" + month + "_" + year;
            }
        }

        protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            string message = null;
            string month = ddlMonth.SelectedValue;
            string year = ddlYear.SelectedValue;
            string branch = ddlBranchID.SelectedValue;

            if (string.IsNullOrEmpty(branch))
            {
                message = "Pilih branchName terlebih dahulu !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else if (string.IsNullOrEmpty(year))
            {
                message = "Pilih Year !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else if (string.IsNullOrEmpty(month))
            {
                message = "Pilih Month !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else
            {
                AuditSchedule.Text = branch.Substring(5) + "_" + month + "_" + year;
            }


        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            string message = null;
            string month = ddlMonth.SelectedValue;
            string year = ddlYear.SelectedValue;
            string branch = ddlBranchID.SelectedValue;

            if (string.IsNullOrEmpty(branch))
            {
                message = "Pilih branchName terlebih dahulu !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else if (string.IsNullOrEmpty(year))
            {
                message = "Pilih Year !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else if (string.IsNullOrEmpty(month))
            {
                message = "Pilih Month !";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
            else
            {
                AuditSchedule.Text = branch.Substring(5) + "_" + month + "_" + year;
            }
        }

        protected void btnUser_Click(object sender, EventArgs e)
        {
            Response.Redirect("ManageUser.aspx");
        }
    }
}