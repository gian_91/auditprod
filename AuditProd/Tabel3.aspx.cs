﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using AuditProd.DAL;
using System.Globalization;

namespace AuditProd
{
    public partial class Tabel3 : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        string id = string.Empty;
        string id2 = string.Empty;
        string id3 = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!string.IsNullOrEmpty((string)Session["Id_LHP"]))
            {
                TxbId.Text = Session["Id_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id = TxbId.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id2_LHP"]))
            {
                TxbId2.Text = Session["Id2_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id2 = TxbId2.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id4_LHP"]))
            {
                TxbId3.Text = Session["Id4_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id3 = TxbId3.Text;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            BindGrid();
        }

        private void Get_Rdc()
        {
            string message = string.Empty;
            SqlDataReader reader = null;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from RawDataCurrent where CenterID=@AccountID and LoanStatus=@LoanStatus and BranchName=@BranchName ", con);
            cmd.Parameters.AddWithValue("@AccountID", TxtCenterIDdAdd.Text);
            cmd.Parameters.AddWithValue("@LoanStatus", "Active Loan");
            cmd.Parameters.AddWithValue("@BranchName", Txbcbng.Text);
            reader = cmd.ExecuteReader();
            reader.Read();
            {
                if (reader.HasRows == true)
                {
                    TxtCenterNameAdd.Text = Convert.ToString(reader["CenterName"]);
                    TxtDisburseAdd.Text = Convert.ToString(reader["DisbursementDate"]);
                    lblmessageAdd.Visible = false;
                }
                else
                {
                    TxtCenterNameAdd.Text = string.Empty;
                    TxtDisburseAdd.Text = string.Empty;
                    lblmessageAdd.Visible = true;
                    lblmessageAdd.Text = "CenterID Tidak Ditemukan";
                }
                con.Close();
            }
        }

        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            this.Get_Rdc();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            //sb.Append("alert('Search Results Are in Progress ...');");
            sb.Append("$('#addModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ModelScript", sb.ToString(), false);

        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            Session["Id_LHP"] = null;
            Session["Id2_LHP"] = null;
            Session["Id4_LHP"] = null;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        public void BindGrid()
        {

            if (!(string.IsNullOrEmpty(id)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel3 where ID_LHP_Acquisition={0}", id);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();

                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);

                }

            }
            else if (!(string.IsNullOrEmpty(id2)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel3 where ID_LHP_Collection={0}", id2);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
            else if (!(string.IsNullOrEmpty(id3)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel3 where ID_LHP_CPU={0}", id3);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //Getddl1edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                TxtCenterIDEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text);
                TxtCenterNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                TxtDisburseEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                TxtRemarksEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                //ddlsmplingEdit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            string cntrid = TxtCenterIDEdit.Text;
            string cntrname = TxtCenterNameEdit.Text;
            string disb = TxtDisburseEdit.Text;
            string rfm = TxtRemarksEdit.Text;
            string ss = ddlsmplingEdit.SelectedItem.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdate(No, cntrid, cntrname, disb, rfm, ss, dt);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, string cntrid, string cntrname, string disb, string rfm, string ss, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveTabel3Edit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@cntrid", cntrid);
                updateCmd.Parameters.AddWithValue("@cntrname", cntrname);
                updateCmd.Parameters.AddWithValue("@disb", disb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@doe", dt.ToString());
                updateCmd.Parameters.AddWithValue("@ss", ss);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            txtcabangModal.Text = Txbcbng.Text;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_A = id;
            string IDProc_B = id2;
            string IDProc_C = id3;
            //string dd = TxtDueDaysAdd.Text;

            if (string.IsNullOrEmpty(IDProc_A))
            {
                IDProc_A = null;
            }
            if (string.IsNullOrEmpty(IDProc_B))
            {
                IDProc_B = null;
            }
            if (string.IsNullOrEmpty(IDProc_C))
            {
                IDProc_C = null;
            }

            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string Headdb = txbUsername.Text;

            if (!(string.IsNullOrEmpty(IDProc_A)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string mod = "LHP_Acquisition";
                    string cntrid = TxtCenterIDdAdd.Text;
                    string cntrname = TxtCenterNameAdd.Text;
                    string population = TxtDisburseAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, mod, cntrid, cntrname, population, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }

            else if (!(string.IsNullOrEmpty(IDProc_B)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string mod = "LHP_Collection";
                    string cntrid = TxtCenterNameAdd.Text;
                    string cntrname = TxtCenterNameAdd.Text;
                    string population = TxtDisburseAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, mod, cntrid, cntrname, population, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }

            }
            else if (!(string.IsNullOrEmpty(IDProc_C)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string mod = "LHP_CPU";
                    string cntrid = TxtCenterNameAdd.Text;
                    string cntrname = TxtCenterNameAdd.Text;
                    string population = TxtDisburseAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, mod, cntrid, cntrname, population, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }

            }

        }

        private void executeAdd(string idlhk2, string idlhk3, string idlhk4, string mod, string cntrid, string cntrname, string population, string rem, string samp,
            string dt, string Headdbs)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveTabel3", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk2", idlhk2 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk3", idlhk3 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk4", idlhk4 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@cntrid", cntrid);
                addCmd.Parameters.AddWithValue("@cntrname", cntrname);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@doe", dt);
                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@rem", rem);
                addCmd.Parameters.AddWithValue("@samp", samp);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_LHP_Tabel3 where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}