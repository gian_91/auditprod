﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using AuditProd.DAL;
using System.Web.Script.Serialization;




namespace AuditProd
{
    /// <summary>   
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public GetRawID GetAccountTabel2(string AccountID)
        {
            GetRawID GetRaw = new GetRawID();

            string cs = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("GetDetialByID", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@PersonID";
                parameter.Value = AccountID;

                cmd.Parameters.Add(parameter);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                    if (rdr.HasRows == true)
                    {
                        //GetRaw.AccountID = rdr["AccountID"].ToString();
                        GetRaw.ClientName = rdr["ClientName"].ToString();
                        GetRaw.CenterName = rdr["CenterName"].ToString();
                        GetRaw.DueDays = rdr["DueDays"].ToString();
                    }
                    else
                    {
                        //GetRaw.AccountID = string.Empty;
                        GetRaw.ClientName = string.Empty;
                        GetRaw.CenterName = string.Empty;
                        GetRaw.DueDays = string.Empty; 
                    }
                con.Close();
            }
            return GetRaw;            
        }

        [WebMethod]
        public GetRawID GetAccountTabel1_2(string AccountID)
        {
            GetRawID GetRaw = new GetRawID();

            string cs = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SP_SearchAccID", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parameter = new SqlParameter();
                parameter.ParameterName = "@AccID";
                parameter.Value = AccountID;

                cmd.Parameters.Add(parameter);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                    if (rdr.HasRows == true)
                    {
                        //GetRaw.AccountID = rdr["AccountID"].ToString();
                        GetRaw.ClientName = rdr["ClientName"].ToString();
                        GetRaw.CenterName = rdr["CenterName"].ToString();
                        GetRaw.DueDays = rdr["DueDays"].ToString();
                        GetRaw.DisbursementDate = rdr["DisbursementDate"].ToString();
                    }
                    else
                    {
                        //GetRaw.AccountID = string.Empty;
                        GetRaw.ClientName = string.Empty;
                        GetRaw.CenterName = string.Empty;
                        GetRaw.DueDays = string.Empty;
                    }
                con.Close();
            }
            return GetRaw;
        }

        [WebMethod]
        public GetRawID GetAccountTabel3(string AccountID)
        {
            GetRawID GetRaw = new GetRawID();

            string cs = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SP_SearchTabel3", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AccID", AccountID);
                //cmd.Parameters.AddWithValue("@cabang", cabang);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                    if (rdr.HasRows == true)
                    {
                        //GetRaw.AccountID = rdr["AccountID"].ToString();
                        GetRaw.CenterName = rdr["CenterName"].ToString();
                        GetRaw.DisbursementDate = rdr["DisbursementDate"].ToString();
                    }
                    else
                    {
                        //GetRaw.AccountID = string.Empty;
                        GetRaw.ClientName = string.Empty;
                        GetRaw.CenterName = string.Empty;
                    }
                con.Close();
            }
            return GetRaw;
        }

        [WebMethod]
        public GetRawID GetAccountTabel6(string AccountID)
        {
            GetRawID GetRaw = new GetRawID();

            string cs = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("SP_SearchTabel6", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AccID", AccountID);
                //cmd.Parameters.AddWithValue("@cabang", cabang);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                    if (rdr.HasRows == true)
                    {
                        GetRaw.ClientName = rdr["ClientName"].ToString();
                        GetRaw.CenterName = rdr["CenterName"].ToString();
                        GetRaw.DueDays = rdr["DueDays"].ToString();
                        GetRaw.DisbursementDate = rdr["DisbursementDate"].ToString();
                    }
                    else
                    {
                        GetRaw.ClientName = string.Empty;
                        GetRaw.CenterName = string.Empty;
                        GetRaw.DueDays = string.Empty;
                        GetRaw.DisbursementDate = string.Empty;
                    }
                con.Close();
            }
            return GetRaw;
        }
    }
}
