﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="AuditProd.ForgotPassword" %>

<!DOCTYPE html>

<script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
<script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
<link href="Scripts/Bootstrap/bootstrap.min.css" rel="stylesheet" media="screen" />
<script type="text/javascript">
    window.onload = function () {
        var txtPassword = document.getElementById("txtPassword");
        var txtConfirmPassword = document.getElementById("txtConfirmPassword");
        txtPassword.onchange = ConfirmPassword
        txtConfirmPassword.onkeyup = ConfirmPassword
        function ConfirmPassword() {
            txtConfirmPassword.setCustomValidity("");
            if (txtPassword.value != txtConfirmPassword.value) {
                txtConfirmPassword.setCustomValidity("Passwords do not match.");
            }
        }
    }
</script>
<style type="text/css">
    body {
        font-family: Arial;
        font-size: 10pt;
        background: url("Images/background_full.jpg") local;
        background-size: cover;
        margin-top: 100px;
    }

    input[type=text], input[type=password] {
        width: 200px;
    }

    table {
        background: #F7F7F7;
        border: 1px solid #ccc;
    }

        table th {
            background-color: #f00;
            color: #f00;
            font-weight: bold;
        }

        table th, table td {
            padding: 5px;
            color: #414751;
        }

    .posisi {
        width: 400px;
        margin-left: auto;
        margin-right: auto;
        margin-top: 150px;
    }
</style>

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <title></title>
</head>

<body>
        <div align="center">
        <form method="post" action="ForgotPassword.aspx" id="form2" runat="server">
            <div style="max-width: 400px;">
                <h2 class="form-signin-heading" style="color: #FFFFFF">Recovery Password</h2>
<%--                <br />
                <label for="txtUsername" style="color: #FFFFFF">
                    Username</label>
                <input name="txtUsername" type="text" id="txtUsername" class="form-control" placeholder="Enter Username" runat="server"
                    required />--%>
                <br />
                <label for="txtNik" style="color: #FFFFFF">
                    NIK</label>
                <input name="txtNik" type="text" id="txtNik" class="form-control" placeholder="Enter NIK" runat="server" autocomplete="off" 
                    pattern="^[0-9]*$" title="_Only Number Format For NIK_" />
                <br />
                <label for="txtPassword" style="color: #FFFFFF">
                    Email</label>
                <input name="txtPassword" type="text" id="Email"  runat="server" class="form-control" placeholder="Enter Email" autocomplete="off"  />
                <br />
                <input type="submit" name="btnSignup" value="Send" id="btnSignup" class="btn btn-primary" onserverclick="registration_click" runat="server" />
                <input type="submit" name="btnCancel" value="Cancel" id="Submit1" class="btn btn-primary" onserverclick="Submit1_ServerClick" runat="server" />
            </div>
            <br />
            <br />
            <asp:Label ID="lblMessage" runat="server" />
        </form>
    </div>
</body>
</html>
