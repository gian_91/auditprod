﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using AuditProd.DAL;
using System.Globalization;

namespace AuditProd
{
    public partial class Tabel12 : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        string id4_lhp = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!string.IsNullOrEmpty((string)Session["Id4_LHP"]))
            {
                TxbId4.Text = Session["Id4_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id4_lhp = TxbId4.Text;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            BindGrid();
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            Session["Id4_LHP"] = null;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        public void BindGrid()
        {

            if (!(string.IsNullOrEmpty(id4_lhp)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel12 where ID_LHP_CPU={0}", id4_lhp);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                TxtCNedit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text).ToString();
                TxtRONameOnBRNetEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text).ToString();
                TxtActualEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                //TxtKondisiEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)////Buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            string cn = TxtCNedit.Text;
            string ro = TxtRONameOnBRNetEdit.Text;
            string act = TxtActualEdit.Text;
            string ss = ddlsmplingEdit.SelectedItem.Text;
            executeUpdate(No, cn, ro, act, ss);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, string cn, string ro, string act, string ss)///Buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveTabel12Edit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@cn", cn);
                updateCmd.Parameters.AddWithValue("@ro", ro);
                updateCmd.Parameters.AddWithValue("@act", act);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.Parameters.AddWithValue("@ss", ss);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///Buat add, mengeluarkan form add
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///Klik buton add di form add
        {
            string IDProc_A = id4_lhp;

            if (string.IsNullOrEmpty(IDProc_A))
            {
                IDProc_A = null;
            }

            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string Headdb = txbUsername.Text;

            if (!(string.IsNullOrEmpty(IDProc_A)))
            {
                try
                {
                    string idlhk4 = IDProc_A;
                    string cn = TxtCNAdd.Text;
                    string ro = TxtRONameOnBRNetAdd.Text;
                    string mod = "LHP_CPU";
                    string act = TxtActualAdd.Text;
                    string ss = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk4, cn, ro, mod, act, ss, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }
        }

        private void executeAdd(string idlhk4, string cn, string ro, string mod, string act, string ss, string dt, string Headdbs)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveTabel12", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk4", idlhk4 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("cn", cn);
                addCmd.Parameters.AddWithValue("@ro", ro);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@act", act);
                addCmd.Parameters.AddWithValue("@ss", ss);
                addCmd.Parameters.AddWithValue("@doe", dt);
                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_LHP_Tabel12 where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }
    }
}