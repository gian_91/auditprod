﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using AuditProd.DAL;

namespace AuditProd
{
    public partial class Tabel1_2 : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        string id = string.Empty;
        string id2 = string.Empty;
        string id_lhp = string.Empty;
        string id2_lhp = string.Empty;
        string id4_lhp = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            txbUsername.Text = this.Page.User.Identity.Name;
            if (!string.IsNullOrEmpty((string)Session["Id"]))
            {
                TxbId.Text = Session["Id"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id = TxbId.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id2"]))
            {
                TxbId2.Text = Session["Id2"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id2 = TxbId2.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id_LHP"]))
            {
                TxbId_lhp.Text = Session["Id_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id_lhp = TxbId_lhp.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id2_LHP"]))///Id2_LHP
            {
                TxbId2_lhp.Text = Session["Id2_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id2_lhp = TxbId2_lhp.Text;
            }
            else if (!string.IsNullOrEmpty((string)Session["Id4_LHP"]))
            {
                TxbId4_lhp.Text = Session["Id4_LHP"].ToString();
                TxbJenTabel.Text = Session["Modul"].ToString();
                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                Txbcbng.Text = Session["cbng"].ToString();
                id4_lhp = TxbId4_lhp.Text;
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
            BindGrid();

        }

        //private void Get_Rdc()
        //{
        //    string message = string.Empty;
        //    SqlDataReader reader = null;
        //    String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        //    SqlConnection con = new SqlConnection(strConnString);
        //    con.Open();
        //    SqlCommand cmd = new SqlCommand("SP_SearchAccID", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@AccID", TxtAccountIdAdd.Text);
        //    cmd.Parameters.AddWithValue("@Cabang", Txbcbng.Text);            
        //    reader = cmd.ExecuteReader();
        //    reader.Read();            
        //    {
        //        if (reader.HasRows == true)
        //        {
        //            TxtClientNameAdd.Text = Convert.ToString(reader["ClientName"]);
        //            TxtCenterNameAdd.Text = Convert.ToString(reader["CenterName"]);
        //            TxtDueDaysAdd.Text = Convert.ToString(reader["DueDays"]);
        //            TxtDisburseAdd.Text = Convert.ToString(reader["DisbursementDate"]);
        //            MessageId.Visible = false;
        //        }
        //        else
        //        {
        //            TxtClientNameAdd.Text = string.Empty;
        //            TxtCenterNameAdd.Text = string.Empty;
        //            TxtDueDaysAdd.Text = string.Empty;
        //            TxtDisburseAdd.Text = string.Empty;
        //            MessageId.Visible = true;
        //            MessageId.Text = "Account Id Tidak Ditemukan.";
        //        }
        //        con.Close();                
        //    }
        //}

        //protected void btnSearchRaw_ServerClick(object sender, EventArgs e)
        //{
        //    this.Get_Rdc();
        //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //    sb.Append(@"<script type='text/javascript'>");
        //    //sb.Append("alert('Search Results Are in Progress ...');");
        //    sb.Append("$('#addModal').modal('hide');");
        //    sb.Append(@"</script>");
        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ModelScript", sb.ToString(), false);

        //}

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            Session["Id"] = null;
            Session["Id2"] = null;
            Session["Id_LHP"] = null;
            Session["Id2_LHP"] = null;
            Session["Id4_LHP"] = null;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("LHP.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }   

        public void BindGrid()
        {

            if (!(string.IsNullOrEmpty(id)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel1 where ID_LHK_Acquisition={0}", id);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();

                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);

                }

            }
            else if (!(string.IsNullOrEmpty(id2)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel1 where ID_LHK_Collection={0}", id2);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();

                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
            else if (!(string.IsNullOrEmpty(id_lhp)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel1 where ID_LHP_Acquisition={0}", id_lhp);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();

                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
            else if (!(string.IsNullOrEmpty(id2_lhp)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel1 where ID_LHP_Collection={0}", id2_lhp);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();

                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
            else if (!(string.IsNullOrEmpty(id4_lhp)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHK_LHP_Tabel1 where ID_LHP_CPU={0}", id4_lhp);
                    SqlConnection con = new SqlConnection(cnString);
                    //Fetch data from mysql database
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();

                }
                catch
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                IEnumerable<DataRow> query = from i in dt.AsEnumerable()
                                             where i.Field<Int32>("No").Equals(code)
                                             select i;
                DataTable detailTable = query.CopyToDataTable<DataRow>();
                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //Getddl1edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();
                TxtAccountIdEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[4].Text).ToString();
                TxtClientNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                TxtCenterNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                TxtDueDaysEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                TxtDisburseEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                TxtPicNameEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                TxtRemarksEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                //ddlsmplingEdit.Text = HttpUtility.HtmlDecode(gvrow.Cells[11].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            string rfb = TxtPicNameEdit.Text;
            string rfm = TxtRemarksEdit.Text;
            string ddledit = ddlsmplingEdit.SelectedItem.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdate(No, rfb, rfm, dt, ddledit);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, string rfb, string rfm, string dt, string ddledit)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveTabel1Edit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@indyear", rfb);
                updateCmd.Parameters.AddWithValue("@rem", rfm);
                updateCmd.Parameters.AddWithValue("@doe", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.Parameters.AddWithValue("@ddledit", ddledit);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_A = id;
            string IDProc_B = id2;
            string IDProc_C = id_lhp;
            string IDProc_D = id2_lhp;
            string IDProc_E = id4_lhp;
            string dd = TxtDueDaysAdd.Text;

            if (string.IsNullOrEmpty(IDProc_A))
            {
                IDProc_A = null;
            }
            if (string.IsNullOrEmpty(IDProc_B))
            {
                IDProc_B = null;
            }
            if (string.IsNullOrEmpty(IDProc_C))
            {
                IDProc_C = null;
            }
            if (string.IsNullOrEmpty(IDProc_D))
            {
                IDProc_D = null;
            }
            if (string.IsNullOrEmpty(IDProc_E))
            {
                IDProc_E = null;
            }
            if (string.IsNullOrEmpty(dd))
            {
                dd = null;
            }

            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            string Headdb = txbUsername.Text;

            if (!(string.IsNullOrEmpty(IDProc_A)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string idlhk5 = IDProc_D;
                    string idlhk6 = IDProc_E;
                    string code = TxtAccountIdAdd.Text;
                    string mod = "LHK_Acquisition";
                    string name = TxtClientNameAdd.Text;
                    string region = TxtCenterNameAdd.Text;
                    string continent = dd;
                    string population = TxtDisburseAdd.Text;
                    string indyear = TxtPicNameAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, idlhk5, idlhk6, code, mod, name, region, continent, population, indyear, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);

                }
            }

            else if (!(string.IsNullOrEmpty(IDProc_B)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string idlhk5 = IDProc_D;
                    string idlhk6 = IDProc_E;
                    string code = TxtAccountIdAdd.Text;
                    string mod = "LHK_Collection";
                    string name = TxtClientNameAdd.Text;
                    string region = TxtCenterNameAdd.Text;
                    string continent = dd;
                    string population = TxtDisburseAdd.Text;
                    string indyear = TxtPicNameAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, idlhk5, idlhk6, code, mod, name, region, continent, population, indyear, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }
            else if (!(string.IsNullOrEmpty(IDProc_C)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string idlhk5 = IDProc_D;
                    string idlhk6 = IDProc_E;
                    string code = TxtAccountIdAdd.Text;
                    string mod = "LHP_Acquisition";
                    string name = TxtClientNameAdd.Text;
                    string region = TxtCenterNameAdd.Text;
                    string continent = dd;
                    string population = TxtDisburseAdd.Text;
                    string indyear = TxtPicNameAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, idlhk5, idlhk6, code, mod, name, region, continent, population, indyear, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }
            else if (!(string.IsNullOrEmpty(IDProc_D)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string idlhk5 = IDProc_D;
                    string idlhk6 = IDProc_E;
                    string code = TxtAccountIdAdd.Text;
                    string mod = "LHP_Collection";
                    string name = TxtClientNameAdd.Text;
                    string region = TxtCenterNameAdd.Text;
                    string continent = dd;
                    string population = TxtDisburseAdd.Text;
                    string indyear = TxtPicNameAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, idlhk5, idlhk6, code, mod, name, region, continent, population, indyear, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }
            else if (!(string.IsNullOrEmpty(IDProc_E)))
            {
                try
                {
                    string idlhk2 = IDProc_A;
                    string idlhk3 = IDProc_B;
                    string idlhk4 = IDProc_C;
                    string idlhk5 = IDProc_D;
                    string idlhk6 = IDProc_E;
                    string code = TxtAccountIdAdd.Text;
                    string mod = "LHP_CPU";
                    string name = TxtClientNameAdd.Text;
                    string region = TxtCenterNameAdd.Text;
                    string continent = dd;
                    string population = TxtDisburseAdd.Text;
                    string indyear = TxtPicNameAdd.Text;
                    string rem = TxtRemarksAdd.Text;
                    string samp = ddlsmplingAdd.SelectedItem.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();

                    executeAdd(idlhk2, idlhk3, idlhk4, idlhk5, idlhk6, code, mod, name, region, continent, population, indyear, rem, samp, dt, Headdbs);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError...');", true);
                }
            }

        }

        private void executeAdd(string idlhk2, string idlhk3, string idlhk4, string idlhk5, string idlhk6, string code, string mod, string name, string region, string continent, string population,
            string indyear, string rem, string samp, string dt, string Headdbs)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveTabel1", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idlhk2", idlhk2 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk3", idlhk3 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk4", idlhk4 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk5", idlhk5 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@idlhk6", idlhk6 ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent ?? (object)DBNull.Value);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", dt);
                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@rem", rem);
                addCmd.Parameters.AddWithValue("@samp", samp);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#deleteModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHK_LHP_Tabel1 where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert(' Connection StringError... ');", true);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

    }
}