﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SlipSetoranBank.aspx.cs" Inherits="AuditProd.SlipSetoranBank" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="Scripts/jquery.number.js" type="text/javascript"></script>
    <script src="Scripts/jquery.number.min.js" type="text/javascript"></script>
    <script src="Scripts/autoNumeric.js"></script>

    <script type="text/javascript">
        function HideLabel() {
            var seconds = 2;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
            }, seconds * 1000);
        };
</script>
    <script>
        jQuery(function ($) {
            $('[id$=TxbTotalSlip],[id$=TxbNTColProj],[id$=TxbNTPreclosure],[id$=TxbNTPartial],[id$=TxbNTWriteOff],[id$=TxbNTUjam],[id$=TxbNTTotal],[id$=TxbNTVariance]').autoNumeric(
                {
                    aSep: '',
                    aDec: '.',
                    vMin: '-9999999999'
                });
        });
    </script>
        <style type="text/css">
        .txt100rb {
            width: 100px;            
        }

        .txt50rb {
            width: 100px;
        }

        .txt {
            padding-left: 5px;
            width: 155px;
        }

        .auto-style3 {
            width: 231px;
            height: 49px;
        }

        .bordered {
            width: 314px;
            height: 225px;
            padding: 3px;
            float: left;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered2 {
            width: 310px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .bordered3 {
            width: 292px;
            height: 225px;
            padding: 3px;
            float: right;
            border: 1px solid green;
            border-radius: 5px;
        }

        .Table {
            display: table;
            padding-left: 10px;
        }

        .TableHeader {
            display: table;
            padding-left: 300px;
        }

        .Table-ttd {
            display: table;
            position: relative;
            right: 50px;
        }

        .TableButton {
            display: table;
            position: static;
            padding-left: 730px;
        }

        .div-tableatas {
            display: table;
            width: auto;
            background-color: #eee;
            border: none;
            border-spacing: 10px; /* cellspacing:poor IE support for  this */
        }

        .div-table-row {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-cell {
            display: table-cell;
            width: 140px;
        }

        .div-table-cell-kotak-kanan {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 5px;
        }

        .div-table-cell-kotak-kanan-2 {
            display: table-cell;
            width: auto;
            clear: both;
            padding-left: 13px;
        }

        .div-table-header-kanan {
            width: 420px;
            height: 115px;
            border: 1px solid Blue;
            box-sizing: border-box;
            right: -500px;
            position: relative;
        }

        .div-grup {
            width: auto;
            display: table-column-group;
        }

        .div-tableMid {
            display: table;
            width: auto;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 2px; /* cellspacing:poor IE support for  this */
        }

        .div-table-rowMid {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-rowMid-ttd {
            display: table-row;
            width: auto;
            clear: both;
        }

        .div-table-colMid {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 190px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidBuktiSetoran {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 420px;
            height: 35px;
            background-color: #5ab6fb;
        }

        .div-table-colMidNilaiTotal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 370px;
            height: 35px;
            background-color: #5ab6fb;
        }

        .div-table-colMidTotalSlip {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 420px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidTotalUang1 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 185px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidTotalUang2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 185px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidTotalUang3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 185px;
            height: 35px;
            background-color: #352d2d;
        }

        .div-table-colMidTextMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidTextBoxMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 210px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidVarianceMenu {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 605px;
            height: 35px;
            background-color: #f85c5c;
        }
        
        .div-table-colMidVarianceTotal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 185px;
            height: 35px;
            background-color: #f85c5c;
        }

        .div-tableMid-TextArea {
            display: table;
            width: 860px;
            background-color: #eee;
            border: 1px solid #666666;
            border-spacing: 5px; /* cellspacing:poor IE support for  this */
        }

        .div-table-colTanggal {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 127px;
            height: 35px;
            background-color: none;
        }

        .div-table-colTanggal2 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 105px;
            height: 35px;
            background-color: none;
            padding-left: 100px;
        }

        .div-table-colTanggal3 {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 70px;
            height: 35px;
            background-color: none;
        }

        .div-table-colMidSelisih {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 850px;
            height: 35px;
            background-color: whitesmoke;
        }

        .div-table-colMidSelisihtextArea {
            float: left; /* fix for  buggy browsers */
            display: table-column;
            width: 840px;
            height: 185px;
            background-color: whitesmoke;
        }

        .div-table-colTTD {
            float: right;
            display: table-column;
            width: 280px;
            height: 20px;
            background-color: none;
        }

        .textarea2 {
            font-family: 'Times New Roman';
            width: 840px;
            height: 175px;
            font-size: 15px;
        }

        .display-validate {
            clear: both;
            display: block;
            position: relative;
            left: 210px;
            bottom: 30px;
        }

      @media print {
          #printbtn, [id$=btnSave], [id$=btnExit], [id$=ExportExcel] {
              display: none;
          }
      }
    </style>   
        <script>
            $(function () {
                $("[id$=TxbTnglColProj]").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true
                });
            });
            $(function () {
                $("[id$=TxbTnglBwh]").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    changeYear: true
                });
            });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
     <fieldset>     
        <legend></legend>
        <div class="Table">
            <div class="div-table-header-kanan">
                <label align="center">IAC GL - 04B</label>
                <hr />
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="TxbDibuatHead">Dibuat </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbDibuatHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label1" runat="server" AssociatedControlID="TxbDireviewHead">Direview </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbDireviewHead" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell-kotak-kanan">
                        <asp:Label ID="Label4" runat="server" AssociatedControlID="TxbTanggalHeadDibuat">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan">
                        <asp:TextBox ID="TxbTanggalHeadDibuat" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:Label ID="Label2" runat="server" AssociatedControlID="TxbTanggalHeadDireview">Tanggal </asp:Label>
                    </div>
                    <div class="div-table-cell-kotak-kanan-2">
                        <asp:TextBox ID="TxbTanggalHeadDireview" runat="server" ReadOnly="true" Width="110px" Height="20px"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="Table">
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblCabang" runat="server" AssociatedControlID="txbCabang">Bina Artha Cabang </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbCabang" runat="server" ReadOnly="true" Width="210px"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSave" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txtIDSaveTemp" runat="server" ReadOnly="true" Width="195px" Visible="false"></asp:TextBox>
                        <asp:TextBox runat="server" ID="TxbModul" ReadOnly="True" Visible="false" />
<%--                        <asp:TextBox runat="server" ID="txbVarhidden" ReadOnly="True" Visible="true" />--%>
                    </div>
                </div>
                <div class="div-table-row">
                    <div class="div-table-cell">
                        <asp:Label ID="lblPeriode" runat="server" AssociatedControlID="txbPeriodeStart">Periode Audit </asp:Label>
                    </div>
                    <div class="div-table-cell">
                        <asp:TextBox ID="txbPeriodeStart" runat="server" ReadOnly="true" Width="90px"></asp:TextBox>to
                    <asp:TextBox ID="txbPeriodeEnd" runat="server" ReadOnly="true" Width="91px"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div align="center" class="TableHeader">
                <h2>Berita Acara Cash Vault</h2>
                <h4>Internal Audit & Control Department</h4>
            </div>
            <br />
            <div class="Table">
                <div class="div-tableMid">
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidBuktiSetoran" align="center">Bukti Setor Bank</div>
                        <div class="div-table-colMidNilaiTotal" align="Center">Nilai Total</div>
                    </div>
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidTotalSlip" align="center">Total Slip Setor Bank</div>
                        <div class="div-table-colMidTotalUang3" align="Center"></div>
                        <div class="div-table-colMidTotalUang2" align="Center">Rp<asp:TextBox ID="TxbTotalSlip" runat="server" Width="145px" AutoCompleteType="Disabled"
                             AutoPostBack="true" OnTextChanged="TxbTotalSlip_TextChanged" ></asp:TextBox>,-</div>
                    </div>
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidTextMenu">Collection Projection Tanggal</div>
                        <div class="div-table-colMidTextBoxMenu"><asp:TextBox ID="TxbTnglColProj" runat="server" Width="140px" AutoCompleteType="Disabled"></asp:TextBox></div>
                        <div class="div-table-colMidTotalUang1" align="Center">Rp<asp:TextBox ID="TxbNTColProj" runat="server" Width="145px" 
                            OnTextChanged="TxbNTColProj_TextChanged" AutoCompleteType="Disabled" AutoPostBack="true" Class="numericOnly" ></asp:TextBox>,-</div>
                        <div class="div-table-colMidTotalUang2" align="Center"></div>
                    </div>
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidTextMenu">Preclosure</div>
                        <div class="div-table-colMidTextBoxMenu"><asp:TextBox ID="TxbPreclosure" runat="server" Width="100px" AutoCompleteType="Disabled"></asp:TextBox>Mitra</div>
                        <div class="div-table-colMidTotalUang1" align="Center">Rp<asp:TextBox ID="TxbNTPreclosure" runat="server" Width="145px" 
                            OnTextChanged="TxbNTPreclosure_TextChanged" AutoCompleteType="Disabled" AutoPostBack="true" ></asp:TextBox>,-</div>
                        <div class="div-table-colMidTotalUang2" align="Center"></div>
                    </div>
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidTextMenu">Partial/Tunggakan</div>
                        <div class="div-table-colMidTextBoxMenu"><asp:TextBox ID="TxbPartial" runat="server" Width="100px" AutoCompleteType="Disabled"></asp:TextBox>Mitra</div>
                        <div class="div-table-colMidTotalUang1" align="Center">Rp<asp:TextBox ID="TxbNTPartial" runat="server" Width="145px" 
                            OnTextChanged="TxbNTPartial_TextChanged" AutoCompleteType="Disabled" AutoPostBack="true" ></asp:TextBox>,-</div>
                        <div class="div-table-colMidTotalUang2" align="Center"></div>
                    </div>
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidTextMenu">Write Off</div>
                        <div class="div-table-colMidTextBoxMenu"><asp:TextBox ID="TxbWriteOff" runat="server" Width="100px" AutoCompleteType="Disabled"></asp:TextBox>Mitra</div>
                        <div class="div-table-colMidTotalUang1" align="Center">Rp<asp:TextBox ID="TxbNTWriteOff" runat="server" Width="145px" OnTextChanged="TxbNTWriteOff_TextChanged" AutoCompleteType="Disabled" AutoPostBack="true" ></asp:TextBox>,-</div>
                        <div class="div-table-colMidTotalUang2" align="Center"></div>
                    </div>
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidTextMenu">UTJ</div>
                        <div class="div-table-colMidTextBoxMenu"><asp:TextBox ID="TxbUjam" runat="server" Width="100px" AutoCompleteType="Disabled"></asp:TextBox>Mitra</div>
                        <div class="div-table-colMidTotalUang1" align="Center">Rp<asp:TextBox ID="TxbNTUjam" runat="server" Width="145px" OnTextChanged="TxbNTUjam_TextChanged" AutoCompleteType="Disabled" AutoPostBack="true" ></asp:TextBox>,-</div>
                        <div class="div-table-colMidTotalUang2" align="Center"></div>
                    </div>
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidTotalSlip" align="center">Total</div>
                        <div class="div-table-colMidTotalUang3" align="Center"></div>
                        <div class="div-table-colMidTotalUang2" align="Center">Rp<asp:TextBox ID="TxbNTTotal" runat="server" Width="145px" ReadOnly="true" AutoPostBack="false"></asp:TextBox>,-</div>
                    </div>
                    <div class="div-table-rowMid">
                        <div class="div-table-colMidVarianceMenu" align="center">Variance</div>
                        <div class="div-table-colMidVarianceTotal" align="Center">Rp<asp:TextBox ID="TxbNTVariance" runat="server" Width="147px" ReadOnly="true" AutoPostBack="false" ></asp:TextBox>,-</div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="Table">
            <div class="div-tableMid-TextArea">
                <div class="div-table-rowMid">
                    <div class="div-table-colMidSelisih">
                        <label>Penjelasan Selisih :</label>
                    </div>
                    <div class="div-table-colMidSelisihtextArea">
                        <textarea class="textarea2" id="RemarksPenjSelisih" runat="server"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="Table">
            <div class="Table" align="center">
                <div class="div-table-colTanggal">
                    <label>Dilaksanakan Di</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbDilaksanakan" runat="server" Width="210px" AutoCompleteType="Disabled" ReadOnly="true"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal2">
                    <label>Pada Tanggal</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbTnglBwh" runat="server" Width="110px" AutoCompleteType="Disabled"></asp:TextBox>
                </div>
                <div class="div-table-colTanggal3">
                    <label>Pada Jam</label>
                </div>
                <div class="div-table-colTanggal">
                    <asp:TextBox ID="TxbPdJam" runat="server" Width="110px"></asp:TextBox>
                </div>
            </div>
        </div>
<%--        <br />--%>
        <br />
        <div class="Table">
            <div class="Table-ttd">
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Diperiksa Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Disaksikan Oleh,</div>
                    <div class="div-table-colTTD" align="Center">Mengetahui,</div>
                </div>
                <br />
                <br />
                <br />
                <br />
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                    <div class="div-table-colTTD" align="Center">(...............................)</div>
                </div>
                <div class="div-table-rowMid-ttd">
                    <div class="div-table-colTTD" align="center">Internal Audit&Control</div>
                    <div class="div-table-colTTD" align="Center">Admin/Kasir</div>
                    <div class="div-table-colTTD" align="Center">Branch Manager</div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="Table">
            <div class="TableButton">
                <input id="printbtn" type="button" value="Print" onclick="window.print();">
                <asp:Button ID="btnExit" runat="server" Text="Exit" Width="70px" OnClick="btnExit_Click" />
                <asp:Button ID="btnSave" runat="server" Text="Save" Width="70px" OnClick="btnSave_Click" />
                <%--<asp:Button ID="ExportExcel" runat="server" Text="Export" Width="70px" OnClick="ExportExcel_Click"/>--%>
                <asp:Label ID="lblMessage" runat="server" Text="Data Success In Saved" ForeColor="Blue" Visible="false" ></asp:Label>
            </div>
        </div>
        <div>
            <asp:TextBox ID="TxtCreateDate" runat="server" Visible="false"></asp:TextBox>
            <asp:TextBox ID="TxtReviewDate" runat="server" Visible="false"></asp:TextBox>
        </div>
    </fieldset>
</asp:Content>
