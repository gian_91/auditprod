﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AuditProd.DAL
{
    public class GetRawID
    {
        //public int ID { get; set; }
        public string AccountID { get; set; }
        public string ClientName { get; set; }
        public string DueDays { get; set; }
        public string CenterName { get; set; }
        public string DisbursementDate { get; set; }
    }
}