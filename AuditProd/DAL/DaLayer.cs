﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.Security;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace AuditProd.DAL
{
    public class DaLayer
    {
        SqlConnection con = new SqlConnection("Data Source=LAPTOP-JHSVRKR3;Initial Catalog=Login_Page;User ID=sa;Password=P@ssw0rd");
        //SqlCommand cmd;
        SqlDataAdapter da;
        SqlDataReader dr;
        DataTable dt;

        public DataTable Get_MasterBranch()
        {
            da = new SqlDataAdapter("SELECT [ID],[BranchId_BranchName] FROM [Master_Branch]", con);
            dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public DataTable Get_MasterMonth()
        {
            da = new SqlDataAdapter("SELECT ID,MonthName FROM Master_Month", con);
            dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public DataTable Get_MasterYear()
        {
            da = new SqlDataAdapter("SELECT ID,year FROM Master_Year", con);
            dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public DataTable getMitra()
        {
            da = new SqlDataAdapter("select ID_DT,IdDoctest,KTPMitra_UKM from Doc_Test", con);
            dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public string Encript(string clearText)
        {
            string EncryptionKey = "B4V2OI8J4K4Rt4";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;

        }

        public string Decrypt(string cipherText)
        {
            string EncryptionKey = "B4V2OI8J4K4Rt4";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

    }
}