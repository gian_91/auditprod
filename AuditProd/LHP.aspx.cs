﻿using System.Web.Services;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AuditProd.DAL;
using System.Globalization;

namespace AuditProd
{
    public partial class LHP : System.Web.UI.Page
    {
        DataTable dt;
        DaLayer dl = new DaLayer();
        protected void Page_Load(object sender, EventArgs e)
        {
            //TxbModul.Text = "LHP";
            txbUsername.Text = this.Page.User.Identity.Name;

            if (!IsPostBack)
            {
                if (Session["BranchID"] != null)
                    txbCabang.Text = Session["BranchID"].ToString();
                if (Session["AuditStart"] != null)
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                if (Session["AuditEnd"] != null)
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                {
                    Response.Redirect("Login.aspx");
                }

                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {
                    BindGrid();
                    BindGridColl();
                    BindGridAdm();
                    BindGridCPU();
                    BindGridHR();
                    BindGridProc();
                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {
                    BindGrid();
                    BindGridColl();
                    BindGridAdm();
                    BindGridCPU();
                    BindGridHR();
                    BindGridProc();
                }

            }

        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("AuditBranch.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("AuditBranch.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }


        /// <summary>
        /// AcquisitionProcess
        /// </summary>
        public void BindGrid()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_Acquisition where ID_LHP={0}", IDProc_A);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_Acquisition where ID_LHP={0}", IDProc_B);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    //Bind the fetched data to gridview
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                    con.Close();
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }
        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                IEnumerable<DataRow> query = from i in dt.AsEnumerable()
                                             where i.Field<Int32>("No").Equals(code)
                                             select i;
                DataTable detailTable = query.CopyToDataTable<DataRow>();
                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //BindGrid();
                Getddl1edit();
                GridViewRow gvrow = GridView1.Rows[index];
                lblCountryCode.Text = GridView1.DataKeys[index].Value.ToString();////HttpUtility.HtmlDecode(gvrow.Cells[3].Text).ToString();
                //ddl1edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                //ddl2edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                //ddl3edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                //ddl4edit.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                txtRFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txtRFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                lblResult.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#editModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#deleteModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("detail_click"))
            {
                string code = GridView1.DataKeys[index].Value.ToString();
                string tblCode = string.Empty;
                string modul = string.Empty;
                string cbng = string.Empty;
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from LHP_Acquisition where [No] = @IDProc", con);
                cmd.Parameters.AddWithValue("@IDProc", code);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    txbtabel.Text = Convert.ToString(dr["LHP_Acquisition_Tabel"]);
                    TxbModul.Text = Convert.ToString(dr["Tipe"]);
                    txbcbng.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                }
                con.Close();
                tblCode = txbtabel.Text;
                modul = TxbModul.Text;
                cbng = txbcbng.Text;
                Session["cbng"] = cbng;
                Session["Code"] = tblCode;
                Session["Modul"] = modul;
                Session["Id_LHP"] = code;
                Session["ID_SH"].ToString();
                Session["ID_SH_Temp"].ToString();

                if (tblCode == "A10" || tblCode == "A11" || tblCode == "A12" || tblCode == "A13" || tblCode == "A15")
                {
                    Response.Redirect("Tabel1_2.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "A1" || tblCode == "A2")
                {
                    Response.Redirect("Tabel2.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "A21")
                {
                    Response.Redirect("Tabel3_2.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Kode Tabel Tidak Ditemukan di LHP Acquisition Process, Cek kembali');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)////buat edit
        {
            int No = Convert.ToInt32(lblCountryCode.Text);
            string category = ddl1edit.SelectedItem.Text;
            string tod = ddl2edit.SelectedItem.Text;
            string cf = ddl3edit.SelectedItem.Text;
            string tabel = ddl4edit.SelectedItem.Text;
            string rfb = txtRFB.Text;
            string rfm = txtRFM.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdate(No, category, tod, cf, tabel, rfb, rfm, dt);///masuk ke private void update                  
            BindGrid();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#editModal').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdate(int No, string category, string tod, string cf, string tabel, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveLHPAcquisitionEdit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@category", category);
                updateCmd.Parameters.AddWithValue("@tod", tod);
                updateCmd.Parameters.AddWithValue("@cf", cf);
                updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)///buat add, menegluarkan form add
        {
            Getddl1();
            txtTotalPopulation.Text = string.Empty;
            txtIndYear.Text = string.Empty;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#addModal').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddRecord_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;
            string modul = "LHP_Acquisition";
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            string Headdb = txbUsername.Text;
            string BAVcbng = txbCabang.Text;
            string datestart = txbPeriodeStart.Text;
            string dateend = txbPeriodeEnd.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_A);
                    string code = ddl1.SelectedItem.Text;
                    string name = ddl2.SelectedItem.Text;
                    string region = ddl4.SelectedItem.Text;
                    string continent = ddl3.SelectedItem.Text;
                    string population = txtTotalPopulation.Text;
                    string indyear = txtIndYear.Text;
                    string dt = doe.ToString();
                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();

                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    //DateTime datestarts = DateTime.Parse(datestart);
                    //DateTime dateends = DateTime.Parse(dateend);

                    executeAdd(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    //txtidLHP.Text = "12";
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_B);
                    string code = ddl1.SelectedItem.Text;
                    string name = ddl2.SelectedItem.Text;
                    string region = ddl4.SelectedItem.Text;
                    string continent = ddl3.SelectedItem.Text;
                    string population = txtTotalPopulation.Text;
                    string indyear = txtIndYear.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();

                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();
                    //DateTime datestarts = DateTime.Parse(datestart);
                    //DateTime dateends = DateTime.Parse(dateend);

                    executeAdd(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGrid();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#addModal').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }

        }

        private void executeAdd(int idLHP, string code, string name, string continent, string region, string population, string indyear, string mod, string doe,
            string Headdbs, string BAVcbngs, string datestarts, string dateends)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveLHPAcquisition", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idLHP", idLHP);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", doe);
                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@BAVcbngs", BAVcbngs);
                addCmd.Parameters.AddWithValue("@datestarts", DateTime.ParseExact(datestarts.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));
                addCmd.Parameters.AddWithValue("@dateends", DateTime.ParseExact(dateends.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDelete(code);
            BindGrid();
            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
            //sb.Append(@"<script type='text/javascript'>");
            //sb.Append("alert('Record deleted Successfully');");
            //sb.Append("$('#deleteModal').modal('hide');");
            //sb.Append(@"</script>");
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDelete(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHP_Acquisition where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Record deleted Successfully');");
                sb.Append("$('#deleteModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

            }
            catch (SqlException me)
            {                
                System.Console.Write(me.Message);
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("alert('Cannot delete table. Please delete the Table Detail first');");
                sb.Append("$('#deleteModal').modal('hide');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
            }

        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGrid();
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
        }

        private void Getddl1()
        {
            ddl1.AppendDataBoundItems = true;
            ddl2.AppendDataBoundItems = true;
            ddl3.AppendDataBoundItems = true;
            ddl4.AppendDataBoundItems = true;

            ddl1.Items.Clear();
            ddl1.Items.Add(new ListItem("--Select category--", ""));
            ddl2.Items.Clear();
            ddl2.Items.Add(new ListItem("--Select Type--", ""));
            ddl3.Items.Clear();
            ddl3.Items.Add(new ListItem("--Select Finding--", ""));
            ddl4.Items.Clear();
            ddl4.Items.Add(new ListItem("--Select Tabel--", ""));


            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_Acquisition_Category],[Category_Name] from Master_LHP_Acquisition_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl1.DataSource = cmd.ExecuteReader();
                ddl1.DataTextField = "Category_Name";
                ddl1.DataValueField = "ID_LHP_Acquisition_Category";
                ddl1.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl2.Items.Clear();
            ddl2.Items.Add(new ListItem("--Select Type--", ""));
            ddl3.Items.Clear();
            ddl3.Items.Add(new ListItem("--Select Finding--", ""));
            ddl4.Items.Clear();
            ddl4.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Acquisition_TOD, TOD_Name from Master_LHP_Acquisition_TOD " +
                               "where ID_LHP_Acquisition_Category=@ID_LHP_Acquisition_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Acquisition_Category", ddl1.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddl2.DataSource = cmd.ExecuteReader();
                ddl2.DataTextField = "TOD_Name";
                ddl2.DataValueField = "ID_LHP_Acquisition_TOD";
                ddl2.DataBind();

                if (ddl2.Items.Count > 1)
                {
                    ddl1.Enabled = true;
                    ddl2.Enabled = true;
                }
                else
                {
                    ddl2.Enabled = false;
                    ddl3.Enabled = false;
                    ddl4.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl3.Items.Clear();
            ddl3.Items.Add(new ListItem("--Select Finding--", ""));
            //ddl3.AppendDataBoundItems = true;
            ddl4.Items.Clear();
            ddl4.Items.Add(new ListItem("--Select Tabel--", ""));
            //ddl4.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Acquisition_Finding, Finding_Nama from Master_LHP_Acquisition_Finding " +
                                        "where ID_LHP_Acquisition_TOD=@ID_LHP_Acquisition_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Acquisition_TOD", ddl2.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl3.DataSource = cmd.ExecuteReader();
                ddl3.DataTextField = "Finding_Nama";
                ddl3.DataValueField = "ID_LHP_Acquisition_Finding";
                ddl3.DataBind();

                if (ddl3.Items.Count > 1)
                {
                    ddl3.Enabled = true;
                    //ddl4.Enabled = true;
                }

                else
                {
                    ddl3.Enabled = false;
                    ddl4.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddl3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl4.Items.Clear();
            ddl4.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Acquisition_Tabel, Tabel_Name from Master_LHP_Acquisition_Tabel " +
                                        "where ID_LHP_Acquisition_Finding=@ID_LHP_Acquisition_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Acquisition_Finding", ddl3.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl4.DataSource = cmd.ExecuteReader();
                ddl4.DataTextField = "Tabel_Name";
                ddl4.DataValueField = "ID_LHP_Acquisition_Tabel";
                ddl4.DataBind();

                if (ddl4.Items.Count > 1)
                {
                    ddl4.Enabled = true;
                }

                else
                {
                    ddl4.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        private void Getddl1edit()
        {
            ddl1edit.AppendDataBoundItems = true;
            ddl2edit.AppendDataBoundItems = true;
            ddl3edit.AppendDataBoundItems = true;
            ddl4edit.AppendDataBoundItems = true;

            ddl1edit.Items.Clear();
            ddl1edit.Items.Add(new ListItem("--Select Category--", ""));
            ddl2edit.Items.Clear();
            ddl2edit.Items.Add(new ListItem("--Select Type--", ""));
            ddl3edit.Items.Clear();
            ddl3edit.Items.Add(new ListItem("--Select Finding--", ""));
            ddl4edit.Items.Clear();
            ddl4edit.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_Acquisition_Category],[Category_Name] from Master_LHP_Acquisition_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl1edit.DataSource = cmd.ExecuteReader();
                ddl1edit.DataTextField = "Category_Name";
                ddl1edit.DataValueField = "ID_LHP_Acquisition_Category";
                ddl1edit.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddl1edit_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl2edit.Items.Clear();
            ddl2edit.Items.Add(new ListItem("--Select Type--", ""));
            ddl3edit.Items.Clear();
            ddl3edit.Items.Add(new ListItem("--Select Finding--", ""));
            ddl4edit.Items.Clear();
            ddl4edit.Items.Add(new ListItem("--Select Tabel--", ""));

            ddl2edit.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Acquisition_TOD, TOD_Name from Master_LHP_Acquisition_TOD " +
                               "where ID_LHP_Acquisition_Category=@ID_LHP_Acquisition_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Acquisition_Category", ddl1edit.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddl2edit.DataSource = cmd.ExecuteReader();
                ddl2edit.DataTextField = "TOD_Name";
                ddl2edit.DataValueField = "ID_LHP_Acquisition_TOD";
                ddl2edit.DataBind();

                if (ddl1edit.Items.Count > 1)
                {
                    ddl1edit.Enabled = true;
                }
                else
                {
                    ddl2edit.Enabled = false;
                    ddl3edit.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddl2edit_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl3edit.Items.Clear();
            ddl3edit.Items.Add(new ListItem("--Select Finding--", ""));
            ddl3edit.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Acquisition_Finding, Finding_Nama from Master_LHP_Acquisition_Finding " +
                                        "where ID_LHP_Acquisition_TOD=@ID_LHP_Acquisition_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Acquisition_TOD", ddl2edit.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl3edit.DataSource = cmd.ExecuteReader();
                ddl3edit.DataTextField = "Finding_Nama";
                ddl3edit.DataValueField = "ID_LHP_Acquisition_Finding";
                ddl3edit.DataBind();

                if (ddl3edit.Items.Count > 1)
                {
                    ddl3edit.Enabled = true;
                }

                else
                {
                    ddl3edit.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddl3edit_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddl4edit.Items.Clear();
            ddl4edit.Items.Add(new ListItem("--Select Tabel--", ""));
            ddl4edit.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Acquisition_Tabel, Tabel_Name from Master_LHP_Acquisition_Tabel " +
                                        "where ID_LHP_Acquisition_Finding=@ID_LHP_Acquisition_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Acquisition_Finding", ddl3edit.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddl4edit.DataSource = cmd.ExecuteReader();
                ddl4edit.DataTextField = "Tabel_Name";
                ddl4edit.DataValueField = "ID_LHP_Acquisition_Tabel";
                ddl4edit.DataBind();

                if (ddl4edit.Items.Count > 1)
                {
                    ddl4edit.Enabled = true;
                }

                else
                {
                    ddl4edit.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }



        /// <summary>
        /// CollectionProcess
        /// </summary>
        public void BindGridColl()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_Collection where ID_LHP={0}", IDProc_A);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView2.DataSource = dt;
                    GridView2.DataBind();
                    con.Close();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_Collection where ID_LHP={0}", IDProc_B);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView2.DataSource = dt;
                    GridView2.DataBind();
                    con.Close();
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }
        }

        protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {
                string code = GridView2.DataKeys[index].Value.ToString();
                IEnumerable<DataRow> query = from i in dt.AsEnumerable()
                                             where i.Field<Int32>("No").Equals(code)
                                             select i;
                DataTable detailTable = query.CopyToDataTable<DataRow>();
                DetailsView1.DataSource = detailTable;
                DetailsView1.DataBind();
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#detailModal').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DetailModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //BindGridColl();
                Getddl1Coledit();
                GridViewRow gvrow = GridView2.Rows[index];
                Label1.Text = GridView2.DataKeys[index].Value.ToString();////HttpUtility.HtmlDecode(gvrow.Cells[3].Text).ToString();
                //ddledit1coll.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                //ddledit2coll.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                //ddledit3coll.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                //ddledit4coll.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                txbEditCollRFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txbEditCollRFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                Label2.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#Div2Edit').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                //ClientScript.RegisterStartupScript(GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView2.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#Div4Del').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("detail_click"))
            {
                string code = GridView2.DataKeys[index].Value.ToString();
                string tblCode = string.Empty;
                string modul = string.Empty;
                string message = string.Empty;
                string cbng = string.Empty;
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from LHP_Collection where [No] = @IDProc", con);
                cmd.Parameters.AddWithValue("@IDProc", code);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    txbtabel.Text = Convert.ToString(dr["LHP_Collection_Tabel"]);
                    TxbModul.Text = Convert.ToString(dr["Tipe"]);
                    txbcbng.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                }
                con.Close();
                tblCode = txbtabel.Text;
                modul = TxbModul.Text;
                cbng = txbcbng.Text;
                Session["cbng"] = cbng;
                Session["Code"] = tblCode;
                Session["Modul"] = modul;
                Session["Id2_LHP"] = code;
                Session["ID_SH"].ToString();
                Session["ID_SH_Temp"].ToString();

                if (tblCode == "B1" || tblCode == "B2" || tblCode == "B3" || tblCode == "B4" || tblCode == "B5" || tblCode == "B16" || tblCode == "B23" || tblCode == "B29" || tblCode == "B30" ||
                    tblCode == "B31" || tblCode == "B32" || tblCode == "B33" || tblCode == "B34" || tblCode == "B48")
                {
                    Response.Redirect("Tabel1_2.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "B11" || tblCode == "B12")
                {
                    Response.Redirect("Tabel2.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "B8" || tblCode == "B10" || tblCode == "B13" || tblCode == "B14" || tblCode == "B22" || tblCode == "B27" || tblCode == "B28")
                {
                    Response.Redirect("Tabel3_2.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "B24" || tblCode == "B25")
                {
                    Response.Redirect("Tabel6.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "B17" || tblCode == "B18" || tblCode == "B19" || tblCode == "B20" || tblCode == "B21")
                {
                    Response.Redirect("Tabel16.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Kode Tabel Tidak Ditemukan di LHP Acquisition Process, Cek kembali');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }

            }

        }

        protected void BtnSaveColl_Click(object sender, EventArgs e)
        {
            int No = Convert.ToInt32(Label1.Text);
            string category = ddledit1coll.SelectedItem.Text;
            string tod = ddledit2coll.SelectedItem.Text;
            string cf = ddledit3coll.SelectedItem.Text;
            string tabel = ddledit4coll.SelectedItem.Text;
            string rfb = txbEditCollRFB.Text;
            string rfm = txbEditCollRFM.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdateCol(No, category, tod, cf, tabel, rfb, rfm, dt);///masuk ke private void update                  
            BindGridColl();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#Div2Edit').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

        }

        private void executeUpdateCol(int No, string category, string tod, string cf, string tabel, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveLHPCollectionEdit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@category", category);
                updateCmd.Parameters.AddWithValue("@tod", tod);
                updateCmd.Parameters.AddWithValue("@cf", cf);
                updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnAddCollection_Click(object sender, EventArgs e)///buat add, menegluarkan form ad
        {
            Getddl1Col();
            txbAddCollRFB.Text = string.Empty;
            txbAddCollRFM.Text = string.Empty;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#Div3Add').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }

        protected void btnAddColl_Click(object sender, EventArgs e)///klik buton add di form add
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;
            string modul = "LHP_Collection";
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            string Headdb = txbUsername.Text;
            string BAVcbng = txbCabang.Text;
            string datestart = txbPeriodeStart.Text;
            string dateend = txbPeriodeEnd.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    //txtidLHP.Text = "12";
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_A);
                    string code = ddladd1coll.SelectedItem.Text;
                    string name = ddladd2coll.SelectedItem.Text;
                    string region = ddladd4coll.SelectedItem.Text;
                    string continent = ddladd3coll.SelectedItem.Text;
                    string population = txbAddCollRFB.Text;
                    string indyear = txbAddCollRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddCol(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridColl();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3Add').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    //txtidLHP.Text = "12";
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_B);
                    string code = ddladd1coll.SelectedItem.Text;
                    string name = ddladd2coll.SelectedItem.Text;
                    string region = ddladd4coll.SelectedItem.Text;
                    string continent = ddladd3coll.SelectedItem.Text;
                    string population = txbAddCollRFB.Text;
                    string indyear = txbAddCollRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddCol(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridColl();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3Add').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }

        }

        private void executeAddCol(int idLHP, string code, string name, string continent, string region, string population, string indyear, string mod, string doe,
            string Headdbs, string BAVcbngs, string datestarts, string dateends)///Adding data
        {
            //txtidLHP.Text = "12";
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveLHPCollection", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idLHP", idLHP);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", doe);

                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@BAVcbngs", BAVcbngs);
                addCmd.Parameters.AddWithValue("@datestarts", DateTime.ParseExact(datestarts.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));
                addCmd.Parameters.AddWithValue("@dateends", DateTime.ParseExact(dateends.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnDelCol_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDeleteCol(code);
            BindGridColl();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#Div4Del').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDeleteCol(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHP_Collection where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGridColl();
            GridView2.PageIndex = e.NewPageIndex;
            GridView2.DataBind();
        }

        private void Getddl1Col()
        {
            ddladd1coll.AppendDataBoundItems = true;
            ddladd2coll.AppendDataBoundItems = true;
            ddladd3coll.AppendDataBoundItems = true;
            ddladd4coll.AppendDataBoundItems = true;

            ddladd1coll.Items.Clear();
            ddladd1coll.Items.Add(new ListItem("--Select category--", ""));
            ddladd2coll.Items.Clear();
            ddladd2coll.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3coll.Items.Clear();
            ddladd3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4coll.Items.Clear();
            ddladd4coll.Items.Add(new ListItem("--Select Tabel--", ""));


            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_Collection_Category],[Category_Name] from Master_LHP_Collection_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd1coll.DataSource = cmd.ExecuteReader();
                ddladd1coll.DataTextField = "Category_Name";
                ddladd1coll.DataValueField = "ID_LHP_Collection_Category";
                ddladd1coll.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddladd1coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd2coll.Items.Clear();
            ddladd2coll.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3coll.Items.Clear();
            ddladd3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4coll.Items.Clear();
            ddladd4coll.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Collection_TOD, TOD_Name from Master_LHP_Collection_TOD " +
                               "where ID_LHP_Collection_Category=@ID_LHP_Collection_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Collection_Category", ddladd1coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd2coll.DataSource = cmd.ExecuteReader();
                ddladd2coll.DataTextField = "TOD_Name";
                ddladd2coll.DataValueField = "ID_LHP_Collection_TOD";
                ddladd2coll.DataBind();

                if (ddladd2coll.Items.Count > 1)
                {
                    ddladd1coll.Enabled = true;
                    ddladd2coll.Enabled = true;
                }
                else
                {
                    ddladd2coll.Enabled = false;
                    ddladd3coll.Enabled = false;
                    ddladd4coll.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd2coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd3coll.Items.Clear();
            ddladd3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4coll.Items.Clear();
            ddladd4coll.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Collection_Finding, Finding_Nama from Master_LHP_Collection_Finding " +
                                        "where ID_LHP_Collection_TOD=@ID_LHP_Collection_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Collection_TOD", ddladd2coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd3coll.DataSource = cmd.ExecuteReader();
                ddladd3coll.DataTextField = "Finding_Nama";
                ddladd3coll.DataValueField = "ID_LHP_Collection_Finding";
                ddladd3coll.DataBind();

                if (ddladd3coll.Items.Count > 1)
                {
                    ddladd3coll.Enabled = true;
                    //ddl4.Enabled = true;
                }

                else
                {
                    ddladd3coll.Enabled = false;
                    ddladd4coll.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd3coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd4coll.Items.Clear();
            ddladd4coll.Items.Add(new ListItem("--Select Tabel--", ""));
            //ddl4.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Collection_Tabel, Tabel_Name from Master_LHP_Collection_Tabel " +
                                        "where ID_LHP_Collection_Finding=@ID_LHP_Collection_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Collection_Finding", ddladd3coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd4coll.DataSource = cmd.ExecuteReader();
                ddladd4coll.DataTextField = "Tabel_Name";
                ddladd4coll.DataValueField = "ID_LHP_Collection_Tabel";
                ddladd4coll.DataBind();

                if (ddladd4coll.Items.Count > 1)
                {
                    ddladd4coll.Enabled = true;
                }

                else
                {
                    ddladd4coll.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        private void Getddl1Coledit()
        {
            ddledit1coll.AppendDataBoundItems = true;
            ddledit2coll.AppendDataBoundItems = true;
            ddledit3coll.AppendDataBoundItems = true;
            ddledit4coll.AppendDataBoundItems = true;

            ddledit1coll.Items.Clear();
            ddledit1coll.Items.Add(new ListItem("--Select Category--", ""));
            ddledit2coll.Items.Clear();
            ddledit2coll.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3coll.Items.Clear();
            ddledit3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4coll.Items.Clear();
            ddledit4coll.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_Collection_Category],[Category_Name] from Master_LHP_Collection_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit1coll.DataSource = cmd.ExecuteReader();
                ddledit1coll.DataTextField = "Category_Name";
                ddledit1coll.DataValueField = "ID_LHP_Collection_Category";
                ddledit1coll.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddledit1coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit2coll.Items.Clear();
            ddledit2coll.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3coll.Items.Clear();
            ddledit3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4coll.Items.Clear();
            ddledit4coll.Items.Add(new ListItem("--Select Tabel--", ""));

            ddledit2coll.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Collection_TOD, TOD_Name from Master_LHP_Collection_TOD " +
                               "where ID_LHP_Collection_Category=@ID_LHP_Collection_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Collection_Category", ddledit1coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit2coll.DataSource = cmd.ExecuteReader();
                ddledit2coll.DataTextField = "TOD_Name";
                ddledit2coll.DataValueField = "ID_LHP_Collection_TOD";
                ddledit2coll.DataBind();

                if (ddledit1coll.Items.Count > 1)
                {
                    ddledit1coll.Enabled = true;
                }
                else
                {
                    ddledit2coll.Enabled = false;
                    ddledit3coll.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit2coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit3coll.Items.Clear();
            ddledit3coll.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit3coll.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Collection_Finding, Finding_Nama from Master_LHP_Collection_Finding " +
                                        "where ID_LHP_Collection_TOD=@ID_LHP_Collection_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Collection_TOD", ddledit2coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit3coll.DataSource = cmd.ExecuteReader();
                ddledit3coll.DataTextField = "Finding_Nama";
                ddledit3coll.DataValueField = "ID_LHP_Collection_Finding";
                ddledit3coll.DataBind();

                if (ddledit3coll.Items.Count > 1)
                {
                    ddledit3coll.Enabled = true;
                }

                else
                {
                    ddledit3coll.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit3coll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit4coll.Items.Clear();
            ddledit4coll.Items.Add(new ListItem("--Select Tabel--", ""));
            ddledit4coll.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Collection_Tabel, Tabel_Name from Master_LHP_Collection_Tabel " +
                                        "where ID_LHP_Collection_Finding=@ID_LHP_Collection_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Collection_Finding", ddledit3coll.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit4coll.DataSource = cmd.ExecuteReader();
                ddledit4coll.DataTextField = "Tabel_Name";
                ddledit4coll.DataValueField = "ID_LHP_Collection_Tabel";
                ddledit4coll.DataBind();

                if (ddledit4coll.Items.Count > 1)
                {
                    ddledit4coll.Enabled = true;
                }

                else
                {
                    ddledit4coll.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }



        /// <summary>
        /// AdministrationProcess
        /// </summary>
        public void BindGridAdm()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_Administration where ID_LHP={0}", IDProc_A);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView3.DataSource = dt;
                    GridView3.DataBind();
                    con.Close();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_Administration where ID_LHP={0}", IDProc_B);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView3.DataSource = dt;
                    GridView3.DataBind();
                    con.Close();
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }
        }

        protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //BindGridAdm();
                Getddl1AdmEdit();
                GridViewRow gvrow = GridView3.Rows[index];
                Label3.Text = GridView3.DataKeys[index].Value.ToString();///HttpUtility.HtmlDecode(gvrow.Cells[3].Text).ToString();
                //ddledit1adm.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                //ddledit2adm.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                //ddledit3adm2.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                //ddledit4adm.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                txbeditAdmRFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txbeditAdmRFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                Label2.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#Div2EditAdm').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);

            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView3.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#DivAdmDel').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("detail_click"))
            {
                string code = GridView3.DataKeys[index].Value.ToString();
                string tblCode = string.Empty;
                string modul = string.Empty;
                string message = string.Empty;
                string cbng = string.Empty;
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from LHP_Administration where [No] = @IDProc", con);
                cmd.Parameters.AddWithValue("@IDProc", code);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    txbtabel.Text = Convert.ToString(dr["LHP_Administration_Tabel"]);
                    TxbModul.Text = Convert.ToString(dr["Tipe"]);
                    txbcbng.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                }
                con.Close();
                tblCode = txbtabel.Text;
                modul = TxbModul.Text;
                cbng = txbcbng.Text;
                Session["cbng"] = cbng;
                Session["Code"] = tblCode;
                Session["Modul"] = modul;
                Session["Id3_LHP"] = code;
                Session["ID_SH"].ToString();
                Session["ID_SH_Temp"].ToString();

                if (tblCode == "C2" || tblCode == "C3" || tblCode == "C4" || tblCode == "C16" || tblCode == "C17")
                {
                    Response.Redirect("Tabel7.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "C5" || tblCode == "C7" || tblCode == "C10" || tblCode == "C13" || tblCode == "C14" || tblCode == "C15" || tblCode == "C18" || tblCode == "C19" || tblCode == "C20")
                {
                    Response.Redirect("Tabel8.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "C1" || tblCode == "C11")
                {
                    Response.Redirect("Tabel9.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "C12")
                {
                    Response.Redirect("Tabel11.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "C6" || tblCode == "C8" || tblCode == "C9")
                {
                    Response.Redirect("Tabel16.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Kode Tabel Tidak Ditemukan di LHP Acquisition Process, Cek kembali');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }

            }

        }

        protected void btnAddAdministration_Click(object sender, EventArgs e)////add modal view
        {
            Getddl1Adm();
            txbAddAdmRFB.Text = string.Empty;
            txbAddAdmRFM.Text = string.Empty;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#Div3AddAdm').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddAdm_Click(object sender, EventArgs e)///add save
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;
            string modul = "LHP_Administration";
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            string Headdb = txbUsername.Text;
            string BAVcbng = txbCabang.Text;
            string datestart = txbPeriodeStart.Text;
            string dateend = txbPeriodeEnd.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    //txtidLHP.Text = "12";
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_A);
                    string code = ddladd1adm.SelectedItem.Text;
                    string name = ddladd2adm.SelectedItem.Text;
                    string region = ddladd4adm.SelectedItem.Text;
                    string continent = ddladd3adm.SelectedItem.Text;
                    string population = txbAddAdmRFB.Text;
                    string indyear = txbAddAdmRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddAdm(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridColl();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3AddAdm').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    //txtidLHP.Text = "12";
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_B);
                    string code = ddladd1adm.SelectedItem.Text;
                    string name = ddladd2adm.SelectedItem.Text;
                    string region = ddladd4adm.SelectedItem.Text;
                    string continent = ddladd3adm.SelectedItem.Text;
                    string population = txbAddAdmRFB.Text;
                    string indyear = txbAddAdmRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddAdm(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridAdm();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3AddAdm').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }

        }

        private void executeAddAdm(int idLHP, string code, string name, string continent, string region, string population, string indyear, string mod, string doe,
        string Headdbs, string BAVcbngs, string datestarts, string dateends)///Adding data
        {
            //txtidLHP.Text = "12";
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveLHPAdministration", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idLHP", idLHP);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", doe);

                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@BAVcbngs", BAVcbngs);
                addCmd.Parameters.AddWithValue("@datestarts", DateTime.ParseExact(datestarts.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));
                addCmd.Parameters.AddWithValue("@dateends", DateTime.ParseExact(dateends.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnEditAdmistration_Click(object sender, EventArgs e)
        {
            int No = Convert.ToInt32(Label3.Text);
            string category = ddledit1adm.SelectedItem.Text;
            string tod = ddledit2adm.SelectedItem.Text;
            string cf = ddledit3adm2.SelectedItem.Text;
            string tabel = ddledit4adm.SelectedItem.Text;
            string rfb = txbeditAdmRFB.Text;
            string rfm = txbeditAdmRFM.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdateAdm(No, category, tod, cf, tabel, rfb, rfm, dt);///masuk ke private void update                  
            BindGridAdm();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#Div2EditAdm').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

        }

        private void executeUpdateAdm(int No, string category, string tod, string cf, string tabel, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveLHPAdministrationEdit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@category", category);
                updateCmd.Parameters.AddWithValue("@tod", tod);
                updateCmd.Parameters.AddWithValue("@cf", cf);
                updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnDelAdm_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDeleteAdm(code);
            BindGridAdm();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#DivAdmDel').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        private void executeDeleteAdm(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHP_Administration where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGridAdm();
            GridView3.PageIndex = e.NewPageIndex;
            GridView3.DataBind();
        }

        private void Getddl1Adm()
        {
            ddladd1adm.AppendDataBoundItems = true;
            ddladd2adm.AppendDataBoundItems = true;
            ddladd3adm.AppendDataBoundItems = true;
            ddladd4adm.AppendDataBoundItems = true;

            ddladd1adm.Items.Clear();
            ddladd1adm.Items.Add(new ListItem("--Select category--", ""));
            ddladd2adm.Items.Clear();
            ddladd2adm.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3adm.Items.Clear();
            ddladd3adm.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4adm.Items.Clear();
            ddladd4adm.Items.Add(new ListItem("--Select Tabel--", ""));


            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_Administration_Category],[Category_Name] from Master_LHP_Administration_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd1adm.DataSource = cmd.ExecuteReader();
                ddladd1adm.DataTextField = "Category_Name";
                ddladd1adm.DataValueField = "ID_LHP_Administration_Category";
                ddladd1adm.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddladd1adm_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd2adm.Items.Clear();
            ddladd2adm.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3adm.Items.Clear();
            ddladd3adm.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4adm.Items.Clear();
            ddladd4adm.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Administration_TOD, TOD_Name from Master_LHP_Administration_TOD " +
                               "where ID_LHP_Administration_Category=@ID_LHP_Administration_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Administration_Category", ddladd1adm.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd2adm.DataSource = cmd.ExecuteReader();
                ddladd2adm.DataTextField = "TOD_Name";
                ddladd2adm.DataValueField = "ID_LHP_Administration_TOD";
                ddladd2adm.DataBind();

                if (ddladd2adm.Items.Count > 1)
                {
                    ddladd1adm.Enabled = true;
                    ddladd2adm.Enabled = true;
                }
                else
                {
                    ddladd2adm.Enabled = false;
                    ddladd3adm.Enabled = false;
                    ddladd4adm.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd2adm_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd3adm.Items.Clear();
            ddladd3adm.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4adm.Items.Clear();
            ddladd4adm.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Administration_Finding, Finding_Nama from Master_LHP_Administration_Finding " +
                                        "where ID_LHP_Administration_TOD=@ID_LHP_Administration_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Administration_TOD", ddladd2adm.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd3adm.DataSource = cmd.ExecuteReader();
                ddladd3adm.DataTextField = "Finding_Nama";
                ddladd3adm.DataValueField = "ID_LHP_Administration_Finding";
                ddladd3adm.DataBind();

                if (ddladd3adm.Items.Count > 1)
                {
                    ddladd3adm.Enabled = true;
                    //ddl4.Enabled = true;
                }

                else
                {
                    ddladd3adm.Enabled = false;
                    ddladd4adm.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd3adm_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd4adm.Items.Clear();
            ddladd4adm.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Administration_Tabel, Tabel_Name from Master_LHP_Administration_Tabel " +
                                        "where ID_LHP_Administration_Finding=@ID_LHP_Administration_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Administration_Finding", ddladd3adm.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd4adm.DataSource = cmd.ExecuteReader();
                ddladd4adm.DataTextField = "Tabel_Name";
                ddladd4adm.DataValueField = "ID_LHP_Administration_Tabel";
                ddladd4adm.DataBind();

                if (ddladd4adm.Items.Count > 1)
                {
                    ddladd4adm.Enabled = true;
                }

                else
                {
                    ddladd4adm.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        private void Getddl1AdmEdit()
        {
            ddledit1adm.AppendDataBoundItems = true;
            ddledit2adm.AppendDataBoundItems = true;
            ddledit3adm2.AppendDataBoundItems = true;
            ddledit4adm.AppendDataBoundItems = true;

            ddledit1adm.Items.Clear();
            ddledit1adm.Items.Add(new ListItem("--Select Category--", ""));
            ddledit2adm.Items.Clear();
            ddledit2adm.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3adm2.Items.Clear();
            ddledit3adm2.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4adm.Items.Clear();
            ddledit4adm.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_Administration_Category],[Category_Name] from Master_LHP_Administration_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit1adm.DataSource = cmd.ExecuteReader();
                ddledit1adm.DataTextField = "Category_Name";
                ddledit1adm.DataValueField = "ID_LHP_Administration_Category";
                ddledit1adm.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit1adm_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit2adm.Items.Clear();
            ddledit2adm.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3adm2.Items.Clear();
            ddledit3adm2.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4adm.Items.Clear();
            ddledit4adm.Items.Add(new ListItem("--Select Tabel--", ""));

            ddledit2adm.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Administration_TOD, TOD_Name from Master_LHP_Administration_TOD " +
                               "where ID_LHP_Administration_Category=@ID_LHP_Administration_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Administration_Category", ddledit1adm.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit2adm.DataSource = cmd.ExecuteReader();
                ddledit2adm.DataTextField = "TOD_Name";
                ddledit2adm.DataValueField = "ID_LHP_Administration_TOD";
                ddledit2adm.DataBind();

                if (ddledit1adm.Items.Count > 1)
                {
                    ddledit1adm.Enabled = true;
                }
                else
                {
                    ddledit2adm.Enabled = false;
                    ddledit3adm2.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddledit2adm_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit3adm2.Items.Clear();
            ddledit3adm2.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit3adm2.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Administration_Finding, Finding_Nama from Master_LHP_Administration_Finding " +
                                        "where ID_LHP_Administration_TOD=@ID_LHP_Administration_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Administration_TOD", ddledit2adm.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit3adm2.DataSource = cmd.ExecuteReader();
                ddledit3adm2.DataTextField = "Finding_Nama";
                ddledit3adm2.DataValueField = "ID_LHP_Administration_Finding";
                ddledit3adm2.DataBind();

                if (ddledit3adm2.Items.Count > 1)
                {
                    ddledit3adm2.Enabled = true;
                }

                else
                {
                    ddledit3adm2.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit3adm2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit4adm.Items.Clear();
            ddledit4adm.Items.Add(new ListItem("--Select Tabel--", ""));
            ddledit4adm.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Administration_Tabel, Tabel_Name from Master_LHP_Administration_Tabel " +
                                        "where ID_LHP_Administration_Finding=@ID_LHP_Administration_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Administration_Finding", ddledit3adm2.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit4adm.DataSource = cmd.ExecuteReader();
                ddledit4adm.DataTextField = "Tabel_Name";
                ddledit4adm.DataValueField = "ID_LHP_Administration_Tabel";
                ddledit4adm.DataBind();

                if (ddledit4adm.Items.Count > 1)
                {
                    ddledit4adm.Enabled = true;
                }

                else
                {
                    ddledit4adm.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }



        /// <summary>
        /// CPUProcess
        /// </summary>
        public void BindGridCPU()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_CPU where ID_LHP={0}", IDProc_A);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView4.DataSource = dt;
                    GridView4.DataBind();
                    con.Close();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_CPU where ID_LHP={0}", IDProc_B);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView4.DataSource = dt;
                    GridView4.DataBind();
                    con.Close();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }
        }

        protected void GridView4_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                //BindGridCPU();
                Getddl1CPUEdit();
                GridViewRow gvrow = GridView4.Rows[index];
                Label5.Text = GridView4.DataKeys[index].Value.ToString();///HttpUtility.HtmlDecode(gvrow.Cells[3].Text).ToString();
                //ddledit1adm.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[5].Text);
                //ddledit2adm.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[6].Text);
                //ddledit3adm2.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[7].Text);
                //ddledit4adm.SelectedItem.Text = HttpUtility.HtmlDecode(gvrow.Cells[8].Text);
                txbeditCPURFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txbeditCPURFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                Label2.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#Div2EditCPU').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView4.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#DivCPUDel').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("detail_click"))
            {
                string code = GridView4.DataKeys[index].Value.ToString();
                string tblCode = string.Empty;
                string modul = string.Empty;
                string message = string.Empty;
                string cbng = string.Empty;
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from LHP_CPU where [No] = @IDProc", con);
                cmd.Parameters.AddWithValue("@IDProc", code);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    txbtabel.Text = Convert.ToString(dr["LHP_CPU_Tabel"]);
                    TxbModul.Text = Convert.ToString(dr["Tipe"]);
                    txbcbng.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                }
                con.Close();
                tblCode = txbtabel.Text;
                modul = TxbModul.Text;
                cbng = txbcbng.Text;
                Session["cbng"] = cbng;
                Session["Code"] = tblCode;
                Session["Modul"] = modul;
                Session["Id4_LHP"] = code;
                Session["ID_SH"].ToString();
                Session["ID_SH_Temp"].ToString();

                if (tblCode == "D1" || tblCode == "D8" || tblCode == "D9")
                {
                    Response.Redirect("Tabel1_2.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "D5" || tblCode == "D6")
                {
                    Response.Redirect("Tabel3_2.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "D7")
                {
                    Response.Redirect("Tabel7.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "D3" || tblCode == "D10")
                {
                    Response.Redirect("Tabel8.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "D4")
                {
                    Response.Redirect("Tabel12.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "D2")
                {
                    Response.Redirect("Tabel16.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Kode Tabel Tidak Ditemukan di LHP CPU Process, Cek kembali');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
            }

        }

        protected void GridView4_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGridCPU();
            GridView4.PageIndex = e.NewPageIndex;
            GridView4.DataBind();
        }

        protected void btnAddCPU_Click(object sender, EventArgs e)
        {
            Getddl1CPUAdd();
            txbAddCpuRFB.Text = string.Empty;
            txbAddCpuRFM.Text = string.Empty;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#Div3AddCPU').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);

        }///add New row

        protected void btnAddCpu2_Click(object sender, EventArgs e)
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;
            string modul = "LHP_CPU";
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            string Headdb = txbUsername.Text;
            string BAVcbng = txbCabang.Text;
            string datestart = txbPeriodeStart.Text;
            string dateend = txbPeriodeEnd.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_A);
                    string code = ddladd1CPU.SelectedItem.Text;
                    string name = ddladd2CPU.SelectedItem.Text;
                    string continent = ddladd3CPU.SelectedItem.Text;///Case Finding
                    string region = ddladd4CPU.SelectedItem.Text;///Tabel
                    string population = txbAddCpuRFB.Text;
                    string indyear = txbAddCpuRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddCPU(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridCPU();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3AddCPU').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_B);
                    string code = ddladd1CPU.SelectedItem.Text;
                    string name = ddladd2CPU.SelectedItem.Text;
                    string continent = ddladd3CPU.SelectedItem.Text;///Case Finding
                    string region = ddladd4CPU.SelectedItem.Text;///Tabel
                    string population = txbAddCpuRFB.Text;
                    string indyear = txbAddCpuRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddCPU(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridCPU();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3AddCPU').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

        }//Add Save Row

        private void executeAddCPU(int idLHP, string code, string name, string continent, string region, string population, string indyear, string mod, string doe,
        string Headdbs, string BAVcbngs, string datestarts, string dateends)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveLHPCPU", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idLHP", idLHP);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", doe);

                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@BAVcbngs", BAVcbngs);
                addCmd.Parameters.AddWithValue("@datestarts", DateTime.ParseExact(datestarts.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));
                addCmd.Parameters.AddWithValue("@dateends", DateTime.ParseExact(dateends.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnEditCPU_Click(object sender, EventArgs e)///Edit Row
        {
            int No = Convert.ToInt32(Label5.Text);
            string category = ddledit1CPU.SelectedItem.Text;
            string tod = ddledit2CPU.SelectedItem.Text;
            string cf = ddledit3CPU.SelectedItem.Text;
            string tabel = ddledit4CPU.SelectedItem.Text;
            string rfb = txbeditCPURFB.Text;
            string rfm = txbeditCPURFM.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdateCPU(No, category, tod, cf, tabel, rfb, rfm, dt);///masuk ke private void update                  
            BindGridCPU();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#Div2EditCPU').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);

        }

        private void executeUpdateCPU(int No, string category, string tod, string cf, string tabel, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveLHPCPUEdit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@category", category);
                updateCmd.Parameters.AddWithValue("@tod", tod);
                updateCmd.Parameters.AddWithValue("@cf", cf);
                updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnDelCPU_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDeleteCPU(code);
            BindGridCPU();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#DivCPUDel').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);

        }

        private void executeDeleteCPU(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHP_CPU where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        private void Getddl1CPUEdit()
        {
            ddledit1CPU.AppendDataBoundItems = true;
            ddledit2CPU.AppendDataBoundItems = true;
            ddledit3CPU.AppendDataBoundItems = true;
            ddledit4CPU.AppendDataBoundItems = true;

            ddledit1CPU.Items.Clear();
            ddledit1CPU.Items.Add(new ListItem("--Select Category--", ""));
            ddledit2CPU.Items.Clear();
            ddledit2CPU.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3CPU.Items.Clear();
            ddledit3CPU.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4CPU.Items.Clear();
            ddledit4CPU.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_CPU_Category],[Category_Name] from Master_LHP_CPU_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit1CPU.DataSource = cmd.ExecuteReader();
                ddledit1CPU.DataTextField = "Category_Name";
                ddledit1CPU.DataValueField = "ID_LHP_CPU_Category";
                ddledit1CPU.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddledit1CPU_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit2CPU.Items.Clear();
            ddledit2CPU.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3CPU.Items.Clear();
            ddledit3CPU.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4CPU.Items.Clear();
            ddledit4CPU.Items.Add(new ListItem("--Select Tabel--", ""));

            ddledit2CPU.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_CPU_TOD, TOD_Name from Master_LHP_CPU_TOD " +
                               "where ID_LHP_CPU_Category=@ID_LHP_CPU_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_CPU_Category", ddledit1CPU.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit2CPU.DataSource = cmd.ExecuteReader();
                ddledit2CPU.DataTextField = "TOD_Name";
                ddledit2CPU.DataValueField = "ID_LHP_CPU_TOD";
                ddledit2CPU.DataBind();

                if (ddledit1CPU.Items.Count > 1)
                {
                    ddledit1CPU.Enabled = true;
                }
                else
                {
                    ddledit2CPU.Enabled = false;
                    ddledit3CPU.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit2CPU_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit3CPU.Items.Clear();
            ddledit3CPU.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit3CPU.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_CPU_Finding, Finding_Nama from Master_LHP_CPU_Finding " +
                                        "where ID_LHP_CPU_TOD=@ID_LHP_CPU_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_CPU_TOD", ddledit2CPU.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit3CPU.DataSource = cmd.ExecuteReader();
                ddledit3CPU.DataTextField = "Finding_Nama";
                ddledit3CPU.DataValueField = "ID_LHP_CPU_Finding";
                ddledit3CPU.DataBind();

                if (ddledit3CPU.Items.Count > 1)
                {
                    ddledit3CPU.Enabled = true;
                }

                else
                {
                    ddledit3CPU.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit3CPU_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit4CPU.Items.Clear();
            ddledit4CPU.Items.Add(new ListItem("--Select Tabel--", ""));
            ddledit4CPU.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_CPU_Tabel, Tabel_Name from Master_LHP_CPU_Tabel " +
                                        "where ID_LHP_CPU_Finding=@ID_LHP_CPU_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_CPU_Finding", ddledit3CPU.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit4CPU.DataSource = cmd.ExecuteReader();
                ddledit4CPU.DataTextField = "Tabel_Name";
                ddledit4CPU.DataValueField = "ID_LHP_CPU_Tabel";
                ddledit4CPU.DataBind();

                if (ddledit4CPU.Items.Count > 1)
                {
                    ddledit4CPU.Enabled = true;
                }

                else
                {
                    ddledit4CPU.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        private void Getddl1CPUAdd()
        {
            ddladd1CPU.AppendDataBoundItems = true;
            ddladd2CPU.AppendDataBoundItems = true;
            ddladd3CPU.AppendDataBoundItems = true;
            ddladd4CPU.AppendDataBoundItems = true;

            ddladd1CPU.Items.Clear();
            ddladd1CPU.Items.Add(new ListItem("--Select Category--", ""));
            ddladd2CPU.Items.Clear();
            ddladd2CPU.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3CPU.Items.Clear();
            ddladd3CPU.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4CPU.Items.Clear();
            ddladd4CPU.Items.Add(new ListItem("--Select Tabel--", ""));


            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_CPU_Category],[Category_Name] from Master_LHP_CPU_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd1CPU.DataSource = cmd.ExecuteReader();
                ddladd1CPU.DataTextField = "Category_Name";
                ddladd1CPU.DataValueField = "ID_LHP_CPU_Category";
                ddladd1CPU.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddladd1CPU_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd2CPU.Items.Clear();
            ddladd2CPU.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3CPU.Items.Clear();
            ddladd3CPU.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4CPU.Items.Clear();
            ddladd4CPU.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_CPU_TOD, TOD_Name from Master_LHP_CPU_TOD " +
                               "where ID_LHP_CPU_Category=@ID_LHP_CPU_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_CPU_Category", ddladd1CPU.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd2CPU.DataSource = cmd.ExecuteReader();
                ddladd2CPU.DataTextField = "TOD_Name";
                ddladd2CPU.DataValueField = "ID_LHP_CPU_TOD";
                ddladd2CPU.DataBind();

                if (ddladd2CPU.Items.Count > 1)
                {
                    ddladd1CPU.Enabled = true;
                    ddladd2CPU.Enabled = true;
                }
                else
                {
                    ddladd2CPU.Enabled = false;
                    ddladd3CPU.Enabled = false;
                    ddladd4CPU.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd2CPU_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd3CPU.Items.Clear();
            ddladd3CPU.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4CPU.Items.Clear();
            ddladd4CPU.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_CPU_Finding, Finding_Nama from Master_LHP_CPU_Finding " +
                                        "where ID_LHP_CPU_TOD=@ID_LHP_CPU_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_CPU_TOD", ddladd2CPU.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd3CPU.DataSource = cmd.ExecuteReader();
                ddladd3CPU.DataTextField = "Finding_Nama";
                ddladd3CPU.DataValueField = "ID_LHP_CPU_Finding";
                ddladd3CPU.DataBind();

                if (ddladd3CPU.Items.Count > 1)
                {
                    ddladd3CPU.Enabled = true;
                    //ddl4.Enabled = true;
                }

                else
                {
                    ddladd3CPU.Enabled = false;
                    ddladd4CPU.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddladd3CPU_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd4CPU.Items.Clear();
            ddladd4CPU.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_CPU_Tabel, Tabel_Name from Master_LHP_CPU_Tabel " +
                                        "where ID_LHP_CPU_Finding=@ID_LHP_CPU_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_CPU_Finding", ddladd3CPU.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd4CPU.DataSource = cmd.ExecuteReader();
                ddladd4CPU.DataTextField = "Tabel_Name";
                ddladd4CPU.DataValueField = "ID_LHP_CPU_Tabel";
                ddladd4CPU.DataBind();

                if (ddladd4CPU.Items.Count > 1)
                {
                    ddladd4CPU.Enabled = true;
                }

                else
                {
                    ddladd4CPU.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }



        /// <summary>
        /// HR Process
        /// </summary>
        public void BindGridHR()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_HR where ID_LHP={0}", IDProc_A);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView5.DataSource = dt;
                    GridView5.DataBind();
                    con.Close();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }

            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_HR where ID_LHP={0}", IDProc_B);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView5.DataSource = dt;
                    GridView5.DataBind();
                    con.Close();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }
        }

        protected void GridView5_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGridHR();
            GridView5.PageIndex = e.NewPageIndex;
            GridView5.DataBind();
        }

        protected void GridView5_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                Getddl1HREdit();
                GridViewRow gvrow = GridView5.Rows[index];
                Label7.Text = GridView5.DataKeys[index].Value.ToString();
                txbeditHRRFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txbeditHRRFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                Label2.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#Div2EditHR').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView5.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#DivHRDel').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("detail_click"))
            {
                string code = GridView5.DataKeys[index].Value.ToString();
                string tblCode = string.Empty;
                string modul = string.Empty;
                string message = string.Empty;
                string cbng = string.Empty;
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from LHP_HR where [No] = @IDProc", con);
                cmd.Parameters.AddWithValue("@IDProc", code);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    txbtabel.Text = Convert.ToString(dr["LHP_HR_Tabel"]);
                    TxbModul.Text = Convert.ToString(dr["Tipe"]);
                    txbcbng.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                }
                con.Close();
                tblCode = txbtabel.Text;
                modul = TxbModul.Text;
                cbng = txbcbng.Text;
                Session["cbng"] = cbng;
                Session["Code"] = tblCode;
                Session["Modul"] = modul;
                Session["Id5_LHP"] = code;
                Session["ID_SH"].ToString();
                Session["ID_SH_Temp"].ToString();

                if (tblCode == "F1")
                {
                    Response.Redirect("Tabel14.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else if (tblCode == "F2")
                {
                    Response.Redirect("Tabel15.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Kode Tabel Tidak Ditemukan di LHP HR Process, Cek kembali');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
            }

        }

        protected void btnAddHR_Click(object sender, EventArgs e)//////add New row
        {
            Getddl1HRAdd();
            txbAddHRRFB.Text = string.Empty;
            txbAddHRRFM.Text = string.Empty;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#Div3AddHR').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddHR2_Click(object sender, EventArgs e)
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;
            string modul = "LHP_HR";
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            string Headdb = txbUsername.Text;
            string BAVcbng = txbCabang.Text;
            string datestart = txbPeriodeStart.Text;
            string dateend = txbPeriodeEnd.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_A);
                    string code = ddladd1HR.SelectedItem.Text;
                    string name = ddladd2HR.SelectedItem.Text;
                    string continent = ddladd3HR.SelectedItem.Text;///Case Finding
                    string region = ddladd4HR.SelectedItem.Text;///Tabel
                    string population = txbAddHRRFB.Text;
                    string indyear = txbAddHRRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddHR(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridHR();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3AddHR').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_B);
                    string code = ddladd1HR.SelectedItem.Text;
                    string name = ddladd2HR.SelectedItem.Text;
                    string continent = ddladd3HR.SelectedItem.Text;///Case Finding
                    string region = ddladd4HR.SelectedItem.Text;///Tabel
                    string population = txbAddHRRFB.Text;
                    string indyear = txbAddHRRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddHR(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridHR();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3AddHR').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

        }

        private void executeAddHR(int idLHP, string code, string name, string continent, string region, string population, string indyear, string mod, string doe,
            string Headdbs, string BAVcbngs, string datestarts, string dateends)///Adding data
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveLHPHR", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idLHP", idLHP);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", doe);

                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@BAVcbngs", BAVcbngs);
                addCmd.Parameters.AddWithValue("@datestarts", DateTime.ParseExact(datestarts.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));
                addCmd.Parameters.AddWithValue("@dateends", DateTime.ParseExact(dateends.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }
        }

        protected void btnEditHR_Click(object sender, EventArgs e)
        {
            int No = Convert.ToInt32(Label7.Text);
            string category = ddledit1HR.SelectedItem.Text;
            string tod = ddledit2HR.SelectedItem.Text;
            string cf = ddledit3HR.SelectedItem.Text;
            string tabel = ddledit4HR.SelectedItem.Text;
            string rfb = txbeditHRRFB.Text;
            string rfm = txbeditHRRFM.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdateHR(No, category, tod, cf, tabel, rfb, rfm, dt);///masuk ke private void update                  
            BindGridHR();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#Div2EditHR').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdateHR(int No, string category, string tod, string cf, string tabel, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveLHPHREdit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@category", category);
                updateCmd.Parameters.AddWithValue("@tod", tod);
                updateCmd.Parameters.AddWithValue("@cf", cf);
                updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        protected void btnDelHR_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDeleteHR(code);
            BindGridHR();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#DivHRDel').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        private void executeDeleteHR(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHP_HR where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        private void Getddl1HREdit()
        {
            ddledit1HR.AppendDataBoundItems = true;
            ddledit2HR.AppendDataBoundItems = true;
            ddledit3HR.AppendDataBoundItems = true;
            ddledit4HR.AppendDataBoundItems = true;

            ddledit1HR.Items.Clear();
            ddledit1HR.Items.Add(new ListItem("--Select Category--", ""));
            ddledit2HR.Items.Clear();
            ddledit2HR.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3HR.Items.Clear();
            ddledit3HR.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4HR.Items.Clear();
            ddledit4HR.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_HR_Category],[Category_Name] from Master_LHP_HR_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit1HR.DataSource = cmd.ExecuteReader();
                ddledit1HR.DataTextField = "Category_Name";
                ddledit1HR.DataValueField = "ID_LHP_HR_Category";
                ddledit1HR.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddledit1HR_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit2HR.Items.Clear();
            ddledit2HR.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3HR.Items.Clear();
            ddledit3HR.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4HR.Items.Clear();
            ddledit4HR.Items.Add(new ListItem("--Select Tabel--", ""));

            ddledit2HR.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_HR_TOD, TOD_Name from Master_LHP_HR_TOD " +
                               "where ID_LHP_HR_Category=@ID_LHP_HR_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_HR_Category", ddledit1HR.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit2HR.DataSource = cmd.ExecuteReader();
                ddledit2HR.DataTextField = "TOD_Name";
                ddledit2HR.DataValueField = "ID_LHP_HR_TOD";
                ddledit2HR.DataBind();

                if (ddledit1HR.Items.Count > 1)
                {
                    ddledit1HR.Enabled = true;
                }
                else
                {
                    ddledit2HR.Enabled = false;
                    ddledit3HR.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit2HR_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit3HR.Items.Clear();
            ddledit3HR.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit3HR.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_HR_Finding, Finding_Nama from Master_LHP_HR_Finding " +
                                        "where ID_LHP_HR_TOD=@ID_LHP_HR_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_HR_TOD", ddledit2HR.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit3HR.DataSource = cmd.ExecuteReader();
                ddledit3HR.DataTextField = "Finding_Nama";
                ddledit3HR.DataValueField = "ID_LHP_HR_Finding";
                ddledit3HR.DataBind();

                if (ddledit3HR.Items.Count > 1)
                {
                    ddledit3HR.Enabled = true;
                }

                else
                {
                    ddledit3HR.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddledit3HR_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit4HR.Items.Clear();
            ddledit4HR.Items.Add(new ListItem("--Select Tabel--", ""));
            ddledit4HR.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_HR_Tabel, Tabel_Name from Master_LHP_HR_Tabel " +
                                        "where ID_LHP_HR_Finding=@ID_LHP_HR_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_HR_Finding", ddledit3HR.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit4HR.DataSource = cmd.ExecuteReader();
                ddledit4HR.DataTextField = "Tabel_Name";
                ddledit4HR.DataValueField = "ID_LHP_HR_Tabel";
                ddledit4HR.DataBind();

                if (ddledit4HR.Items.Count > 1)
                {
                    ddledit4HR.Enabled = true;
                }

                else
                {
                    ddledit4HR.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        private void Getddl1HRAdd()
        {
            ddladd1HR.AppendDataBoundItems = true;
            ddladd2HR.AppendDataBoundItems = true;
            ddladd3HR.AppendDataBoundItems = true;
            ddladd4HR.AppendDataBoundItems = true;

            ddladd1HR.Items.Clear();
            ddladd1HR.Items.Add(new ListItem("--Select Category--", ""));
            ddladd2HR.Items.Clear();
            ddladd2HR.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3HR.Items.Clear();
            ddladd3HR.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4HR.Items.Clear();
            ddladd4HR.Items.Add(new ListItem("--Select Tabel--", ""));


            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_HR_Category],[Category_Name] from Master_LHP_HR_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd1HR.DataSource = cmd.ExecuteReader();
                ddladd1HR.DataTextField = "Category_Name";
                ddladd1HR.DataValueField = "ID_LHP_HR_Category";
                ddladd1HR.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddladd1HR_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd2HR.Items.Clear();
            ddladd2HR.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3HR.Items.Clear();
            ddladd3HR.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4HR.Items.Clear();
            ddladd4HR.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_HR_TOD, TOD_Name from Master_LHP_HR_TOD " +
                               "where ID_LHP_HR_Category=@ID_LHP_HR_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_HR_Category", ddladd1HR.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd2HR.DataSource = cmd.ExecuteReader();
                ddladd2HR.DataTextField = "TOD_Name";
                ddladd2HR.DataValueField = "ID_LHP_HR_TOD";
                ddladd2HR.DataBind();

                if (ddladd2HR.Items.Count > 1)
                {
                    ddladd1HR.Enabled = true;
                    ddladd2HR.Enabled = true;
                }
                else
                {
                    ddladd2HR.Enabled = false;
                    ddladd3HR.Enabled = false;
                    ddladd4HR.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd2HR_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd3HR.Items.Clear();
            ddladd3HR.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4HR.Items.Clear();
            ddladd4HR.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_HR_Finding, Finding_Nama from Master_LHP_HR_Finding " +
                                        "where ID_LHP_HR_TOD=@ID_LHP_HR_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_HR_TOD", ddladd2HR.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd3HR.DataSource = cmd.ExecuteReader();
                ddladd3HR.DataTextField = "Finding_Nama";
                ddladd3HR.DataValueField = "ID_LHP_HR_Finding";
                ddladd3HR.DataBind();

                if (ddladd3HR.Items.Count > 1)
                {
                    ddladd3HR.Enabled = true;
                }

                else
                {
                    ddladd3HR.Enabled = false;
                    ddladd4HR.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd3HR_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd4HR.Items.Clear();
            ddladd4HR.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_HR_Tabel, Tabel_Name from Master_LHP_HR_Tabel " +
                                        "where ID_LHP_HR_Finding=@ID_LHP_HR_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_HR_Finding", ddladd3HR.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd4HR.DataSource = cmd.ExecuteReader();
                ddladd4HR.DataTextField = "Tabel_Name";
                ddladd4HR.DataValueField = "ID_LHP_HR_Tabel";
                ddladd4HR.DataBind();

                if (ddladd4HR.Items.Count > 1)
                {
                    ddladd4HR.Enabled = true;
                }

                else
                {
                    ddladd4HR.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }



        /// <summary>
        /// Procurement Process
        /// </summary>
        public void BindGridProc()
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_Procurement where ID_LHP={0}", IDProc_A);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView6.DataSource = dt;
                    GridView6.DataBind();
                    con.Close();
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string cnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                    string query = string.Format(@"SELECT  * FROM LHP_Procurement where ID_LHP={0}", IDProc_B);
                    SqlConnection con = new SqlConnection(cnString);
                    con.Open();
                    SqlDataAdapter dAdapter = new SqlDataAdapter(query, con);
                    DataSet ds = new DataSet();
                    dAdapter.Fill(ds);
                    dt = ds.Tables[0];
                    GridView6.DataSource = dt;
                    GridView6.DataBind();
                    con.Close();

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);

                }
            }

        }

        protected void GridView6_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BindGridProc();
            GridView6.PageIndex = e.NewPageIndex;
            GridView6.DataBind();
        }

        protected void GridView6_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName.Equals("detail"))
            {

            }
            else if (e.CommandName.Equals("editRecord"))///buat edit
            {
                Getddl1ProcEdit();
                GridViewRow gvrow = GridView6.Rows[index];
                Label7.Text = GridView6.DataKeys[index].Value.ToString();
                txbeditProcRFB.Text = HttpUtility.HtmlDecode(gvrow.Cells[9].Text);
                txbeditProcRFM.Text = HttpUtility.HtmlDecode(gvrow.Cells[10].Text);
                Label2.Visible = false;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#Div2EditProc').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("deleteRecord"))
            {
                string code = GridView6.DataKeys[index].Value.ToString();
                hfCode.Value = code;
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(@"<script type='text/javascript'>");
                sb.Append("$('#DivProcDel').modal('show');");
                sb.Append(@"</script>");
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "DeleteModalScript", sb.ToString(), false);
            }
            else if (e.CommandName.Equals("detail_click"))
            {
                string code = GridView6.DataKeys[index].Value.ToString();
                string tblCode = string.Empty;
                string modul = string.Empty;
                string message = string.Empty;
                string cbng = string.Empty;
                String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                SqlConnection con = new SqlConnection(strConnString);
                con.Open();
                SqlCommand cmd = new SqlCommand("select * from LHP_Procurement where [No] = @IDProc", con);
                cmd.Parameters.AddWithValue("@IDProc", code);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    txbtabel.Text = Convert.ToString(dr["LHP_Procurement_Tabel"]);
                    TxbModul.Text = Convert.ToString(dr["Tipe"]);
                    txbcbng.Text = Convert.ToString(dr["Bina_Artha_Cabang"]);
                }
                con.Close();
                tblCode = txbtabel.Text;
                modul = TxbModul.Text;
                cbng = txbcbng.Text;
                Session["cbng"] = cbng;
                Session["Code"] = tblCode;
                Session["Modul"] = modul;
                Session["Id6_LHP"] = code;
                Session["ID_SH"].ToString();
                Session["ID_SH_Temp"].ToString();

                if (tblCode == "E1")
                {
                    Response.Redirect("Tabel13.aspx?" + dl.Encript(modul) + dl.Encript(tblCode));
                }
                else
                {
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Kode Tabel Tidak Ditemukan di LHP Procurement Process, Cek kembali');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
            }

        }

        protected void btnAddProc_Click(object sender, EventArgs e)
        {
            Getddl1ProcAdd();
            txbAddProcRFB.Text = string.Empty;
            txbAddProcRFM.Text = string.Empty;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("$('#Div3AddProc').modal('show');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddShowModalScript", sb.ToString(), false);
        }

        protected void btnAddProc2_Click(object sender, EventArgs e)
        {
            string IDProc_B = txtIDSaveTemp.Text;
            string IDProc_A = txtIDSave.Text;
            string modul = "LHP_Procurement";
            string doe = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");

            string Headdb = txbUsername.Text;
            string BAVcbng = txbCabang.Text;
            string datestart = txbPeriodeStart.Text;
            string dateend = txbPeriodeEnd.Text;

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                try
                {
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_A);
                    string code = ddladd1Proc.SelectedItem.Text;
                    string name = ddladd2Proc.SelectedItem.Text;
                    string continent = ddladd3Proc.SelectedItem.Text;///Case Finding
                    string region = ddladd4Proc.SelectedItem.Text;///Tabel
                    string population = txbAddProcRFB.Text;
                    string indyear = txbAddProcRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddProc(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridProc();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3AddProc').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);

                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                try
                {
                    string mod = modul.ToString();
                    int idLHP = Convert.ToInt32(IDProc_B);
                    string code = ddladd1Proc.SelectedItem.Text;
                    string name = ddladd2Proc.SelectedItem.Text;
                    string continent = ddladd3Proc.SelectedItem.Text;///Case Finding
                    string region = ddladd4Proc.SelectedItem.Text;///Tabel
                    string population = txbAddProcRFB.Text;
                    string indyear = txbAddProcRFM.Text;
                    string dt = doe.ToString();

                    string Headdbs = Headdb.ToString();
                    string BAVcbngs = BAVcbng.ToString();
                    string datestarts = datestart.ToString();
                    string dateends = dateend.ToString();

                    executeAddProc(idLHP, code, name, continent, region, population, indyear, mod, doe, Headdbs, BAVcbngs, datestarts, dateends);
                    BindGridProc();
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("alert('Record Added Successfully');");
                    sb.Append("$('#Div3AddProc').modal('hide');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AddHideModalScript", sb.ToString(), false);
                }
                catch (SqlException ex)
                {
                    System.Console.Error.Write(ex.Message);
                }
            }

        }

        private void executeAddProc(int idLHP, string code, string name, string continent, string region, string population, string indyear, string mod, string doe,
            string Headdbs, string BAVcbngs, string datestarts, string dateends)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand addCmd = new SqlCommand("SP_SaveLHPProc", conn);
                addCmd.CommandType = CommandType.StoredProcedure;
                addCmd.Parameters.AddWithValue("@idLHP", idLHP);
                addCmd.Parameters.AddWithValue("@mod", mod);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.Parameters.AddWithValue("@name", name);
                addCmd.Parameters.AddWithValue("@continent", continent);
                addCmd.Parameters.AddWithValue("@region", region);
                addCmd.Parameters.AddWithValue("@population", population);
                addCmd.Parameters.AddWithValue("@indyear", indyear);
                addCmd.Parameters.AddWithValue("@doe", doe);

                addCmd.Parameters.AddWithValue("@Headdbs", Headdbs);
                addCmd.Parameters.AddWithValue("@BAVcbngs", BAVcbngs);
                addCmd.Parameters.AddWithValue("@datestarts", DateTime.ParseExact(datestarts.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));
                addCmd.Parameters.AddWithValue("@dateends", DateTime.ParseExact(dateends.Trim(), "dd-MM-yyyy", CultureInfo.InvariantCulture));

                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

        protected void btnEditProc_Click(object sender, EventArgs e)
        {
            int No = Convert.ToInt32(Label7.Text);
            string category = ddledit1Proc.SelectedItem.Text;
            string tod = ddledit2Proc.SelectedItem.Text;
            string cf = ddledit3Proc.SelectedItem.Text;
            string tabel = ddledit4Proc.SelectedItem.Text;
            string rfb = txbeditProcRFB.Text;
            string rfm = txbeditProcRFM.Text;
            string dt = DateTime.Now.ToString("yyyyMMdd HH:mm:ss");
            executeUpdateProc(No, category, tod, cf, tabel, rfb, rfm, dt);///masuk ke private void update                  
            BindGridProc();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Records Updated Successfully');");
            sb.Append("$('#Div2EditProc').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditHideModalScript", sb.ToString(), false);
        }

        private void executeUpdateProc(int No, string category, string tod, string cf, string tabel, string rfb, string rfm, string dt)///buat edit(klik button update di form edit)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand updateCmd = new SqlCommand("SP_SaveLHPProcEdit", conn);
                updateCmd.CommandType = CommandType.StoredProcedure;
                updateCmd.Parameters.AddWithValue("@category", category);
                updateCmd.Parameters.AddWithValue("@tod", tod);
                updateCmd.Parameters.AddWithValue("@cf", cf);
                updateCmd.Parameters.AddWithValue("@tabel", tabel);
                updateCmd.Parameters.AddWithValue("@rfb", rfb);
                updateCmd.Parameters.AddWithValue("@rfm", rfm);
                updateCmd.Parameters.AddWithValue("@dt", dt);
                updateCmd.Parameters.AddWithValue("@No", No);
                updateCmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException me)
            {
                System.Console.Error.Write(me.InnerException.Data);
            }
        }

        private void Getddl1ProcEdit()
        {
            ddledit1Proc.AppendDataBoundItems = true;
            ddledit2Proc.AppendDataBoundItems = true;
            ddledit3Proc.AppendDataBoundItems = true;
            ddledit4Proc.AppendDataBoundItems = true;

            ddledit1Proc.Items.Clear();
            ddledit1Proc.Items.Add(new ListItem("--Select Category--", ""));
            ddledit2Proc.Items.Clear();
            ddledit2Proc.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3Proc.Items.Clear();
            ddledit3Proc.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4Proc.Items.Clear();
            ddledit4Proc.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_Procurement_Category],[Category_Name] from Master_LHP_Procurement_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit1Proc.DataSource = cmd.ExecuteReader();
                ddledit1Proc.DataTextField = "Category_Name";
                ddledit1Proc.DataValueField = "ID_LHP_Procurement_Category";
                ddledit1Proc.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddledit1Proc_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit2Proc.Items.Clear();
            ddledit2Proc.Items.Add(new ListItem("--Select Type--", ""));
            ddledit3Proc.Items.Clear();
            ddledit3Proc.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit4Proc.Items.Clear();
            ddledit4Proc.Items.Add(new ListItem("--Select Tabel--", ""));

            ddledit2Proc.AppendDataBoundItems = true;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Procurement_TOD, TOD_Name from Master_LHP_Procurement_TOD " +
                               "where ID_LHP_Procurement_Category=@ID_LHP_Procurement_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Procurement_Category", ddledit1Proc.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit2Proc.DataSource = cmd.ExecuteReader();
                ddledit2Proc.DataTextField = "TOD_Name";
                ddledit2Proc.DataValueField = "ID_LHP_Procurement_TOD";
                ddledit2Proc.DataBind();

                if (ddledit1Proc.Items.Count > 1)
                {
                    ddledit1Proc.Enabled = true;
                }
                else
                {
                    ddledit2Proc.Enabled = false;
                    ddledit3Proc.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit2Proc_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit3Proc.Items.Clear();
            ddledit3Proc.Items.Add(new ListItem("--Select Finding--", ""));
            ddledit3Proc.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Procurement_Finding, Finding_Nama from Master_LHP_Procurement_Finding " +
                                        "where ID_LHP_Procurement_TOD=@ID_LHP_Procurement_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Procurement_TOD", ddledit2Proc.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddledit3Proc.DataSource = cmd.ExecuteReader();
                ddledit3Proc.DataTextField = "Finding_Nama";
                ddledit3Proc.DataValueField = "ID_LHP_Procurement_Finding";
                ddledit3Proc.DataBind();

                if (ddledit3Proc.Items.Count > 1)
                {
                    ddledit3Proc.Enabled = true;
                }

                else
                {
                    ddledit3Proc.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddledit3Proc_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddledit4Proc.Items.Clear();
            ddledit4Proc.Items.Add(new ListItem("--Select Tabel--", ""));
            ddledit4Proc.AppendDataBoundItems = true;

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Procurement_Tabel, Tabel_Name from Master_LHP_Procurement_Tabel " +
                                        "where ID_LHP_Procurement_Finding=@ID_LHP_Procurement_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Procurement_Finding", ddledit3Proc.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddledit4Proc.DataSource = cmd.ExecuteReader();
                ddledit4Proc.DataTextField = "Tabel_Name";
                ddledit4Proc.DataValueField = "ID_LHP_Procurement_Tabel";
                ddledit4Proc.DataBind();

                if (ddledit4Proc.Items.Count > 1)
                {
                    ddledit4Proc.Enabled = true;
                }

                else
                {
                    ddledit4Proc.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        private void Getddl1ProcAdd()
        {
            ddladd1Proc.AppendDataBoundItems = true;
            ddladd2Proc.AppendDataBoundItems = true;
            ddladd3Proc.AppendDataBoundItems = true;
            ddladd4Proc.AppendDataBoundItems = true;

            ddladd1Proc.Items.Clear();
            ddladd1Proc.Items.Add(new ListItem("--Select Category--", ""));
            ddladd2Proc.Items.Clear();
            ddladd2Proc.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3Proc.Items.Clear();
            ddladd3Proc.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4Proc.Items.Clear();
            ddladd4Proc.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select [ID_LHP_Procurement_Category],[Category_Name] from Master_LHP_Procurement_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd1Proc.DataSource = cmd.ExecuteReader();
                ddladd1Proc.DataTextField = "Category_Name";
                ddladd1Proc.DataValueField = "ID_LHP_Procurement_Category";
                ddladd1Proc.DataBind();
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }
        }

        protected void ddladd1Proc_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd2Proc.Items.Clear();
            ddladd2Proc.Items.Add(new ListItem("--Select Type--", ""));
            ddladd3Proc.Items.Clear();
            ddladd3Proc.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4Proc.Items.Clear();
            ddladd4Proc.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Procurement_TOD, TOD_Name from Master_LHP_Procurement_TOD " +
                               "where ID_LHP_Procurement_Category=@ID_LHP_Procurement_Category";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Procurement_Category", ddladd1Proc.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd2Proc.DataSource = cmd.ExecuteReader();
                ddladd2Proc.DataTextField = "TOD_Name";
                ddladd2Proc.DataValueField = "ID_LHP_Procurement_TOD";
                ddladd2Proc.DataBind();

                if (ddladd2Proc.Items.Count > 1)
                {
                    ddladd1Proc.Enabled = true;
                    ddladd2Proc.Enabled = true;
                }
                else
                {
                    ddladd2Proc.Enabled = false;
                    ddladd3Proc.Enabled = false;
                    ddladd4Proc.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd2Proc_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd3Proc.Items.Clear();
            ddladd3Proc.Items.Add(new ListItem("--Select Finding--", ""));
            ddladd4Proc.Items.Clear();
            ddladd4Proc.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Procurement_Finding, Finding_Nama from Master_LHP_Procurement_Finding " +
                                        "where ID_LHP_Procurement_TOD=@ID_LHP_Procurement_TOD";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Procurement_TOD", ddladd2Proc.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {
                con.Open();
                ddladd3Proc.DataSource = cmd.ExecuteReader();
                ddladd3Proc.DataTextField = "Finding_Nama";
                ddladd3Proc.DataValueField = "ID_LHP_Procurement_Finding";
                ddladd3Proc.DataBind();

                if (ddladd3Proc.Items.Count > 1)
                {
                    ddladd3Proc.Enabled = true;
                }

                else
                {
                    ddladd3Proc.Enabled = false;
                    ddladd4Proc.Enabled = false;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void ddladd3Proc_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddladd4Proc.Items.Clear();
            ddladd4Proc.Items.Add(new ListItem("--Select Tabel--", ""));

            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            String strQuery = "select ID_LHP_Procurement_Tabel, Tabel_Name from Master_LHP_Procurement_Tabel " +
                                        "where ID_LHP_Procurement_Finding=@ID_LHP_Procurement_Finding";
            SqlConnection con = new SqlConnection(strConnString);
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID_LHP_Procurement_Finding", ddladd3Proc.SelectedItem.Value);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = strQuery;
            cmd.Connection = con;

            try
            {

                con.Open();
                ddladd4Proc.DataSource = cmd.ExecuteReader();
                ddladd4Proc.DataTextField = "Tabel_Name";
                ddladd4Proc.DataValueField = "ID_LHP_Procurement_Tabel";
                ddladd4Proc.DataBind();

                if (ddladd4Proc.Items.Count > 1)
                {
                    ddladd4Proc.Enabled = true;
                }

                else
                {
                    ddladd4Proc.Enabled = false;
                }

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
                con.Dispose();
            }

        }

        protected void btnDelProc_Click(object sender, EventArgs e)
        {
            string code = hfCode.Value;
            executeDeleteProc(code);
            BindGridProc();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(@"<script type='text/javascript'>");
            sb.Append("alert('Record deleted Successfully');");
            sb.Append("$('#DivProcDel').modal('hide');");
            sb.Append(@"</script>");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "delHideModalScript", sb.ToString(), false);
        }

        private void executeDeleteProc(string code)
        {
            string connString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            try
            {
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                string updatecmd = "delete from LHP_Procurement where No=@code";
                SqlCommand addCmd = new SqlCommand(updatecmd, conn);
                addCmd.Parameters.AddWithValue("@code", code);
                addCmd.ExecuteNonQuery();
                conn.Close();

            }
            catch (SqlException me)
            {
                System.Console.Write(me.Message);
            }

        }

    }
}