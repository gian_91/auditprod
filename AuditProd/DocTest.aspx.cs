﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Globalization;
using System.Data;
using System.Web.Services;
using AuditProd.DAL;

namespace AuditProd
{
    public partial class DocTest : System.Web.UI.Page
    {
        DaLayer ObjDAL2 = new DaLayer();
        string message = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            TxbModul.Text = "Doc_Test";
            if (!IsPostBack)
            {
                txbUsername.Text = this.Page.User.Identity.Name;
                if (Session["BranchID"] != null)///Session["Audit_Schedule"] untuk ambil export data by season audit_schedule yang di save contoh = Bareng_Februari_2010
                    txbCabang.Text = Session["BranchID"].ToString();
                else
                {
                    message = "Data Branch Id tidak tersedia";
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

                }

                if (!string.IsNullOrEmpty(Session["Audit_Schedule"] as string))
                    TxbAuditSchedule.Text = Session["Audit_Schedule"].ToString();
                else
                {
                    message = "Data AuditSchedule Id tidak tersedia";
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

                }

                if (!string.IsNullOrEmpty(Session["AuditStart"] as string))
                    txbPeriodeStart.Text = Session["AuditStart"].ToString();
                else
                {
                    message = "Data Periode AuditStart tidak tersedia";
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

                }

                if (!string.IsNullOrEmpty(Session["AuditEnd"] as string))
                    txbPeriodeEnd.Text = Session["AuditEnd"].ToString();
                else
                {
                    message = "Data Periode AuditEnd tidak tersedia";
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                }

                txtIDSave.Text = Session["ID_SH"].ToString();
                txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
                if (!(string.IsNullOrEmpty(txtIDSave.Text)))
                {
                    LoadSaveID();
                }
                else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
                {
                    LoadSaveIDTemp();
                }

            }
            else
            {
                if (Session["ID_SH"] == null || Session["ID_SH_Temp"] == null)
                    Response.Redirect("Login.aspx");

            }

        }

        [WebMethod]
        public static List<string> GetEmpNames(string empName)///Search by list AccountID
        {
            List<string> Emp = new List<string>();
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            string query = string.Format("select AccountID from RawDataCurrent where AccountID LIKE '{0}%'", empName);
            {
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    con.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Emp.Add(reader.GetString(0));
                    }
                }
                con.Close();
            }
            return Emp;
        }

        protected void btnCcek_Click(object sender, EventArgs e)
        {
            Get_Rdc();
            Doc_Test();
        }

        protected void btnExit_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();

            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                Session["ID_SH"] = txtIDSave.Text;
                Response.Redirect("AuditBranch.aspx?ID=" + txtIDSave.Text);
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                Session["ID_SH_Temp"] = txtIDSaveTemp.Text;
                Response.Redirect("AuditBranch.aspx?ID=" + txtIDSaveTemp.Text);
            }
        }

        object ToDBValue(string str)
        {
            return string.IsNullOrEmpty(str) ? DBNull.Value : (object)str;
        }

        public void btnSave_Click(object sender, EventArgs e)
        {
            txtIDSave.Text = Session["ID_SH"].ToString();
            txtIDSaveTemp.Text = Session["ID_SH_Temp"].ToString();
            if (!(string.IsNullOrEmpty(txtIDSave.Text)))
            {
                SavedID();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)) && (string.IsNullOrEmpty(TxtCreateDate.Text)) && (string.IsNullOrEmpty(TxtReviewDate.Text)))
            {
                SavedReview();
            }
            else if (!(string.IsNullOrEmpty(txtIDSaveTemp.Text)))
            {
                SavedIDTemp();
            }
        }

        private void Doc_Test()///Search View bawah
        {
            //SqlDataReader reader;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [ID_DT],[IdDoctest],[AccountId],[KTPMitra_UKM],[KTPPenjamin],[KK_SKD],[FormUKM],[FotoMitra_UKM],[KTPMitra_Disb],[FormDisb],[FotoMitra_Disb]," +
            "[Remarks_UKM_KtpMitra],[Remarks_UKM_KtpPenjamin],[Remarks_UKM_FamilyCard],[Remarks_UKM_FormUkm],[Remarks_UKM_FotoMitra],[Remarks_Disb_KtpMItra],[Remarks_Disb_FormDisb]," +
            "[Remarks_Disb_FotoMitra],[Doe] FROM Doc_Test  where AccountId=@AccountId order by  ID_DT ASC", con);
            cmd.Parameters.AddWithValue("@AccountId", txbID_DocTest.Text);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows == false)
            {
                RemarksKtpUkmm.InnerText = string.Empty;
                RemarksKtpPenjamin.InnerText = string.Empty;
                RemarksFamilycard.InnerText = string.Empty;
                RemarksFormUkm.InnerText = string.Empty;
                RemarksFtMitraUKM.InnerText = string.Empty;
                RemarksKtpDisb.InnerText = string.Empty;
                RemarksFormDisb.InnerText = string.Empty;
                RemarksFtMitraDisb.InnerText = string.Empty;

                rbtUKMMitra.ClearSelection();
                rbtUKMPenjamin.ClearSelection();
                rbtUKMKK.ClearSelection();
                rbtUKMFromUKM.ClearSelection();
                rbtUKMFtMitra.ClearSelection();
                rbtDisbKTP.ClearSelection();
                rbtDisbFormDisb.ClearSelection();
                rbtDisbFtMitra.ClearSelection();

            }
            else
            {
                while (reader.Read())
                {
                    txbAccountId.Text = Convert.ToString(reader["AccountId"]);
                    RemarksKtpUkmm.InnerText = Convert.ToString(reader["Remarks_UKM_KtpMitra"]);
                    RemarksKtpPenjamin.InnerText = Convert.ToString(reader["Remarks_UKM_KtpPenjamin"]);
                    RemarksFamilycard.InnerText = Convert.ToString(reader["Remarks_UKM_FamilyCard"]);
                    RemarksFormUkm.InnerText = Convert.ToString(reader["Remarks_UKM_FormUkm"]);
                    RemarksFtMitraUKM.InnerText = Convert.ToString(reader["Remarks_UKM_FotoMitra"]);
                    RemarksKtpDisb.InnerText = Convert.ToString(reader["Remarks_Disb_KtpMItra"]);
                    RemarksFormDisb.InnerText = Convert.ToString(reader["Remarks_Disb_FormDisb"]);
                    RemarksFtMitraDisb.InnerText = Convert.ToString(reader["Remarks_Disb_FotoMitra"]);

                    rbtUKMMitra.SelectedValue = Convert.ToString(reader["KTPMitra_UKM"]);
                    rbtUKMPenjamin.SelectedValue = Convert.ToString(reader["KTPPenjamin"]);
                    rbtUKMKK.SelectedValue = Convert.ToString(reader["KK_SKD"]);
                    rbtUKMFromUKM.SelectedValue = Convert.ToString(reader["FormUKM"]);
                    rbtUKMFtMitra.SelectedValue = Convert.ToString(reader["FotoMitra_UKM"]);
                    rbtDisbKTP.SelectedValue = Convert.ToString(reader["KTPMitra_Disb"]);
                    rbtDisbFormDisb.SelectedValue = Convert.ToString(reader["FormDisb"]);
                    rbtDisbFtMitra.SelectedValue = Convert.ToString(reader["FotoMitra_Disb"]);
                }
                con.Close();
            }

        }

        private void Get_Rdc()
        {
            string message = string.Empty;
            SqlDataReader reader = null;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("select AccountID,BranchName,ClientName,CenterName,DisbursementDate,DueDays,LoanStatus,SOName,ROName,OpenedDate,LoanOutstandingPrincipal," +
            "DisbursedAmount from RawDataCurrent where AccountID=@AccountID and BranchName=@Cabang ", con);
            cmd.Parameters.AddWithValue("@AccountID", txtAccountId.Text);
            cmd.Parameters.AddWithValue("@Cabang", txbCabang.Text);
            //cmd.Parameters.AddWithValue("@ID", txbID_RDT.Text);
            reader = cmd.ExecuteReader();
            reader.Read();
            {
                if (reader.HasRows == true)
                {
                    txtOutstanding.Text = Convert.ToString(reader["LoanOutstandingPrincipal"]);
                    txtTicketSize.Text = Convert.ToString(reader["DisbursedAmount"]);
                    txbID_DocTest.Text = Convert.ToString(reader["AccountID"]);
                    //txbID_RDT.Text = Convert.ToString(reader["ID"]);
                    txtNamaMitra.Text = Convert.ToString(reader["ClientName"]);
                    txtCentName.Text = Convert.ToString(reader["CenterName"]);
                    txtDisbDate.Text = Convert.ToString(reader["DisbursementDate"]);
                    txtPAR.Text = Convert.ToString(reader["DueDays"]);
                    txtLoanStat.Text = Convert.ToString(reader["LoanStatus"]);
                    txtNamaSO.Text = Convert.ToString(reader["SOName"]);
                    txtNamaRO.Text = Convert.ToString(reader["ROName"]);
                    txtUkmDate.Text = Convert.ToString(reader["OpenedDate"]);
                }
                else
                {
                    txtOutstanding.Text = string.Empty;
                    txtTicketSize.Text = string.Empty;
                    txbID_DocTest.Text = string.Empty;
                    txbID_RDT.Text = string.Empty;
                    txtNamaMitra.Text = string.Empty;
                    txtCentName.Text = string.Empty;
                    txtDisbDate.Text = string.Empty;
                    txtPAR.Text = string.Empty;
                    txtLoanStat.Text = string.Empty;
                    txtNamaSO.Text = string.Empty;
                    txtNamaRO.Text = string.Empty;
                    txtUkmDate.Text = string.Empty;
                    message = "Data tidak Ditemukan di cabang " + txbCabang.Text;
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

                }
                con.Close();
            }


        }

        private void LoadSaveID()
        {
            //SqlDataReader reader;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [IdDoctest],[Bina_Artha_Cabang],[Periode_Audit_Start],[Periode_Audit_End],[AccountId],[Nama_Mitra],[Center_Name],[Disb_Date],[Par],[LoanStatus]," +
            "[NamaSo],[NamaRo],[UKMDate],[OutStandingPrincipal],[TicketSize],[KTPMitra_UKM],[KTPPenjamin],[KK_SKD],[FormUKM],[FotoMitra_UKM],[KTPMitra_Disb],[FormDisb],[FotoMitra_Disb]," +
            "[Remarks_UKM_KtpMitra],[Remarks_UKM_KtpPenjamin],[Remarks_UKM_FamilyCard],[Remarks_UKM_FormUkm],[Remarks_UKM_FotoMitra],[Remarks_Disb_KtpMItra],[Remarks_Disb_FormDisb]," +
            "[Remarks_Disb_FotoMitra],[Doe] FROM Doc_Test  where IdDoctest=@IdDoctest", con);
            cmd.Parameters.AddWithValue("@IdDoctest", txtIDSave.Text);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows == false)
            {
                RemarksKtpUkmm.InnerText = string.Empty;
                RemarksKtpPenjamin.InnerText = string.Empty;
                RemarksFamilycard.InnerText = string.Empty;
                RemarksFormUkm.InnerText = string.Empty;
                RemarksFtMitraUKM.InnerText = string.Empty;
                RemarksKtpDisb.InnerText = string.Empty;
                RemarksFormDisb.InnerText = string.Empty;
                RemarksFtMitraDisb.InnerText = string.Empty;

                rbtUKMMitra.ClearSelection();
                rbtUKMPenjamin.ClearSelection();
                rbtUKMKK.ClearSelection();
                rbtUKMFromUKM.ClearSelection();
                rbtUKMFtMitra.ClearSelection();
                rbtDisbKTP.ClearSelection();
                rbtDisbFormDisb.ClearSelection();
                rbtDisbFtMitra.ClearSelection();

            }
            else
            {
                while (reader.Read())
                {
                    txbCabang.Text = Convert.ToString(reader["Bina_Artha_Cabang"]);
                    txbPeriodeStart.Text = Convert.ToString(reader["Periode_Audit_Start"]);
                    txbPeriodeEnd.Text = Convert.ToString(reader["Periode_Audit_End"]);
                    txbAccountId.Text = Convert.ToString(reader["AccountId"]);

                    txtNamaMitra.Text = Convert.ToString(reader["Nama_Mitra"]);
                    txtCentName.Text = Convert.ToString(reader["Center_Name"]);
                    txtDisbDate.Text = Convert.ToString(reader["Disb_Date"]);
                    txtPAR.Text = Convert.ToString(reader["Par"]);
                    txtLoanStat.Text = Convert.ToString(reader["LoanStatus"]);
                    txtNamaSO.Text = Convert.ToString(reader["NamaSo"]);
                    txtNamaRO.Text = Convert.ToString(reader["NamaRo"]);
                    txtUkmDate.Text = Convert.ToString(reader["UKMDate"]);
                    txtOutstanding.Text = Convert.ToString(reader["OutStandingPrincipal"]);
                    txtTicketSize.Text = Convert.ToString(reader["TicketSize"]);

                    RemarksKtpUkmm.InnerText = Convert.ToString(reader["Remarks_UKM_KtpMitra"]);
                    RemarksKtpPenjamin.InnerText = Convert.ToString(reader["Remarks_UKM_KtpPenjamin"]);
                    RemarksFamilycard.InnerText = Convert.ToString(reader["Remarks_UKM_FamilyCard"]);
                    RemarksFormUkm.InnerText = Convert.ToString(reader["Remarks_UKM_FormUkm"]);
                    RemarksFtMitraUKM.InnerText = Convert.ToString(reader["Remarks_UKM_FotoMitra"]);
                    RemarksKtpDisb.InnerText = Convert.ToString(reader["Remarks_Disb_KtpMItra"]);
                    RemarksFormDisb.InnerText = Convert.ToString(reader["Remarks_Disb_FormDisb"]);
                    RemarksFtMitraDisb.InnerText = Convert.ToString(reader["Remarks_Disb_FotoMitra"]);

                    rbtUKMMitra.SelectedValue = Convert.ToString(reader["KTPMitra_UKM"]);
                    rbtUKMPenjamin.SelectedValue = Convert.ToString(reader["KTPPenjamin"]);
                    rbtUKMKK.SelectedValue = Convert.ToString(reader["KK_SKD"]);
                    rbtUKMFromUKM.SelectedValue = Convert.ToString(reader["FormUKM"]);
                    rbtUKMFtMitra.SelectedValue = Convert.ToString(reader["FotoMitra_UKM"]);
                    rbtDisbKTP.SelectedValue = Convert.ToString(reader["KTPMitra_Disb"]);
                    rbtDisbFormDisb.SelectedValue = Convert.ToString(reader["FormDisb"]);
                    rbtDisbFtMitra.SelectedValue = Convert.ToString(reader["FotoMitra_Disb"]);
                }
                con.Close();
            }

        }

        private void LoadSaveIDTemp()
        {
            //SqlDataReader reader;
            String strConnString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            SqlConnection con = new SqlConnection(strConnString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT [IdDoctest],[Bina_Artha_Cabang],[Periode_Audit_Start],[Periode_Audit_End],[AccountId],[Nama_Mitra],[Center_Name],[Disb_Date],[Par]," +
            "[LoanStatus],[NamaSo],[NamaRo],[UKMDate],[OutStandingPrincipal],[TicketSize],[KTPMitra_UKM],[KTPPenjamin],[KK_SKD],[FormUKM],[FotoMitra_UKM],[KTPMitra_Disb],[FormDisb]," +
            "[FotoMitra_Disb],[Remarks_UKM_KtpMitra],[Remarks_UKM_KtpPenjamin],[Remarks_UKM_FamilyCard],[Remarks_UKM_FormUkm],[Remarks_UKM_FotoMitra],[Remarks_Disb_KtpMItra]," +
            "[Remarks_Disb_FormDisb],[Remarks_Disb_FotoMitra],[Doe],[ReviewDate] FROM Doc_Test  where IdDoctest=@IdDoctest", con);
            cmd.Parameters.AddWithValue("@IdDoctest", txtIDSaveTemp.Text);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows == false)
            {
                RemarksKtpUkmm.InnerText = string.Empty;
                RemarksKtpPenjamin.InnerText = string.Empty;
                RemarksFamilycard.InnerText = string.Empty;
                RemarksFormUkm.InnerText = string.Empty;
                RemarksFtMitraUKM.InnerText = string.Empty;
                RemarksKtpDisb.InnerText = string.Empty;
                RemarksFormDisb.InnerText = string.Empty;
                RemarksFtMitraDisb.InnerText = string.Empty;

                rbtUKMMitra.ClearSelection();
                rbtUKMPenjamin.ClearSelection();
                rbtUKMKK.ClearSelection();
                rbtUKMFromUKM.ClearSelection();
                rbtUKMFtMitra.ClearSelection();
                rbtDisbKTP.ClearSelection();
                rbtDisbFormDisb.ClearSelection();
                rbtDisbFtMitra.ClearSelection();
            }
            else
            {
                while (reader.Read())
                {
                    txbCabang.Text = Convert.ToString(reader["Bina_Artha_Cabang"]);
                    txbPeriodeStart.Text = Convert.ToString(reader["Periode_Audit_Start"]);
                    txbPeriodeEnd.Text = Convert.ToString(reader["Periode_Audit_End"]);
                    txbAccountId.Text = Convert.ToString(reader["AccountId"]);

                    txtNamaMitra.Text = Convert.ToString(reader["Nama_Mitra"]);
                    txtCentName.Text = Convert.ToString(reader["Center_Name"]);
                    txtDisbDate.Text = Convert.ToString(reader["Disb_Date"]);
                    txtPAR.Text = Convert.ToString(reader["Par"]);
                    txtLoanStat.Text = Convert.ToString(reader["LoanStatus"]);
                    txtNamaSO.Text = Convert.ToString(reader["NamaSo"]);
                    txtNamaRO.Text = Convert.ToString(reader["NamaRo"]);
                    txtUkmDate.Text = Convert.ToString(reader["UKMDate"]);
                    txtOutstanding.Text = Convert.ToString(reader["OutStandingPrincipal"]);
                    txtTicketSize.Text = Convert.ToString(reader["TicketSize"]);
                    TxtCreateDate.Text = Convert.ToString(reader["Doe"]);
                    TxtReviewDate.Text = Convert.ToString(reader["ReviewDate"]);

                    RemarksKtpUkmm.InnerText = Convert.ToString(reader["Remarks_UKM_KtpMitra"]);
                    RemarksKtpPenjamin.InnerText = Convert.ToString(reader["Remarks_UKM_KtpPenjamin"]);
                    RemarksFamilycard.InnerText = Convert.ToString(reader["Remarks_UKM_FamilyCard"]);
                    RemarksFormUkm.InnerText = Convert.ToString(reader["Remarks_UKM_FormUkm"]);
                    RemarksFtMitraUKM.InnerText = Convert.ToString(reader["Remarks_UKM_FotoMitra"]);
                    RemarksKtpDisb.InnerText = Convert.ToString(reader["Remarks_Disb_KtpMItra"]);
                    RemarksFormDisb.InnerText = Convert.ToString(reader["Remarks_Disb_FormDisb"]);
                    RemarksFtMitraDisb.InnerText = Convert.ToString(reader["Remarks_Disb_FotoMitra"]);

                    rbtUKMMitra.SelectedValue = Convert.ToString(reader["KTPMitra_UKM"]);
                    rbtUKMPenjamin.SelectedValue = Convert.ToString(reader["KTPPenjamin"]);
                    rbtUKMKK.SelectedValue = Convert.ToString(reader["KK_SKD"]);
                    rbtUKMFromUKM.SelectedValue = Convert.ToString(reader["FormUKM"]);
                    rbtUKMFtMitra.SelectedValue = Convert.ToString(reader["FotoMitra_UKM"]);
                    rbtDisbKTP.SelectedValue = Convert.ToString(reader["KTPMitra_Disb"]);
                    rbtDisbFormDisb.SelectedValue = Convert.ToString(reader["FormDisb"]);
                    rbtDisbFtMitra.SelectedValue = Convert.ToString(reader["FotoMitra_Disb"]);
                }
                con.Close();
            }

        }

        private void SavedID()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SaveDocTest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdDoctest", SqlDbType.Int).Value = txtIDSave.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(txbUsername.Text) ? (object)DBNull.Value : txbUsername.Text);
                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                    cmd.Parameters.AddWithValue("@AccountId", string.IsNullOrEmpty(txtAccountId.Text) ? (object)DBNull.Value : txtAccountId.Text);
                    cmd.Parameters.AddWithValue("@Nama_Mitra", string.IsNullOrEmpty(txtNamaMitra.Text) ? (object)DBNull.Value : txtNamaMitra.Text);
                    cmd.Parameters.AddWithValue("@Center_Name", string.IsNullOrEmpty(txtCentName.Text) ? (object)DBNull.Value : txtCentName.Text);
                    cmd.Parameters.AddWithValue("@Disb_Date", string.IsNullOrEmpty(txtDisbDate.Text) ? (object)DBNull.Value : txtDisbDate.Text);

                    cmd.Parameters.AddWithValue("@Par", string.IsNullOrEmpty(txtPAR.Text) ? (object)DBNull.Value : txtPAR.Text);
                    cmd.Parameters.AddWithValue("@LoanStatus", string.IsNullOrEmpty(txtLoanStat.Text) ? (object)DBNull.Value : txtLoanStat.Text);
                    cmd.Parameters.AddWithValue("@NamaSo", string.IsNullOrEmpty(txtNamaSO.Text) ? (object)DBNull.Value : txtNamaSO.Text);
                    cmd.Parameters.AddWithValue("@NamaRo", string.IsNullOrEmpty(txtNamaRO.Text) ? (object)DBNull.Value : txtNamaRO.Text);
                    cmd.Parameters.AddWithValue("@UKMDate", string.IsNullOrEmpty(txtUkmDate.Text) ? (object)DBNull.Value : txtUkmDate.Text);
                    cmd.Parameters.AddWithValue("@OutStandingPrincipal", string.IsNullOrEmpty(txtOutstanding.Text) ? (object)DBNull.Value : txtOutstanding.Text);
                    cmd.Parameters.AddWithValue("@TicketSize", string.IsNullOrEmpty(txtTicketSize.Text) ? (object)DBNull.Value : txtTicketSize.Text);

                    cmd.Parameters.AddWithValue("@KTPMitra_UKM", string.IsNullOrEmpty(rbtUKMFtMitra.SelectedValue) ? (object)DBNull.Value : rbtUKMFtMitra.SelectedValue);
                    cmd.Parameters.AddWithValue("@KTPPenjamin", string.IsNullOrEmpty(rbtUKMPenjamin.SelectedValue) ? (object)DBNull.Value : rbtUKMPenjamin.SelectedValue);
                    cmd.Parameters.AddWithValue("@KK_SKD", string.IsNullOrEmpty(rbtUKMKK.SelectedValue) ? (object)DBNull.Value : rbtUKMKK.SelectedValue);
                    cmd.Parameters.AddWithValue("@FormUKM", string.IsNullOrEmpty(rbtUKMFromUKM.SelectedValue) ? (object)DBNull.Value : rbtUKMFromUKM.SelectedValue);
                    cmd.Parameters.AddWithValue("@FotoMitra_UKM", string.IsNullOrEmpty(rbtUKMFtMitra.SelectedValue) ? (object)DBNull.Value : rbtUKMFtMitra.SelectedValue);
                    cmd.Parameters.AddWithValue("@KTPMitra_Disb", string.IsNullOrEmpty(rbtDisbKTP.SelectedValue) ? (object)DBNull.Value : rbtDisbKTP.SelectedValue);
                    cmd.Parameters.AddWithValue("@FormDisb", string.IsNullOrEmpty(rbtDisbFormDisb.SelectedValue) ? (object)DBNull.Value : rbtDisbFormDisb.SelectedValue);
                    cmd.Parameters.AddWithValue("@FotoMitra_Disb", string.IsNullOrEmpty(rbtDisbFtMitra.SelectedValue) ? (object)DBNull.Value : rbtDisbFtMitra.SelectedValue);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.Date).Value = DBNull.Value;

                    cmd.Parameters.AddWithValue("@Remarks_UKM_KtpMitra", ToDBValue(RemarksKtpUkmm.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_KtpPenjamin", ToDBValue(RemarksKtpPenjamin.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FamilyCard", ToDBValue(RemarksFamilycard.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FormUkm", ToDBValue(RemarksFormUkm.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FotoMitra", ToDBValue(RemarksFtMitraUKM.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_KtpMItra", ToDBValue(RemarksKtpDisb.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_FormDisb", ToDBValue(RemarksFormDisb.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_FotoMitra", ToDBValue(RemarksFtMitraDisb.InnerText));

                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }

        private void SavedReview()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SaveDocTest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdDoctest", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(txbUsername.Text) ? (object)DBNull.Value : txbUsername.Text);
                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                    cmd.Parameters.AddWithValue("@AccountId", string.IsNullOrEmpty(txtAccountId.Text) ? (object)DBNull.Value : txtAccountId.Text);
                    cmd.Parameters.AddWithValue("@Nama_Mitra", string.IsNullOrEmpty(txtNamaMitra.Text) ? (object)DBNull.Value : txtNamaMitra.Text);
                    cmd.Parameters.AddWithValue("@Center_Name", string.IsNullOrEmpty(txtCentName.Text) ? (object)DBNull.Value : txtCentName.Text);
                    cmd.Parameters.AddWithValue("@Disb_Date", string.IsNullOrEmpty(txtDisbDate.Text) ? (object)DBNull.Value : txtDisbDate.Text);

                    cmd.Parameters.AddWithValue("@Par", string.IsNullOrEmpty(txtPAR.Text) ? (object)DBNull.Value : txtPAR.Text);
                    cmd.Parameters.AddWithValue("@LoanStatus", string.IsNullOrEmpty(txtLoanStat.Text) ? (object)DBNull.Value : txtLoanStat.Text);
                    cmd.Parameters.AddWithValue("@NamaSo", string.IsNullOrEmpty(txtNamaSO.Text) ? (object)DBNull.Value : txtNamaSO.Text);
                    cmd.Parameters.AddWithValue("@NamaRo", string.IsNullOrEmpty(txtNamaRO.Text) ? (object)DBNull.Value : txtNamaRO.Text);
                    cmd.Parameters.AddWithValue("@UKMDate", string.IsNullOrEmpty(txtUkmDate.Text) ? (object)DBNull.Value : txtUkmDate.Text);
                    cmd.Parameters.AddWithValue("@OutStandingPrincipal", string.IsNullOrEmpty(txtOutstanding.Text) ? (object)DBNull.Value : txtOutstanding.Text);
                    cmd.Parameters.AddWithValue("@TicketSize", string.IsNullOrEmpty(txtTicketSize.Text) ? (object)DBNull.Value : txtTicketSize.Text);

                    cmd.Parameters.AddWithValue("@KTPMitra_UKM", string.IsNullOrEmpty(rbtUKMFtMitra.SelectedValue) ? (object)DBNull.Value : rbtUKMFtMitra.SelectedValue);
                    cmd.Parameters.AddWithValue("@KTPPenjamin", string.IsNullOrEmpty(rbtUKMPenjamin.SelectedValue) ? (object)DBNull.Value : rbtUKMPenjamin.SelectedValue);
                    cmd.Parameters.AddWithValue("@KK_SKD", string.IsNullOrEmpty(rbtUKMKK.SelectedValue) ? (object)DBNull.Value : rbtUKMKK.SelectedValue);
                    cmd.Parameters.AddWithValue("@FormUKM", string.IsNullOrEmpty(rbtUKMFromUKM.SelectedValue) ? (object)DBNull.Value : rbtUKMFromUKM.SelectedValue);
                    cmd.Parameters.AddWithValue("@FotoMitra_UKM", string.IsNullOrEmpty(rbtUKMFtMitra.SelectedValue) ? (object)DBNull.Value : rbtUKMFtMitra.SelectedValue);
                    cmd.Parameters.AddWithValue("@KTPMitra_Disb", string.IsNullOrEmpty(rbtDisbKTP.SelectedValue) ? (object)DBNull.Value : rbtDisbKTP.SelectedValue);
                    cmd.Parameters.AddWithValue("@FormDisb", string.IsNullOrEmpty(rbtDisbFormDisb.SelectedValue) ? (object)DBNull.Value : rbtDisbFormDisb.SelectedValue);
                    cmd.Parameters.AddWithValue("@FotoMitra_Disb", string.IsNullOrEmpty(rbtDisbFtMitra.SelectedValue) ? (object)DBNull.Value : rbtDisbFtMitra.SelectedValue);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DateTime.Now;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.Date).Value = DBNull.Value;

                    cmd.Parameters.AddWithValue("@Remarks_UKM_KtpMitra", ToDBValue(RemarksKtpUkmm.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_KtpPenjamin", ToDBValue(RemarksKtpPenjamin.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FamilyCard", ToDBValue(RemarksFamilycard.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FormUkm", ToDBValue(RemarksFormUkm.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FotoMitra", ToDBValue(RemarksFtMitraUKM.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_KtpMItra", ToDBValue(RemarksKtpDisb.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_FormDisb", ToDBValue(RemarksFormDisb.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_FotoMitra", ToDBValue(RemarksFtMitraDisb.InnerText));

                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);

                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }

        private void SavedIDTemp()
        {
            string message = string.Empty;
            string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand("SP_SaveDocTest", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdDoctest", SqlDbType.Int).Value = txtIDSaveTemp.Text;
                    cmd.Parameters.AddWithValue("@Modul", string.IsNullOrEmpty(TxbModul.Text) ? (object)DBNull.Value : TxbModul.Text);
                    cmd.Parameters.AddWithValue("@Header_Dibuat", string.IsNullOrEmpty(txbUsername.Text) ? (object)DBNull.Value : txbUsername.Text);
                    cmd.Parameters.AddWithValue("@Bina_Artha_Cabang", string.IsNullOrEmpty(txbCabang.Text) ? (object)DBNull.Value : txbCabang.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_Start", string.IsNullOrEmpty(txbPeriodeStart.Text) ? (object)DBNull.Value : txbPeriodeStart.Text);
                    cmd.Parameters.AddWithValue("@Periode_Audit_End", string.IsNullOrEmpty(txbPeriodeEnd.Text) ? (object)DBNull.Value : txbPeriodeEnd.Text);

                    cmd.Parameters.AddWithValue("@AccountId", string.IsNullOrEmpty(txtAccountId.Text) ? (object)DBNull.Value : txtAccountId.Text);
                    cmd.Parameters.AddWithValue("@Nama_Mitra", string.IsNullOrEmpty(txtNamaMitra.Text) ? (object)DBNull.Value : txtNamaMitra.Text);
                    cmd.Parameters.AddWithValue("@Center_Name", string.IsNullOrEmpty(txtCentName.Text) ? (object)DBNull.Value : txtCentName.Text);
                    cmd.Parameters.AddWithValue("@Disb_Date", string.IsNullOrEmpty(txtDisbDate.Text) ? (object)DBNull.Value : txtDisbDate.Text);

                    cmd.Parameters.AddWithValue("@Par", string.IsNullOrEmpty(txtPAR.Text) ? (object)DBNull.Value : txtPAR.Text);
                    cmd.Parameters.AddWithValue("@LoanStatus", string.IsNullOrEmpty(txtLoanStat.Text) ? (object)DBNull.Value : txtLoanStat.Text);
                    cmd.Parameters.AddWithValue("@NamaSo", string.IsNullOrEmpty(txtNamaSO.Text) ? (object)DBNull.Value : txtNamaSO.Text);
                    cmd.Parameters.AddWithValue("@NamaRo", string.IsNullOrEmpty(txtNamaRO.Text) ? (object)DBNull.Value : txtNamaRO.Text);
                    cmd.Parameters.AddWithValue("@UKMDate", string.IsNullOrEmpty(txtUkmDate.Text) ? (object)DBNull.Value : txtUkmDate.Text);
                    cmd.Parameters.AddWithValue("@OutStandingPrincipal", string.IsNullOrEmpty(txtOutstanding.Text) ? (object)DBNull.Value : txtOutstanding.Text);
                    cmd.Parameters.AddWithValue("@TicketSize", string.IsNullOrEmpty(txtTicketSize.Text) ? (object)DBNull.Value : txtTicketSize.Text);

                    cmd.Parameters.AddWithValue("@KTPMitra_UKM", string.IsNullOrEmpty(rbtUKMFtMitra.SelectedValue) ? (object)DBNull.Value : rbtUKMFtMitra.SelectedValue);
                    cmd.Parameters.AddWithValue("@KTPPenjamin", string.IsNullOrEmpty(rbtUKMPenjamin.SelectedValue) ? (object)DBNull.Value : rbtUKMPenjamin.SelectedValue);
                    cmd.Parameters.AddWithValue("@KK_SKD", string.IsNullOrEmpty(rbtUKMKK.SelectedValue) ? (object)DBNull.Value : rbtUKMKK.SelectedValue);
                    cmd.Parameters.AddWithValue("@FormUKM", string.IsNullOrEmpty(rbtUKMFromUKM.SelectedValue) ? (object)DBNull.Value : rbtUKMFromUKM.SelectedValue);
                    cmd.Parameters.AddWithValue("@FotoMitra_UKM", string.IsNullOrEmpty(rbtUKMFtMitra.SelectedValue) ? (object)DBNull.Value : rbtUKMFtMitra.SelectedValue);
                    cmd.Parameters.AddWithValue("@KTPMitra_Disb", string.IsNullOrEmpty(rbtDisbKTP.SelectedValue) ? (object)DBNull.Value : rbtDisbKTP.SelectedValue);
                    cmd.Parameters.AddWithValue("@FormDisb", string.IsNullOrEmpty(rbtDisbFormDisb.SelectedValue) ? (object)DBNull.Value : rbtDisbFormDisb.SelectedValue);
                    cmd.Parameters.AddWithValue("@FotoMitra_Disb", string.IsNullOrEmpty(rbtDisbFtMitra.SelectedValue) ? (object)DBNull.Value : rbtDisbFtMitra.SelectedValue);
                    cmd.Parameters.Add("Doe", SqlDbType.DateTime).Value = DBNull.Value;
                    cmd.Parameters.Add("ReviewDate", SqlDbType.DateTime).Value = DateTime.Now;

                    cmd.Parameters.AddWithValue("@Remarks_UKM_KtpMitra", ToDBValue(RemarksKtpUkmm.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_KtpPenjamin", ToDBValue(RemarksKtpPenjamin.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FamilyCard", ToDBValue(RemarksFamilycard.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FormUkm", ToDBValue(RemarksFormUkm.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_UKM_FotoMitra", ToDBValue(RemarksFtMitraUKM.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_KtpMItra", ToDBValue(RemarksKtpDisb.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_FormDisb", ToDBValue(RemarksFormDisb.InnerText));
                    cmd.Parameters.AddWithValue("@Remarks_Disb_FotoMitra", ToDBValue(RemarksFtMitraDisb.InnerText));

                    int temp = cmd.ExecuteNonQuery();
                    if (temp < 0)
                    {
                        message = "Data Berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    else
                    {
                        message = "Data tidak berhasil disimpan";
                        ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                    }
                    con.Close();
                }

            }
        }
    }
}