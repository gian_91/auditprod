﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using AuditProd.DAL;

namespace AuditProd
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        DaLayer dl = new DaLayer();
        protected void Page_Load(object sender, EventArgs e)
        {
            txtNik.Value = this.Page.User.Identity.Name;
        }

        protected void registration_click(object sender, EventArgs e)
        {
            {
                int rowsAffected = 0;
                string query = "UPDATE [tlbuser] SET [Password] = @Password, [ChangePassDate] = GETDATE() WHERE [NIK] = @NIK AND [Password] = @CurrentPassword";
                string constr = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    using (SqlCommand cmd = new SqlCommand(query))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter())
                        {
                            cmd.Parameters.AddWithValue("@NIK", this.Page.User.Identity.Name);
                            cmd.Parameters.AddWithValue("@CurrentPassword", dl.Encript(PasswordExist.Value.Trim()));
                            cmd.Parameters.AddWithValue("@Password", dl.Encript(txtPassword.Value.Trim()));
                            cmd.Connection = con;
                            con.Open();
                            rowsAffected = cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                    string message = string.Empty;
                    switch (rowsAffected)
                    {
                        case 0:
                            message = "Current Password does not match with our database records.";
                            break;
                        default:
                            message = "Change Password successful.";
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "redirectJS", "setTimeout(function() { window.location.replace('Login.aspx') }, 3000);", true);
                            break;
                    }

                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
                }

            }

        }

        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        private string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }
    }
}